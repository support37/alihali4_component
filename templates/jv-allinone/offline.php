<?php
/**
 * @package     Joomla.Site
 * @subpackage  Template.system
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
$app = JFactory::getApplication();
// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

require_once JPATH_ADMINISTRATOR . '/components/com_users/helpers/users.php';
$twofactormethods = UsersHelper::getTwoFactorMethods();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<jdoc:include type="head" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300|Roboto+Condensed:400,300|Oswald:400,300'" type="text/css" />
	<link rel="stylesheet" href="<?php echo JUri::root(true)?>/plugins/system/jvlibs/javascripts/jquery/bootstrap/css/bootstrap.min.css" type="text/css" />   
	<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $app->getTemplate(); ?>/css/template.css" type="text/css" />
	<?php if ($this->direction == 'rtl') : ?>
		<link rel="stylesheet" href="<?php echo JUri::root(true)?>/plugins/system/jvlibs/javascripts/jquery/bootstrap/css/bootstrap-rtl.min.css" type="text/css" />   
		<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $app->getTemplate(); ?>/css/template.style.rtl.css" type="text/css" />
	<?php endif; ?>
	<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $app->getTemplate(); ?>/colors/green/style.css" type="text/css" /> 
</head>
<body class="offline-page">
	<div id="frame" class="container">
		<div class="offline-head clearfix">
			<?php if ($app->get('offline_image') && file_exists($app->get('offline_image'))) : ?>
				<img src="<?php echo $app->get('offline_image'); ?>" alt="<?php echo htmlspecialchars($app->get('sitename')); ?>" class="pull-left"/>
			<?php endif; ?>
				<h1>
					<?php echo htmlspecialchars($app->get('sitename')); ?>
				</h1>			
		</div>
		<?php if ($app->get('display_offline_message', 1) == 1 && str_replace(' ', '', $app->get('offline_message')) != '') : ?>
			<div class="offline_message">
				<?php echo $app->get('offline_message'); ?>
			</div>
		<?php elseif ($app->get('display_offline_message', 1) == 2 && str_replace(' ', '', JText::_('JOFFLINE_MESSAGE')) != '') : ?>
			<div class="offline_message">
				<?php echo JText::_('JOFFLINE_MESSAGE'); ?>
			</div>
		<?php endif; ?>
		<jdoc:include type="message" />
		<form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="form-login">
		<fieldset class="input">
			<div class="row">
				<div class="col-sm-6">
					<p id="form-login-username" class="form-group">
						<label for="username"><?php echo JText::_('JGLOBAL_USERNAME'); ?></label>
						<input name="username" id="username" type="text" class="form-control" alt="<?php echo JText::_('JGLOBAL_USERNAME'); ?>" size="18" />
					</p>
					<?php if (count($twofactormethods) > 1) : ?>
					<p id="form-login-secretkey" class="form-group">
						<label for="secretkey"><?php echo JText::_('JGLOBAL_SECRETKEY'); ?></label>
						<input type="text" name="secretkey" class="form-control" size="18" alt="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>" id="secretkey" />
					</p>
					<?php endif; ?>
				</div>
				<div class="col-sm-6">
					<p id="form-login-password" class="form-group">
						<label for="passwd"><?php echo JText::_('JGLOBAL_PASSWORD'); ?></label>
						<input type="password" name="password" class="form-control" size="18" alt="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>" id="passwd" />
					</p>
				</div>
			</div>			
			<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
			<p id="form-login-remember" class="form-group">
				<label for="remember" class="checkbox"><input type="checkbox" name="remember" class="" value="yes" alt="<?php echo JText::_('JGLOBAL_REMEMBER_ME'); ?>" id="remember" /><?php echo JText::_('JGLOBAL_REMEMBER_ME'); ?></label>
			</p>
			<?php endif; ?>
			<p id="submit-buton" class="form-group">
				<input type="submit" name="Submit" class="btn button-green login " value="<?php echo JText::_('JLOGIN'); ?>" />
			</p>
			<input type="hidden" name="option" value="com_users" />
			<input type="hidden" name="task" value="user.login" />
			<input type="hidden" name="return" value="<?php echo base64_encode(JUri::base()); ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</fieldset>
		</form>
	</div>
</body>
</html>