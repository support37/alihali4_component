<?php if( $this['block']->count('content-bottom') ):?>
<!--Block content-bottom-->
    <section id="block-content-bottom">
        <div class="container">
            <jdoc:include type="position" name="content-bottom"  />
        </div>
    </section>
<!--/Block content-bottom-->
<?php endif;?>

<?php if( $this['block']->count('fullbottom') ):?>
<!--Block Full bottom-->
	<section id="block-fullbottom">
         	<jdoc:include type="position" name="fullbottom"  />
    </section>
<!--/Block Full bottom-->
<?php endif;?>

<?php if( $this['block']->count('top') ):?>
<!--Block top-->
	<section id="block-top">
    	<div class="container">
    		<jdoc:include type="block" name="top"  />
        </div>
    </section>
<!--/Block top-->
<?php endif;?>
<?php if( $this['block']->count('userb') ):?>
<!--userb -->
    <section id="block-userb">
    <div class="container">
            <jdoc:include type="block" name="userb"  />
        </div>
    </section>
<!--/userb-->
<?php endif;?>
<?php if( $this['block']->count('user-1') ):?>
<!--Block user-1-->
	<section id="block-user-1">
    	<div class="container">
         	<jdoc:include type="position" name="user-1"  />
        </div>
    </section>
<!--/Block user-1-->
<?php endif;?>
<?php if( $this['block']->count('user-2') ):?>
<!--Block user-2-->
	<section id="block-user-2">
    	<div class="container">
         	<jdoc:include type="position" name="user-2"  />
        </div>
    </section>
<!--/Block user-2-->
<?php endif;?>
<?php if( $this['block']->count('user-3') ):?>
<!--Block user-3-->
	<section id="block-user-3">
    	<div class="container">
         	<jdoc:include type="position" name="user-3"  />
        </div>
    </section>
<!--/user-3-->
<?php endif;?>
<?php if( $this['block']->count('user-4') ):?>
<!--user-4-->
	<section id="block-user-4">
         	<jdoc:include type="position" name="user-4"  />
    </section>
<!--/user-4-->
<?php endif;?>
<?php if( $this['block']->count('user-5') ):?>
<!--user-5-->
	<section id="block-user-5">
    <div class="container">
         	<jdoc:include type="position" name="user-5"  />
              </div>
    </section>
<!--/user-5-->
<?php endif;?>
<?php if( $this['block']->count('topb') ):?>
<!--Block topb-->
	<section id="block-topb">
    	<div class="container">
    		<jdoc:include type="block" name="topb" />
        </div>
    </section>
<!--/Block topb-->
<?php endif;?>
<?php if( $this['block']->count('bottom') ):?>
<!--Block bottom-->
	<section id="block-bottom" >
    	<div class="container">
         	<jdoc:include type="block" name="bottom"  />
        </div>
    </section>
<!--/Block bottom-->
<?php endif;?>
<?php if( $this['block']->count('user-6') ):?>
<!--user-6-->
	<section id="block-user-6">
    <div class="container">
         	<jdoc:include type="position" name="user-6"  />
              </div>
    </section>
<!--/user-6-->
<?php endif;?>
<?php if( $this['block']->count('user-7') ):?>
<!--user-7-->
	<section id="block-user-7">
    <div class="container">
         	<jdoc:include type="position" name="user-7"  />
              </div>
    </section>
<!--/user-7-->
<?php endif;?>
<?php if( $this['block']->count('user-8') ):?>
<!--user-8-->
	<section id="block-user-8">
    <div class="container">
         	<jdoc:include type="position" name="user-8"  />
              </div>
    </section>
<!--/user-8-->
<?php endif;?>
<?php if( $this['block']->count('user-9') ):?>
<!--user-9-->
	<section id="block-user-9">
    <div class="container">
         	<jdoc:include type="position" name="user-9"  />
              </div>
    </section>
<!--/user-9-->
<?php endif;?>
<?php if( $this['block']->count('user-10') ):?>
<!--user-10-->
	<section id="block-user-10">
    <div class="container">
         	<jdoc:include type="position" name="user-10"  />
              </div>
    </section>
<!--/user-10-->
<?php endif;?>
<?php if( $this['block']->count('bottomb') ):?>
<!--Block-bottomb-->
	<section id="block-bottomb">
    	<div class="container">
         	<jdoc:include type="block" name="bottomb" />
        </div>
    </section>
<!--/Block-bottomb-->
<?php endif;?>
<?php if($this['block']->count('contentbottom')):?>
<section class="contentbottom">
    <div class="container">
        <jdoc:include type="block" name="contentbottom" grid-mode="fluid" />
    </div>
</section>
<?php endif;?>
 
<?php if( $this['position']->count('footer') ):?>
<!--Block Footer-->
	<footer id="block-footer" >
    	<div class="container">
	       <jdoc:include type="position" name="footer" style="raw"/>
        </div>
    </footer>
<!--/Block Footer-->
<?php endif;?>
<a href="#" class="back-to-top"></a>