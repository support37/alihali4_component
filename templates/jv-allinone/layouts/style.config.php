<?php
/*
 # @template	JV All In One
 # @version		2.5
 # ------------------------------------------------------------------------
 # @author    	Open Source Code Solutions Co
 # @copyright 	Copyright (C) 2015 joomlavi.com. All Rights Reserved.
 # @license 	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
 # @websites 	http://www.joomlavi.com
 # @technical support  http://www.joomlavi.com/my-tickets.html
*/

/* Add style*/
$this['asset']->addStyle($this['path']->url('theme::css/jquery.selectbox.css'));
/* Add script*/
$this['asset']->addScript($this['path']->url('theme::js/script.js'));
$this['asset']->addScript($this['path']->url('theme::js/uikit.js'));
$this['asset']->addScript($this['path']->url('theme::js/hikashop.js'));
$this['asset']->addScript($this['path']->url('theme::js/modernizr.js')); 
$this['asset']->addScript($this['path']->url('theme::js/owl.carousel.min.js'));
$this['asset']->addScript($this['path']->url('theme::js/jquery.slicknav.min.js')); 
$this['asset']->addScript($this['path']->url('theme::js/jquery-scrolltofixed-min.js'));
$this['asset']->addScript($this['path']->url('theme::js/jquery-selectify.js'));
$this['asset']->addScript($this['path']->url('theme::js/parallax-plugin.js'));
$this['asset']->addScript($this['path']->url('theme::js/jquery.prettyPhoto.js'));
$this['asset']->addScript($this['path']->url('theme::js/jquery.bxslider.min.js'));
$this['asset']->addScript($this['path']->url('theme::js/jquery.zoom.js'));
$this['asset']->addScript($this['path']->url('theme::js/jquery.sharrre-1.3.0.min.js'));
?>