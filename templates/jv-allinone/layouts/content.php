<?php 
    $app  = JFactory::getApplication(); 
    $view = $app->input->getCmd('view', '');
?>
	<div id="main-content"  class="<?php echo (($view !="productdetails") && ($view !="product"))?$this['option']->get('template.content.class'):'col-md-12'; ?>">
	    <?php if($this['block']->count('contenttop')):?>
        <div class="contenttop">        
        	<jdoc:include type="block" name="contenttop" grid-mode="fluid" />
        </div>
        <?php endif;?>
        <div id="content">
	        <jdoc:include type="message" />
			<jdoc:include type="component" />
	    </div>
    </div>
