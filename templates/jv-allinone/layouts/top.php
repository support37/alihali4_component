<?php if( $this['position']->count('searchtop') ):?>
<!--Search-->
	<div id="searchtop">
    <div class="container">
		    <jdoc:include type="position" name="searchtop"  style="none"  />
     	<span id="search-beack" ></span>
     </div>
	</div>
<!--/Search-->
<?php endif;?>



<?php if( $this['block']->count('panel') ):?>
<!--Block top-->
	<section id="block-panel" class="top-bar">
    	<div class="container">
    		<jdoc:include type="block" name="panel" />
        </div>
    </section>
<!--/Block top-->
<?php endif;?>


<!--Block Header -->
<header id="block-header">
    	<div class="container">
                <jdoc:include type="position" name="logo" />
                <?php if( $this['position']->count('topbanner') ):?>
                    <div class="topbanner pull-right"><jdoc:include type="position" name="topbanner" /></div>
                <?php endif;?>
                <?php if( $this['position']->count('offcanvas') ):?>
                    <a href="#offcanvas" data-uk-offcanvas="" class="btn btn-offcanvas"><span></span><span></span><span></span></a>
                <?php endif;?>
				<?php if( $this['position']->count('menu') ):?>
                    <section id="block-mainnav">
                            <jdoc:include type="position" name="menu" style="none" />
                
                    </section >
                <?php endif;?>
                <a class="flexMenuToggle" href="JavaScript:void(0);" ><span></span><span></span><span></span></a>                
        </div>
        <div class="mainnav-5"></div>
</header>
<!--/Block Header-->


<?php if( $this['position']->count('slideshow') ):?>
<!--Block Slide-->
	<section id="block-slide">
		    <jdoc:include type="position" name="slideshow"  style="none"  />
	</section>
<!--/Block Slide-->
<?php endif;?>



<?php if( $this['position']->count('breadcrumb') ):?>
<!--Block Breadcrumb-->
	<section id="block-breadcrumb" class='page-title'>
    	<div class="container">
			<jdoc:include type="position" name="breadcrumb" style="none" />
		</div>
	</section>
<!--/Block Breadcrumb-->
<?php endif;?>



<?php if( $this['block']->count('fulltop') ):?>
<!--Block fulltop-->
	<section id="block-fulltop">
         	<jdoc:include type="position" name="fulltop"  />
    </section>
<!--/Block fulltop-->
<?php endif;?>


<?php if( $this['block']->count('usert') ):?>
<!--Block User Top-->
    <section id="block-usert">
        <div class="container">
            <jdoc:include type="block" name="usert"  />
        </div>
    </section>
<!--/Block User Top-->
<?php endif;?>

<?php if( $this['block']->count('content-top') ):?>
<!--Block content-top-->
    <section id="block-content-top">
        <div class="container">
            <jdoc:include type="position" name="content-top"  />
        </div>
    </section>
<!--/Block content-top-->
<?php endif;?>