<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
<head>

	<?php
	echo $this['template']->render('head');	

    include($this['path']->findPath('style.config.php'));
    ?>    
<?php 
$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
?>
</head>
<body class="<?php echo $this['option']->get('template.body.class'); ?>

<?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '')
	. (isset($customdisplay) ? $customdisplay : '');
?>
" 	<?php if($this['option']->get('styles.bgcolor')){ ?>
	style="background-color: <?php echo $this['option']->get('styles.bgcolor'); ?>"
	<?php } ?>
>
	<div id="wrapper">
        <div id="mainsite">
            <span class="flexMenuToggle" ></span> 
            <?php  echo $this['template']->render('top'); ?>
            <section id="block-main">
                <div class="container">
                	<div class="row">
                        <?php echo $this['template']->render('content'); ?>
                        <?php if (($view !="productdetails") && ($view !="product")) { ?>
                        <?php echo $this['template']->render('sidebar-a'); ?>             	            
                        <?php echo $this['template']->render('sidebar-b'); ?>
                        <?php } ?>
                     </div>   
                </div>
            </section>
            <?php echo $this['template']->render('bottom'); ?>
            
            

            
           
        </div> 
	</div>
    
<?php if( $this['position']->count('demo')):?>    
<div id="switcher">
<a href="javascript:void(0)" class="show-switcher-icon" id="button-switch"></a>
<jdoc:include type="position" name="color" style="none" />
<jdoc:include type="position" name="demo" style="none" />
</div> 
<?php endif;?>  
<?php if( $this['position']->count('offcanvas')):?>
    <div id="offcanvas" class="uk-offcanvas">
        <div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
            <div class="uk-panel">
                <jdoc:include type="position" name="offcanvas"/>
            </div>
        </div>
    </div>
<?php endif;?>  
<?php echo $this['position']->render('analytic.none'); ?>  


</body>
</html>