<?php
/**
 * @version		$Id: profile.php 1812 2013-01-14 18:45:06Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>

<!-- K2 user profile form -->
<form action="<?php echo JURI::root(true); ?>/index.php" enctype="multipart/form-data" method="post" name="userform" autocomplete="off" class="form-validate">
	<?php if($this->params->def('show_page_title',1)): ?>
	<h1 class="componentheading<?php echo $this->escape($this->params->get('pageclass_sfx')); ?> titlePage">
		<?php echo $this->escape($this->params->get('page_title')); ?>
	</h1>
	<?php endif; ?>

	<div id="k2Container" class="k2AccountPage">
		<div class="panel panel-default">
			<div class="panel-heading"><h4 class="panel-title"><?php echo JText::_('K2_ACCOUNT_DETAILS'); ?></h4></div>
			<div class="panel-body">
				<div class="form-horizontal">
					<div class="form-group ">
					    <label class="control-label col-sm-2" for="username"><?php echo JText::_('K2_USER_NAME'); ?></label>
					    <span class="control-label text-left col-sm-10 user-name"><b><?php echo $this->user->get('username'); ?></b></span>
				  	</div>


				  	<div class="form-group">
					    <label class="control-label col-sm-2" id="namemsg" for="name"><?php echo JText::_('K2_NAME'); ?></label>
					    <div class="col-sm-8">
					      <input type="text" name="<?php echo $this->nameFieldName; ?>" id="name" size="40" value="<?php echo $this->escape($this->user->get( 'name' )); ?>" class="form-control required" maxlength="50" />
					      <div class="bottom-border"></div>
					    </div>
				  	</div>

				  	<div class="form-group">
					    <label class="control-label col-sm-2" id="emailmsg" for="email"><?php echo JText::_('K2_EMAIL'); ?></label>
					    <div class="col-sm-8">
					      <input type="text" id="email" name="<?php echo $this->emailFieldName; ?>" size="40" value="<?php echo $this->escape($this->user->get( 'email' )); ?>" class="form-control  required validate-email" maxlength="100" />
					      <div class="bottom-border"></div>
					    </div>
				  	</div>

				  	<?php if(version_compare(JVERSION, '2.5', 'ge')): ?>
				  	<div class="form-group">
					    <label class="control-label col-sm-2" id="email2msg" for="email2"><?php echo JText::_('K2_CONFIRM_EMAIL'); ?> *</label>
					    <div class="col-sm-8">
					      <input type="text" id="email2" name="jform[email2]" size="40" value="<?php echo $this->escape($this->user->get( 'email' )); ?>" class="form-control  required validate-email" maxlength="100" />
					      <div class="bottom-border"></div>
					    </div>
				  	</div>
				  	<?php endif; ?>

				  	<div class="form-group">
					    <label class="control-label col-sm-2" id="pwmsg" for="password"><?php echo JText::_('K2_PASSWORD'); ?></label>
					    <div class="col-sm-8">
					      <input class="form-control  validate-password" type="password" id="password" name="<?php echo $this->passwordFieldName; ?>" size="40" value="" />
					      <div class="bottom-border"></div>
					    </div>
				  	</div>

				  	<div class="form-group">
					    <label class="control-label col-sm-2" id="pw2msg" for="password2"><?php echo JText::_('K2_VERIFY_PASSWORD'); ?></label>
					    <div class="col-sm-8">
					      <input class="form-control  validate-passverify" type="password" id="password2" name="<?php echo $this->passwordVerifyFieldName; ?>" size="40" value="" />
					      <div class="bottom-border"></div>
					    </div>
				  	</div>
				</div>
			</div>
		</div>
		<div class="bottom-border"></div>

		<div class="panel panel-default">
			<div class="panel-heading"><h4 class="panel-title"><?php echo JText::_('K2_PERSONAL_DETAILS'); ?></h4></div>
			<div class="panel-body">
				<div class="form-horizontal">
					<div class="form-group">
					    <label class="control-label col-sm-2" id="gendermsg" for="gender"><?php echo JText::_('K2_GENDER'); ?></label>
					    <?php echo $this->lists['gender']; ?>
				  	</div>
				  	<div class="form-group">
					    <label class="control-label col-sm-2" id="descriptionmsg" for="description"><?php echo JText::_('K2_DESCRIPTION'); ?></label>
					    <div class="col-sm-10">
					     <?php echo $this->editor; ?>
					    </div>
				  	</div>

				  	<div class="form-group">
					    <label class="control-label col-sm-2" id="imagemsg" for="image"><?php echo JText::_( 'K2_USER_IMAGE_AVATAR' ); ?></label>
					    <div class="col-sm-6">
					      	<input type="file" id="image" name="image" class="form-control"/>
					      	<div class="bottom-border"></div>
							<?php if ($this->K2User->image): ?>
							<div>
							<br>
							<img class="thumbnail k2AccountPageImage" src="<?php echo JURI::root(true).'/media/k2/users/'.$this->K2User->image; ?>" alt="<?php echo $this->user->name; ?>" />
							<input type="checkbox" name="del_image" id="del_image" />
							<label for="del_image"><?php echo JText::_('K2_CHECK_THIS_BOX_TO_DELETE_CURRENT_IMAGE_OR_JUST_UPLOAD_A_NEW_IMAGE_TO_REPLACE_THE_EXISTING_ONE'); ?></label>
							</div>
							<?php endif; ?>
					    </div>
				  	</div>

				  	<div class="form-group">
					    <label class="control-label col-sm-2" id="urlmsg" for="url"><?php echo JText::_('K2_URL'); ?></label>
					    <div class="col-sm-6">
					      <input type="text" size="50" value="<?php echo $this->K2User->url; ?>" name="url" id="url" class="form-control"/>
					      <div class="bottom-border"></div>
					    </div>
				  	</div>
				</div>
			</div>
		</div>
		<div class="bottom-border"></div>

		<?php if(count(array_filter($this->K2Plugins))): ?>
		<!-- K2 Plugin attached fields -->
		<div class="panel panel-default">
			<div class="panel-heading"><h4 class="panel-title"><?php echo JText::_('K2_ADDITIONAL_DETAILS'); ?></h4></div>
			<div class="panel-body">
				<div class="form-horizontal">
					<?php foreach($this->K2Plugins as $K2Plugin): ?>
					<?php if(!is_null($K2Plugin)): ?>
				  	<div class="form-group">
					    <?php echo $K2Plugin->fields; ?>
				  	</div>
				  	<?php endif; ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="bottom-border"></div>
		<?php endif; ?>

		<?php if(isset($this->params) && version_compare(JVERSION, '1.6', 'lt')): ?>

		<div class="panel panel-default">
			<div class="panel-heading"><h4 class="panel-title"><?php echo JText::_('K2_ADMINISTRATIVE_DETAILS'); ?></h4></div>
			<div class="panel-body">
				<div class="form-horizontal">
				  	<div class="form-group">
					    <?php echo $this->params->render('params'); ?>
				  	</div>
				</div>
			</div>
		</div>
		<div class="bottom-border"></div>
		<?php endif; ?>


		<!-- Joomla! 1.6+ JForm implementation -->
		<?php if(isset($this->form)): ?>
		<?php foreach ($this->form->getFieldsets() as $fieldset): // Iterate through the form fieldsets and display each one.?>
			<?php if($fieldset->name != 'core'): ?>
			<?php $fields = $this->form->getFieldset($fieldset->name);?>
			<?php if (count($fields)):?>
				<div class="panel panel-default core-form">
					<?php if (isset($fieldset->label)):// If the fieldset has a label set, display it as the legend.?>
					<div class="panel-heading"><h4 class="panel-title"><?php echo JText::_($fieldset->label);?></h4></div>
					<?php endif;?>
					<div class="panel-body">
						<div class="form-horizontal">
						<?php foreach($fields as $field):// Iterate through the fields in the set and display them.?>
							<?php if ($field->hidden):// If the field is hidden, just display the input.?>
								<div class="form-group">
								    <?php echo $field->input;?>
							  	</div>
							<?php else:?>
								<div class="form-group">
								    <div class="control-label col-sm-2">
								    	<?php echo $field->label; ?>
										<?php if (!$field->required && $field->type != 'Spacer'): ?>
											<span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL');?></span>
										<?php endif; ?>
								    </div>
								    <div class="col-sm-6">
								      <?php echo $field->input;?>
								      <div class="bottom-border  clearfix"></div>
								    </div>
							  	</div>
							<?php endif;?>
						<?php endforeach;?>
						</div>
					</div>
				</div>
				<div class="bottom-border"></div>
			<?php endif;?>
			<?php endif; ?>
		<?php endforeach;?>
		<?php endif; ?>

		<div class="k2AccountPageUpdate">
			<button class="button-green btn validate" type="submit" onclick="submitbutton( this.form );return false;">
				<i class="fa fa-save"></i>&nbsp;<?php echo JText::_('K2_SAVE'); ?>
			</button>
		</div>
	</div>
	<input type="hidden" name="<?php echo $this->usernameFieldName; ?>" value="<?php echo $this->user->get('username'); ?>" />
	<input type="hidden" name="<?php echo $this->idFieldName; ?>" value="<?php echo $this->user->get('id'); ?>" />
	<input type="hidden" name="gid" value="<?php echo $this->user->get('gid'); ?>" />
	<input type="hidden" name="option" value="<?php echo $this->optionValue; ?>" />
	<input type="hidden" name="task" value="<?php echo $this->taskValue; ?>" />
	<input type="hidden" name="K2UserForm" value="1" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
