<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);

?>

<!-- Start K2 Item Layout -->


	<!-- Plugins: BeforeDisplay -->
	<?php echo $this->item->event->BeforeDisplay; ?>

	<!-- K2 Plugins: K2BeforeDisplay -->
	<?php echo $this->item->event->K2BeforeDisplay; ?>
	<div class="post-meta">
		<div class="date">
			<?php if($this->item->params->get('catItemDateCreated')): ?>
			<h2>
				<span><?php echo JHTML::_('date', $this->item->created , 'd');?></span>
				<span class="month"><?php echo JHTML::_('date', $this->item->created , 'M');?></span>
				<span class="year"><?php echo JHTML::_('date', $this->item->created , 'Y');?></span>
			</h2>
			<div class="bottom-border"></div>
			<?php endif; ?>
		</div>
	</div><!-- and post-meta -->

 
<?php if($this->item->params->get('catItemImageGallery') ){ ?>
	<!-- Slider -->
	<?php if($this->item->params->get('catItemImageGallery') && !empty($this->item->gallery)): ?>
    <div class="off-pager imageGallery">
    	
			<!-- Item image gallery -->
			<?php echo $this->item->gallery; ?>
		
	</div> <!-- and shop-slider-container -->
	<?php endif; ?>
  	<!--and Slider-->
  	<div class="blog-item-description">
		<?php if($this->item->params->get('catItemTitle')): ?>
		<!-- Item title -->
		<h3 class="catItemTitle">
			

			<?php if ($this->item->params->get('catItemTitleLinked')): ?>
			<a href="<?php echo $this->item->link; ?>">
				<?php echo $this->item->title; ?>
			</a>
			<?php else: ?>
			<?php echo $this->item->title; ?>
			<?php endif; ?>

			<?php if($this->item->params->get('catItemFeaturedNotice') && $this->item->featured): ?>
			<!-- Featured flag -->
			<span class="itemfeature" title="<?php echo JText::_('K2_FEATURED'); ?>">
		  		<sup>
			  		<i class="fa fa-star"></i>
			  	</sup>
		  	</span>
			<?php endif; ?>

			<?php if(isset($this->item->editLink)): ?>
			<!-- Item edit link -->
			<span class="catItemEditLink">
				<a class="modal" rel="{handler:'iframe',size:{x:990,y:550}}" href="<?php echo $this->item->editLink; ?>"><i class="fa fa-edit"></i></a>
			</span>
			<?php endif; ?>
		</h3>
		<?php endif; ?>
		<?php if($this->item->params->get('catItemTags') && count($this->item->tags)): ?>
		<!-- Item tags -->
		<span class="tags">
			<?php foreach ($this->item->tags as $key => $tag): ?>
				<a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a> <?php echo ( ($key+1) == count($this->item->tags))?'':', '; ?>
			<?php endforeach; ?>
		</span>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemRating')): ?>
		<!-- Item Rating -->
		<div class="catItemRatingBlock">
			<span><?php echo JText::_('K2_RATE_THIS_ITEM'); ?></span>
			<div class="itemRatingForm">
				<ul class="itemRatingList">
					<li class="itemCurrentRating" id="itemCurrentRating<?php echo $this->item->id; ?>" style="width:<?php echo $this->item->votingPercentage; ?>%;"></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_1_STAR_OUT_OF_5'); ?>" class="one-star">1</a></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_2_STARS_OUT_OF_5'); ?>" class="two-stars">2</a></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_3_STARS_OUT_OF_5'); ?>" class="three-stars">3</a></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_4_STARS_OUT_OF_5'); ?>" class="four-stars">4</a></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_5_STARS_OUT_OF_5'); ?>" class="five-stars">5</a></li>
				</ul>
				<div id="itemRatingLog<?php echo $this->item->id; ?>" class="itemRatingLog"><?php echo $this->item->numOfvotes; ?></div>
				<div class="clr"></div>
			</div>
			<div class="clr"></div>
		</div>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemIntroText')): ?>
		<!-- Item introtext -->
		<div class="catItemIntroText">
			<?php echo str_replace('</p>','',$this->item->introtext).'[...]</p>' ?>
		</div>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemAttachments') && count($this->item->attachments)): ?>
		  <!-- Item attachments -->
		  <div class="itemAttachmentsBlock">
			  <span class="fa fa-paperclip"></span>
			    <?php foreach ($this->item->attachments as $attachment): ?>
				    <a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>"><?php echo $attachment->title; ?> <?php if($this->item->params->get('catItemAttachmentsCounter')): ?>
				    <span class="hasTooltip" title="<?php echo ($attachment->hits==1) ? JText::_('K2_DOWNLOAD') : JText::_('K2_DOWNLOADS'); ?>">(<?php echo $attachment->hits; ?>)</span>
				    <?php endif; ?></a>
			    <?php endforeach; ?>
			  </ul>
		  </div>
		  <?php endif; ?>

		<?php if ($this->item->params->get('catItemReadMore')): ?>
		<!-- Item "read more..." link -->
			<a class="more" href="<?php echo $this->item->link; ?>"><?php echo JText::_('TPL_READ_MORE'); ?> <i class="fa fa-angle-right"></i></a>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemCommentsAnchor') && ( ($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1')) ): ?>
		<!-- Anchor link to comments below -->
			<?php if(!empty($this->item->event->K2CommentsCounter)): ?>
				<!-- K2 Plugins: K2CommentsCounter -->
				<?php echo $this->item->event->K2CommentsCounter; ?>
			<?php else: ?>
				<?php if($this->item->numOfComments > 0): ?>
				<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor" class="comment"><i class="fa fa-comments"></i> 
					<?php echo $this->item->numOfComments; ?>
				</a>
				<?php else: ?>
				<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor" class="comment"><i class="fa fa-comments"></i> 
					<?php echo JText::_('0'); ?>
				</a>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemAuthor')): ?>
		<!-- Item Author -->
		<span class="pull-right">
			<?php if(isset($this->item->author->link) && $this->item->author->link): ?>
			<a rel="author" href="<?php echo $this->item->author->link; ?>"><i class="fa fa-user"></i>&nbsp;<?php echo $this->item->author->name; ?></a>
			<?php else: ?>
			<?php echo $this->item->author->name; ?>
			<?php endif; ?>
		</span>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemCategory')): ?>
		<!-- Item category name -->
		<div class="pull-right">
			<a href="<?php echo $this->item->category->link; ?>"><i class="fa fa-folder"></i>&nbsp;<?php echo $this->item->category->name; ?></a>
		</div>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemHits')): ?>
		<!-- Item Hits -->
		<div class="pull-right">
			<span class="catItemHits">
				<i class="fa fa-eye"></i>&nbsp;<?php echo $this->item->hits; ?>
			</span>
		</div>
		<?php endif; ?>
	</div>
<?php } elseif ($this->item->params->get('catItemVideo')) { ?>
	<div class="blog-item1-2">
		<?php if($this->item->params->get('catItemVideo') && !empty($this->item->video)): ?>
		<!-- Item video -->
		<div class="video"> 
			<?php if($this->item->videoType=='embedded'): ?>
			<div class="catItemVideoEmbedded">
				<?php echo $this->item->video; ?>
			</div>
			<?php else: ?>
			<div class="catItemVideo"><?php echo $this->item->video; ?></div>
			<?php endif; ?>
		</div>
		<?php elseif($this->item->params->get('catItemImage') && !empty($this->item->image)): ?>
			<!-- Item Image -->
			<div class="catItemImageBlock">
				<span class="catItemImage">
				<a href="<?php echo $this->item->link; ?>" title="<?php if(!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>">
					<img src="<?php echo $this->item->image; ?>" alt="<?php if(!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>" style="width:<?php echo $this->item->imageWidth; ?>px; height:auto;" />
				</a>
				</span>
			</div>
		<?php endif; ?>

		
		<div class="blog-item-description">
			<?php if($this->item->params->get('catItemTitle')): ?>
			<!-- Item title -->
			<h3 class="catItemTitle">
				

				<?php if ($this->item->params->get('catItemTitleLinked')): ?>
				<a href="<?php echo $this->item->link; ?>">
					<?php echo $this->item->title; ?>
				</a>
				<?php else: ?>
				<?php echo $this->item->title; ?>
				<?php endif; ?>

				<?php if($this->item->params->get('catItemFeaturedNotice') && $this->item->featured): ?>
				<!-- Featured flag -->
				<span class="itemfeature" title="<?php echo JText::_('K2_FEATURED'); ?>">
			  		<sup>
				  		<i class="fa fa-star"></i>
				  	</sup>
			  	</span>
				<?php endif; ?>
				<?php if(isset($this->item->editLink)): ?>
				<!-- Item edit link -->
				<span class="catItemEditLink">
					<a class="modal" rel="{handler:'iframe',size:{x:990,y:550}}" href="<?php echo $this->item->editLink; ?>"><i class="fa fa-edit"></i></a>
				</span>
				<?php endif; ?>
			</h3>
			<?php endif; ?>
			<?php if($this->item->params->get('catItemTags') && count($this->item->tags)): ?>
			<!-- Item tags -->
			<span class="tags">
				<?php foreach ($this->item->tags as $key => $tag): ?>
					<a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a> <?php echo ( ($key+1) == count($this->item->tags))?'':', '; ?>
				<?php endforeach; ?>
			</span>
			<?php endif; ?>

			<?php if($this->item->params->get('catItemRating')): ?>
			<!-- Item Rating -->
			<div class="catItemRatingBlock">
				<span><?php echo JText::_('K2_RATE_THIS_ITEM'); ?></span>
				<div class="itemRatingForm">
					<ul class="itemRatingList">
						<li class="itemCurrentRating" id="itemCurrentRating<?php echo $this->item->id; ?>" style="width:<?php echo $this->item->votingPercentage; ?>%;"></li>
						<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_1_STAR_OUT_OF_5'); ?>" class="one-star">1</a></li>
						<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_2_STARS_OUT_OF_5'); ?>" class="two-stars">2</a></li>
						<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_3_STARS_OUT_OF_5'); ?>" class="three-stars">3</a></li>
						<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_4_STARS_OUT_OF_5'); ?>" class="four-stars">4</a></li>
						<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_5_STARS_OUT_OF_5'); ?>" class="five-stars">5</a></li>
					</ul>
					<div id="itemRatingLog<?php echo $this->item->id; ?>" class="itemRatingLog"><?php echo $this->item->numOfvotes; ?></div>
					<div class="clr"></div>
				</div>
				<div class="clr"></div>
			</div>
			<?php endif; ?>

			<?php if($this->item->params->get('catItemIntroText')): ?>
			<!-- Item introtext -->
			<div class="catItemIntroText">
				<?php echo str_replace('</p>','',$this->item->introtext).'[...]</p>' ?>
			</div>
			<?php endif; ?>

			
			<?php if($this->item->params->get('catItemAttachments') && count($this->item->attachments)): ?>
		  <!-- Item attachments -->
		  <div class="itemAttachmentsBlock">
			  <span class="fa fa-paperclip"></span>
			    <?php foreach ($this->item->attachments as $attachment): ?>
				    <a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>"><?php echo $attachment->title; ?> <?php if($this->item->params->get('catItemAttachmentsCounter')): ?>
				    <span class="hasTooltip" title="<?php echo ($attachment->hits==1) ? JText::_('K2_DOWNLOAD') : JText::_('K2_DOWNLOADS'); ?>">(<?php echo $attachment->hits; ?>)</span>
				    <?php endif; ?></a>
			    <?php endforeach; ?>
			  </ul>
		  </div>
		  <?php endif; ?>
		  
			<?php if ($this->item->params->get('catItemReadMore')): ?>
			<!-- Item "read more..." link -->
				<a class="more" href="<?php echo $this->item->link; ?>"><?php echo JText::_('TPL_READ_MORE'); ?> <i class="fa fa-angle-right"></i></a>
			<?php endif; ?>

			<?php if($this->item->params->get('catItemCommentsAnchor') && ( ($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1')) ): ?>
			<!-- Anchor link to comments below -->
				<?php if(!empty($this->item->event->K2CommentsCounter)): ?>
					<!-- K2 Plugins: K2CommentsCounter -->
					<?php echo $this->item->event->K2CommentsCounter; ?>
				<?php else: ?>
					<?php if($this->item->numOfComments > 0): ?>
					<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor" class="comment"><i class="fa fa-comments"></i> 
						<?php echo $this->item->numOfComments; ?>
					</a>
					<?php else: ?>
					<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor" class="comment"><i class="fa fa-comments"></i> 
						<?php echo JText::_('0'); ?>
					</a>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>

			<?php if($this->item->params->get('catItemAuthor')): ?>
			<!-- Item Author -->
			<span class="pull-right">
				<?php if(isset($this->item->author->link) && $this->item->author->link): ?>
				<a rel="author" href="<?php echo $this->item->author->link; ?>"><i class="fa fa-user"></i>&nbsp;<?php echo $this->item->author->name; ?></a>
				<?php else: ?>
				<?php echo $this->item->author->name; ?>
				<?php endif; ?>
			</span>
			<?php endif; ?>

			<?php if($this->item->params->get('catItemCategory')): ?>
			<!-- Item category name -->
			<div class="pull-right">
				<a href="<?php echo $this->item->category->link; ?>"><i class="fa fa-folder"></i>&nbsp;<?php echo $this->item->category->name; ?></a>
			</div>
			<?php endif; ?>

			<?php if($this->item->params->get('catItemHits')): ?>
			<!-- Item Hits -->
			<div class="pull-right">
				<span class="catItemHits">
					<i class="fa fa-eye"></i>&nbsp;<?php echo $this->item->hits; ?>
				</span>
			</div>
			<?php endif; ?>
		</div>
		<!-- End description -->
	</div>
	<!-- End blog-item1-2 -->
<?php } elseif ($this->item->params->get('catItemExtraFields')) { ?>

	<div class="blog-item-quote blog-item-top">
		<?php if($this->item->params->get('catItemExtraFields') && count($this->item->extra_fields)): ?>
	  	<!-- Item extra fields -->
			<?php foreach ($this->item->extra_fields as $key=>$extraField): ?>
			<?php if($extraField->value != ''): ?>
				<?php if($extraField->name == 'Quote Content'): ?>
				<p><?php echo $extraField->value; ?></p>
				<?php endif; ?>
			<?php endif; ?>
			<?php endforeach; ?>


			<?php foreach ($this->item->extra_fields as $key=>$extraField): ?>
			<?php if($extraField->value != ''): ?>
				<?php if($extraField->name == 'Author'): ?>
				<span class="autor"><?php echo $extraField->value; ?></span>
				<?php endif; ?>
			<?php endif; ?>
			<?php endforeach; ?>
	  	<?php endif; ?>

		<?php if($this->item->params->get('catItemTags') && count($this->item->tags)): ?>
		<!-- Item tags -->
		<span class="tags">
			<?php foreach ($this->item->tags as $key => $tag): ?>
				<a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a> <?php echo ( ($key+1) == count($this->item->tags))?'':', '; ?>
			<?php endforeach; ?>
		</span>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemRating')): ?>
		<!-- Item Rating -->
		<div class="catItemRatingBlock">
			<span><?php echo JText::_('K2_RATE_THIS_ITEM'); ?></span>
			<div class="itemRatingForm">
				<ul class="itemRatingList">
					<li class="itemCurrentRating" id="itemCurrentRating<?php echo $this->item->id; ?>" style="width:<?php echo $this->item->votingPercentage; ?>%;"></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_1_STAR_OUT_OF_5'); ?>" class="one-star">1</a></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_2_STARS_OUT_OF_5'); ?>" class="two-stars">2</a></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_3_STARS_OUT_OF_5'); ?>" class="three-stars">3</a></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_4_STARS_OUT_OF_5'); ?>" class="four-stars">4</a></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_5_STARS_OUT_OF_5'); ?>" class="five-stars">5</a></li>
				</ul>
				<div id="itemRatingLog<?php echo $this->item->id; ?>" class="itemRatingLog"><?php echo $this->item->numOfvotes; ?></div>
				<div class="clr"></div>
			</div>
			<div class="clr"></div>
		</div>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemAttachments') && count($this->item->attachments)): ?>
		  <!-- Item attachments -->
		  <div class="itemAttachmentsBlock">
			  <span class="fa fa-paperclip"></span>
			    <?php foreach ($this->item->attachments as $attachment): ?>
				    <a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>"><?php echo $attachment->title; ?> <?php if($this->item->params->get('catItemAttachmentsCounter')): ?>
				    <span class="hasTooltip" title="<?php echo ($attachment->hits==1) ? JText::_('K2_DOWNLOAD') : JText::_('K2_DOWNLOADS'); ?>">(<?php echo $attachment->hits; ?>)</span>
				    <?php endif; ?></a>
			    <?php endforeach; ?>
			  </ul>
		  </div>
		  <?php endif; ?>
		

		<?php if ($this->item->params->get('catItemReadMore')): ?>
		<!-- Item "read more..." link -->
			<a class="more" href="<?php echo $this->item->link; ?>"><?php echo JText::_('TPL_READ_MORE'); ?> <i class="fa fa-angle-right"></i></a>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemCommentsAnchor') && ( ($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1')) ): ?>
		<!-- Anchor link to comments below -->
			<?php if(!empty($this->item->event->K2CommentsCounter)): ?>
				<!-- K2 Plugins: K2CommentsCounter -->
				<?php echo $this->item->event->K2CommentsCounter; ?>
			<?php else: ?>
				<?php if($this->item->numOfComments > 0): ?>
				<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor" class="comment"><i class="fa fa-comments"></i> 
					<?php echo $this->item->numOfComments; ?>
				</a>
				<?php else: ?>
				<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor" class="comment"><i class="fa fa-comments"></i> 
					<?php echo JText::_('0'); ?>
				</a>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemAuthor')): ?>
		<!-- Item Author -->
		<span class="pull-right">
			<?php if(isset($this->item->author->link) && $this->item->author->link): ?>
			<a rel="author" href="<?php echo $this->item->author->link; ?>"><i class="fa fa-user"></i>&nbsp;<?php echo $this->item->author->name; ?></a>
			<?php else: ?>
			<?php echo $this->item->author->name; ?>
			<?php endif; ?>
		</span>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemCategory')): ?>
		<!-- Item category name -->
		<div class="pull-right">
			<a href="<?php echo $this->item->category->link; ?>"><i class="fa fa-folder"></i>&nbsp;<?php echo $this->item->category->name; ?></a>
		</div>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemHits')): ?>
		<!-- Item Hits -->
		<div class="pull-right">
			<span class="catItemHits">
				<i class="fa fa-eye"></i>&nbsp;<?php echo $this->item->hits; ?>
			</span>
		</div>
		<?php endif; ?>
	</div>
	<!-- End blog-item-quote -->
<?php } else { ?>

	<?php if($this->item->params->get('catItemImage') && !empty($this->item->image)): ?>
	<!-- Item Image -->
	<div class="catItemImageBlock">
	  <span class="catItemImage">
	    <a href="<?php echo $this->item->link; ?>" title="<?php if(!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>">
	    	<img src="<?php echo $this->item->image; ?>" alt="<?php if(!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>" style="width:<?php echo $this->item->imageWidth; ?>px; height:auto;" />
	    </a>
	  </span>
	</div>
	<?php endif; ?>

	<?php if(
	$this->item->params->get('catItemTitle') ||
	$this->item->params->get('catItemTags') ||
	$this->item->params->get('catItemRating') ||
	$this->item->params->get('catItemIntroText') ||
	$this->item->params->get('catItemReadMore')
	): ?>
	<div class="blog-item-description">
	<?php endif; ?>
		<?php if($this->item->params->get('catItemTitle')): ?>
		<!-- Item title -->
		<h3 class="catItemTitle">
			
			<?php if ($this->item->params->get('catItemTitleLinked')): ?>
			<a href="<?php echo $this->item->link; ?>">
				<?php echo $this->item->title; ?>
			</a>
			<?php else: ?>
			<?php echo $this->item->title; ?>
			<?php endif; ?>

			<?php if($this->item->params->get('catItemFeaturedNotice') && $this->item->featured): ?>
			<!-- Featured flag -->
			<span class="itemfeature" title="<?php echo JText::_('K2_FEATURED'); ?>">
		  		<sup>
			  		<i class="fa fa-star"></i>
			  	</sup>
		  	</span>
			<?php endif; ?>
			<?php if(isset($this->item->editLink)): ?>
			<!-- Item edit link -->
			<span class="catItemEditLink">
				<a class="modal" rel="{handler:'iframe',size:{x:990,y:550}}" href="<?php echo $this->item->editLink; ?>"><i class="fa fa-edit"></i></a>
			</span>
			<?php endif; ?>
		</h3>
		<?php endif; ?>
		<?php if($this->item->params->get('catItemTags') && count($this->item->tags)): ?>
		<!-- Item tags -->
		<span class="tags">
			<?php foreach ($this->item->tags as $key => $tag): ?>
				<a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a> <?php echo ( ($key+1) == count($this->item->tags))?'':', '; ?>
			<?php endforeach; ?>
		</span>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemRating')): ?>
		<!-- Item Rating -->
		<div class="catItemRatingBlock">
			<span><?php echo JText::_('K2_RATE_THIS_ITEM'); ?></span>
			<div class="itemRatingForm">
				<ul class="itemRatingList">
					<li class="itemCurrentRating" id="itemCurrentRating<?php echo $this->item->id; ?>" style="width:<?php echo $this->item->votingPercentage; ?>%;"></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_1_STAR_OUT_OF_5'); ?>" class="one-star">1</a></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_2_STARS_OUT_OF_5'); ?>" class="two-stars">2</a></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_3_STARS_OUT_OF_5'); ?>" class="three-stars">3</a></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_4_STARS_OUT_OF_5'); ?>" class="four-stars">4</a></li>
					<li><a href="#" data-id="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_5_STARS_OUT_OF_5'); ?>" class="five-stars">5</a></li>
				</ul>
				<div id="itemRatingLog<?php echo $this->item->id; ?>" class="itemRatingLog"><?php echo $this->item->numOfvotes; ?></div>
				<div class="clr"></div>
			</div>
			<div class="clr"></div>
		</div>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemIntroText')): ?>
		<!-- Item introtext -->
		<div class="catItemIntroText">
			<?php echo str_replace('</p>','',$this->item->introtext).'[...]</p>' ?>
		</div>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemAttachments') && count($this->item->attachments)): ?>
		  <!-- Item attachments -->
		  <div class="itemAttachmentsBlock">
			  <span class="fa fa-paperclip"></span>
			    <?php foreach ($this->item->attachments as $attachment): ?>
				    <a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>"><?php echo $attachment->title; ?> <?php if($this->item->params->get('catItemAttachmentsCounter')): ?>
				    <span class="hasTooltip" title="<?php echo ($attachment->hits==1) ? JText::_('K2_DOWNLOAD') : JText::_('K2_DOWNLOADS'); ?>">(<?php echo $attachment->hits; ?>)</span>
				    <?php endif; ?></a>
			    <?php endforeach; ?>
			  </ul>
		  </div>
		  <?php endif; ?>

		

		<?php if ($this->item->params->get('catItemReadMore')): ?>
		<!-- Item "read more..." link -->
			<a class="more" href="<?php echo $this->item->link; ?>"><?php echo JText::_('TPL_READ_MORE'); ?> <i class="fa fa-angle-right"></i></a>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemCommentsAnchor') && ( ($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1')) ): ?>
		<!-- Anchor link to comments below -->
			<?php if(!empty($this->item->event->K2CommentsCounter)): ?>
				<!-- K2 Plugins: K2CommentsCounter -->
				<?php echo $this->item->event->K2CommentsCounter; ?>
			<?php else: ?>
				<?php if($this->item->numOfComments > 0): ?>
				<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor" class="comment"><i class="fa fa-comments"></i> 
					<?php echo $this->item->numOfComments; ?>
				</a>
				<?php else: ?>
				<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor" class="comment"><i class="fa fa-comments"></i> 
					<?php echo JText::_('0'); ?>
				</a>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemAuthor')): ?>
		<!-- Item Author -->
		<span class="pull-right">
			<?php if(isset($this->item->author->link) && $this->item->author->link): ?>
			<a rel="author" href="<?php echo $this->item->author->link; ?>"><i class="fa fa-user"></i>&nbsp;<?php echo $this->item->author->name; ?></a>
			<?php else: ?>
			<?php echo $this->item->author->name; ?>
			<?php endif; ?>
		</span>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemCategory')): ?>
		<!-- Item category name -->
		<div class="pull-right">
			<a href="<?php echo $this->item->category->link; ?>"><i class="fa fa-folder"></i>&nbsp;<?php echo $this->item->category->name; ?></a>
		</div>
		<?php endif; ?>

		<?php if($this->item->params->get('catItemHits')): ?>
		<!-- Item Hits -->
		<div class="pull-right">
			<span class="catItemHits">
				<i class="fa fa-eye"></i>&nbsp;<?php echo $this->item->hits; ?>
			</span>
		</div>
		<?php endif; ?>
		<?php if(
		$this->item->params->get('catItemTitle') ||
		$this->item->params->get('catItemTags') ||
		$this->item->params->get('catItemRating') ||
		$this->item->params->get('catItemIntroText') ||
		$this->item->params->get('catItemReadMore')
		): ?>
		</div>
		<?php endif; ?>
	
<?php  }; ?>


	<div class="bottom-border"></div>
