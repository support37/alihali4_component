<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('bootstrap.tooltip');
?>
<div class="login-mod-custom">

<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" class="login-form form-inline">
  <?php if ($params->get('pretext')) : ?>
  <div class="pretext">
    <p><?php echo $params->get('pretext'); ?></p>
  </div>
  <?php endif; ?>
  <div class="userdata">
    <?php if (!$params->get('usetext')) : ?>

	<div class="rowform">
        <label id="username-lbl" for="modlgn-username" class="required" aria-invalid="false"><?php echo JText::_('MOD_LOGIN_VALUE_USERNAME') ?><span class="star">&nbsp;*</span></label>        <div class="wrapinput lineinput"> 
		<input id="modlgn-username" type="text" name="username"  tabindex="0" size="25" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME') ?>" />
		</div>
      </div>
	
	
    <?php else: ?>
    <label for="modlgn-username"><?php echo JText::_('MOD_LOGIN_VALUE_USERNAME') ?></label>
    <input id="modlgn-username" type="text" name="username"  tabindex="0" size="25" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME') ?>" />
    <?php endif; ?>
    <?php if (!$params->get('usetext')) : ?>
	
	
	<div class="rowform">
        <label id="password-lbl" for="modlgn-passwd" class="required" aria-invalid="false"><?php echo JText::_('JGLOBAL_PASSWORD') ?><span class="star">&nbsp;*</span></label>        
		<div class="wrapinput lineinput">
		  <input id="modlgn-passwd" type="password" name="password"  tabindex="0" size="25" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
	
		 </div>
      </div>
	  
	  

    <?php else: ?>
    <label for="modlgn-passwd"><?php echo JText::_('JGLOBAL_PASSWORD') ?></label>
    <input id="modlgn-passwd" type="password" name="password" class="input-small" tabindex="0" size="25" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
    <?php endif; ?>
    <?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
	
	<div class="rowform rememberme">
		<label></label>
        <div class="wrapinput">
		   <input id="modlgn-remember" type="checkbox" name="remember" class="inputbox checkbox" value="yes"/> 
          <label for="modlgn-remember"><?php echo JText::_('MOD_LOGIN_REMEMBER_ME') ?></label>
        </div>
      </div>
	  

    <?php endif; ?>
	
	<div class="rowform">
		<label></label>
	  <button type="submit" tabindex="0" name="Submit" class="button-green upp"><?php echo JText::_('JLOGIN') ?></button>     
      </div>
	  
	  
    <div id="form-login-submit" class="control-group">
     
    </div>
    <?php
			$usersConfig = JComponentHelper::getParams('com_users');
			if ($usersConfig->get('allowUserRegistration')) : ?>
			
			<div class="rowform">
			<label></label>
			<ul class="Unordered">
				<li><a class="c-pointer" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>"> <?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_PASSWORD'); ?></a></li> 
				<li><a class="c-pointer" href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>"> <?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_USERNAME'); ?></a></li> 
				<li><a class="c-pointer" href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>"> <?php echo JText::_('MOD_LOGIN_REGISTER'); ?></a></li> 
    
			</ul>
			</div>
			
			
    <?php endif; ?>
    <input type="hidden" name="option" value="com_users" />
    <input type="hidden" name="task" value="user.login" />
    <input type="hidden" name="return" value="<?php echo $return; ?>" />
    <?php echo JHtml::_('form.token'); ?> </div>
  <?php if ($params->get('posttext')) : ?>
  <div class="posttext">
    <p><?php echo $params->get('posttext'); ?></p>
  </div>
  <?php endif; ?>
</form>

</div>