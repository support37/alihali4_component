<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.4.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2014 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

JVJSlib::add('jquery.plugins.masonry');
JVJSlib::add('jquery.plugins.imagesloaded'); 
 
defined('_JEXEC') or die('Restricted access');
$session = JFactory::getSession();
//$session->clear('gridview');
if(isset($_REQUEST['gridtype'])){
    $gridfull = $_REQUEST['gridtype'];
    $session->set('gridfull', $gridfull);
}else{
    $gridfull = 0;
    if($session->get('gridfull')) $gridfull = $session->get('gridfull');
}
?>

<?php
$app = JFactory::getApplication();
$mainDivName=$this->params->get('main_div_name');
$carouselEffect=$this->params->get('carousel_effect');
$enableCarousel=$this->params->get('enable_carousel');

$textCenterd=$this->params->get('text_center');
$this->align="left";
if($textCenterd){
	$this->align="center";
}
$height=$this->params->get('image_height');
$width=$this->params->get('image_width');
$this->borderClass="";

if($this->params->get('border_visible',1) == 1){
	$this->borderClass="hikashop_subcontainer_border";
}
if($this->params->get('border_visible',1) == 2){
	$this->borderClass="thumbnail";
}
if(empty($width) && empty($height)){
	$width=$this->image->main_thumbnail_x;
	$height=$this->image->main_thumbnail_y;
}
if(!empty($this->rows)){
	$row = reset($this->rows);
	$this->image->checkSize($width,$height,$row);
	$this->newSizes= new stdClass();
	$this->newSizes->height=$height;
	$this->newSizes->width=$width;
	$this->image->main_thumbnail_y=$height;
	$this->image->main_thumbnail_x=$width;
}

if((!empty($this->rows) || !$this->module || JRequest::getVar('hikashop_front_end_main',0)) && $this->pageInfo->elements->total){
	$pagination = $this->config->get('pagination','bottom');
	if(in_array($pagination,array('top','both')) && $this->params->get('show_limit') && $this->pageInfo->elements->total){
		$this->pagination->form = '_top';
?>




<div class="hikashop_products_pagination hikashop_products_pagination_top">
	<div class="shop-content-header">
		<form action="<?php echo hikashop_currentURL(); ?>" method="post" name="adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top">
			<input type="hidden" name='limit_<?php echo $this->params->get('main_div_name').$this->category_selected;?>' id="<?php echo $this->params->get('main_div_name').$this->category_selected;?>">
			<span class="pull-left">
			<span class="clearfix">
				<span class="pull-left"><?php echo $this->pagination->getResultsCounter(); ?> <?php echo JText::_('PRODUCTS'); ?></span> <br>
				<span class="pull-left"><?php echo JText::_('VIEWS'); ?></span><span class="pull-left"> :</span>
				<a href="#" onclick="document.adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top.limit_<?php echo $this->params->get('main_div_name').$this->category_selected;?>.value=<?php echo $this->params->get('limit'); ?>; document.adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top.submit();return false;"><?php echo $this->params->get('limit'); ?></a>/

				<a href="#" onclick="document.adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top.limit_<?php echo $this->params->get('main_div_name').$this->category_selected;?>.value=<?php echo $this->params->get('limit')*2; ?>; document.adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top.submit();return false;"><?php echo $this->params->get('limit')*2; ?></a>/

				<a href="#" onclick="document.adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top.limit_<?php echo $this->params->get('main_div_name').$this->category_selected;?>.value=<?php echo $this->params->get('limit')*3; ?>; document.adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top.submit();return false;"><?php echo $this->params->get('limit')*3; ?></a>/

				<a href="#" onclick="document.adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top.limit_<?php echo $this->params->get('main_div_name').$this->category_selected;?>.value=<?php echo $this->params->get('limit')*4; ?>; document.adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top.submit();return false;"><?php echo $this->params->get('limit')*4; ?></a>/

				<a href="#" onclick="document.adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top.limit_<?php echo $this->params->get('main_div_name').$this->category_selected;?>.value=<?php echo $this->params->get('limit')*5; ?>; document.adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top.submit();return false;"><?php echo $this->params->get('limit')*5; ?></a>/

				<a href="#" onclick="document.adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top.limit_<?php echo $this->params->get('main_div_name').$this->category_selected;?>.value=0; document.adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_top.submit();return false;"><?php echo  JText::_('HIKA_ALL'); ?></a>
			</span>
			</span>

			<input type="hidden" name="filter_order_<?php echo $this->params->get('main_div_name').$this->category_selected;?>" value="<?php echo $this->pageInfo->filter->order->value; ?>" />
			<input type="hidden" name="filter_order_Dir_<?php echo $this->params->get('main_div_name').$this->category_selected;?>" value="<?php echo $this->pageInfo->filter->order->dir; ?>" />
			<?php echo JHTML::_( 'form.token' ); ?>
		</form>


		<form action="<?php echo hikashop_currentURL(); ?>" method="post" name="gridview">
			<a href="#" class="pull-left shop-item-grid <?php if(isset($gridfull) && !$gridfull) echo 'shop-item-grid-hover'; ?>" onclick="document.gridview.gridtype.value=0; document.gridview.submit();return false;"><div class="bottom-border"></div></a>

			<a href="#" class="pull-left shop-item-detail <?php if(isset($gridfull) && $gridfull) echo 'shop-item-detail-hover'; ?>" onclick="document.gridview.gridtype.value=1; document.gridview.submit();return false;">
				<div class="bottom-border"></div>
			</a>
			<input type="hidden" name="gridtype" />
		</form>
		

	


<?php
   $document = JFactory::getDocument();
   $renderer = $document->loadRenderer('module');
   $db = JFactory::getDBO();
   $db->setQuery("SELECT * FROM #__modules WHERE position='sortby' AND published=1 ORDER BY ordering");
	$modules = $db->loadObjectList();
   if( count( $modules ) > 0 ) :
   ?>
	<div class="pull-right sort-buy hidden-xs">
        <div class="pull-right sort-buy-button">{jvhotspot sortby}</div>
        <p class="pull-left"><?php echo JText::_('TPL_SORTBY');?></p>
    </div>
   <?php endif; ?>

       
	</div>
</div>
<?php
	}
	
$idPanel = rand();	
?>
	<div   class="shop-content">
	<div id="div_hikashop_products<?php echo $idPanel?>" class="row div_hikashop_products gridItem <?php if(isset($gridfull) && $gridfull) echo 'gridactive'; ?>">
<?php

	if(!empty($this->rows)){

		if ($this->config->get('show_quantity_field')>=2) {
?>
		<form action="<?php echo hikashop_completeLink('product&task=updatecart'); ?>" method="post" name="hikashop_product_form_<?php echo $this->params->get('main_div_name'); ?>" enctype="multipart/form-data">
<?php
		}
		if($enableCarousel){
			$this->setLayout('carousel');
			echo $this->loadTemplate();
		}
		else
		{
			$columns = (int)$this->params->get('columns');
			if(empty($columns) || $columns<1) $columns = 1;
			$width = (int)(100/$columns)-1;
			$current_column = 1;
			$current_row = 1;

			if(HIKASHOP_RESPONSIVE) {
				switch($columns) {
					case 12:
					case 6:
					case 4:
					case 3:
					case 2:
					case 1:
						$row_fluid = 12;
						$span = $row_fluid / $columns;
						break;
					case 10:
					case 8:
					case 7:
						$row_fluid = $columns;
						$span = 1;
						break;
					case 5:
						$row_fluid = 10;
						$span = 2;
						break;
					case 9: // special case
						$row_fluid = 10;
						$span = 1;
						break;
				}
			}





			if($gridfull){
				if(isset($_POST['gridtype'])){
					if($_POST['gridtype']==0){
						$grid=$columns;

					}
					else{
						$grid=$_POST['gridtype'];

					}
					$session->set('gridview', $grid);
				}
			}
			else{
				$session->set('gridview', $columns);
			}
			$columns = $session->get('gridview');
			if($columns==1){
				$session->set('itemrowimg','col-md-4');
				$session->set('itemrowdes','col-md-8');
			}
			else{
				$session->clear('itemrowimg');
				$session->clear('itemrowdes');
			}


			foreach($this->rows as $row){
				
                    $in_cart = false;
                    $classCart = hikashop_get('class.cart');
                    $fullcart = $classCart->loadFullCart(true,true,true);
                    if($fullcart->products) foreach($fullcart->products as $product){
                        if($product->product_id == $row->product_id || $product->product_parent_id == $row->product_id){
                            $in_cart = true;
                        }
                    }


			if(!HIKASHOP_RESPONSIVE) {
?>		<div class="masonry-style col-md-<?php echo (int)12/$columns; ?> col-sm-<?php echo (int)12/$columns; ?> col-xs-6 col-650">

			<div class="item hkfrm-addcart <?php if($in_cart) echo 'exist-in-cart';?>">
				<div class="innerItem">

<?php
				} else {
?>
			<div class="masonry-style col-md-<?php echo $span; ?> col-sm-<?php echo $span; ?> col-xs-6 col-650">
				<div class="item hkfrm-addcart <?php if($in_cart) echo 'exist-in-cart';?>">
					<div class="innerItem">
<?php
				}

				if($row->product_parent_id != 0 && isset($row->main_product_quantity_layout)){
					$row->product_quantity_layout = $row->main_product_quantity_layout;
				}
				if(!empty($row->product_quantity_layout) &&  $row->product_quantity_layout != 'inherit'){
					$qLayout = $row->product_quantity_layout;
				}else{
					$categoryQuantityLayout = '';
					if(!empty($row->categories) ) {
						foreach($row->categories as $category) {
							if(!empty($category->category_quantity_layout) && $this->quantityDisplayType->check($category->category_quantity_layout, $app->getTemplate())) {
								$categoryQuantityLayout = $category->category_quantity_layout;
								break;
							}
						}
					}
					if(!empty($categoryQuantityLayout) && $categoryQuantityLayout != 'inherit'){
						$qLayout = $categoryQuantityLayout;
					}else{
						$qLayout = $this->config->get('product_quantity_display','show_default');
					}
				}
				JRequest::setVar('quantitylayout',$qLayout);
				$this->row =& $row;
				$this->setLayout('listing_'.$this->params->get('div_item_layout_type'));
				echo $this->loadTemplate();

				if(!HIKASHOP_RESPONSIVE) {
?>

				</div>
                <div class="bottom-border">	</div>
			</div>

			</div> 
<?php
				} else {
?>
					</div>
				</div>
			</div>
<?php
				}
				if($current_column>=$columns){
					$current_row++;
					if(!HIKASHOP_RESPONSIVE) {
?>
			
<?php
					}
					$current_column=0;
				}
				$current_column++;
			}
			
		}
?>
			
<?php
		if ($this->config->get('show_quantity_field')>=2) {
			$this->ajax = 'if(hikashopCheckChangeForm(\'item\',\'hikashop_product_form_'.$this->params->get('main_div_name').'\')){ return hikashopModifyQuantity(\'\',field,1,\'hikashop_product_form_'.$this->params->get('main_div_name').'\'); } return false;';
			$this->row = new stdClass();
			$this->row->prices = array($this->row);
			$this->row->product_quantity = -1;
			$this->row->product_min_per_order = 0;
			$this->row->product_max_per_order = -1;
			$this->row->product_sale_start = 0;
			$this->row->product_sale_end = 0;
			$this->row->prices = array('filler');
	$this->params->set('show_quantity_field',2);
			$this->setLayout('quantity');
			echo $this->loadTemplate();
			if(!empty($this->ajax) && $this->config->get('redirect_url_after_add_cart','stay_if_cart')=='ask_user'){
?>
			<input type="hidden" name="popup" value="1"/>
<?php
			}
?>
			<input type="hidden" name="hikashop_cart_type_0" id="hikashop_cart_type_0" value="cart"/>
			<input type="hidden" name="add" value="1"/>
			<input type="hidden" name="ctrl" value="product"/>
			<input type="hidden" name="task" value="updatecart"/>
			<input type="hidden" name="return_url" value="<?php echo urlencode(base64_encode(urldecode($this->redirect_url)));?>"/>
		</form>
<?php
		}
	}
?>
	</div>
	</div>
	<?php if(in_array($pagination,array('bottom','both')) && $this->params->get('show_limit') && $this->pageInfo->elements->total){ $this->pagination->form = '_bottom'; ?>
	<form action="<?php echo hikashop_currentURL(); ?>" method="post" name="adminForm_<?php echo $this->params->get('main_div_name').$this->category_selected;?>_bottom">
		<div class="hikashop_products_pagination hikashop_products_pagination_bottom">
		<?php echo $this->pagination->getListFooter($this->params->get('limit')); ?>
		<span class="hikashop_results_counter"><?php echo $this->pagination->getResultsCounter(); ?></span>
		</div>
		<input type="hidden" name="filter_order_<?php echo $this->params->get('main_div_name').$this->category_selected;?>" value="<?php echo $this->pageInfo->filter->order->value; ?>" />
		<input type="hidden" name="filter_order_Dir_<?php echo $this->params->get('main_div_name').$this->category_selected;?>" value="<?php echo $this->pageInfo->filter->order->dir; ?>" />
		<?php echo JHTML::_( 'form.token' ); ?>
	</form>
<?php }
}
?>
<script type="text/javascript">
jQuery(function($){
	var $container = $('#div_hikashop_products<?php echo $idPanel?>').imagesLoaded(function(){
		$container.masonry({
		  itemSelector: '.masonry-style'
		});
	});
	
	$container.children().each(function(i){
		var item = $(this), info = item.find('.info');
		item.hover(function(){
			item.css('z-index',500-i);
			info.stop().slideDown(300);
		},function(){
			info.stop().slideUp(300,function(){
				item.css('z-index','');
			});
		});
	});
});		
</script>