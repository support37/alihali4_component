<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.3.0
 * @author	hikashop.com
 * @copyright	(C) 2010-2014 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
if(!empty($this->rows)){
	
$columnsitem = $this->params->get('columns');
if ( $columnsitem > 6)
	$columnsitem = 6;
if ( $columnsitem < 1)
	$columnsitem = 1;
	


	
?>
		<div class="hikashop_products_list_fix <?php if( $columnsitem ==1)  echo 'cols_list_'.$columnsitem; else echo 'row gridItem'; ?>" >
			

			<?php foreach($this->rows as $row){
				$this->row =& $row;
				$height = $this->params->get('image_height');
				$width = $this->params->get('image_width');
				if(empty($height)) $height=$this->config->get('thumbnail_y');
				if(empty($width)) $width=$this->config->get('thumbnail_x');
				$divWidth=$width;
				$divHeight=$height;
				$this->image->checkSize($divWidth,$divHeight,$row);
				$link = hikashop_completeLink('product&task=show&cid='.$this->row->product_id.'&name='.$this->row->alias.$this->itemid.$this->category_pathway); ?>
                
                <?php if ( $columnsitem > 1) { ?>
                <div  class="col-md-<?php echo number_format(12/$columnsitem,0); ?> col-sm-<?php echo number_format(12/$columnsitem,0); ?> ">
                <?php } ?>
					
                 <div class="item ">
                 <div class="innerItem">
					<?php if($this->config->get('thumbnail')){ ?>
						<div class="moduleItemImage">

								<?php if($this->params->get('link_to_product_page',1)){ ?>
									<a href="<?php echo $link;?>" title="<?php echo $this->escape($this->row->product_name); ?>">
								<?php }
									$image_options = array('default' => true,'forcesize'=>$this->config->get('image_force_size',true),'scale'=>$this->config->get('image_scale_mode','inside'));
									$img = $this->image->getThumbnail(@$this->row->file_path, array('width' => $this->image->main_thumbnail_x, 'height' => $this->image->main_thumbnail_y), $image_options);
									if($img->success) {
										echo '<img class="hikashop_product_listing_image" title="'.$this->escape(@$this->row->file_description).'" alt="'.$this->escape(@$this->row->file_name).'" src="'.$img->url.'"/>';
									}
									$main_thumb_x = $this->image->main_thumbnail_x;
									$main_thumb_y = $this->image->main_thumbnail_y;
									if($this->params->get('display_badges',1)){
										$this->classbadge->placeBadges($this->image, $this->row->badges, -10, 0);
									}
									$this->image->main_thumbnail_x = $main_thumb_x;
									$this->image->main_thumbnail_y = $main_thumb_y;

								if($this->params->get('link_to_product_page',1)){ ?>
									</a>
								<?php } ?>

						</div>
					<?php } ?>
                    
                    
                    <div class=" content-item-description">
					<h3>
	
							<?php if($this->params->get('link_to_product_page',1)){ ?>
								<a href="<?php echo $link;?>">
							<?php }
								echo $this->row->product_name;
							if($this->params->get('link_to_product_page',1)){ ?>
								</a>
							<?php } ?>

						<?php if(!empty($this->row->extraData->afterProductName)) { echo implode("\r\n",$this->row->extraData->afterProductName); } ?>
					</h3>
                    

					<?php if ($this->config->get('show_code')){ ?>
						<div class="hikashop_product_code_row">
							<?php if($this->params->get('link_to_product_page',1)){ ?>
								<a href="<?php echo $link;?>">
							<?php }
							echo $this->row->product_code;
							if($this->params->get('link_to_product_page',1)){ ?>
								</a>
							<?php } ?>
						</div>
					<?php } ?>
                    
					<?php if($this->params->get('show_vote_product')){ ?>
						<div class="hikashop_product_vote_row">
							<?php
							$this->row =& $row;
							$this->setLayout('listing_vote');
							echo $this->loadTemplate();
							?>
						</div>
					<?php } ?>
					
					<?php
						if($this->params->get('show_price','-1')=='-1'){
							$config =& hikashop_config();
							$this->params->set('show_price',$config->get('show_price'));
						}
						if($this->params->get('show_price')){ ?>
						<div class="price">
						<?php
							$this->setLayout('listing_price');
							echo $this->loadTemplate();
						?>
						</div>
					<?php } ?>
                    
                    

					<?php if($this->params->get('add_to_cart')){ ?>
						<div class="hikashop_product_add_to_cart_row">
							<?php
								$this->setLayout('add_to_cart_listing');
								echo $this->loadTemplate();
							?>
						</div>
					<?php } ?>

					<?php
					if(JRequest::getVar('hikashop_front_end_main',0) && JRequest::getVar('task')=='listing' && $this->params->get('show_compare')) {
						if( $this->params->get('show_compare') == 1 ) {
					?>
						<div class="hikashop_product_compare_row">
							<?php
							$js = 'setToCompareList('.$this->row->product_id.',\''.$this->escape($this->row->product_name).'\',this); return false;';
							echo $this->cart->displayButton(JText::_('ADD_TO_COMPARE_LIST'),'compare',$this->params,$link,$js,'',0,1,'hikashop_compare_button');
							?>
						</div>
					<?php } else { ?>
						<div class="hikashop_product_compare_row">
							<input type="checkbox" class="hikashop_compare_checkbox" id="hikashop_listing_chk_<?php echo $this->row->product_id;?>" onchange="setToCompareList(<?php echo $this->row->product_id;?>,'<?php echo $this->escape($this->row->product_name); ?>',this);"><label for="hikashop_listing_chk_<?php echo $this->row->product_id;?>"><?php echo JText::_('ADD_TO_COMPARE_LIST'); ?></label>
						</div>
					<?php }
					} ?>
                    
                    </div>

                   </div>
					<div class="bottom-border">	</div>                    
				</div>
                
                <?php if ( $columnsitem > 1) { ?>
                </div>
                <?php } ?>     
                           
			<?php } ?>

		</div>
<?php } ?>
