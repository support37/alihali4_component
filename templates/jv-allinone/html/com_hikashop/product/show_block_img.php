<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.3.2
 * @author	hikashop.com
 * @copyright	(C) 2010-2014 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
$variant_name = '';
$variant_main = '_main';
$display_mode = '';
if(!empty($this->variant_name)) {
	$variant_name = $this->variant_name;
	if(substr($variant_name, 0, 1) != '_')
		$variant_name = '_' . $variant_name;
	$variant_main = $variant_name;
	$display_mode = 'display:none;';
}
?>
<div id="hikashop_product_image<?php echo $variant_main;?>" class="hikashop_global_image_div shop-content-item" style="<?php echo $display_mode;?>">
	<div class=" shop-slider-container">
		<?php 
			if(!empty($this->element->badges))
			echo $this->classbadge->placeBadges($this->image, $this->element->badges, '0', '0',false);
		?>
		<ul id="hikashop_main_image_div<?php echo $variant_name;?>" class="hikashop_main_image_div gallery product-slider owl-carousel" data-thumb=".<?php echo "product-slider-pager-{$variant_name}"; ?>">
		<?php
			if(!empty ($this->element->images)){
				$image = reset($this->element->images);
			}
			$height = (int)$this->config->get('product_image_y');
			$width = (int)$this->config->get('product_image_x');
			if(empty($height)) $height = (int)$this->config->get('thumbnail_y');
			if(empty($width)) $width = (int)$this->config->get('thumbnail_x');
			$divWidth = $width;
			$divHeight = $height;
			$this->image->checkSize($divWidth,$divHeight,$image);


			$style = '';
			if (!empty ($this->element->images) && count($this->element->images) > 1) {
				if (!empty($height)) {
					$style = ' style="height:' . ($height + 20) . 'px;"';
				}
			}

			$variant_name='';
			if(isset($this->variant_name)){
				$variant_name=$this->variant_name;
			}
			if(!empty ($this->element->images)){
				foreach ($this->element->images as $key => $image) {

					if($this->image->override) {
						echo $this->image->display(@$image->file_path,true,@$image->file_name,'id="hikashop_main_image'.$variant_name.'" style="margin-top:10px;margin-bottom:10px;display:inline-block;vertical-align:middle"','id="hikashop_main_image_link"', $width,  $height);
					} else {
						if(empty($this->popup))
							$this->popup = hikashop_get('helper.popup');
						$image_options = array('default' => true,'forcesize'=>$this->config->get('image_force_size',true),'scale'=>$this->config->get('image_scale_mode','inside'));
						$img = $this->image->getThumbnail(@$image->file_path, array('width' => $width, 'height' => $height), $image_options);
						if($img->success) {
								$attr = 'title="'.$this->escape(@$image->file_description).'"';
							if (!empty ($this->element->images) && count($this->element->images) > 1) {
								$attr .= 'onclick="return window.localPage.openImage(\'hikashop_main_image'.$variant_name.'\');"';
							}
							$html = '<img id="hikashop_main_image'.$variant_name.'" title="'.$this->escape(@$image->file_description).'" alt="'.$this->escape(@$image->file_name).'" src="'.$img->origin_url.'"/>';
						
							echo '<li class="item" rel="zoom">';
							echo $html;
							echo '<a class="zoom-item" rel="prettyPhoto[pp_gal'.$variant_name.']" href="'.$img->origin_url.'" title="'.$this->escape(@$image->file_name).'"></a>';
							echo '</li>';
						}
					}
				}
			} else {
				$image_options = array('default' => true,'forcesize'=>$this->config->get('image_force_size',true),'scale'=>$this->config->get('image_scale_mode','inside'));
				$img = $this->image->getThumbnail(@$image->file_path, array('width' => $width, 'height' => $height), $image_options);
				$html = '<img id="hikashop_main_image'.$variant_name.'" style="" title="'.$this->escape(@$image->file_description).'" alt="'.$this->escape(@$image->file_name).'" src="'.$img->origin_url.'"/>';
				echo $html;
			}

			?>
		</ul>
		<!-- End slide -->
		<div class="bottom-border"></div>
		<div id="hikashop_small_image_div<?php echo $variant_name;?>" class="shop-slider-pager owl-carousel <?php echo "product-slider-pager-{$variant_name}"?>">
			<?php
			if (!empty ($this->element->images) && count($this->element->images) > 1) {
				$firstThunb = true;
				foreach ($this->element->images as $key => $image) {
					if($this->image->override) {
						echo $this->image->display($image->file_path, 'hikashop_main_image'.$variant_name, $image->file_name, 'class="hikashop_child_image"','', $width,  $height);
					} else {
						if(empty($this->popup))
							$this->popup = hikashop_get('helper.popup');
						$img = $this->image->getThumbnail(@$image->file_path, array('width' => $width, 'height' => $height), $image_options);
						if($img->success) {
							$id = null;
							if($firstThunb) {
								$id = 'hikashop_first_thumbnail';
								$firstThunb = false;
							}
								$attr = 'title="'.$this->escape(@$image->file_description).'" onmouseover="return window.localPage.changeImage(this, \'hikashop_main_image'.$variant_name.'\', \''.$img->url.'\', '.$img->width.', '.$img->height.', \''.str_replace("'","\'",@$image->file_description).'\', \''.str_replace("'","\'",@$image->file_name).'\');"';
							$html = '<a href="javascript:void(0);" data-slide-index="'.$key.'"><img  title="'.$this->escape(@$image->file_description).'" alt="'.$this->escape(@$image->file_name).'" src="'.$img->url.'"/></a>';
							if(empty($variant_name)) {
								echo $html;
							} else {
								echo $html;
							}
						}
					}
				}
			}

		?>
		</div>
		<!-- End Thumb -->
	</div>
</div>
<script type="text/javascript">
if(!window.localPage)
	window.localPage = {};
if(!window.localPage.images)
	window.localPage.images = {};
window.localPage.changeImage = function(el, id, url, width, height, title, alt) {
	var d = document, target = d.getElementById(id);
	if(!target) return false;
	target.src = url;
	target.width = width;
	target.height = height;
	target.title = title;
	target.alt = alt;
	window.localPage.images[id] = el;
	return false;
};
window.localPage.openImage = function(id) {
	if(!window.localPage.images[id])
		window.localPage.images[id] = document.getElementById('hikashop_first_thumbnail<?php echo $variant_name;?>');
	window.localPage.images[id].click();
	return false;
};
</script>
