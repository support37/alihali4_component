
<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.3.5
 * @author	hikashop.com
 * @copyright	(C) 2010-2015 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?>

<?php
// hack image
$database = JFactory::getDBO();
$productClass=hikashop_get('class.product');
$query = 'SELECT * FROM '.hikashop_table('file').' WHERE file_ref_id IN ('. $this->row->product_id .') AND file_type IN (\'product\',\'file\') ORDER BY file_ordering ASC, file_id ASC';
$database->setQuery($query);
$product_files = $database->loadObjectList();
if(!empty($product_files)){
	$productClass->addFiles($this->row,$product_files);
}
// end hack image
$link = hikashop_contentLink('product&task=show&cid='.$this->row->product_id.'&name='.$this->row->alias.$this->itemid.$this->category_pathway,$this->row);

if(!empty($this->row->extraData->top)) { echo implode("\r\n",$this->row->extraData->top); }

if($this->config->get('thumbnail',1)){ ?>
<!-- PRODUCT IMG 
<div style="height:0px;text-align:center;clear:both;" class="hikashop_product_image">
	<div style="position:relative;text-align:center;clear:both;width:<?php echo $this->image->main_thumbnail_x;?>px;margin: auto;" class="hikashop_product_image_subdiv">
		<?php if($this->params->get('link_to_product_page',1)){ ?>
			<a href="<?php echo $link;?>" title="<?php echo $this->escape($this->row->product_name); ?>">
		<?php }
			$image_options = array('default' => true,'forcesize'=>$this->config->get('image_force_size',true),'scale'=>$this->config->get('image_scale_mode','inside'));
			$img = $this->image->getThumbnail(@$this->row->file_path, array('width' => $this->image->main_thumbnail_x, 'height' => $this->image->main_thumbnail_y), $image_options);
			if($img->success) {
				echo '<img class="hikashop_product_listing_image" title="'.$this->escape(@$this->row->file_description).'" alt="'.$this->escape(@$this->row->file_name).'" src="'.$img->url.'"/>';
			}
			$main_thumb_x = $this->image->main_thumbnail_x;
			$main_thumb_y = $this->image->main_thumbnail_y;
			if($this->params->get('display_badges',1)){
				$this->classbadge->placeBadges($this->image, $this->row->badges, -10, 0);
			}
			$this->image->main_thumbnail_x = $main_thumb_x;
			$this->image->main_thumbnail_y = $main_thumb_y;

		if($this->params->get('link_to_product_page',1)){ ?>
			</a>
		<?php } ?>
	</div>
</div>
 EO PRODUCT IMG -->
<?php
$session = JFactory::getSession();
//$session->clear('gridview');
$gridcol1=$session->get('itemrowimg');
$gridcol2=$session->get('itemrowdes');
?>
	
<div class="<?php if(isset($gridcol1)) echo $gridcol1;?> hikashop_product_image moduleItemImage">
	<div class="hikashop_product_image_subdiv">
	<?php 
		$main_thumb_x = $this->image->main_thumbnail_x;
		$main_thumb_y = $this->image->main_thumbnail_y;
		if($this->params->get('display_badges',1)){
		$this->classbadge->placeBadges($this->image, $this->row->badges, -10, 0);
		}
		$this->image->main_thumbnail_x = $main_thumb_x;
		$this->image->main_thumbnail_y = $main_thumb_y;
		
		if(isset($this->row->images)){ 
	?>
	
	


	
	
													
					    

													

													

 <!-- and shop-slider-container -->


	<script>	
		jQuery(function($){
			/*$('.jvslider-custom-<?php echo $this->row->product_id; ?>').bxSlider({
				touchEnabled:false,
				pagerCustom: '#paging-slide-custom-<?php echo $this->row->product_id; ?>',
				controls:false
			});	*/		
		});
	</script>
	

<div id="bxslider_<?php echo $this->row->product_id; ?>" class="jvresslide jv-slide-hikashop">
	
	
	
	<ul class="owl-carousel ntd-owl-mainproduct jvslider-custom-<?php echo $this->row->product_id; ?>">
		<?php foreach ($this->row->images as $image) :?>
		<?php $img = $this->image->getThumbnail(@$image->file_path, array('width' => $this->image->main_thumbnail_x, 'height' => $this->image->main_thumbnail_y), $image_options); ?>
			<li>
			<?php if($this->params->get('link_to_product_page',1)): ?>
				<a href="<?php echo $link;?>" title="<?php echo $this->escape($this->row->product_name); ?>">
			<?php endif; ?>
			<img src="<?php echo $img->url; ?>" title="<?php echo $this->escape(@$this->row->file_description); ?>" alt="<?php echo $this->escape(@$this->row->file_name); ?>  ">
			<?php if($this->params->get('link_to_product_page',1)): ?>
				</a>
			<?php endif; ?>
			
			
			</li>
		<?php endforeach; ?>		
	</ul>
	
	<div class="control-custom">
	<div id="paging-slide-custom-<?php echo $this->row->product_id; ?>" class="bxslider-paging">
		<ul class="owl-carousel ntd-owl-thumbnailproduct">
			<?php foreach ($this->row->images as $k=>$image) :?>
			<li>
			<?php $img = $this->image->getThumbnail(@$image->file_path, array('width' => 47, 'height' => 47), $image_options); ?>	
				<?php if($this->params->get('link_to_product_page',1)): ?>
					<a data-slide-index="<?php echo $k; ?>" href="javascript:void(0);" title="<?php echo $this->escape($this->row->product_name); ?>">
				<?php endif; ?>
				<img src="<?php echo $img->url; ?>" title="<?php echo $this->escape(@$this->row->file_description); ?>" alt="<?php echo $this->escape(@$this->row->file_name); ?>  ">
				<?php if($this->params->get('link_to_product_page',1)): ?>
					</a>
				<?php endif; ?>
			</li>
			<?php endforeach; ?>	
		</ul>
	</div>
	</div>

	
	
</div>
	<?php }else{
	
	?>
	
	
	<?php if($this->params->get('link_to_product_page',1)){ ?>
			<a href="<?php echo $link;?>" title="<?php echo $this->escape($this->row->product_name); ?>">
		<?php }
			$image_options = array('default' => true,'forcesize'=>$this->config->get('image_force_size',true),'scale'=>$this->config->get('image_scale_mode','inside'));
			$img = $this->image->getThumbnail(@$this->row->file_path, array('width' => $this->image->main_thumbnail_x, 'height' => $this->image->main_thumbnail_y), $image_options);
			if($img->success) {
				echo '<img class="hikashop_product_listing_image" title="'.$this->escape(@$this->row->file_description).'" alt="'.$this->escape(@$this->row->file_name).'" src="'.$img->url.'"/>';
			}
			$main_thumb_x = $this->image->main_thumbnail_x;
			$main_thumb_y = $this->image->main_thumbnail_y;
			if($this->params->get('display_badges',1)){
				$this->classbadge->placeBadges($this->image, $this->row->badges, -10, 0);
			}
			$this->image->main_thumbnail_x = $main_thumb_x;
			$this->image->main_thumbnail_y = $main_thumb_y;

		if($this->params->get('link_to_product_page',1)){ ?>
			</a>
		<?php } ?>
	
	
	
	
	
	
	
	
	<?php
	
	
	} ?>

	</div>
</div>

<div class="<?php if(isset($gridcol2)) echo $gridcol2;?> content-item-description"> <!-- add this line by ntd -->

	
<!-- PRODUCT VOTE -->
<!-- add this line by ntd -->
<?php
if($this->params->get('show_vote_product')){
	echo '<div class="hikashop_product_vote_row">';
	$this->setLayout('listing_vote');
	echo $this->loadTemplate();
	echo '</div>';
}
?>
<!-- add this line by ntd -->
<!-- EO PRODUCT VOTE -->

<!-- PRODUCT NAME -->
<h3 class="moduleItemTitle"><!-- add this line by ntd -->

	<?php if($this->params->get('link_to_product_page',1)){ ?>
		<a href="<?php echo $link;?>">
	<?php }
		echo $this->row->product_name;
	if($this->params->get('link_to_product_page',1)){ ?>
		</a>
	<?php } ?>

</h3><!-- add this line by ntd -->
<!-- EO PRODUCT NAME -->

<!-- product by categoris related -->
<div class="clearfix color-1 related-categories-list">
<?php
$q = 'SELECT c.* '.
	' FROM ' . hikashop_table('product_category').' as pc '.
	' INNER JOIN '.hikashop_table('category').' AS c ON pc.category_id = c.category_id '.
	' WHERE pc.product_id = '.$this->row->product_id;
$database->setQuery($q);
$product_categories = $database->loadObjectList();
$categories = array();
if($product_categories) foreach ($product_categories as $category) $categories[] = $category->category_name;
if(count($categories) > 4) $categories = array_splice($categories, 0, 4);
//if($categories) echo implode (',',$categories);
foreach($categories as $k=>$c){if($k==count($categories)-1) echo '<span class="pull-left margin-reset">'.$c.'</span>'; else echo '<span class="pull-left margin-reset">'.$c.',</span>';}
?>
</div>
<!-- end -->

<?php } ?>
<!-- PRODUCT PRICE -->
<div class="price"><!-- add this line by ntd -->

<?php
if($this->params->get('show_price','-1')=='-1'){
	$config =& hikashop_config();
	$this->params->set('show_price',$config->get('show_price'));
}
if($this->params->get('show_price')){
	$this->setLayout('listing_price');
	echo $this->loadTemplate();
}

?>
</div><!-- add this line by ntd -->
<!-- EO PRODUCT PRICE -->


<!-- PRODUCT CODE -->

		<?php if ($this->config->get('show_code')) { ?>
	<div class='hikashop_product_code_list'>		        
			<?php if($this->params->get('link_to_product_page',1)){ ?>
				<a href="<?php echo $link;?>">
			<?php }
			echo $this->row->product_code;
			if($this->params->get('link_to_product_page',1)){ ?>
				</a>
			<?php } ?>
	</div>            
		<?php } ?>

<!-- EO PRODUCT CODE -->

<?php if(!empty($this->row->extraData->afterProductName)) { echo implode("\r\n",$this->row->extraData->afterProductName); } ?>



<!-- ADD TO CART BUTTON AREA -->
<?php
if($this->params->get('add_to_cart') || $this->params->get('add_to_wishlist')){
	$this->setLayout('add_to_cart_listing');
	echo $this->loadTemplate();
}?>
<!-- EO ADD TO CART BUTTON AREA -->

<!-- COMPARISON AREA -->
<?php
if(JRequest::getVar('hikashop_front_end_main',0) && JRequest::getVar('task')=='listing' && $this->params->get('show_compare')) { ?>
	<br/><?php
	if( $this->params->get('show_compare') == 1 ) {
		$js = 'setToCompareList('.$this->row->product_id.',\''.$this->escape($this->row->product_name).'\',this); return false;';
		echo $this->cart->displayButton(JText::_('ADD_TO_COMPARE_LIST'),'compare',$this->params,$link,$js,'',0,1,'hikashop_compare_button');
?>

<?php } else { ?>
	<input type="checkbox" class="hikashop_compare_checkbox" id="hikashop_listing_chk_<?php echo $this->row->product_id;?>" onchange="setToCompareList(<?php echo $this->row->product_id;?>,'<?php echo $this->escape($this->row->product_name); ?>',this);"><label for="hikashop_listing_chk_<?php echo $this->row->product_id;?>"><?php echo JText::_('ADD_TO_COMPARE_LIST'); ?></label>
<?php }
} ?>
<!-- EO COMPARISON AREA -->
<?php if(!empty($this->row->extraData->bottom)) { echo implode("\r\n",$this->row->extraData->bottom); } ?>


</div> 

