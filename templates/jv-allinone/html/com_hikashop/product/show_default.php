<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.3.5
 * @author	hikashop.com
 * @copyright	(C) 2010-2015 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
$database = JFactory::getDBO();
$doc = JFactory::getDocument();
$doc->addScript(JUri::root(true)."/templates/jv-allinone/js/jquery.csbuttons.min.js");
	$shareJS = '
	(function($){
		$(function(){		
			$(\'.shareItem\').cSButtons();
		});
	})(jQuery);		
	';
	$doc->addScriptDeclaration($shareJS);

?><div id="hikashop_product_top_part" class="hikashop_product_top_part clearfix">
<?php if(!empty($this->element->extraData->topBegin)) { echo implode("\r\n",$this->element->extraData->topBegin); } ?>
	<h1 class="titlePage">
		<span id="hikashop_product_name_main" class="hikashop_product_name_main">
			<?php
			if (hikashop_getCID('product_id')!=$this->element->product_id && isset ($this->element->main->product_name))
				echo $this->element->main->product_name;
			else
				echo $this->element->product_name;
			?>
		</span>

	</h1>
<?php if(!empty($this->element->extraData->topEnd)) { echo implode("\r\n",$this->element->extraData->topEnd); } ?>
</div>
<div class="shop shop-single">
	<div class="row">

		<div class="col-sm-6 col-md-4">
    		<?php
			if(!empty($this->element->extraData->leftBegin)) { echo implode("\r\n",$this->element->extraData->leftBegin); }
			$this->row = & $this->element;
			$this->setLayout('show_block_img');
			echo $this->loadTemplate();
			if(!empty($this->element->extraData->leftEnd)) { echo implode("\r\n",$this->element->extraData->leftEnd); }
			?>
		</div> 
		<!-- and shop-content-item-container -->
			
		<div class="col-sm-6 col-md-5">

			<!-- Price  -->
			<?php
			if(!empty($this->element->extraData->rightBegin))
				echo implode("\r\n",$this->element->extraData->rightBegin);
			?>
			<span id="hikashop_product_price_main" class="price">
				<?php
				if ($this->params->get('show_price')) {
					$this->row = & $this->element;
					$this->setLayout('listing_price');
					echo $this->loadTemplate();
				}
				?>
			</span>
			<!-- End price -->

			<!-- SKU  -->				
			<span class="clearfix gray-italic skuproduct">
				<span class="pull-left margin-reset"><?php echo JText::_('TPL_SKU'); ?></span>
				<span class="pull-left margin-reset">:</span> 
				<?php //if ($this->config->get('show_code')) { ?>
				<span id="hikashop_product_code_main" class="pull-left margin-reset">
				<?php if(strlen($this->element->product_code) > 30) {?>
					<abbr title="<?php echo $this->element->product_code;?>" style="border:none">
					<?php echo substr($this->element->product_code, 0, 30)?>
					</abbr>
				<?php } else  echo $this->element->product_code; ?>
					
				</span>
				<?php //} ?>
			</span>
			<!-- End SKU -->

			<!-- Vote  -->
			<div id="hikashop_product_vote_mini" class="rating hikashop_product_vote_mini">
					<?php
					$config =& hikashop_config();
					if($this->params->get('show_vote_product') == '-1'){
						$this->params->set('show_vote_product',$config->get('show_vote_product'));
					}
					if($this->params->get('show_vote_product')){
						$js = '';
						$this->params->set('vote_type','product');
						if(isset($this->element->main)){
							$product_id = $this->element->main->product_id;
						}else{
							$product_id = $this->element->product_id;
						}
						$this->params->set('vote_ref_id',$product_id);
						echo hikashop_getLayout('vote', 'mini', $this->params, $js);
					}
					?>
					
			</div>
			<!-- End vote -->

			<!-- Description  -->
			<?php
			if(!empty($this->element->extraData->bottomBegin))
				echo implode("\r\n",$this->element->extraData->bottomBegin);
			?>
			<div class="hikashop_product_description_main">
				<?php echo substr(strip_tags(preg_replace('#<hr *id="system-readmore" */>.*#is','',$this->row->product_description)),0,300).'...'; ?>
			</div>

			<!-- End Description -->

			<!-- characteristic  -->
			<div class="clearfix selectbox">
			<?php
			if($this->params->get('characteristic_display')!='list'){
			$this->setLayout('show_block_characteristic');
			echo $this->loadTemplate();
			?>
			</div>
			<!-- <a href="#" class="clear-selection"><?php //echo JText::_('TPL_CLEAR_SELECTION'); ?></a> -->
			<!-- End Characteristic -->

			<!-- Option   -->
			<?php
			$form = ',0';
			if (!$this->config->get('ajax_add_to_cart', 1)) {
				$form = ',\'hikashop_product_form\'';
			}
			if (hikashop_level(1) && !empty ($this->element->options)) {
			?>
				<div id="hikashop_product_options" class="hikashop_product_options">
					<?php
					$this->setLayout('option');
					echo $this->loadTemplate();
					?>
				</div>
				<div class="bottom-border"></div>
				<?php
				$form = ',\'hikashop_product_form\'';
				if ($this->config->get('redirect_url_after_add_cart', 'stay_if_cart') == 'ask_user') {
				?>
					<input type="hidden" name="popup" value="1"/>
				<?php
				}
			} 

			if (!$this->params->get('catalogue') && ($this->config->get('display_add_to_cart_for_free_products') || !empty ($this->element->prices))) {
				if (!empty ($this->itemFields)) {
					$form = ',\'hikashop_product_form\'';
					if ($this->config->get('redirect_url_after_add_cart', 'stay_if_cart') == 'ask_user') {
					?>
						<input type="hidden" name="popup" value="1"/>
					<?php
					}
					$this->setLayout('show_block_custom_item');
					echo $this->loadTemplate();
				}
			}

			$this->formName = $form;
			if($this->params->get('show_price')){ ?>
				<span id="hikashop_product_price_with_options_main" class="hikashop_product_price_with_options_main">
				</span>
			<?php }?>
			<!-- End Option -->
			<!-- Quantity -->
			<div class="clearfix customrtl-detail-ntd">
			<?php
			if(empty ($this->element->characteristics) || $this->params->get('characteristic_display')!='list'){ ?>
			
				<div id="hikashop_product_quantity_main" class="hikashop_product_quantity_main quantity">
					<span class="pull-left title"><?php echo JText::_('TPL_ITEM_PRODUCT_QUANTITY'); ?>:</span>
					<div class="quantity-block" id="quantity-block"></div>
					<div class="clearfix"></div>
					
					<?php
					$this->row = & $this->element;
					$this->ajax = 'if(hikashopCheckChangeForm(\'item\',\'hikashop_product_form\')){ return hikashopModifyQuantity(\'' . $this->row->product_id . '\',field,1' . $form . ',\'cart\'); } else { return false; }';
					$this->setLayout('quantity');
					echo $this->loadTemplate();
					?>
				</div>
				
			<?php } ?>
			
				<div id="hikashop_product_contact_main" class="hikashop_product_contact_main"><?php
				$contact = $this->config->get('product_contact',0);
				if (hikashop_level(1) && ($contact == 2 || ($contact == 1 && !empty ($this->element->product_contact)))) {
					$empty = '';
					$params = new HikaParameter($empty);
					global $Itemid;
					$url_itemid='';
					if(!empty($Itemid)){
						$url_itemid='&Itemid='.$Itemid;
					}
					echo $this->cart->displayButton(JText :: _('CONTACT_US_FOR_INFO'), 'contact_us', $params, hikashop_completeLink('product&task=contact&cid=' . $this->element->product_id.$url_itemid), 'window.location=\'' . hikashop_completeLink('product&task=contact&cid=' . $this->element->product_id.$url_itemid) . '\';return false;');
				}?></div>
			</div>
			<!-- End Quantity -->

			<div class="clearfix"></div>
			<span class="gray-border"></span>
			<span class="rtlmargin-custom clearfix gray-italic">
				<span class="pull-left margin-reset"><?php echo JText::_('TPL_ITEM_PRODUCT_CATEGORIES'); ?></span>
				<span class="pull-left margin-reset"> : </span>
				<?php

				$q = 'SELECT c.* '.
					' FROM ' . hikashop_table('product_category').' as pc '.
					' INNER JOIN '.hikashop_table('category').' AS c ON pc.category_id = c.category_id '.
					' WHERE pc.product_id = '.hikashop_getCID('product_id');
					$database->setQuery($q);
					$product_categories = $database->loadObjectList();
					$categories = array();
					if($product_categories) {
						foreach ($product_categories as $category) $categories[] = $category->category_name;
                        foreach($categories as $c) $c = '<span class="pull-left margin-reset">'.$c.'</span>';
                        echo implode(', ', $categories);
                    }
				?>
			<!-- end -->
			</span>
			<span class="rtlmargin-custom clearfix gray-italic">
				<span class="pull-left margin-reset"><?php echo JText::_('TPL_ITEM_PRODUCT_TAGS'); ?></span>
				<span class="pull-left margin-reset"> : </span>
				<?php
                if($this->element->product_parent_id){
                    $tag_product_id = $this->element->product_parent_id;
                }else{
                    $tag_product_id = $this->element->product_id;
                }
                $database = JFactory::getDBO();
                $tags = array();
                $filters=array('a.product_id='.$tag_product_id);
                hikashop_addACLFilters($filters,'product_access','a');
                $query = 'SELECT a.*, b.product_category_id, b.category_id, b.ordering FROM '.hikashop_table('product').' AS a LEFT JOIN '.hikashop_table('product_category').' AS b ON a.product_id = b.product_id WHERE '.implode(' AND ',$filters). ' LIMIT 1';
                $database->setQuery($query);
                $tag_product_element = $database->loadObject();
                if(!empty($tag_product_element)){
                    $tagsHelper = hikashop_get('helper.tags');
                    $tags = $tagsHelper->loadTags('product', $tag_product_element);
                }
                if($tags){
                    $tag_lists = array();
                    foreach($tags as $tag){
                        $tag_lists[] = $tag->tag_id;
                    }
                    if($tag_lists){
                        $database->setQuery('SELECT title FROM #__tags WHERE id IN ('.implode(',',$tag_lists).') ORDER BY FIND_IN_SET(id, '.$database->quote(implode(',',$tag_lists)).')');
                        if($tags = $database->loadColumn()){
                            foreach($tags as $tag){
                                $tag = '<span class="pull-left margin-reset">'.$tag.'</span>';
                            }
                            echo implode(', ', $tags);
                        }
                    }
                }
				?>
			</span>


			<span id="hikashop_product_id_main" class="hikashop_product_id_main">
				<input type="hidden" name="product_id" value="<?php echo $this->element->product_id; ?>" />
			</span>
			<?php
			if(!empty($this->element->extraData->rightEnd))
				echo implode("\r\n",$this->element->extraData->rightEnd);
			}?>
		</div>
		<div class="col-md-3 shop-sidebar">
			<!-- Share -->
			<span class="title"><?php echo JText::_('TPL_ITEM_PRODUCT_SHARE'); ?></span>
			<?php
				$pluginsClass = hikashop_get('class.plugins');
				$plugin = $pluginsClass->getByName('content', 'hikashopsocial');
				if(!empty($plugin) && (@$plugin->published || @ $plugin->enabled)) {
					$this->setLayout('show_block_social');
					echo $this->loadTemplate();
				} else {?>
					<ul class="share">
						<?php
							if (!empty($this->element->images[0])) {
								$image = JURI::root().'/media/com_hikashop/upload/'.$this->element->images[0]->file_path;
							} else {
								$image = '';
							}
						?>
						<li><a href="javascript:" class="shareItem" data-type="facebook" data-url="<?php echo $doc->base;?>" data-txt="<?php echo $doc->title;?>"  data-media="<?php echo $image;?>"><i class="fa fa-facebook"></i></a></li>
						<li><a href="javascript:" class="shareItem" data-type="pinterest" data-url="<?php echo $doc->base;?>" data-txt="<?php echo $doc->title;?>" data-media="<?php echo $image;?>"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="javascript:" class="shareItem" data-type="google" data-url="<?php echo $doc->base;?>" data-txt="<?php echo $doc->title;?>"  data-media="<?php echo $image;?>"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="javascript:" class="shareItem" data-type="linkedin" data-url="<?php echo $doc->base;?>" data-txt="<?php echo $doc->title;?>"  data-media="<?php echo $image;?>"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="javascript:" class="shareItem" data-type="twitter" data-url="<?php echo $doc->base;?>" data-txt="<?php echo $doc->title;?>" data-media="<?php echo $image;?>"><i class="fa fa-twitter"></i></a></li>
					</ul>

			<?php }
			?>

			<!-- Brand -->
			<div class="clearfix"></div>
			<?php
			if($this->config->get('manufacturer_display', 0) && !empty($this->element->product_manufacturer_id)){
				$class = hikashop_get('class.category');
				$manufacturer = $class->get($this->element->product_manufacturer_id, true);
				global $Itemid;
				$categoryClass = hikashop_get('class.category');
				$categoryClass->addAlias($manufacturer);
				?>
				<span class="title"><?php echo JText::_('TPL_ITEM_PRODUCT_BRAND'); ?></span>
				<div class="custom_brand_image">	
				<?php
				echo '<a href="'.hikashop_contentLink('category&task=listing&cid='.$manufacturer->category_id.'&name='.$manufacturer->alias.'&Itemid='.$Itemid,$manufacturer).'">'.$this->image->display($manufacturer->file_path,false,$manufacturer->file_name, '', '', $this->image->main_thumbnail_x, $this->image->main_thumbnail_y).'</a>';
				?>
				</div>
				<?php
			}
			?>
			

			<!-- Our services -->
			<div class="our-services">
				<span class="title"><?php echo JText::_('TPL_ITEM_PRODUCT_SERVICES'); ?></span>
				<?php
				$document = JFactory::getDocument();
				$renderer = $document->loadRenderer('modules');
				$position = "services";
				$options = array('style' => 'raw');
				echo $renderer->render($position, $options, null);
				?>
				
			</div>
		</div>
	</div>
</div>
<!-- End shop -->


<div id="hikashop_product_bottom_part" class="hikashop_product_bottom_part">
	<span id="hikashop_product_url_main" class="hikashop_product_url_main">
		<?php
		if (!empty ($this->element->product_url)) {
			echo JText :: sprintf('MANUFACTURER_URL', '<a href="' . $this->element->product_url . '" target="_blank">' . $this->element->product_url . '</a>');
		}
		?>
	</span>
	<?php
	$this->setLayout('show_block_product_files');
	//echo $this->loadTemplate();
	?>
	<?php
	if(!empty($this->element->extraData->bottomMiddle))
		//echo implode("\r\n",$this->element->extraData->bottomMiddle);
	?>
	<?php
	if(!empty($this->element->extraData->bottomEnd))
		//echo implode("\r\n",$this->element->extraData->bottomEnd);
	?>
</div>
