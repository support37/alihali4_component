<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.3.2
 * @author	hikashop.com
 * @copyright	(C) 2010-2014 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
if(JPluginHelper::isEnabled('authentication', 'openid')) {
	$lang = &JFactory::getLanguage();
	$lang->load( 'plg_authentication_openid', JPATH_ADMINISTRATOR );
	$langScript = 	'var JLanguage = {};'.
		' JLanguage.WHAT_IS_OPENID = \''.JText::_( 'WHAT_IS_OPENID' ).'\';'.
		' JLanguage.LOGIN_WITH_OPENID = \''.JText::_( 'LOGIN_WITH_OPENID' ).'\';'.
		' JLanguage.NORMAL_LOGIN = \''.JText::_( 'NORMAL_LOGIN' ).'\';'.
		' var comlogin = 1;';
	$document = &JFactory::getDocument();
	$document->addScriptDeclaration( $langScript );
	JHTML::_('script', 'openid.js');
}
?>
<div class="userdata">
	<div id="form-login-username" class="form-group">
		<div class="input-group">
		  <span class="input-group-addon hasTooltip" data-toggle="tooltip" title="<?php echo JText::_('HIKA_USERNAME'); ?>"><i class="fa fa-user"></i></span>
		  <input id="modlgn-username" type="text" name="username" class="form-control" tabindex="1" size="18" placeholder="<?php echo JText::_('HIKA_USERNAME'); ?>" />
	      <a href="<?php echo JRoute::_('index.php?option=com_users&view=remind');?>" class="input-group-addon hasTooltip" title="<?php echo JText::_('HIKA_FORGOT_YOUR_USERNAME'); ?>"><i class="fa fa-question"></i></a>
		</div>
		<div class="bottom-border"></div>
	</div>
	<div id="form-login-password" class="form-group">
		<div class="input-group">
		  <span class="input-group-addon hasTooltip" title="<?php echo JText::_('HIKA_PASSWORD') ?>"><i class="fa fa-key"></i></span>
		  <input id="modlgn-passwd" type="password" name="passwd" class="form-control" tabindex="2" size="18" placeholder="<?php echo JText::_('HIKA_PASSWORD') ?>" />
	      <a href="<?php echo JRoute::_('index.php?option=com_users&view=reset');?>" class="input-group-addon hasTooltip" title="<?php echo JText::_('HIKA_FORGOT_YOUR_PASSWORD'); ?>"><i class="fa fa-question"></i></a>
		</div>
		<div class="bottom-border"></div>
	</div>
<?php if(JPluginHelper::isEnabled('system', 'remember')) { ?>
	<div id="form-login-remember" class="form-group">
		<input id="modlgn-remember" type="checkbox" name="remember" value="yes"/>
		<label for="modlgn-remember" class="control-label"><?php echo JText::_('HIKA_REMEMBER_ME') ?></label>
		
	</div>
<?php } ?>
	<div id="form-login-submit" class="form-group">
		<div class="controls">
			<?php echo $this->cart->displayButton(JText::_('HIKA_LOGIN'), 'login', @$this->params, '',' document.hikashop_checkout_form.submit(); return false;','', 0, 1, 'button-green'); ?>
		</div>
	</div>
</div>
