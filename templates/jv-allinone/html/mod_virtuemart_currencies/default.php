<?php // no direct access
defined('_JEXEC') or die('Restricted access');
vmJsApi::jQuery();
JHtml::_('formbehavior.chosen', '.jCurrencies select');
$document 	= JFactory::getDocument();
$document->addScriptDeclaration("
	(function(\$){
		\$(function(){
			\$('#virtuemart_currency_id').each(function(){
				el = \$(this);
				el.on('change',function(){
					el.parent('.input-group').find('.btn').click();
				});
			});         		
		});
	})(jQuery);
");

?>

<!-- Currency Selector Module -->
<?php echo $text_before ?>
<form action="<?php echo vmURI::getCleanUrl() ?>" method="post" class="jCurrencies">
	<div class="input-group ">
		<?php echo JHTML::_('select.genericlist', $currencies, 'virtuemart_currency_id', 'class=" form-control"', 'virtuemart_currency_id', 'currency_txt', $virtuemart_currency_id) ; ?>
		<span class="input-group-btn">
			<button class="btn button-green" type="submit" name="submit" title="<?php echo vmText::_('MOD_VIRTUEMART_CURRENCIES_CHANGE_CURRENCIES') ?>" ><i class="fa fa-check"></i></button>
		</span>
    </div><!-- /input-group -->
</form>
