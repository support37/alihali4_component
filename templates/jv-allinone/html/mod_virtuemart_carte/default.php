<?php // no direct access
defined('_JEXEC') or die('Restricted access');

$nmod = str_replace('mod_', '', ModVirtuemartCarteHelper::$_MOD_PREFIX);
?>

<!-- Virtuemart 2 Ajax Card -->
<div class="vmCartModule <?php echo $params->get('moduleclass_sfx'); ?>" id="vmCartModule">

        <?php if ($show_product_list) :?> 
        <?php $valTotal =  explode(" ",$data->billTotal); ?>
        <div class="dropdown">
            <span class="view_cart_link " data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                <i class="fa fa-suitcase"></i> <b  class="total_products"><?php echo  $data->totalProduct;?> </b><span> <?php echo Jtext::_('TPL_ITEMS');?> - <?php echo $valTotal[1];?></span> <i class="fa fa-angle-down"></i>
            </span>
            <?php if ( count($data->products) ) {?>
            
            <div class="dropdown-menu dropdown-menu-right shop-card" role="menu">
                <div class="divsubmenu ModuleMiniSidebar mini-sidebar  moduleMiniCart">
                    <div class="vm_cart_products">
                        <div class="vmcontainer">

                            <?php foreach ($data->products as $pkey=>$product):?> 
                            <div class="product_row clearfix shop-card-products">

                                <?php if (isset($product["image"]) && !empty($product["image"])) : ?>
                                    <?php $img = $product["image"]?>
                                    <div class="blogThumbnail">
                                        <div class="image"><?php echo $img?></div>
                                    </div>
                                <?php endif; ?>
                                
                                
                                <div class="ItemBody shop-card-products-features">
                                    <form data-miniaction="rcart" method="post">  
                                            <button class="vmicon vmicon vm2-remove_from_cart">
                                                <i class="fa fa-times"></i>
                                            </button>                             
                                        <input type='hidden' class="remove" name="quantity[<?php echo $pkey?>]" value="0">
                                        <input type='hidden' name='method' value='updateCart'/> 
                                        <input type='hidden' name='module' value='<?php echo $nmod ?>'/>
                                        <input type='hidden' name='option' value='com_ajax'/>
                                        <input type='hidden' name='format' value='json'/>  
                                    </form>
                                    <h5 class="moduleItemTitle product_name"><?php echo  $product['product_name'] ?></h5>
                                    <div class="priceWrap">
                                        <?php if ($show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) : ?>
                                            <span class="pull-right " ><span class="subtotal_with_tax value PricesalesPrice"><?php echo $product['subtotal_with_tax'] ?></span></span>
                                        <?php endif; ?>
                                        <div class="quantity value"><?php echo Jtext::_('TPL_QUANTITY');?>: <?php echo  $product['quantity'] ?></div> 
                                        
                                        <?php if ( !empty($product['customProductData']) ) : ?>
                                            <div class="customProductData"><?php echo $product['customProductData'] ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach;?> 
                                                                      
                        </div>
                        <div class="product_row clearfix shop-card-products">
                            <div class="gray-border"></div>
                        </div> 
                    </div>
                           
                    <div class="module-topcart clearfix">
                        <h6 class="total pull-left">
                            <?php echo $data->billTotal; ?>
                        </h6>
                        
                        <div class="show_cart cartProceed pull-right">
                            <?php echo  $data->cart_show; ?>
                        </div>
                        
                    </div><!-- end div.module-topcart -->
                                                     
                </div><!-- end div.divsubmenu -->
            </div>
            <?php } else { echo '<span class="vm-empty"><i class="fa fa-shopping-cart"></i>&nbsp;'.Jtext::_('TPL_VM_CART_EMPTY').'</span>';}?>
        </div>
        
        <?php endif; ?>
        
	    <div class="payments_signin_button"></div>
        
        <noscript>
        <?php echo vmText::_('MOD_VIRTUEMART_CART_AJAX_CART_PLZ_JAVASCRIPT') ?>
        </noscript>                                     
</div>

