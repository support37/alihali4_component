<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>
<div class="photo-news row">
	<?php if(count($items)):?>
	<?php foreach ($items as $key=>$item):	?>
	<?php if ($key == 0 ):?>
	<div class="big col-xs-12">
		<div class="post-2 zoom">
			<?php if($params->get('itemImage') && isset($item->image)): ?>
			<div class="zoom">
				<img alt="<?php echo $item->title; ?>" src="<?php echo $item->image; ?>">
			</div>
			<?php endif; ?>
			<?php if($params->get('itemDateCreated')): ?>
			<h5>
			      <?php echo JHTML::_('date', $item->created, "d M Y"); ?>
			</h5>
			<?php endif; ?>
			<?php if($params->get('itemTitle')): ?>
			<h3>
	      		<a href="<?php echo $item->link; ?>" title="<?php echo $item->title; ?>"><?php echo $item->title; ?></a>
			</h3>
			<?php endif; ?>
			<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>	
				<?php if(!empty($item->event->K2CommentsCounter)): ?>
					<!-- K2 Plugins: K2CommentsCounter -->
					<?php echo $item->event->K2CommentsCounter; ?>
				<?php else: ?>
					<?php if($item->numOfComments>0): ?>
					<a class="comment c-pointer" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<i class="fa fa-comments"></i>&nbsp;<?php echo $item->numOfComments; ?>
					</a>
					<?php else: ?>
					<a class="comment c-pointer" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<i class="fa fa-comments"></i>&nbsp;0
					</a>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
	<?php endif; ?>
	<?php endforeach; ?>

	<?php foreach ($items as $key=>$item):	?>
	<?php if ($key > 0 ):?>
	<div class="small col-xs-6">
		<div class="post-2 zoom">
			<?php if($params->get('itemImage') && isset($item->image)): ?>
			<div class="zoom">
				<a href="<?php echo $item->link; ?>" title="<?php echo $item->title; ?>"><img alt="<?php echo $item->title; ?>" src="<?php echo $item->image; ?>"></a>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<?php endif; ?>
	<?php endforeach; ?>

	<?php endif; ?>
</div>