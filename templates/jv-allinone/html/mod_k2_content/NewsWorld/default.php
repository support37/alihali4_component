<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>
<div class="row">
	<div id="owl-demo-7" class="owl-demo-7 owl-carousel col-xs-small none cursor-move">
		<?php if(count($items)):?>
		<?php foreach ($items as $key=>$item):	?>
	    <div class="news-item col-md-12">
	    	<?php if($params->get('itemDateCreated')): ?>
			<h5>
			      <?php echo JHTML::_('date', $item->created, "d M Y"); ?>
			</h5>
			<?php endif; ?>
	    	<?php if($params->get('itemImage') && isset($item->image)): ?>
				<img alt="<?php echo $item->title; ?>" src="<?php echo $item->image; ?>">
			<?php endif; ?>
	    	<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>	
				<?php if(!empty($item->event->K2CommentsCounter)): ?>
					<!-- K2 Plugins: K2CommentsCounter -->
					<?php echo $item->event->K2CommentsCounter; ?>
				<?php else: ?>
					<?php if($item->numOfComments>0): ?>
					<a class="comment" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<i class="fa fa-comments"></i>&nbsp;<?php echo $item->numOfComments; ?>
					</a>
					<?php else: ?>
					<a class="comment" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
						<i class="fa fa-comments"></i>&nbsp;0
					</a>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
	    	<?php if($params->get('itemTitle')): ?>
			<h3>
	      		<a href="<?php echo $item->link; ?>" title="<?php echo $item->title; ?>"><?php echo $item->title; ?></a>
			</h3>
			<?php endif; ?>
	    	<div>
				<?php if($params->get('itemIntroText')): ?>
				<?php echo $item->introtext; ?>
				<?php endif; ?>
			</div>

		</div>
		
		<?php endforeach; ?>
		<?php endif; ?>

    </div>
</div>
