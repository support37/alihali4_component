<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;
?>
<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">
	<?php if($params->get('itemPreText')): ?>
	<p class="modulePretext"><?php echo $params->get('itemPreText'); ?></p>
	<?php endif; ?>
	<?php if(count($items)): ?>
    <?php foreach ($items as $key=>$item):	?>
    <div>
		<a class="pop-products" href="<?php echo $item->link; ?>">
			<?php if($params->get('itemImage') && isset($item->image)): ?>
			<span class="pop-products-img" style="background-image: url(<?php echo $item->image; ?>)">
				<img src="templates/jv-allinone/images/img1x1.png" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>">
			</span>
			<?php endif; ?>
			<span class="pop-products-description blog-pop-products-description">
				<?php if($params->get('itemTitle')): ?>
		     <?php echo $item->title; ?>
		      <?php endif; ?>
				<br>	
				<span class="pop-products-price">
					<?php if($params->get('itemDateCreated')): ?>
			   		<?php echo JHTML::_('date', $item->created, JText::_('d M Y')); ?>
			      <?php endif; ?>
				</span>
			</span>
		</a>
    </div>
    <div class="bottom-border"></div>
    <?php endforeach; ?>
  <?php endif; ?>
</div>
