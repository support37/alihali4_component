<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>
<div class="features-post">
	<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?> container">
		<div class="row">
			

			<?php if(count($items)): ?>
				<div class="col-md-12 col-sm-12 col-xs-12 post-slider">
		  			<div id="owl-demo-9" class="owl-demo-9 owl-carousel cursor-move">
					    <?php foreach ($items as $key=>$item):	?>
					    <?php if ($key < 4):?>
					    <div class="post-slide">
					      	<?php if($params->get('itemImage') && isset($item->image)): ?>
							<img alt="<?php echo $item->title; ?>" src="<?php echo $item->image; ?>">
							<?php endif; ?>
							<?php if($params->get('itemTitle')): ?>
							<a href="<?php echo $item->link; ?>" class="post-slide-header upp">
								<?php 
								$string = $item->title; 
								$out = wordwrap($string, 30, "</span><br><span>"); ?>
								<span>
									<?php echo $out; ?>
								</span>
							</a>
							<?php endif; ?>
							<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>	
								<?php if(!empty($item->event->K2CommentsCounter)): ?>
									<!-- K2 Plugins: K2CommentsCounter -->
									<?php echo $item->event->K2CommentsCounter; ?>
								<?php else: ?>
									<?php if($item->numOfComments>0): ?>
									<a class="comment c-pointer" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
										<i class="fa fa-comments"></i>&nbsp;<?php echo $item->numOfComments; ?>
									</a>
									<?php else: ?>
									<a class="comment c-pointer" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
										<i class="fa fa-comments"></i>&nbsp;0
									</a>
									<?php endif; ?>
								<?php endif; ?>
							<?php endif; ?>
					    </div>
					    <?php endif; ?>
					    <?php endforeach; ?>
			  		</div>
			  	</div>
		  		<!-- End  post-slider -->
		  		<div class="col-md-12 col-sm-12 col-xs-12 posts-container">
					<div class="row">
						<?php foreach ($items as $key=>$item):	?>
							<?php if ($key == 4):?>
							<div class="col-md-12">
								<div class="post-1">
									<?php if($params->get('itemImage') && isset($item->image)): ?>
									<div class="zoom"><img alt="<?php echo $item->title; ?>" src="<?php echo $item->image; ?>"></div>
									<?php endif; ?>
									<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>	
										<?php if(!empty($item->event->K2CommentsCounter)): ?>
											<!-- K2 Plugins: K2CommentsCounter -->
											<?php echo $item->event->K2CommentsCounter; ?>
										<?php else: ?>
											<?php if($item->numOfComments>0): ?>
											<a class="comment c-pointer" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
												<i class="fa fa-comments"></i>&nbsp;<?php echo $item->numOfComments; ?>
											</a>
											<?php else: ?>
											<a class="comment c-pointer" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
												<i class="fa fa-comments"></i>&nbsp;0
											</a>
											<?php endif; ?>
										<?php endif; ?>
									<?php endif; ?>
									<?php if($params->get('itemDateCreated')): ?>
									<h5>
									    <?php echo JHTML::_('date', $item->created, "d M Y"); ?>
									</h5>
									<?php endif; ?>
									<?php if($params->get('itemTitle')): ?>
									<h3>
							      		<a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
									</h3>
									<?php endif; ?>
								</div>
							</div>
							<?php endif; ?>
							<?php if ($key > 4):?>
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="post-2 zoom">
									<?php if($params->get('itemImage') && isset($item->image)): ?>
									<div class="zoom"><img alt="<?php echo $item->title; ?>" src="<?php echo $item->image; ?>"></div>
									<?php endif; ?>
									<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>	
										<?php if(!empty($item->event->K2CommentsCounter)): ?>
											<!-- K2 Plugins: K2CommentsCounter -->
											<?php echo $item->event->K2CommentsCounter; ?>
										<?php else: ?>
											<?php if($item->numOfComments>0): ?>
											<a class="comment c-pointer" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
												<i class="fa fa-comments"></i>&nbsp;<?php echo $item->numOfComments; ?>
											</a>
											<?php else: ?>
											<a class="comment c-pointer" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
												<i class="fa fa-comments"></i>&nbsp;0
											</a>
											<?php endif; ?>
										<?php endif; ?>
									<?php endif; ?>
									<?php if($params->get('itemDateCreated')): ?>
									<h5 class="date">
									      <?php echo JHTML::_('date', $item->created, "d M Y"); ?>
									</h5>
									<?php endif; ?>
									<?php if($params->get('itemTitle')): ?>
									<h5>
							      			<a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
									</h5>
									<?php endif; ?>
								</div>
							</div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
				<!-- End posts-container -->
		  <?php endif; ?>
		</div>
	</div>
</div>