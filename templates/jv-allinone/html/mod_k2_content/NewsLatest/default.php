<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>
<div class="latest-news">
<?php if(count($items)):?>	
	<?php foreach ($items as $key=>$item):	?>
	<div class="la-rev">
		<?php if($params->get('itemImage') && isset($item->image)): ?>
		<div class="img-revievs">
			<img alt="<?php echo $item->title; ?>" src="<?php echo $item->image; ?>">
		</div>
		<?php endif; ?>
		
		<?php if($params->get('itemTitle')): ?>
		<h3>
      			<a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
		</h3>
		<?php endif; ?>


		<?php
		$database = JFactory::getDBO();
		$query = 'SELECT pc.*  FROM #__k2_rating as pc  WHERE pc.itemID = '.$item->id;
		$database->setQuery($query);
		$k2_ratings = $database->loadObjectList();
		$ratings = array();
		$ra = 0;
		if($k2_ratings) {foreach ($k2_ratings as $rating) $ratings[] = $rating->rating_sum;}
		if (implode($ratings) != "") {$ra = implode($ratings);}
        $VotesPercentage = ($ra/5)*100;
		?>
		<div class="rating" itemtype="http://schema.org/Review" itemscope="">
			<ul class="itemRatingList">
				<li class="itemCurrentRating" id="itemCurrentRating<?php echo $item->id; ?>" style="width:<?php echo $VotesPercentage; ?>%;"></li>
				<li><a data-id="<?php echo $item->id; ?>" title="<?php echo JText::_('K2_1_STAR_OUT_OF_5'); ?>" class="one-star">1</a></li>
				<li><a data-id="<?php echo $item->id; ?>" title="<?php echo JText::_('K2_2_STARS_OUT_OF_5'); ?>" class="two-stars">2</a></li>
				<li><a data-id="<?php echo $item->id; ?>" title="<?php echo JText::_('K2_3_STARS_OUT_OF_5'); ?>" class="three-stars">3</a></li>
				<li><a data-id="<?php echo $item->id; ?>" title="<?php echo JText::_('K2_4_STARS_OUT_OF_5'); ?>" class="four-stars">4</a></li>
				<li><a data-id="<?php echo $item->id; ?>" title="<?php echo JText::_('K2_5_STARS_OUT_OF_5'); ?>" class="five-stars">5</a></li>
			</ul>
			<?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>	
				<?php if(!empty($item->event->K2CommentsCounter)): ?>
					<!-- K2 Plugins: K2CommentsCounter -->
					<?php echo $item->event->K2CommentsCounter; ?>
				<?php else: ?>
					<?php if($item->numOfComments>0): ?>
					<span>
						&nbsp;<?php echo $item->numOfComments; ?> <?php echo JText::_('TPL_REVIEW_S'); ?>
					</span>
					<?php else: ?>
					<span >
						&nbsp;0 <?php echo JText::_('TPL_REVIEW_S'); ?>
					</span>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
		</div>

	</div>
	<?php endforeach; ?>
<?php endif; ?>

</div>
