<?php
/**
 # Module		JV Contact
 # @version		3.0.1
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright © 2008-2012 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/

// No direct access to this file
defined( '_JEXEC' ) or die( 'Restricted access' );

?>

<!-- design -->
<div class="mapsInfo">

<?php echo @$myparams['captcha'][1];?>

	
<?php echo $myparams['social'];?>
<?php echo $myparams['map'];?>

<?php if($myparams['textheader']){?>
<div class="textheader titleform"><?php echo $myparams['textheader'];?></div>
<?php }?>

<?php if($myparams['moreinfo']){?>
<div class="moreinfo"><?php echo $myparams['moreinfo'];?></div>
<?php }?>

   
    
   <?php echo $myparams['captcha'][0];?>
    <?php echo JHTML::_('form.token');?>
    



</div>
