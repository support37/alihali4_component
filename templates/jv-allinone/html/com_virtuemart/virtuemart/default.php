<?php
/**
*
* Description
*
* @package	VirtueMart
* @subpackage
* @author
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: default.php 8507 2014-10-22 16:09:24Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JHtml::_( 'behavior.modal' );
?>
<div class="view-virtuemart">
<?php if ($this->vendor->vendor_store_name) : ?>
	<h1 class="titlePage">
		<?php echo $this->escape($this->vendor->vendor_store_name); ?>
	</h1>
<?php endif; ?>

<?php # Vendor Store Description

if(VmConfig::isSuperVendor() ){
	$add_product_link = JURI::root() . 'index.php?option=com_virtuemart&tmpl=component&view=product&task=edit&virtuemart_product_id=0&manage=1' ;
	echo '<p><a href="'.$add_product_link.'" title="'.vmText::_( 'COM_VIRTUEMART_PRODUCT_FORM_NEW_PRODUCT' ).'" class="btn btn-default btn-sm up"><i class="gala-pen"></i> '.vmText::_( 'COM_VIRTUEMART_PRODUCT_FORM_NEW_PRODUCT' ).'</a> </p>';
}

if (!empty($this->vendor->vendor_store_desc) and VmConfig::get('show_store_desc', 1)) { ?>
<div class="vendor-store-desc">
	<?php echo $this->vendor->vendor_store_desc; ?>
</div>
<?php } ?>

<?php
# load categories from front_categories if exist
if ($this->categories and VmConfig::get('show_categories', 1)) echo $this->renderVmSubLayout('categories_home',array('categories'=>$this->categories));

# Show template for : topten,Featured, Latest Products if selected in config BE
if (!empty($this->products) ) {
	$products_per_row = VmConfig::get ( 'homepage_products_per_row', 3 ) ;

	$featured_products_rows = VmConfig::get ( 'featured_products_rows', 1 ) ;
	$topTen_products_rows = VmConfig::get ( 'topTen_products_rows', 1 ) ;
	$recent_products_rows = VmConfig::get ( 'recent_products_rows', 1 ) ;
	$latest_products_rows = VmConfig::get ( 'latest_products_rows', 1 ) ;

	echo $this->renderVmSubLayout('products_home',
		array(
			'products'=>$this->products,
			'currency'=>$this->currency,
			'products_per_row'=>$products_per_row,
			'featured_products_rows'=>$featured_products_rows,
			'topten_products_rows'=>$topTen_products_rows,
			'recent_products_rows'=>$recent_products_rows,
			'latest_products_rows'=>$latest_products_rows,
			'showRating'=>$this->showRating
		)
	); //$this->loadTemplate('products');
}

?> <?php vmTime('vm view Finished task ','Start'); ?>
</div>