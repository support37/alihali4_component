<?php
/**
 *
 * Show the product details page
 *
 * @package	VirtueMart
 * @subpackage
 * @author Max Milbers, Valerie Isaksen

 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default_images.php 8508 2014-10-22 18:57:14Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
	$imageJS = '
	jQuery(document).ready(function() {
		Virtuemart.updateImageEventListeners();
	});
	Virtuemart.updateImageEventListeners = function() {
		jQuery("a[rel^=\'prettyPhoto\']").prettyPhoto({
			show_title: false,
			social_tools: false
		});
		jQuery(".vmFullImage").each(function(){
			var el = jQuery(this);
			el.owlCarousel({
				direction : jQuery("body").hasClass( "rtl" )?\'rtl\':\'ltr\',
				singleItem : true,
				navigation: true,
				pagination:false,
				afterAction : function(){
					var product = this.$elem.data("product"),
						current = this.currentItem
					;
					jQuery(".additional-images-wrapper")
					.find(".additionalItem")
					.removeClass("active")
					.eq(current)
					.addClass("active");
				}
		    });	
			el.find(".zoom-image").zoom();
		});
		jQuery(".additional-images-wrapper").on("click", ".additionalItem", function(e){console.log(\'lo\');
			e.preventDefault();
			el = jQuery(this);
			var number = el.index();
			jQuery(".vmFullImage").trigger("owl.goTo",number);
		});
		jQuery(".position-breadcrumb").find(".titlePage").html(jQuery(".productdetails-view .product-name").html());
		jQuery(".productdetails-view .product-name").remove();
		jQuery(".clear-selection").click(function(){
		});

		jQuery(\'.listing-related-carousel\').each(function(){
			var el = jQuery(this);
			el.owlCarousel({
				direction : jQuery("body").hasClass( "rtl" )?\'rtl\':\'ltr\',
			    items : el.data(\'items\'),
			    itemsDesktop : [1199,4],
				itemsDesktopSmall : [992,3],
				itemsTablet : [768,2],
				itemsTabletSmall: [450,2],
			    navigation : true,
			    navigationText : ["",""],
			    pagination: false
		    });	
		});
		jQuery(".listing-related-carousel .product-field-display a").each(function(){
			var 
				This = jQuery(this), 
				img = This.find(\'img\'),
				text = jQuery(\'<span>\').append(This.text())
			;
			This.empty().append([img,text]);
		});
		jQuery(\'.shareItem\').cSButtons();
	}
	';
vmJsApi::addJScript('imagepopup',$imageJS);
?>

<?php
$app = JFactory::getApplication();
$templateDir = JURI::base() . 'templates/' . $app->getTemplate();
$database = JFactory::getDBO();
$mediaModel = VmModel::getModel ('media');

$slideNumber =rand (0,1000);

$q = 'SELECT m.* FROM #__virtuemart_product_medias as m  WHERE m.virtuemart_product_id = '.$this->product->virtuemart_product_id;
$database->setQuery($q);
$product_media = $database->loadObjectList();
?>

<div class="vmFullImage" data-product="<?php echo $slideNumber?>">
<?php
	if($product_media) {
		foreach ($product_media as $media){
			$image = $mediaModel->createMediaByIds($media->virtuemart_media_id); ?>
			<span  class="vm-product-media-container-a zoom-image">
				<img src="<?php echo $image[0]->file_url; ?>" alt="<?php echo $image[0]->file_title; ?>" >
				<a class="zoom-item" href="<?php echo $image[0]->file_url; ?>" rel="prettyPhoto[gallery]"></a>
			</span>
			<?php
		}			
    } else {
    	?>
    	<span class="vm-product-media-container-a">
    		<img src="<?php echo $templateDir.'/images/noimage.gif'?>" alt="<?php echo $product->product_name ?>" >
    	</span>
    	<?php
    }
?>
</div>
<div class="bottom-border"></div>
