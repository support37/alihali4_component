<?php
/**
 *
 * Show the product details page
 *
 * @package	VirtueMart
 * @subpackage
 * @author Max Milbers, Eugen Stranz, Max Galt
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2014 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 8508 2014-10-22 18:57:14Z Milbo $ 
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
$doc = JFactory::getDocument();
$doc->addScript(JUri::root(true)."/templates/jv-allinone/js/jquery.csbuttons.min.js");

/* Let's see if we found the product */
if (empty($this->product)) {
	echo vmText::_('COM_VIRTUEMART_PRODUCT_NOT_FOUND');
	echo '<br /><br />  ' . $this->continue_link_html;
	return;
}

echo shopFunctionsF::renderVmSubLayout('askrecomjs',array('product'=>$this->product));

vmJsApi::jDynUpdate();
vmJsApi::addJScript('updDynamicListeners',"
jQuery(document).ready(function() { // GALT: Start listening for dynamic content update.
	// If template is aware of dynamic update and provided a variable let's
	// set-up the event listeners.
	if (Virtuemart.container)
		Virtuemart.updateDynamicUpdateListeners();
}); ");

if(vRequest::getInt('print',false)){ ?>
<body onload="javascript:print();">
<?php } ?>

<!-- <pre> -->
	<?php //echo print_r($doc);?>
<!-- </pre> -->

<div class="productdetails-view productdetails shop">

	<?php
	// Product Navigation
	if (VmConfig::get('product_navigation', 1)) {
	?>
	<div class="product-neighbours clearfix">
		<?php
		if (!empty($this->product->neighbours ['previous'][0])) {
		$prev_link = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->neighbours ['previous'][0] ['virtuemart_product_id'] . '&virtuemart_category_id=' . $this->product->virtuemart_category_id, FALSE);
		echo JHtml::_('link', $prev_link, '<i class="fa fa-arrow-circle-left"></i>&nbsp;'. $this->product->neighbours ['previous'][0]
		['product_name'], array('rel'=>'prev', 'class' => 'previous-page pull-left','data-dynamic-update' => '1'));
		}
		if (!empty($this->product->neighbours ['next'][0])) {
		$next_link = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $this->product->neighbours ['next'][0] ['virtuemart_product_id'] . '&virtuemart_category_id=' . $this->product->virtuemart_category_id, FALSE);
		echo JHtml::_('link', $next_link, $this->product->neighbours ['next'][0] ['product_name'].'&nbsp;<i class="fa fa-arrow-circle-right"></i>', array('rel'=>'next','class' => 'next-page pull-right','data-dynamic-update' => '1'));
		}
		?>
	</div>
	<?php } // Product Navigation END?>

	<!-- Detail Info -->
		<div class="shop shop-single">
		<div class="row">
			<div class="col-md-4">
				<div class="vm-product-image-container">
					<?php
					echo $this->loadTemplate('images');				
					if (count ($this->product->images) > 1) {
						echo $this->loadTemplate('images_additional');
					}
					?>
					<?php echo shopFunctionsF::renderVmSubLayout('badges',array('product'=>$this->product)); ?>
				</div>
			</div>
			<!-- and shop-content-item-container -->
			<div class="col-md-5">
				<?php // Product Title   ?>
				<h1 class="product-name clearfix titlePage"><?php echo $this->product->product_name ?></h1>
				<?php // Product Title END   ?>

				<?php // afterDisplayTitle Event
				echo $this->product->event->afterDisplayTitle ?>
				<?php
				// PDF - Print - Email Icon
				if (VmConfig::get('show_emailfriend') || VmConfig::get('show_printicon') || VmConfig::get('pdf_icon') || !empty($this->edit_link)) {
				?>
				<!-- Single button -->
				<div class="btn-group pull-right product-tools">
					<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-cog"></i>
					</button>
					<ul class="dropdown-menu" role="menu">
						
						<?php
						// Product Edit Link
						if (!empty($this->edit_link)) {
							echo '<li>';
							echo $this->edit_link;
							echo "</li>";
						}
						
						?>
						<?php
						$link = 'index.php?tmpl=component&option=com_virtuemart&amp;view=productdetails&amp;virtuemart_product_id=' . $this->product->virtuemart_product_id;
						$MailLink = 'index.php?option=com_virtuemart&amp;view=productdetails&amp;task=recommend&amp;virtuemart_product_id=' . $this->product->virtuemart_product_id . '&amp;virtuemart_category_id=' . $this->product->virtuemart_category_id . '&amp;tmpl=component';
						if (VmConfig::get('pdf_icon')) {
							echo '<li>';
							echo $this->linkIcon($link . '&amp;format=pdf', 'COM_VIRTUEMART_PDF', 'pdf_button', 'pdf_icon', false);
							echo "</li>";
						}

						if (VmConfig::get('show_printicon')) {
							echo '<li>';
							echo $this->linkIcon($link . '&amp;print=1', 'COM_VIRTUEMART_PRINT', 'printButton', 'show_printicon',false,true,false,'class="printModal"');
							echo "</li>";
						}
						if (VmConfig::get('show_emailfriend')) {
							echo '<li>';
							echo $this->linkIcon($MailLink, 'COM_VIRTUEMART_EMAIL', 'emailButton', 'show_emailfriend', false,true,false,'class="recommened-to-friend"');
							echo "</li>";
						}
						?>
					</ul>
				</div>
				<?php } // PDF - Print - Email Icon END

				echo shopFunctionsF::renderVmSubLayout('prices_mini',array('product'=>$this->product,'currency'=>$this->currency));
				?>
				<span class="gray-italic"><span class="vm-sku"><?php echo JText::_('TPL_SKU').'</span> '.$this->product->product_sku;?></span>
				<div class="vm-product-rating-container text-left">
					<?php echo shopFunctionsF::renderVmSubLayout('rating',array('showRating'=>$this->showRating,'product'=>$this->product)); ?>
				</div>

				<?php
				// Product Short Description
				if (!empty($this->product->product_s_desc)) {
				?>
				<div class="product-short-description">
				<?php
				/** @todo Test if content plugins modify the product description */
				echo nl2br($this->product->product_s_desc);
				?>
				</div>
				<?php
				} // Product Short Description END
				?>
				<?php
				// TODO in Multi-Vendor not needed at the moment and just would lead to confusion
				/* $link = JRoute::_('index2.php?option=com_virtuemart&view=virtuemart&task=vendorinfo&virtuemart_vendor_id='.$this->product->virtuemart_vendor_id);
				$text = vmText::_('COM_VIRTUEMART_VENDOR_FORM_INFO_LBL');
				echo '<span class="bold">'. vmText::_('COM_VIRTUEMART_PRODUCT_DETAILS_VENDOR_LBL'). '</span>'; ?><a class="modal" href="<?php echo $link ?>"><?php echo $text ?></a><br />
				*/
				?>
				<div class="clearfix"></div>
				<div>
				<?php
				
				if (is_array($this->productDisplayShipments)) {
					foreach ($this->productDisplayShipments as $productDisplayShipment) {
						if (!empty($productDisplayShipment)) {
							echo $productDisplayShipment . '<br />';
						}
					}
				}
				if (is_array($this->productDisplayPayments)) {
					foreach ($this->productDisplayPayments as $productDisplayPayment) {
						if (!empty($productDisplayShipment)) {
							echo $productDisplayPayment . '<br />';
							echo '<br/>';
						}
					}
				}
				?>
				</div>
				<?php

				//In case you are not happy using everywhere the same price display fromat, just create your own layout
				//in override /html/fields and use as first parameter the name of your file
				
				echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$this->product));

				echo shopFunctionsF::renderVmSubLayout('stockhandle',array('product'=>$this->product));
				?>
				<span class="gray-border"></span>
				<span class="gray-italic">
					<span class="vm-categories">
					<?php echo vmText::_('COM_VIRTUEMART_CATEGORIES').':' ?>
					</span>
					<?php
					$categoryModel = VmModel::getModel ('category');
					$categoryArray = array();
					foreach ($this->product->categories as $catid) :
						$categoryArray[] = $categoryModel->getCategory ($catid, true);
					endforeach;
					$catArray = array();
					foreach ($categoryArray as $category) :
						$catArray[] = '<a href="'.JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$category->virtuemart_category_id, FALSE).'" class="product-details" title="'. $category->category_name .'">'. $category->category_name .'</a>'; // <- STYLE HOWEVER YOU WANT
					endforeach;
					echo implode(", ", $catArray).'.';
					?>
				</span>
			</div>
			<div class="col-md-3 shop-sidebar">
				<span class="title">Share</span>

				<ul class="share">
					<?php
						if (!empty($this->product->images[0])) {
							$image = JUri::root().'/'.$this->product->images[0]->file_url;
						} else {
							$image = '';
						}
					?>
					<li><a href="javascript:" class="shareItem" data-type="facebook" data-url="<?php echo $doc->base;?>" data-txt="<?php echo $doc->title;?>"  data-media="<?php echo $image;?>"><i class="fa fa-facebook"></i></a></li>
					<li><a href="javascript:" class="shareItem" data-type="pinterest" data-url="<?php echo $doc->base;?>" data-txt="<?php echo $doc->title;?>" data-media="<?php echo $image;?>"><i class="fa fa-pinterest"></i></a></li>
					<li><a href="javascript:" class="shareItem" data-type="google" data-url="<?php echo $doc->base;?>" data-txt="<?php echo $doc->title;?>"  data-media="<?php echo $image;?>"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="javascript:" class="shareItem" data-type="linkedin" data-url="<?php echo $doc->base;?>" data-txt="<?php echo $doc->title;?>"  data-media="<?php echo $image;?>"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="javascript:" class="shareItem" data-type="twitter" data-url="<?php echo $doc->base;?>" data-txt="<?php echo $doc->title;?>" data-media="<?php echo $image;?>"><i class="fa fa-twitter"></i></a></li>
				</ul>

				<?php
				// Manufacturer of the Product
				if (VmConfig::get('show_manufacturers', 1) && !empty($this->product->virtuemart_manufacturer_id)) {
					echo $this->loadTemplate('manufacturer');
				}
				?>
				<!-- Our services -->
				<div class="our-services">
					<span class="title"><?php echo JText::_('TPL_ITEM_PRODUCT_SERVICES'); ?></span>
					<?php
					$document = JFactory::getDocument();
					$renderer = $document->loadRenderer('modules');
					$position = "services";
					$options = array('style' => 'raw');
					echo $renderer->render($position, $options, null);
					?>
					
				</div>
			</div>
		</div>
	</div>
	<!-- End shop -->

	<div role="tabpanel" class="tabpanel tabbable our-servise-content">

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<?php if (!empty($this->product->product_desc)) { ?>
			<li role="presentation" class="active"><a href="#productDesc"  role="tab" data-toggle="tab"><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_DESC_TITLE') ?></a></li>
			<li class="border">.</li>
			<?php } ?>
			<li role="presentation" <?php echo (empty($this->product->product_desc))?'class="active"':'';  ?> ><a href="#proSpec" role="tab" data-toggle="tab"><?php echo JText::_('TPL_ITEM_PRODUCT_ADDITIONAL'); ?></a></li>
			<li class="border">.</li>
			<li role="presentation"><a href="#proReviews" role="tab" data-toggle="tab"><?php echo vmText::_ ('COM_VIRTUEMART_REVIEWS') ?>&nbsp;<span class="revievs">(<?php echo count($this->rating_reviews);?>)</span></a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
			<!-- Product Description -->
			<?php if (!empty($this->product->product_desc)) { ?>
			<div role="tabpanel" class="tab-pane active" id="productDesc">
				<?php echo $this->product->product_desc; ?>
			</div>
			<?php } ?>
			<!-- End productDesc -->
			<!-- Begin Specification -->
			<div role="tabpanel" class="tab-pane <?php echo (empty($this->product->product_desc))?'active':'';  ?> " id="proSpec">
				<?php
				echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'ontop'));
				// event onContentBeforeDisplay
				echo $this->product->event->beforeDisplayContent;
				echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'normal'));
				// Product Packaging
				$product_packaging = '';
				if ($this->product->product_box) {
				?>
				<div class="product-box">
				<?php
				echo vmText::_('COM_VIRTUEMART_PRODUCT_UNITS_IN_BOX') .$this->product->product_box;
				?>
				</div>
				<?php } // Product Packaging END 
				echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'onbot'));
				?>

			</div>
			<!-- End Specification -->
			<div role="tabpanel" class="tab-pane" id="proReviews">
				<?php
					echo $this->loadTemplate('reviews');
				?>
			</div>
		</div>
	</div>

	<?php 

	echo shopFunctionsF::renderVmSubLayout('related_products',array('product'=>$this->product,'position'=>'related_products','class'=> 'product-related-products','customTitle' => true ));
	echo shopFunctionsF::renderVmSubLayout('related_categories',array('product'=>$this->product,'position'=>'related_categories','class'=> 'product-related-categories'));
	?>



	<?php // onContentAfterDisplay event
	echo $this->product->event->afterDisplayContent; ?>

	<?php // Show child categories
	if (VmConfig::get('showCategory', 1)) {
	
	echo $this->loadTemplate('showcategory');
	}?>
	<?php
	echo vmJsApi::writeJS();
	?> 

</div>

<script>
	// GALT
	/*
	 * Notice for Template Developers!
	 * Templates must set a Virtuemart.container variable as it takes part in
	 * dynamic content update.
	 * This variable points to a topmost element that holds other content.
	 */
	// If this <script> block goes right after the element itself there is no
	// need in ready() handler, which is much better.
	//jQuery(document).ready(function() {
	Virtuemart.container = jQuery('.productdetails-view');
	Virtuemart.containerSelector = '.productdetails-view';
	//Virtuemart.container = jQuery('.main');
	//Virtuemart.containerSelector = '.main';
	//});
	jQuery(document).ready(function() {
	// Open print and manufacturer link to Modal window
	  <?php if(VmConfig::get('usefancy',1)) : 
	  $manulink = (!empty($this->product->virtuemart_manufacturer_id))?JRoute::_('index.php?option=com_virtuemart&view=manufacturer&virtuemart_manufacturer_id=' . $this->product->virtuemart_manufacturer_id[0] . '&tmpl=component', FALSE):'';
	  ?>
	  jQuery('a.printModal').click(function(e){
		  jQuery.fancybox({
					href: '<?php echo $link.'&print=1'; ?>',
	                type: 'iframe',
	                height: '500'
				  });
                  e.preventDefault();
	  });

	  jQuery('a.manuModal').click(function(e){
		   jQuery.fancybox({
					href: '<?php echo $manulink ?>',
	                type: 'iframe'
				  });
	              e.preventDefault();
	  });
	  
	  <?php else : 
	  $manulink = (!empty($this->product->virtuemart_manufacturer_id))?JRoute::_('index.php?option=com_virtuemart&view=manufacturer&virtuemart_manufacturer_id=' . $this->product->virtuemart_manufacturer_id[0] . '&tmpl=component', FALSE):'';
	  ?>

	  jQuery('a.printModal').click(function(e){
		  jQuery.facebox({
					iframe: '<?php echo $link.'&print=1'; ?>',
					rev: 'iframe|550|550'
				  });
		          e.preventDefault();
	  });

	  jQuery('a.manuModal').click(function(e){
			jQuery.facebox({
					iframe: '<?php echo $manulink; ?>',
					rev: 'iframe|550|550'
					});
			        e.preventDefault();
      });
      
	  <?php endif; ?>	
	  });  	
</script>

