<?php
/**
 *
 * Show the products in a category
 *
 * @package    VirtueMart
 * @subpackage
 * @author RolandD
 * @author Max Milbers
 * @todo add pagination
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 8508 2014-10-22 18:57:14Z Milbo $
 */

defined ('_JEXEC') or die('Restricted access');
JHtml::_ ('behavior.modal');

$js = "
jQuery(document).ready(function () {
	jQuery('.orderlistcontainer').hover(
		function() { jQuery(this).find('.orderlist').stop().show()},
		function() { jQuery(this).find('.orderlist').stop().hide()}
	)
});
";

vmJsApi::addJScript('vm.hover',$js);
$listing = "";
if (isset ($_COOKIE['listing'])) {$listing = $_COOKIE['listing'];}

?>
<div class="view-virtuemart vm-listing">
	<h1 class='titlePage'><?php echo $this->category->category_name; ?></h1>
	<?php
	if (empty($this->keyword) and !empty($this->category) and !empty($this->category->category_description)) {
		?>
		<div class="category_description"><?php echo $this->category->category_description; ?></div>
	<?php
	}
	// Show child categories
	if (VmConfig::get ('showCategory', 1) and empty($this->keyword)) {
		if (!empty($this->category->haschildren)) {
			echo ShopFunctionsF::renderVmSubLayout('categories',array('categories'=>$this->category->children));
		}
	}

	if($this->showproducts){
	?>
	<div class="browse-view listing-view <?php echo $listing; ?>">
		<?php

		if (!empty($this->keyword)) {?>
			<h3><?php echo $this->keyword; ?></h3>

			<form action="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=category&limitstart=0', FALSE); ?>" method="get">

				<!--BEGIN Search Box -->
				<div class="virtuemart_search">
					<?php echo $this->searchcustom ?>
					<br/>
					<?php echo $this->searchCustomValues ?>
					<input name="keyword" class="inputbox" type="text" size="20" value="<?php echo $this->keyword ?>"/>
					<input type="submit" value="<?php echo vmText::_ ('COM_VIRTUEMART_SEARCH') ?>" class="button" onclick="this.form.keyword.focus();"/>
				</div>
				<input type="hidden" name="search" value="true"/>
				<input type="hidden" name="view" value="category"/>
				<input type="hidden" name="option" value="com_virtuemart"/>
				<input type="hidden" name="virtuemart_category_id" value="<?php echo $this->categoryId; ?>"/>

			</form>
			<!-- End Search Box -->
		<?php  } ?>

		<?php // Show child categories

			?>
		<div class="orderby-displaynumber clearfix">
			<div class="pull-left display-number">
				<span><?php echo $this->vmPagination->getResultsCounter ();?> <?php echo JText::_('TPL_PRODUCTS'); ?></span>
				<br>
				<span>
					<?php echo JText::_('TPL_VIRTUEMART_VIEW_LABEL'); ?>
					<?php
						$catUL = JRoute::_ ( 'index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $this->category->virtuemart_category_id , FALSE);
						$limitFE = VmConfig::get ( 'llimit_init_FE', 12 );
						if ($this->vmPagination->total <= $limitFE) {
						?>
							<span><?php echo JText::_('TPL_VIRTUEMART_VIEW_ALL'); ?></span>
						<?php		
						} else {
							if ($this->vmPagination->total > $limitFE) {
							?>
								<a href="<?php echo $catUL.'/results,1-'.$limitFE*2;?>" title="<?php echo $limitFE*2?>"><?php echo $limitFE*2;?></a>&nbsp;/
							<?php }
							if ($this->vmPagination->total > $limitFE*2) {
							?>
								<a href="<?php echo $catUL.'/results,1-'.$limitFE*3;?>" title="<?php echo $limitFE*3?>"><?php echo $limitFE*3;?></a>&nbsp;/
							<?php }	
							if ($this->vmPagination->total > $limitFE*3) {
							?>
								<a href="<?php echo $catUL.'/results,1-'.$limitFE*4;?>" title="<?php echo $limitFE*4?>"><?php echo $limitFE*4;?></a>&nbsp;/
							<?php }	
							if ($this->vmPagination->total > $limitFE*4) {
							?>
								<a href="<?php echo $catUL.'/results,1-'.$limitFE*5;?>" title="<?php echo $limitFE*5?>"><?php echo $limitFE*5;?></a>&nbsp;/
							<?php }	?>
							<a href="<?php echo $catUL.'/results,1-'.$this->vmPagination->total;?>" title="<?php echo JText::_('TPL_VIRTUEMART_VIEW_ALL'); ?>"><?php echo JText::_('TPL_VIRTUEMART_VIEW_ALL'); ?></a>
						<?php
						}
					?>
				</span>
			</div>
			<div class="pull-left view-layout">
				<a href="javascript:void(0)" title="<?php echo JText::_('TPL_VIRTUEMART_VIEW_GRID'); ?>" class="view-grid <?php echo ($listing)?'':'active'; ?>">
					<span class="inner">
						<span class="i1"></span>
					</span>
					<span class="bottom-border"></span>
				</a>
				<a href="javascript:void(0)" title="<?php echo JText::_('TPL_VIRTUEMART_VIEW_LIST'); ?>" class="view-list <?php echo ($listing)?'active':''; ?>">
					<span class="inner">
						<span class="i1"></span>
						<span class="i2"></span>
					</span>
					<span class="bottom-border"></span>
				</a>
			</div>
			<div class="pull-right vm-order-list">
				<?php echo $this->orderByList['orderby']; ?>
				<?php echo $this->orderByList['manufacturer']; ?>
			</div>
			<!-- <div class="toggleTools pull-right" data-icon="&#xe021;"><i class="gala-down-open-big"></i></div> -->
		</div>
		<!-- end of orderby-displaynumber -->



			<?php
			if (!empty($this->products)) {
			$products = array();
			$products[0] = $this->products;
			echo shopFunctionsF::renderVmSubLayout($this->productsLayout,array('products'=>$products,'currency'=>$this->currency,'products_per_row'=>$this->perRow,'showRating'=>$this->showRating));

			?>

		<div class="vm-pagination pagination"><?php echo $this->vmPagination->getPagesLinks (); ?></div>

			<?php
		} elseif (!empty($this->keyword)) {
			echo vmText::_ ('COM_VIRTUEMART_NO_RESULT') . ($this->keyword ? ' : (' . $this->keyword . ')' : '');
		}
		?>
	</div>
	<?php } ?>
	<!-- end browse-view -->
</div>
<!-- End vm-listing -->