<?php 
/**
 *
 * Show the product rating/review
 *
 * @package    VirtueMart
 * @subpackage
 * @author Truong Nguyen
 * @link http://www.joomlavi.net
 * @copyright Copyright (c) 2004 - 2014 Joomlavi Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default_showprices.php 8024 2015-01-12 15:08:59Z Milbo $
 */
// Check to ensure this file is included in Joomla!

defined('_JEXEC') or die('Restricted access');

$product = $viewData['product'];

if ($viewData['showRating']) {
	$maxrating = VmConfig::get('vm_maximum_rating_scale', 5);
	if (empty($product->rating)) {
	?>
		<div class="ratingbox dummy" title="<?php echo vmText::_('COM_VIRTUEMART_UNRATED'); ?>" >

		</div>
	<?php
	} else {
		$ratingwidth = $product->rating * 16;
  ?>

	<div title=" <?php echo (vmText::_("COM_VIRTUEMART_RATING_TITLE") . round($product->rating) . '/' . $maxrating) ?>" class="ratingbox" >
	  <div class="stars-orange" style="width:<?php echo $ratingwidth.'px'; ?>"></div>
	</div>
	<?php
	}
		$ratingsModel = VmModel::getModel ('ratings');
		$rating_reviews = $ratingsModel->getReviewsByProduct($product->virtuemart_product_id);
		$reviews = count($rating_reviews);
	?>
	<span class="countReview"><?php echo $reviews.' '; echo ($reviews > 0)?JText::_('TPL_REVIEWS'):JText::_('TPL_REVIEW')?> </span>
	<div class="clearfix"></div>
	<?php
}