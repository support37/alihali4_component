<?php
/**
 * sublayout products
 *
 * @package	VirtueMart
 * @author Max Milbers
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2014 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL2, see LICENSE.php
 * @version $Id: cart.php 7682 2014-02-26 17:07:20Z Milbo $
 */

defined('_JEXEC') or die('Restricted access');
$products_per_row = $viewData['products_per_row'];
$currency = $viewData['currency'];
$showRating = $viewData['showRating'];
$verticalseparator = " vertical-separator";

foreach ($viewData['products'] as $type => $products ) {

	$rowsHeight = shopFunctionsF::calculateProductRowsHeights($products,$currency,$products_per_row);

	if(!empty($type) and count($products)>0){
		$productTitle = vmText::_('COM_VIRTUEMART_'.strtoupper($type).'_PRODUCT'); ?>
<div class="<?php echo $type ?>-view listing-view products-home">
  <h4><?php echo $productTitle ?></h4>
		<?php // Start the Output
    }
	$BrowseTotalProducts = count($products);
	$col = 1;
	$nb = 1;
	$row = 1;
	?>
	<div class="row">
		<div class="listing-view-carousel" data-items="<?php echo $products_per_row; ?>">
			<?php foreach ( $products as $product ) { ?>
			<div class="product">
				<div class="spacer">
					<div class="vmproductItemImage">
					<?php
						if (!empty($product->images[0])) {
							$image = $product->images[0]->displayMediaThumb ('class="featuredProductImage" border="0"', FALSE);
						} else {
							$image = '';
						}
					
					echo JHTML::_ ('link', JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id), $image, array('title' => $product->product_name));
					?>
					</div>
					<?php //echo shopFunctionsF::renderVmSubLayout('images',array('product'=>$product)); ?>
					<div class="vm-product-body">
						<div class="vm-product-rating-container">
							<?php echo shopFunctionsF::renderVmSubLayout('rating',array('showRating'=>$showRating, 'product'=>$product));?>
						</div>
						<h3 class="vm-product-name"><?php echo JHtml::link ($product->link, $product->product_name); ?></h3>
						<?php echo shopFunctionsF::renderVmSubLayout('category_list',array('product'=>$product)); ?>
						<?php echo shopFunctionsF::renderVmSubLayout('prices_mini',array('product'=>$product,'currency'=>$currency)); ?>
						<?php echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$product,'rowHeights'=>$rowsHeight[$row])); ?>
						<?php echo shopFunctionsF::renderVmSubLayout('badges',array('product'=>$product)); ?>
					</div>
				</div>
				<div class="bottom-border"></div>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php
	

      if(!empty($type)and count($products)>0){
        // Do we need a final closing row tag?
        //if ($col != 1) {
      ?>
  </div>

    <?php
    }
  }
