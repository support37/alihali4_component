<?php
/**
* sublayout products
*
* @package	VirtueMart
* @author Max Milbers
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2014 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL2, see LICENSE.php
* @version $Id: cart.php 7682 2014-02-26 17:07:20Z Milbo $
*/

defined('_JEXEC') or die('Restricted access');

$product = $viewData['product'];
$position = $viewData['position'];
$customTitle = isset($viewData['customTitle'])? $viewData['customTitle']: false;;

if(isset($viewData['class'])){
	$class = $viewData['class'];
} else {
	$class = 'product-fields';
}

if (!empty($product->customfieldsSorted[$position])) {
	?>
	<div class="product-related <?php echo $class?>">
		<h2><?php echo vmText::_('COM_VIRTUEMART_RELATED_CATEGORIES'); ?></h2>
		<?php
		$custom_title = null;
		?>
		<div class="row">
			<div class="listing-related-carousel" data-items="4">
			<?php 
			foreach ($product->customfieldsSorted[$position] as $field) {
				if ( $field->is_hidden ) //OSP http://forum.virtuemart.net/index.php?topic=99320.0
				continue;
				?>
				<!-- <pre> -->
					<?php //echo print_r($field);?>
				<!-- </pre> -->
				<div class="product-field product-field-type-<?php echo $field->field_type ?>">
					<?php 
					if (!empty($field->display)){
						?><div class="product-field-display"><?php echo $field->display ?></div><div class="bottom-border"></div><?php
					}
					?>
				</div>
			<?php
				$custom_title = $field->custom_title;
			} ?>
			</div>
			<!-- End Listing View Carousel -->
		</div>
	</div>
<?php
} ?>