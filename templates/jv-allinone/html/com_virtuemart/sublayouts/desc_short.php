<?php
/**
 *
 * Show Product Short Description
 *
 * @package    VirtueMart
 * @subpackage
 * @author Truong Nguyen
 * @link http://www.joomlavi.net
 * @copyright Copyright (c) 2004 - 2014 Joomlavi Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default_showprices.php 8024 2015-01-18 15:08:59Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined ('_JEXEC') or die('Restricted access');
$product = $viewData['product'];

$database = JFactory::getDBO();
$lang = JFactory::getLanguage();
$langTag = str_replace('-','_',strtolower($lang->getTag()));

$d = 'SELECT d.product_s_desc FROM #__virtuemart_products_'.$langTag.' as d  WHERE d.virtuemart_product_id = '.$product->virtuemart_product_id;
$database->setQuery($d);
$product_s_desc = $database->loadResult ();

?>
<div class="vm-short-description">
<?php
	echo $product_s_desc;
?>
</div>