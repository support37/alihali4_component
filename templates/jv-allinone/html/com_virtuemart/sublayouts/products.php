<?php
/**
 * sublayout products
 *
 * @package	VirtueMart
 * @author Max Milbers
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2014 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL2, see LICENSE.php
 * @version $Id: cart.php 7682 2014-02-26 17:07:20Z Milbo $
 */
JVJSlib::add('jquery.plugins.masonry');
JVJSlib::add('jquery.plugins.imagesloaded'); 
defined('_JEXEC') or die('Restricted access');
$products_per_row = $viewData['products_per_row'];
$currency = $viewData['currency'];
$showRating = $viewData['showRating'];
$idPanel = rand();?>

<script type="text/javascript">
jQuery(function($){
	var $container = $('#productListing_<?php echo $idPanel?>').imagesLoaded(function(){
		$container.masonry({
		  itemSelector: '.vm-col'
		});
	});
	
	// $container.children().each(function(i){
	// 	var item = $(this), info = item.find('.info');
	// 	item.hover(function(){
	// 		item.css('z-index',1000-i);
	// 		info.stop().slideDown(300);
	// 	},function(){
	// 		info.stop().slideUp(300,function(){
	// 			item.css('z-index','');
	// 		});
	// 	});
	// });
});		
</script>

<?php
foreach ($viewData['products'] as $type => $products ) {

	$rowsHeight = shopFunctionsF::calculateProductRowsHeights($products,$currency,$products_per_row);

	if(!empty($type) and count($products)>0){
		$productTitle = vmText::_('COM_VIRTUEMART_'.strtoupper($type).'_PRODUCT'); ?>
<div class="<?php echo $type ?>-view">
	<h4><?php echo $productTitle ?></h4>
	<?php // Start the Output
    }
	// Calculating Products Per Row
	$cols = ' col-xs-6 col-sm-'.floor ( 12 / $products_per_row );

	$BrowseTotalProducts = count($products);

	$col = 1;
	$nb = 1;
	$row = 1;
	?>
	<div class="row" id="productListing_<?php echo $idPanel;?>">
	<?php
	foreach ( $products as $product ) {
	    // Show Products ?>
	    
		<div class="product vm-col<?php echo ' vm-col-' . $products_per_row . $cols;?>  <?php echo ($col == 1)?'clearrow':''; ?>">
			<div class="spacer">
				<div class="vm-image-box">
				<?php echo shopFunctionsF::renderVmSubLayout('images',array('product'=>$product)); ?>
				</div>
				<div class="vm-product-body">
					<div class="vm-product-rating-container">
						<?php echo shopFunctionsF::renderVmSubLayout('rating',array('showRating'=>$showRating, 'product'=>$product));?>
					</div>
					<h3 class="vm-product-name"><?php echo JHtml::link ($product->link, $product->product_name); ?></h3>
					<?php echo shopFunctionsF::renderVmSubLayout('category_list',array('product'=>$product)); ?>
					<?php echo shopFunctionsF::renderVmSubLayout('prices_mini',array('product'=>$product,'currency'=>$currency)); ?>
					<?php echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$product,'rowHeights'=>$rowsHeight[$row])); ?>
					<?php echo shopFunctionsF::renderVmSubLayout('badges',array('product'=>$product)); ?>
				</div>
				<div class="vm-product-body-list">
					<h3 class="vm-product-name"><?php echo JHtml::link ($product->link, $product->product_name); ?></h3>
					<?php echo shopFunctionsF::renderVmSubLayout('category_list',array('product'=>$product)); ?>
					<div class="vm-product-rating-container">
						<?php echo shopFunctionsF::renderVmSubLayout('rating',array('showRating'=>$showRating, 'product'=>$product));?>
					</div>
					<?php echo shopFunctionsF::renderVmSubLayout('desc_short',array('product'=>$product)); ?>
					<?php echo shopFunctionsF::renderVmSubLayout('prices_mini',array('product'=>$product,'currency'=>$currency)); ?>
					<?php echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$product,'rowHeights'=>$rowsHeight[$row])); ?>
					<?php echo shopFunctionsF::renderVmSubLayout('badges',array('product'=>$product)); ?>
				</div>

			</div>
			<div class="bottom-border"></div>
		</div>
		<?php
	    $nb ++;
	    // Do we need to close the current row now?
	    if ($col == $products_per_row || $nb>$BrowseTotalProducts) { ?>
	      	<?php
	      	$col = 1;
			$row++;
	    } else {
	      $col ++;
	    }
	}
	?>
	</div>
	<?php
      if(!empty($type)and count($products)>0){
        // Do we need a final closing row tag?
        //if ($col != 1) {
    ?>
    <div class="clearfix"></div>
 </div>
    <?php
    // }
    }
  }
