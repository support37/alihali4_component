<?php
/**
 *
 * Show the category list in product
 *
 * @package    VirtueMart
 * @subpackage
 * @author Truong Nguyen
 * @link http://www.joomlavi.net
 * @copyright Copyright (c) 2004 - 2014 Joomlavi Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default_showprices.php 8024 2014-06-12 15:08:59Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined ('_JEXEC') or die('Restricted access');
$product = $viewData['product'];

$categoryModel = VmModel::getModel ('category');
$categoryArray = array();
foreach ($product->categories as $catid) :
	$categoryArray[] = $categoryModel->getCategory ($catid, true);
endforeach;
?>
<div class="vm-product-category">
	<?php
	$catArray = array();
	foreach ($categoryArray as $category) :
		$catArray[] = '<a href="'.JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$category->virtuemart_category_id, FALSE).'" class="vm-product-category-item" title="'. $category->category_name .'">'. $category->category_name .'</a>'; // <- STYLE HOWEVER YOU WANT
	endforeach;
	echo implode(", ", $catArray);
	?>
</div>