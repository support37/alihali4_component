<?php
/**
 *
 * Show the product badges
 *
 * @package    VirtueMart
 * @subpackage
 * @author Truong Nguyen
 * @link http://www.joomlavi.net
 * @copyright Copyright (c) 2004 - 2014 Joomlavi Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default_showprices.php 8024 2014-06-12 15:08:59Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined ('_JEXEC') or die('Restricted access');
// Badge new
$days = VmConfig::get('latest_products_days');
$product = $viewData['product'];
$cdate = strtotime ($product->created_on) ;
$ndate = strtotime ('now') - ($days * 86400); ?>
<div class="vm-badges item-circle"><?php if ($product->prices['discountAmount']){ // Show Badge on Sale ?>
<div class="b-onsale"><?php echo JText::_('TPL_VM_SALE') ?></div>
<?php } elseif ($product->product_special){ // Show Badge Featured/Hot ?>
<div class="b-featured"><?php echo JText::_('TPL_VM_FEATURED') ?></div>
<?php } elseif ($cdate > $ndate){ // Show Badge New ?>
<div class="b-new"><?php echo JText::_('TPL_VM_NEW') ?></div>
<?php } ?></div>