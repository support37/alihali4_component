<?php
/**
 *
 * Show the product images
 *
 * @package    VirtueMart
 * @subpackage
 * @author Truong Nguyen
 * @link http://www.joomlavi.net
 * @copyright Copyright (c) 2004 - 2014 Joomlavi Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default_showprices.php 8024 2014-06-12 15:08:59Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined ('_JEXEC') or die('Restricted access');
$product = $viewData['product'];
$app = JFactory::getApplication();
$templateDir = JURI::base() . 'templates/' . $app->getTemplate();
$database = JFactory::getDBO();
$mediaModel = VmModel::getModel ('media');

$slideNumber =rand (0,1000);

$q = 'SELECT m.* FROM #__virtuemart_product_medias as m  WHERE m.virtuemart_product_id = '.$product->virtuemart_product_id;
$database->setQuery($q);
$product_media = $database->loadObjectList();
?>

<div class="vm-product-media-container" data-product="<?php echo $slideNumber?>">
<?php
	if($product_media) {
		foreach ($product_media as $media){
			$image = $mediaModel->createMediaByIds($media->virtuemart_media_id); ?>
			<a title="<?php echo $product->product_name ?>" href="<?php echo $product->link; ?>" class="vm-product-media-container-a">
				<img src="<?php echo $image[0]->file_url; ?>" alt="<?php echo $image[0]->file_title; ?>" >
			</a>
			<?php
		}			
    } else {
    	?>
    	<a title="<?php echo $product->product_name ?>" href="<?php echo $product->link; ?>" class="vm-product-media-container-a">
    		<img src="<?php echo $templateDir.'/images/noimage.gif'?>" alt="<?php echo $product->product_name ?>" >
    	</a>
    	<?php
    }
?>
</div>
<?php 
if(count($product_media)>1){
?>
<div class="vm-product-thumb">
	<div class="vm-product-thumb-slide <?php echo (count($product_media) <= 4)?'no-nav':'';?>" data-product="<?php echo $slideNumber;?>">
		<?php
		foreach ($product_media as $media){
				$image = $mediaModel->createMediaByIds($media->virtuemart_media_id);
		?>
		<div class="item">
			<img src="<?php echo $image[0]->file_url; ?>" alt="<?php echo $image[0]->file_title; ?>" >
		</div>
		<?php
		}
		?>
	</div>
</div>
<?php
}