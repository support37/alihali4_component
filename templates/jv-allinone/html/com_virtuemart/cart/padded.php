<?php
/**
*
* Layout for the add to cart popup
*
* @package	VirtueMart
* @subpackage Cart
* @author Max Milbers
*
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2013 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: cart.php 2551 2010-09-30 18:52:40Z milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>
<div class="popup-added">
	<div class="popup-added-content clearfix">
		<?php
		if($this->products){
			foreach($this->products as $product){
				if($product->quantity>0){
					echo '<h4 class="product-name">'.vmText::sprintf('COM_VIRTUEMART_CART_PRODUCT_ADDED',$product->product_name,'<span class="gala-border-dashed-2">'.$product->quantity.'</span>').'</h4>';
				} else {
					if(!empty($product->errorMsg)){
						echo '<div>'.$product->errorMsg.'</div>';
					}
				}

			}
		}
		echo '<a class="showcart button-green pull-right" href="' . $this->cart_link . '"><i class="gala-shopping82"></i>&nbsp;' . vmText::_('COM_VIRTUEMART_CART_SHOW') . '</a>';
		echo '<a class="continue_link button-green pull-left" href="' . $this->continue_link . '" ><i class="gala-back"></i>' . vmText::_('COM_VIRTUEMART_CONTINUE_SHOPPING') . '</a>';
		
		//if ($this->errorMsg) echo '<div>'.$this->errorMsg.'</div>';
		?>
		<div class="clearfix"></div>
	</div>
	<?php if(VmConfig::get('popup_rel',1)){
		echo '<div class="productdetails-view " style="max-width: 550px;" >';
		//VmConfig::$echoDebug=true;
		if ($this->products and is_array($this->products) and count($this->products)>0 ) {
			$product = reset($this->products);
			//vmdebug('What the hekc',$product->allIds);
		//if($this->products and !empty($this->products[0])){
			$customFieldsModel = VmModel::getModel('customfields');
			$product->customfields = $customFieldsModel->getCustomEmbeddedProductCustomFields($product->allIds,'R');


			/*$fields = array();
			foreach ($this->product->customfields as $field) {
				//
				if($field->field_type=='R') {
					$fields[] = $field;
				}
			}/*/
			$customFieldsModel->displayProductCustomfieldFE($product,$product->customfields);
			if(!empty($product->customfields)){?><div class="product-related">
				<h4><?php echo vmText::_('COM_VIRTUEMART_RELATED_PRODUCTS'); ?></h4>
				<div class="product-related-products-popup row listing-view-carousel">
				<?php
			}
				foreach($product->customfields as $key => $rFields){
					if(!empty($rFields->display)){?>
					<?php echo (($key%4)==0)?'<div class="clearfix"></div>':""; ?>
	                <div class="product-field col-sm-3 item product-field-type-<?php echo $rFields->field_type ?>">
					<span class="product-field-display"><?php echo $rFields->display ?></span>
					</div>
					<?php }
					} ?>
				</div>
			<?php if(!empty($product->customfields)){?><div class="bottom-border"></div>
				<?php
			}?>
			</div>
		<?php
		}
		echo '</div>';
	}?>
</div>