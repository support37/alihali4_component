<?php
$doc = JFactory::getDocument();
$doc->addScript(JUri::root(true)."/modules/mod_jvslidepro/tmpl/custom/js/jquery.themepunch.plugins.min.js");
$doc->addScript(JUri::root(true)."/modules/mod_jvslidepro/tmpl/custom/js/jquery.themepunch.revolution.min.js");
$doc->addStyleSheet(JUri::root(true)."/modules/mod_jvslidepro/tmpl/custom/css/navstylechange.css");
$doc->addStyleSheet(JUri::root(true)."/modules/mod_jvslidepro/tmpl/custom/css/settings.css");
JVJSLib::add('jquery.plugins.imagesloaded');
$moduleid = "customslide_".$module->id;
$doc->addScriptDeclaration("
;jQuery(function($){
	$('#{$moduleid}').imagesLoaded(function(){
	    $(this).revolution({
		    delay:9999,
		    startwidth:1350,
		    startheight:588,
		    hideThumbs:10,
		    fullWidth:'on'
		});
	});
});
");
?>



<div class="jvslidecustom <?php echo $dataConfigs->get('suffix')?>">
    <div class="tp-banner-container">
        <div id="<?php echo $moduleid?>" class=" cursor-move" >
            <ul>

				<?php  
				$i=1;
				foreach($dataImages as $item): $thumb = $item->get('thumb',false); ?>
                
                
                <li  data-transition="fade" data-slotamount="<?php if ($i==1) echo '7'; else echo '3';  ?>" data-masterspeed="<?php if ($i==1) echo '1500'; else echo '1000';  ?>" class="slide-<?php echo $i; ?>">
                <?php 
                    if($item->get('path')){ ?> <img src="<?php echo $item->path;?>" alt="<?php echo $item->title?>"  data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat" /><?php }
                    if($item->get('desc')){ ?> 
                    
                        	<!-- LAYER White Line -->
							<div class="tp-caption customout white-line-t"
								data-x="center"
								data-y="top"
								data-speed="25"
								data-start="0"
								data-easing="Power4.easeOut"
								data-captionhidden="on"
								><div class="line"></div>
							</div>
							<!-- LAYER White Line -->
							<div class="tp-caption customout white-line-b"
								data-x="center"
								data-y="bottom"
								data-speed="25"
								data-start="0"
								data-easing="Power4.easeOut"
								data-captionhidden="on"
								><div class="line"></div>
							</div>
                                           
                    	<?php echo $item->desc?>
                    
                   <?php } 

                ?>
                </li>
                
                <?php 
				$i ++;
				endforeach;
                 ?>
                    


						
			</ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</div>