<?php
/**
 # MOD_JVLATEST_NEWS - JV Latest News
 # @version		3.x
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright (C) 2013 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/

// No direct access to this file 
defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();

JHTML::addIncludePath(JPATH_SITE.'/components/com_content/helpers');
JHTML::_('behavior.framework', true);
if(count($items)){
    
?>
    <div id="jvlatestnews<?php echo $module->id ?>" class="jvlatestnews ffffff <?php echo $template ;?>">
    
    	<?php if ( $params->get('description') != '' ) { ?>
    	<div class="description"><?php echo $params->get('description'); ?></div>
    <?php } ?>
    
        <div class="jvlatestnews-container ">    
            <div class="jvlatestnews-content">

            <?php 
                $list = $items;
                require JModuleHelper::getLayoutPath('mod_jvlatest_news', $template.'/article_lists');
            ?>

            </div>





        </div>




    </div>


<?php } ?>