<?php // no direct access
defined('_JEXEC') or die('Restricted access');
//JHTML::stylesheet ( 'menucss.css', 'modules/mod_virtuemart_category/css/', false );
?>

<ul class="VMmenu menu<?php echo $class_sfx ?>" >
<?php foreach ($categories as $category) {
	$active_menu = '';
	$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$category->virtuemart_category_id);
	$cattext = $category->category_name;

	$productModel = VmModel::getModel ('product');
	
	$productArray = $productModel->getProductsInCategory($category->virtuemart_category_id);
	$newArray = array();
	$days = VmConfig::get('latest_products_days');
	$ndate = strtotime ('now') - ($days * 86400);
	foreach ($productArray as $key => $product) :
		$cdate = strtotime ($product->created_on) ;
		if ($cdate > $ndate){ 
			$newArray[] =$key;
		}
	endforeach;
	$new =  (count($newArray) > 0)?' <span class="hot bc-bg">'.JText::_('TPL_VM_NEW').'</span>':'';
	//if ($active_category_id == $category->virtuemart_category_id) $active_menu = 'class="active"';
	if (in_array( $category->virtuemart_category_id, $parentCategories)) $active_menu = 'class="active"';
	?>
	<li <?php echo $active_menu ?>>
		<?php echo JHTML::link($caturl, $cattext.$new); ?>

		<?php if ($category->childs ) {
		?>
		
		<ul class="menu<?php echo $class_sfx; ?>">
		<?php
			foreach ($category->childs as $child) {
				$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$child->virtuemart_category_id);
				$cattext = $child->category_name;
				?>

				<li>
					<?php echo JHTML::link($caturl, $cattext); ?>
				</li>
			<?php } ?>
		</ul>
		<?php 	} ?>
	</li>
	<?php
	} ?>
</ul>

 <!-- getProductsInCategory -->