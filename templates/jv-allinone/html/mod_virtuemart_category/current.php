<?php // no direct access
defined('_JEXEC') or die('Restricted access');
//JHTML::stylesheet ( 'menucss.css', 'modules/mod_virtuemart_category/css/', false );
$ID = str_replace('.', '_', substr(microtime(true), -8, 8));

if ($module->postition == "") {
	$categoryModel = VmModel::getModel ('category');
	$mediaModel = VmModel::getModel ('media');
	$catID = $params->get('Parent_Category_id');
	$mediaID = $categoryModel->getCategory($catID)->virtuemart_media_id;
	$image = $mediaModel->createMediaByIds($mediaID);
	$imageURL = $image[0]->file_url;
	$imageName = $image[0]->file_title;
?>
<div class="vmImageCat">
	<img src="<?php echo $imageURL; ?>" alt="<?php echo $imageName; ?>" >
</div>
<?php }?>


<ul class="VMmenu<?php echo $class_sfx ?> vmMenuDefault" id="<?php echo "VMmenu".$ID ?>" >
<?php foreach ($categories as $category) {
		 $active_menu = 'class="VmClose"';
		$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$category->virtuemart_category_id);
		$cattext = $category->category_name;
		
		$productModel = VmModel::getModel ('product');
	
		$productArray = $productModel->getProductsInCategory($category->virtuemart_category_id);
		$newArray = array();
		$days = VmConfig::get('latest_products_days');
		$ndate = strtotime ('now') - ($days * 86400);
		foreach ($productArray as $key => $product) :
			$cdate = strtotime ($product->created_on) ;
			if ($cdate > $ndate){ 
				$newArray[] =$key;
			}
		endforeach;
		$new =  (count($newArray) > 0)?' <span class="hot bc-bg">'.JText::_('TPL_VM_NEW').'</span>':'';
		
		if (in_array( $category->virtuemart_category_id, $parentCategories)) $active_menu = 'class="VmOpen"';

		?>

<li <?php echo $active_menu ?>>
	<div >
		<?php echo JHTML::link($caturl, $cattext.$new);
		if ($category->childs) {
			?>
			<span class="VmArrowdown"> </span>
			<?php
		}
		?>
	</div>
<?php if ($active_menu=='class="VmOpen"') {
	?>
	<ul class="menu<?php echo $class_sfx; ?>">
	<?php
		foreach ($category->childs as $child) {

			$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$child->virtuemart_category_id);
			$cattext = $child->category_name;
			?>

			<li>
				<div ><?php echo JHTML::link($caturl, $cattext); ?></div>
			</li>
			<?php
		}
		?>
	</ul>
	<?php
}
?>
</li>
<?php
	} ?>
</ul>
