<?php // no direct access
defined ('_JEXEC') or die('Restricted access');

if ($display_style != "div") {
	// add javascript - custom by Truong Nguyen
	$doc = JFactory::getDocument();
	$slideJS = '
	(function($){
		$(function(){		
			$(".vmproduct-carousel-'.$module->id.'").each(function(){
				var el = $(this);
				el.owlCarousel({
					direction : $("body").hasClass( "rtl" )?\'rtl\':\'ltr\',
				    items : el.data(\'items\'),
				    itemsDesktop : [1199,(el.data(\'items\') > 4)?4:el.data(\'items\')],
					itemsDesktopSmall : [992,(el.data(\'items\') > 3)?3:el.data(\'items\')],
					itemsTablet : [768,(el.data(\'items\') > 2)?2:el.data(\'items\')],
					itemsTabletSmall: [450,(el.data(\'items\') > 2)?2:el.data(\'items\')],
					singleItem: (el.data(\'items\') == 1)?true:false,
				    navigation : true,
				    navigationText : ["",""],
				    pagination: false
			    });	
			});
		});
	})(jQuery);		
	';
	$doc->addScriptDeclaration($slideJS);
}

vmJsApi::jPrice();


$col = 1;
$pwidth = ' col-sm-' . floor (12 / $products_per_row);
if ($products_per_row > 1) {
	$float = "floatleft";
} else {
	$float = "center";
}
?>
<div class="vmgroup<?php echo $params->get ('moduleclass_sfx') ?> listing-view">

	<?php if ($headerText) { ?>
	<div class="vmheader"><?php echo $headerText ?></div>
	<?php
}
	if ($display_style == "div") {
		?>
		<div class="vmproduct<?php echo $params->get ('moduleclass_sfx'); ?> productdetails vmproductblock">
			<div class="row">
				<?php foreach ($products as $product) { ?>
				<div class="product <?php echo $pwidth ?> <?php echo $float ?>">
					<div class="spacer">
					<div class="vmproductItemImage">
					<?php
						if (!empty($product->images[0])) {
							$image = $product->images[0]->displayMediaThumb ('class="featuredProductImage" border="0"', FALSE);
						} else {
							$image = '';
						}
					
					echo JHTML::_ ('link', JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id), $image, array('title' => $product->product_name));
					?>
					</div>
					<?php
					$url = JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' .
						$product->virtuemart_category_id); ?>
					<div class="vm-product-body">
						<div class="vm-product-rating-container">
							<?php echo shopFunctionsF::renderVmSubLayout('rating',array('showRating'=>true,'product'=>$product)); ?>
						</div>
						<h3><a  class="vmproductItemTitle" href="<?php echo $url ?>"><?php echo $product->product_name ?></a></h3>
						
						<?php 
						if ($show_price and  isset($product->prices)) {
							 if (!empty($product->prices['basePrice'] ) ) echo '<span class="basePrice pop-products-prev-price">'.$currency->createPriceDiv('basePrice','',$product->prices,true).'</span>';
							 // 		echo $currency->priceDisplay($product->prices['salesPrice']);
							 if (!empty($product->prices['salesPrice'] ) ) echo '<span class="salesPrice pop-products-price">'.$currency->createPriceDiv('salesPrice','',$product->prices,true).'</span>';
							 // 		if ($product->prices['salesPriceWithDiscount']>0) echo $currency->priceDisplay($product->prices['salesPriceWithDiscount']);
							 if (!empty($product->prices['salesPriceWithDiscount']) ) echo '<span class="salesPrice pop-products-price">'.$currency->createPriceDiv('salesPriceWithDiscount','',$product->prices,true).'</span>';

							 
						 }
						if ($show_addtocart) {
						 	if (!VmConfig::get ('use_as_catalog', 0)) {
								$stockhandle = VmConfig::get ('stockhandle', 'none');
								if (($stockhandle == 'disableit' or $stockhandle == 'disableadd') and ($product->product_in_stock - $product->product_ordered) < 1) {
									$button_lbl = vmText::_ ('COM_VIRTUEMART_CART_NOTIFY');
									$button_cls = 'notify-button';
									$button_name = 'notifycustomer';
									?>
									<div style="display:inline-block;">
								<a href="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&layout=notify&virtuemart_product_id=' . $product->virtuemart_product_id); ?>" class="notify"><?php echo vmText::_ ('COM_VIRTUEMART_CART_NOTIFY') ?></a>
									</div>
								<?php
								} else {
									?>
								<div class="addtocart-area">

									<form method="post" class="product" action="index.php">
										<?php
										// Product custom_fields
										if (!empty($product->customfieldsCart)) {
											?>
											<div class="product-fields">
												<?php foreach ($product->customfieldsCart as $field) { ?>

												<div style="display:inline-block;" class="product-field product-field-type-<?php echo $field->field_type ?>">
													<?php if($field->show_title == 1) { ?>
														<span class="product-fields-title"><b><?php echo $field->custom_title ?></b></span>
														<?php echo JHTML::tooltip ($field->custom_tip, $field->custom_title, 'tooltip.png'); ?>
													<?php } ?>
													<div class="product-field-display"><?php echo $field->display ?></div>
													<div class="product-field-desc"><?php echo $field->custom_desc ?></div>
												</div>

												<?php } ?>
											</div>
											<?php } ?>

										<div class="addtocart-bar">

											<?php
											// Display the quantity box
											?>
											<!-- <label for="quantity<?php echo $product->virtuemart_product_id;?>" class="quantity_box"><?php echo vmText::_ ('COM_VIRTUEMART_CART_QUANTITY'); ?>: </label> -->
											<span class="quantity-box">
											<input type="text" class="quantity-input" name="quantity[]" value="1"/>
											</span>
											<span class="quantity-controls">
											<input type="button" class="quantity-controls quantity-plus"/>
											<input type="button" class="quantity-controls quantity-minus"/>
											</span>


											<?php
											// Add the button
											$button_lbl = vmText::_ ('COM_VIRTUEMART_CART_ADD_TO');
											$button_cls = ''; //$button_cls = 'addtocart_button';


											?>
											<?php // Display the add to cart button ?>
											<span class="addtocart-button">
												<?php
												if($product->orderable) {
												?>
													<input type="submit" name="addtocart" class="addtocart-button button-green" value="<?php echo vmText::_( 'COM_VIRTUEMART_CART_ADD_TO' ); ?>" title="<?php echo vmText::_( 'COM_VIRTUEMART_CART_ADD_TO' ); ?>" />
												<?php } else { ?>
													<input name="addtocart" class="addtocart-button-disabled button-gray" value="<?php echo vmText::_( 'COM_VIRTUEMART_ADDTOCART_CHOOSE_VARIANT' ); ?>" title="<?php echo vmText::_( 'COM_VIRTUEMART_ADDTOCART_CHOOSE_VARIANT' ); ?>" />
												<?php } ?>
											</span>
										</div>

										<input type="hidden" class="pname" value="<?php echo $product->product_name ?>"/>
										<input type="hidden" name="option" value="com_virtuemart"/>
										<input type="hidden" name="view" value="cart"/>
										<noscript><input type="hidden" name="task" value="add"/></noscript>
										<input type="hidden" name="virtuemart_product_id[]" value="<?php echo $product->virtuemart_product_id ?>"/>
										<input type="hidden" name="virtuemart_category_id[]" value="<?php echo $product->virtuemart_category_id ?>"/>
									</form>
									<div class="clear"></div>
								</div>
								<?php
								}
							}
						 }// echo mod_virtuemart_product::addtocart($product);
						 ?>
					 </div>
				</div>
				<div class="bottom-border"></div>
				</div>
				<?php
				if ($col == $products_per_row && $products_per_row && $col < $totalProd) {
					echo '	</div><div class="row">';
					$col = 1;
				} else {
					$col++;
				}
			} ?>
		</div>
	</div>
		<?php
	} else {
		$last = count ($products) - 1;
		?>
		<div class="row vmproductUl">
		<div class="vmproduct<?php echo $params->get ('moduleclass_sfx'); ?> productdetails vmproduct-carousel-<?php echo $module->id;?>" data-items="<?php echo $products_per_row;?>">
			<?php foreach ($products as $product) : ?>
			<div class="product vm-col col-xs-12">
				<div class="spacer">
					<div class="vmproductItemImage">
					<?php
						if (!empty($product->images[0])) {
							$image = $product->images[0]->displayMediaThumb ('class="featuredProductImage" border="0"', FALSE);
						} else {
							$image = '';
						}
					
					echo JHTML::_ ('link', JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id), $image, array('title' => $product->product_name));
					?>
					</div>
					<?php
					$url = JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' .
						$product->virtuemart_category_id); ?>
					<div class="vm-product-body">
						<div class="vm-product-rating-container">
							<?php echo shopFunctionsF::renderVmSubLayout('rating',array('showRating'=>true,'product'=>$product)); ?>
						</div>
						<h3><a  class="vmproductItemTitle" href="<?php echo $url ?>"><?php echo $product->product_name ?></a></h3>
						<?php echo shopFunctionsF::renderVmSubLayout('category_list',array('product'=>$product)); ?>
						<?php 
						if ($show_price and  isset($product->prices)) {
							echo '<div class="product-price mini">';
							if (!empty($product->prices['basePrice'] ) ) echo '<div class="vm-display"><span class="basePrice PricebasePrice ">'.$currency->createPriceDiv('basePrice','',$product->prices,true).'</span></div>';
							// 		echo $currency->priceDisplay($product->prices['salesPrice']);
							if (!empty($product->prices['salesPrice'] ) ) echo '<div class="vm-display"><span class="salesPrice PricesalesPrice">'.$currency->createPriceDiv('salesPrice','',$product->prices,true).'</span></div>';
							// 		if ($product->prices['salesPriceWithDiscount']>0) echo $currency->priceDisplay($product->prices['salesPriceWithDiscount']);
							if (!empty($product->prices['salesPriceWithDiscount']) ) echo '<div class="vm-display"><span class="salesPrice PricesalesPrice pop-products-price">'.$currency->createPriceDiv('salesPriceWithDiscount','',$product->prices,true).'</span></div>';
							echo '</div>';
							 
						 }
						if ($show_addtocart) {
							
						 	if (!VmConfig::get ('use_as_catalog', 0)) {
								$stockhandle = VmConfig::get ('stockhandle', 'none');
								if (($stockhandle == 'disableit' or $stockhandle == 'disableadd') and ($product->product_in_stock - $product->product_ordered) < 1) {
									$button_lbl = vmText::_ ('COM_VIRTUEMART_CART_NOTIFY');
									$button_cls = 'notify-button';
									$button_name = 'notifycustomer';
									?>
									<div style="display:inline-block;">
								<a href="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&layout=notify&virtuemart_product_id=' . $product->virtuemart_product_id); ?>" class="notify"><?php echo vmText::_ ('COM_VIRTUEMART_CART_NOTIFY') ?></a>
									</div>
								<?php
								} else {
									?>
								<div class="addtocart-area">

									<form method="post" class="product" action="index.php">
										<?php
										// Product custom_fields
										if (!empty($product->customfieldsCart)) {
											?>
											<div class="product-fields">
												<?php foreach ($product->customfieldsCart as $field) { ?>

												<div style="display:inline-block;" class="product-field product-field-type-<?php echo $field->field_type ?>">
													<?php if($field->show_title == 1) { ?>
														<span class="product-fields-title"><b><?php echo $field->custom_title ?></b></span>
														<?php echo JHTML::tooltip ($field->custom_tip, $field->custom_title, 'tooltip.png'); ?>
													<?php } ?>
													<div class="product-field-display"><?php echo $field->display ?></div>
													<div class="product-field-desc"><?php echo $field->custom_desc ?></div>
												</div>

												<?php } ?>
											</div>
											<?php } ?>

										<div class="addtocart-bar">

											<?php
											// Display the quantity box
											?>
											<!-- <label for="quantity<?php echo $product->virtuemart_product_id;?>" class="quantity_box"><?php echo vmText::_ ('COM_VIRTUEMART_CART_QUANTITY'); ?>: </label> -->
											<span class="quantity-box">
											<input type="text" class="quantity-input" name="quantity[]" value="1"/>
											</span>
											<span class="quantity-controls">
											<input type="button" class="quantity-controls quantity-plus"/>
											<input type="button" class="quantity-controls quantity-minus"/>
											</span>


											<?php
											// Add the button
											$button_lbl = vmText::_ ('COM_VIRTUEMART_CART_ADD_TO');
											$button_cls = ''; //$button_cls = 'addtocart_button';


											?>
											<?php // Display the add to cart button ?>
											<span class="addtocart-button">
												<?php
												if($product->orderable) {
												?>
													<input type="submit" name="addtocart" class="addtocart-button button-green" value="<?php echo vmText::_( 'COM_VIRTUEMART_CART_ADD_TO' ); ?>" title="<?php echo vmText::_( 'COM_VIRTUEMART_CART_ADD_TO' ); ?>" />
												<?php } else { ?>
													<input name="addtocart" class="addtocart-button-disabled button-gray" value="<?php echo vmText::_( 'COM_VIRTUEMART_ADDTOCART_CHOOSE_VARIANT' ); ?>" title="<?php echo vmText::_( 'COM_VIRTUEMART_ADDTOCART_CHOOSE_VARIANT' ); ?>" />
												<?php } ?>
											</span>
										</div>

										<input type="hidden" class="pname" value="<?php echo $product->product_name ?>"/>
										<input type="hidden" name="option" value="com_virtuemart"/>
										<input type="hidden" name="view" value="cart"/>
										<noscript><input type="hidden" name="task" value="add"/></noscript>
										<input type="hidden" name="virtuemart_product_id[]" value="<?php echo $product->virtuemart_product_id ?>"/>
										<input type="hidden" name="virtuemart_category_id[]" value="<?php echo $product->virtuemart_category_id ?>"/>
									</form>
									<div class="clear"></div>
								</div>
								<?php
								}
							}

						 }// echo mod_virtuemart_product::addtocart($product);
						 ?>
						 <?php echo shopFunctionsF::renderVmSubLayout('badges',array('product'=>$product)); ?>
					 </div>
				</div>
				<div class="bottom-border"></div>
			</div>
			<?php
			endforeach; ?>
		</div>
		</div>
		<?php
	}
	if ($footerText) : ?>
		<div class="vmfooter<?php echo $params->get ('moduleclass_sfx') ?>">
			<?php echo $footerText ?>
		</div>
		<?php endif; ?>
</div>