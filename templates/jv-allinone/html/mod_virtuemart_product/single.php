<?php // no direct access
defined('_JEXEC') or die('Restricted access');
vmJsApi::jPrice();

?>
<div class="vmgroup<?php echo $params->get( 'moduleclass_sfx' ) ?> vmgroup-single">
	<?php if ($headerText) { ?>
		<div class="vmheader"><?php echo $headerText ?></div>
	<?php } ?>

	<div class="vmproduct<?php echo $params->get('moduleclass_sfx'); ?>">
		<?php foreach ($products as $key => $product) { ?>
		<div class="vmproductItem" data-item="<?php echo $key+1;?>">
			<div class="spacer clearfix">
				<?php
				if (!empty($product->images[0]) )
				$image = $product->images[0]->displayMediaThumb('class="featuredProductImage" ',false) ;
				else $image = '';
				echo '<div class="vmproductItemImage pull-left">';
				echo JHTML::_('link', JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id='.$product->virtuemart_product_id.'&virtuemart_category_id='.$product->virtuemart_category_id),$image,array('title' => $product->product_name) );
				echo '</div>';
				$url = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id='.$product->virtuemart_product_id.'&virtuemart_category_id='.$product->virtuemart_category_id); ?>
				<h5><a class="vmproductItemTitle" href="<?php echo $url ?>"><?php echo $product->product_name; ?></a></h5>
				
				<?php	

				// $product->prices is not set when show_prices in config is unchecked
				if ($show_price and  isset($product->prices)) {
					if (!empty($product->prices['basePrice'] ) ) echo '<span class="basePrice pop-products-prev-price">'.$currency->createPriceDiv('basePrice','',$product->prices,true).'</span>';
					 // 		echo $currency->priceDisplay($product->prices['salesPrice']);
					 if (!empty($product->prices['salesPrice'] ) ) echo '<span class="salesPrice pop-products-price">'.$currency->createPriceDiv('salesPrice','',$product->prices,true).'</span>';
					 // 		if ($product->prices['salesPriceWithDiscount']>0) echo $currency->priceDisplay($product->prices['salesPriceWithDiscount']);
					 if (!empty($product->prices['salesPriceWithDiscount']) ) echo '<span class="salesPrice pop-products-price">'.$currency->createPriceDiv('salesPriceWithDiscount','',$product->prices,true).'</span>';

					 
				 }
				 if ($show_addtocart) {
				 	if (!VmConfig::get ('use_as_catalog', 0)) {
						$stockhandle = VmConfig::get ('stockhandle', 'none');
						if (($stockhandle == 'disableit' or $stockhandle == 'disableadd') and ($product->product_in_stock - $product->product_ordered) < 1) {
							$button_lbl = vmText::_ ('COM_VIRTUEMART_CART_NOTIFY');
							$button_cls = 'notify-button';
							$button_name = 'notifycustomer';
							?>
							<div style="display:inline-block;">
						<a href="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&layout=notify&virtuemart_product_id=' . $product->virtuemart_product_id); ?>" class="notify"><?php echo vmText::_ ('COM_VIRTUEMART_CART_NOTIFY') ?></a>
							</div>
						<?php
						} else {
							?>
						<div class="addtocart-area">

							<form method="post" class="product" action="index.php">
								<?php
								// Product custom_fields
								if (!empty($product->customfieldsCart)) {
									?>
									<div class="product-fields">
										<?php foreach ($product->customfieldsCart as $field) { ?>

										<div style="display:inline-block;" class="product-field product-field-type-<?php echo $field->field_type ?>">
											<?php if($field->show_title == 1) { ?>
												<span class="product-fields-title"><b><?php echo $field->custom_title ?></b></span>
												<?php echo JHTML::tooltip ($field->custom_tip, $field->custom_title, 'tooltip.png'); ?>
											<?php } ?>
											<div class="product-field-display"><?php echo $field->display ?></div>
											<div class="product-field-desc"><?php echo $field->custom_desc ?></div>
										</div>

										<?php } ?>
									</div>
									<?php } ?>

								<div class="addtocart-bar">

									<?php
									// Display the quantity box
									?>
									<!-- <label for="quantity<?php echo $product->virtuemart_product_id;?>" class="quantity_box"><?php echo vmText::_ ('COM_VIRTUEMART_CART_QUANTITY'); ?>: </label> -->
									<span class="quantity-box">
									<input type="text" class="quantity-input" name="quantity[]" value="1"/>
									</span>
									<span class="quantity-controls">
									<input type="button" class="quantity-controls quantity-plus"/>
									<input type="button" class="quantity-controls quantity-minus"/>
									</span>

									<?php // Display the add to cart button ?>
									<span class="addtocart-button">
										<?php
										if($product->orderable) {
										?>
											<input type="submit" name="addtocart" class="addtocart-button button-green" value="<?php echo vmText::_( 'COM_VIRTUEMART_CART_ADD_TO' ); ?>" title="<?php echo vmText::_( 'COM_VIRTUEMART_CART_ADD_TO' ); ?>" />
										<?php } else { ?>
											<input name="addtocart" class="addtocart-button-disabled button-gray" value="<?php echo vmText::_( 'COM_VIRTUEMART_ADDTOCART_CHOOSE_VARIANT' ); ?>" title="<?php echo vmText::_( 'COM_VIRTUEMART_ADDTOCART_CHOOSE_VARIANT' ); ?>" />
										<?php } ?>
									</span>
								</div>

								<input type="hidden" class="pname" value="<?php echo $product->product_name ?>"/>
								<input type="hidden" name="option" value="com_virtuemart"/>
								<input type="hidden" name="view" value="cart"/>
								<noscript><input type="hidden" name="task" value="add"/></noscript>
								<input type="hidden" name="virtuemart_product_id[]" value="<?php echo $product->virtuemart_product_id ?>"/>
								<input type="hidden" name="virtuemart_category_id[]" value="<?php echo $product->virtuemart_category_id ?>"/>
							</form>
							<div class="clear"></div>
						</div>
						<?php
						}
					}
				 }// echo mod_virtuemart_product::addtocart($product);
				 ?>
			</div>
			<div class="bottom-border"></div>
		</div>
		<?php } ?>
		<?php if ($footerText) { ?>
			<div class="vmheader"><?php echo $footerText ?></div>
		<?php } ?>
	</div>
</div>