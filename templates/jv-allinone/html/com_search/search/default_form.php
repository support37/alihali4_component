<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');

$lang = JFactory::getLanguage();
$upper_limit = $lang->getUpperLimitSearchWord();
?>


		<form id="searchForm" action="<?php echo JRoute::_('index.php?option=com_search');?>" method="post">
		<div class="panel-heading">
			<div class="btn-toolbar">
				<div class="input-group">
			      <input type="text" name="searchword" placeholder="<?php echo JText::_('COM_SEARCH_SEARCH_KEYWORD'); ?>" id="search-searchword" size="30" maxlength="<?php echo $upper_limit; ?>" value="<?php echo $this->escape($this->origkeyword); ?>" class="form-control" />
			      <span class="input-group-btn">
			        <button name="Search" onclick="this.form.submit()" class="btn hasTooltip button-green" title="<?php echo JHtml::tooltipText('COM_SEARCH_SEARCH');?>"><span class="fa fa-search"></span></button>
			      </span>
			    </div><!-- /input-group -->
				<input type="hidden" name="task" value="search" />
				<div class="clearfix"></div>
				<div class="bottom-border"></div>
			</div>
		</div>
		<div class="panel-body">
			
				<?php if (!empty($this->searchword)):?>
				<div class="searchintro<?php echo $this->params->get('pageclass_sfx'); ?>">
					<p><?php echo JText::plural('COM_SEARCH_SEARCH_KEYWORD_N_RESULTS', '<span class="badge badge-info">'. $this->total. '</span>');?></p>
				</div>
				<hr>
				<?php endif;?>
			
			<div class="row">
				<div class="col-md-6">
					<fieldset class="phrases">
						<legend><?php echo JText::_('COM_SEARCH_FOR');?>
						</legend>
							<div class="phrases-box">
								<?php echo $this->lists['searchphrase']; ?>
							</div>
							<div class="ordering-box">
								<label for="ordering" class="ordering pull-left">
									<?php echo JText::_('COM_SEARCH_ORDERING');?>
								</label>
								<div class="pull-left">
									<?php echo $this->lists['ordering'];?>
									<div class="bottom-border"></div>
								</div>
							</div>
					</fieldset>
				</div>
				<div class="col-md-6">
					<?php if ($this->params->get('search_areas', 1)) : ?>
						<fieldset class="only">
						<legend><?php echo JText::_('COM_SEARCH_SEARCH_ONLY');?></legend>
						<?php foreach ($this->searchareas['search'] as $val => $txt) :
							$checked = is_array($this->searchareas['active']) && in_array($val, $this->searchareas['active']) ? 'checked="checked"' : '';
						?>
						<input type="checkbox" name="areas[]" value="<?php echo $val;?>" id="area-<?php echo $val;?>" <?php echo $checked;?> >
						<label for="area-<?php echo $val;?>" class="checkbox">
							<?php echo JText::_($txt); ?>
						</label>
						<?php endforeach; ?>
						</fieldset>
					<?php endif; ?>
				</div>
			</div>
		</div>
		
			<?php if ($this->total > 0) : ?>
			<div class="panel-footer">
				<div class="row">
					<div class="form-limit col-md-5 form-horizontal">
						<div class="row">
							<div class=" col-md-3">
								<label for="limit" class="control-label">
									<?php echo JText::_('JGLOBAL_DISPLAY_NUM'); ?>
								</label>
							</div>
							<div class="col-md-4">
								<?php echo $this->pagination->getLimitBox(); ?>
								<div class="bottom-border"></div>
							</div>
							
						</div>
					</div>
					<div class="counter text-right col-md-6 pull-right">
						<?php echo $this->pagination->getPagesCounter(); ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
		
		</form>



