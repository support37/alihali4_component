<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<ul class="list-group search-results<?php echo $this->pageclass_sfx; ?>">
<?php foreach ($this->results as $result) : ?>
	<li class="list-group-item">
		<div class="result-title">
			<?php echo $this->pagination->limitstart + $result->count.'. ';?>
			<?php if ($result->href) :?>
				<a href="<?php echo JRoute::_($result->href); ?>"<?php if ($result->browsernav == 1) :?> target="_blank"<?php endif;?>>
					<b><?php echo $this->escape($result->title);?></b>
				</a>
			<?php else:?>
				<?php echo $this->escape($result->title);?>
			<?php endif; ?>
		</div>
		<?php if ($result->section) : ?>
			<div class="result-category">

				<span class="small<?php echo $this->pageclass_sfx; ?>">
					<i class="fa fa-folder-o"></i>	<?php echo $this->escape($result->section); ?>
				</span>
			</div>
		<?php endif; ?>
		<?php if ($result->text) : ?>
		<div class="result-text">
			<hr>
			<?php echo $result->text; ?>

		</div>
		<?php endif; ?>
		<?php if ($this->params->get('show_date')) : ?>
			<hr>
			<div class="result-created<?php echo $this->pageclass_sfx; ?>">
				
				<small><?php echo JText::sprintf('JGLOBAL_CREATED_DATE_ON', $result->created); ?></small>
			</div>
		<?php endif; ?>
	</li>
<?php endforeach; ?>
</ul>
<?php if ($this->pagination->getPagesLinks()) : ?>
<div class="pagenav panel-footer">
	<?php echo $this->pagination->getPagesLinks(); ?>
</div>
<?php endif; ?>
