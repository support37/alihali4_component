<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if ($this->error) : ?>
	<ul class="list-group">
          <li class="list-group-item list-group-item-danger"><i><?php echo $this->escape($this->error); ?></i></li>
        </ul>
<?php endif; ?>
