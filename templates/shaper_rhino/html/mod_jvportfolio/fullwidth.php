<?php
/**
 * @version     1.0.0
 * @package     com_portfolio
 * @copyright   Copyright (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      joomlavi <info@joomlavi.com> - http://www.joomlavi.com
 */
// no direct access
defined('_JEXEC') or die;
$document = JFactory::getDocument();
$document->addStyleSheet( 'templates/jv-allinone/css/magnific-popup.css');
$document->addScript( 'templates/jv-allinone/js/jquery.magnific-popup.min.js');
$document->addScriptDeclaration("  
jQuery(document).ready(function() {
    jQuery('.box-portfolio').magnificPopup({
      delegate: '.pluss',
      type: 'image',
      mainClass: 'my-mfp-zoom-in',
      removalDelay: 160,
      preloader: false,
      gallery: {
          enabled:true
      }
    });
  });
");

$isFilter = (intval($params->get('filter')));                                                                
$isSort = intval($params->get('sort', 0));
$prefixCol = JvportfolioFrontendHelper::getPrefixCol($column);   
$ncol = intval($column)*2;
$qview = JvportfolioFrontendHelper::getActionQView("col-md-{$column}", "col-md-{$ncol}");   
?>
<?php if($items):?>
<div id="<?php echo "mod-frm-portfolio-{$module->id}"?>" class="fullwidth <?php echo implode(' ', array($params->get('moduleclass_sfx', ''), $prefixCol))?>">
    <?php if($isFilter+$isSort):?>
   <div class="clearfix topPortfolio">
            <?php if($isSort):?>
                <?php require ModJvPortfolioHelper::loadTemplate('_csort')?>
            <?php endif;?>
            <?php if($isFilter):?>
                <?php require ModJvPortfolioHelper::loadTemplate('_cfilter')?>
            <?php endif;?> 

        </div>
    <?php endif;?>
    
    <div class="box-portfolio <?php echo($isFilter ? 'portfolioContainer': '')?> ">
        
        <?php foreach($items as $item):?>
        <div  class="item pfo-item col-xs-6 col-md-<?php echo $column?>"  data-groups='[<?php echo $item->aliasTags?>]' data-name="<?php echo strtolower($item->name)?>" data-date="<?php echo strtotime($item->date_created)?>" data-like="<?php echo $item->cliked?>">
          <div class="overaly">
            <div class="corner">&nbsp;</div>
            <a href="<?php echo JUri::root().$item->image ?>" class="pluss c-pointer pfoPopup" title="<h5><?php echo $item->name?></h5><small><i><?php echo $item->tag?></i></small>">pluss</a>

            <div class="pfo-inner">
              <div class="pfo-inner2">
                <?php if($params->get('showLiked', 0)):?>
                  <a class="likeheart"  href="<?php echo JUri::root()?>?option=com_jvportfolio&amp;task=items.toggleVote&amp;pfid=<?php echo $item->id?>" data-pfvote="<?php echo $item->id?>"><i class="<?php echo ($item->lactive ? 'active' : '')?> fa fa-heart-o">&nbsp;<?php echo $item->cliked?></i></a>
                <?php endif;?>            
                <?php if($params->get('hasTitle', 0)):?>
                  <a class="pfo-title" href="<?php echo $item->link?>"><?php echo $item->name?></a>
                <?php endif;?>

                <?php if($params->get('hasTag', 0)):?>
                  <span class="gray-italic"><?php echo $item->tag?></span>
                <?php endif;?>
                <?php if($params->get('hasDate', 0)):?>
                  <p><?php echo date('d-m-Y', strtotime($item->date_created))?></p>
                <?php endif;?>
              </div>
            </div>
          </div>
          <img alt="<?php echo $item->name?>" src="<?php echo JUri::root().$item->image ?>" />
        </div>
		<?php endforeach;?>
        
        <div class="pf-load">
            <div class="box">
                <i class="fa fa-spinner fa-pulse fa-2x fa-fw margin-bottom"></i>
                <div class=""><?php echo JText::_('Loading the next set of posts...')?></div>
            </div>
        </div>    
    </div>
    <?php if($total):?>
    <?php require ModJvPortfolioHelper::loadTemplate('fullwidth_nav')?>
    <?php endif;?>
</div>
<?php endif;?>