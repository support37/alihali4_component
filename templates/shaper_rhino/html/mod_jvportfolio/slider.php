<?php
/**
 * @version     1.0.0
 * @package     com_portfolio
 * @copyright   Copyright (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      joomlavi <info@joomlavi.com> - http://www.joomlavi.com
 */
// no direct access
defined('_JEXEC') or die;
$document = JFactory::getDocument();
$document->addStyleSheet( 'templates/jv-allinone/css/magnific-popup.css');
$document->addScript( 'templates/jv-allinone/js/jquery.magnific-popup.min.js');
$document->addScriptDeclaration("  
jQuery(document).ready(function() {
    jQuery('.box-portfolio').magnificPopup({
      delegate: '.pluss',
      type: 'image',
      mainClass: 'my-mfp-zoom-in',
      removalDelay: 160,
      preloader: false,
      gallery: {
          enabled:true
      }
    });
  });
");
?>
<?php if($items):?>
    <div class="box-portfolio owl-portfolio owl-carousel portfoliomasonry">
        <?php foreach($items as $item):?>
        <div  class="item">
          <div class="overaly">
            <div class="corner">&nbsp;</div>
            <a href="<?php echo JUri::root().$item->image ?>" class="pluss c-pointer pfoPopup" title="<h5><?php echo $item->name?></h5><small><i><?php echo $item->tag?></i></small>">pluss</a>

            <div class="pfo-inner">
              <div class="pfo-inner2">
                <?php if($params->get('showLiked', 0)):?>
                  <a class="likeheart"  href="<?php echo JUri::root()?>?option=com_jvportfolio&amp;task=items.toggleVote&amp;pfid=<?php echo $item->id?>" data-pfvote="<?php echo $item->id?>"><i class="<?php echo ($item->lactive ? 'active' : '')?> fa fa-heart">&nbsp;<?php echo $item->cliked?></i></a>
                <?php endif;?>            
                <?php if($params->get('hasTitle', 0)):?>
                  <a class="pfo-title" href="<?php echo $item->link?>"><?php echo $item->name?></a>
                <?php endif;?>

                <?php if($params->get('hasTag', 0)):?>
                  <span class="gray-italic"><?php echo $item->tag?></span>
                <?php endif;?>
                <?php if($params->get('hasDate', 0)):?>
                  <p><?php echo date('d-m-Y', strtotime($item->date_created))?></p>
                <?php endif;?>
              </div>
            </div>
          </div>
          <img alt="<?php echo $item->name?>" src="<?php echo JUri::root().$item->image ?>" />
        </div>
        <?php endforeach;?>   
    </div>
<?php endif;?>