<?php
/*
 # Module		JV Contact
 # @version		3.0.1
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright © 2008-2012 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/

// No direct access to this file
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<!-- design -->
<?php echo @$myparams['captcha'][1];?>
<form id="jvcontact<?php echo $moduleid;?>" method="post">
    <?php echo $myparams['map'];?>
   <div class="contact-classic">
        <div class="row">
            <div class="col-md-6">
                <!-- Contact form -->
                <div id="email_send_form" class="contact-form" >
                    <div class="msgsendmailok" id="<?php echo $divmsgid?>">
                        <?php
                        if($msgthankyou && @$post['jvcontact'][$moduleid]){
                            echo '<div class="msgthankyou">'.$msgthankyou.'</div>';
                        }
                        ?>
                    </div>
                    <?php if($myparams['showform']){?>
                        <?php if($fields) foreach($fields as $key=>$field){ ?>
                            <p class="input-<?php echo $key; ?>"><?php echo @$field['label'];?> <?php echo $field['input'];?></p>
                        <?php }?>
                       
                        <?php if($myparams['mailcopy']){?>
                            <span style="float:left;width:100%">
                                <input name="cbcopymail" type="checkbox" value=1 />
                                <label for="cbcopymail"><?php echo $myparams['mailcopy'];?></label>
                            </span>
                        <?php }?>
                    <?php }?>
                    <?php echo $myparams['captcha'][0];?>
                    <p class="red-span">* - <?php echo Jtext::_("TPL_REQUIRED_CONTACT");?></p>
                    <br>
                    <a href="javascript:void(0);" class="button-green" onclick="formsubmit('jvcontact<?php echo $moduleid;?>');"><?php echo $myparams['textsubmit'];?></a>
                    <div id="result">&nbsp;</div>
                </div><!-- and Contact form -->
            </div>
            <div class="col-md-6">
                <?php if($myparams['social']){?>
                <span class="title"><?php echo Jtext::_('TPL_SHARE_CONTACT')?></span>
                <div class="share">
                    <?php echo $myparams['social'];?>
                </div>
                <?php }?>
                <?php echo $myparams['textheader'];?>
                <?php echo $myparams['moreinfo'];?>
                <?php echo $myparams['textfooter'];?>
            </div>
        </div>
    </div>
    <?php echo JHTML::_('form.token');?>
</form>