<?php
/*
# mod_jvajax_search_hikashop - JV Ajax Search HikaShop Module
# @version		1.0.0
# ------------------------------------------------------------------------
# author    Open Source Code Solutions Co
# copyright Copyright (C) 2014 joomlavi.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
# Websites: http://www.joomlavi.com
# Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');
?>
<div class="jv_ajax_search_hikashop jvhikashop_<?php echo $show_style?>" id="jvajax_search_hikashop<?php echo $module->id?>">
    <div class="jvajax_search_hikashop_fields ">
        <?php if($name_filter){?>
        <div class="jvhikashop_name_block">
            <input type="text" class="form-control" placeholder="Input Name" data-bind="value: name, valueUpdate: ['input', 'afterkeydown']">
            <div class="bottom-border"></div>
        </div>
        <?php }?>
        <?php if($price_filter){?>
        <div class="jvajax_search_hikashop_block row">
            <div class="col-xs-6">
                <input type="text" placeholder="From" class="form-control" data-bind="numeric: from, value: from, valueUpdate: ['input', 'afterkeydown']">
                <div class="bottom-border"></div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-6">
                <input type="text" placeholder="To" class="form-control" data-bind="numeric: to, value: to, valueUpdate: ['input', 'afterkeydown']">
                <div class="bottom-border"></div>
            </div>
        </div>
        <?php }?>
    </div>
    <div class="jv_ajax_content_hikashop jvajax_search_hikashop_loading" data-bind="visible: loading" style="display: none; <?php echo $popup_width?>">
        <img src="<?php echo JUri::root().'modules/mod_jvajax_search_hikashop/assets/images/loader.gif';?>" alt=""/>
    </div>
    <div class="jv_ajax_content_hikashop jvajax_search_hikashop_products_ctn" data-bind="simpleGrid: gridProductsModel, simpleGridTemplate: 'ko_jvajax_search_hikashop_products<?php echo $module->id?>', simpleGridPagerTemplate: 'ko_jvajax_search_hikashop_pagination<?php echo $module->id?>', visible: !loading() && products().length" style="display: none; <?php echo $popup_width?>"></div>

    <script type="text/html" id="ko_jvajax_search_hikashop_products<?php echo $module->id?>">
        <!-- ko if: itemsOnCurrentPage().length>0 -->
        <div class="div_hikashop_products row cols<?php echo $max_cols?>" data-bind="foreach: itemsOnCurrentPage">
            <!-- ko foreach: $parent.columns -->
			<div class="col-sm-<?php echo 12/$max_cols?> col-md-<?php echo 12/$max_cols?>">
                <div class="item">
                    <!-- ko if: JVAjaxSearchHikaShopModel<?php echo $module->id?>.show_image() && !JVAjaxSearchHikaShopModel<?php echo $module->id?>.image_pos()-->
                    <a class="image" href="" data-bind="attr: {href: $parent.product_link}, css: JVAjaxSearchHikaShopModel<?php echo $module->id?>.image_align()">
                        <img <?php echo $image_width;?> <?php echo $image_height;?> data-bind="attr: {src: $parent.image_url, alt: $parent.product_name, title: $parent.product_name}">
                    </a>
                    <!-- /ko -->

                    <!-- ko if: JVAjaxSearchHikaShopModel<?php echo $module->id?>.show_title()-->
                    <a class="title" data-bind="text: $parent.product_name, attr: {href: $parent.product_link}"></a>
                    <!-- /ko -->

                    <!-- ko if: JVAjaxSearchHikaShopModel<?php echo $module->id?>.show_image() && JVAjaxSearchHikaShopModel<?php echo $module->id?>.image_pos()-->
                    <a class="image" href="" data-bind="attr: {href: $parent.product_link}, css: JVAjaxSearchHikaShopModel<?php echo $module->id?>.image_align()">
                        <img <?php echo $image_width;?> <?php echo $image_height;?> data-bind="attr: {src: $parent.image_url, alt: $parent.product_name, title: $parent.product_name}">
                    </a>
                    <!-- /ko -->

                    <!-- ko if: JVAjaxSearchHikaShopModel<?php echo $module->id?>.show_desc()-->
                    <div class="short_desc" data-bind="text: $parent.product_description"></div>
                    <!-- /ko -->

                    <!-- ko if: JVAjaxSearchHikaShopModel<?php echo $module->id?>.show_price()-->
                    <span class="price" data-bind="text: $parent.price_formated"></span>
                    <!-- /ko -->

                    <!-- ko if: JVAjaxSearchHikaShopModel<?php echo $module->id?>.show_addtocart()-->
                    <form  class="form_add_to_cart" method="post" data-bind="attr: {action: $parent.addtocart.form_action}">
                        <div class="hikashop_product_stock">
                            <input type="submit" class="btn button hikashop_cart_input_button" name="add" value="Add to cart">
                            <input type="hidden" value="1" class="hikashop_product_quantity_field" name="quantity">
                        </div>
                        <input type="hidden" name="product_id" value="" data-bind="value: $parent.product_id"/>
                        <input type="hidden" name="add" value="1"/>
                        <input type="hidden" name="ctrl" value="product"/>
                        <input type="hidden" name="task" value="updatecart"/>
                        <input type="hidden" name="return_url" value="" data-bind="value: $parent.addtocart.return_url"/>
                    </form>
                    <!-- /ko -->
                </div>
                <div class="bottom-border"></div>
			</div>
            <!-- /ko -->
        </div>
        <!-- /ko -->
    </script>

    <script type="text/html" id="ko_jvajax_search_hikashop_pagination<?php echo $module->id?>">
        <div class="ko-grid-pageLinks hikashop_products_pagination">
            <!-- ko if: maxPageIndex()>0 -->
            <div class="pagination-list">
                <span class="pagenav" data-bind="visible: $root.currentPageIndex() == 0"><i class="icon icon-backward3"></i></span>
                <a class="pagenav" href="javascript:void(0)" data-bind="click: $root.gridPrev, visible: $root.currentPageIndex() != 0"><i class="icon icon-backward3"></i></a>

                <!-- ko foreach: ko.utils.range(0, maxPageIndex) -->
                    <span class="pagenav" data-bind="text: $data + 1, visible: $data == $root.currentPageIndex()"></span>
                    <a class="pagenav" href="javascript:void(0)" data-bind="visible: $data != $root.currentPageIndex() && JVAjaxSearchHikaShopModel<?php echo $module->id?>.showPage($root.maxPageIndex(), $root.currentPageIndex(), $data), text: $data + 1, click: function() { $root.currentPageIndex($data) }"></a>
                    <span data-bind="visible: $data != $root.currentPageIndex() && !JVAjaxSearchHikaShopModel<?php echo $module->id?>.showPage($root.maxPageIndex(), $root.currentPageIndex(), $data) && JVAjaxSearchHikaShopModel<?php echo $module->id?>.showPage($root.maxPageIndex(), $root.currentPageIndex(), $data-1)">...</span>
                <!-- /ko -->

                <span data-bind="visible: $root.currentPageIndex() == maxPageIndex()" class="pagenav"><i class="icon icon-forward4"></i></span>
                <a class="pagenav" href="javascript:void(0)" data-bind="click: $root.gridNext, visible: $root.currentPageIndex() != maxPageIndex()"><i class="icon icon-forward4"></i></a>
            </div>
            <!-- /ko -->
        </div>
    </script>
</div>

<script type="text/javascript">
    $JVHIKA(function($){
        window.JVAjaxSearchHikaShopModel<?php echo $module->id?> = new JVAjaxSearchHikaShopModel({
            currency: '<?php echo $currency;?>',
            cid: '<?php echo $cid;?>',
            Itemid: <?php echo $Itemid;?>,
            max_cols: '<?php echo $max_cols;?>',
            max_rows: '<?php echo $max_rows;?>',
            moduleid: <?php echo $module->id?>,
            show_style: '<?php echo $show_style?>',
            show_image: <?php echo $show_image;?>,
            image_pos: <?php echo $image_pos;?>,
            image_align: '<?php echo $image_align;?>',
            show_title: <?php echo $show_title;?>,
            show_price: <?php echo $show_price;?>,
            show_desc: <?php echo $show_desc;?>,
            show_addtocart: <?php echo $show_addtocart;?>,
            current_url: '<?php echo JUri::getInstance();?>'
        });
        ko.applyBindings(JVAjaxSearchHikaShopModel<?php echo $module->id?>, document.getElementById('jvajax_search_hikashop<?php echo $module->id?>'));
    });
</script>