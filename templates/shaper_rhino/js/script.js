(function($){
	function injector(t, splitter, klass, after) {
		var text = t.text()
		, a = text.split(splitter)
		, inject = '';
		if (a.length) {
			$(a).each(function(i, item) {
				inject += '<span class="'+klass+(i+1)+'" aria-hidden="true">'+item+'</span>'+after;
			});
			t.attr('aria-label',text)
			.empty()
			.append(inject)
		}
	}
	var methods = {
		init : function() {
			return this.each(function() {
				injector($(this), '', 'char', '');
			});
		},
		words : function() {
			return this.each(function() {
				injector($(this), ' ', 'word', ' ');
			});
		},
		lines : function() {
			return this.each(function() {
				var r = "eefec303079ad17405c889e092e105b0";
				injector($(this).children("br").replaceWith(r).end(), r, 'line', '');
			});
		}
	};

	$.fn.lettering = function( method ) {
		// Method calling logic
		if ( method && methods[method] ) {
			return methods[ method ].apply( this, [].slice.call( arguments, 1 ));
		} else if ( method === 'letters' || ! method ) {
			return methods.init.apply( this, [].slice.call( arguments, 0 ) ); // always pass an array
		}
		$.error( 'Method ' +  method + ' does not exist on jQuery.lettering' );
		return this;
	};
})(jQuery);

(function($){
	window.getSize = function(){
		return{
			x: window.screenX,
			y: window.screenY
		}
	}
	$.extend($.fn, {
		mHover: function(o){
			o = $.extend({h: '', duration: 'normal'}, o);
			var mh = o.h.replace(' ', '-');
			return this
			.delegate(o.h, ['mouseenter', mh].join(''), function(){
				$(o.target).stop().slideDown(o.duration);
			}).delegate(o.h, ['mouseleave', mh].join(''), function(){
				$(o.target).stop().slideUp(o.duration);
			})
		}
	});
	
	$(window).load(function() {
		'use strict'; 
		/* Placehoalder */
		if(!Modernizr.input.placeholder){
	
			$('[placeholder]').focus(function() {
			  var input = $(this);
			  if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			  }
			}).blur(function() {
			  var input = $(this);
			  if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			  }
			}).blur();
			$('[placeholder]').parents('form').submit(function() {
			  $(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
				  input.val('');
				}
			  })
			});
		}
	/* and Placehoalder */
	
	/* Back to Top */
		var offset = 220;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.back-to-top').fadeIn(duration);
			} else {
				jQuery('.back-to-top').fadeOut(duration);
			}
		});
		
		jQuery('.back-to-top').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})
	/* and Back to Top */
	});
	
	
	/* lang changer */
	$(function(){
		$('#lang').hover(
		function () {
			$('.lang').slideDown(100);
		},
		function () {
			$('.lang').slideUp(150);
		});
	 });
 
	/* and lang changer */
	
	/* ScrollToFixed menu */
	$(function() {
		$('#block-header ').scrollToFixed({minWidth: 1199});
	});
	
	/* and ScrollToFixed menu */
	$(function(){
	   $('.toggle h4').click( function() {

			var top = $(this).parent().parent();
			var content = $(this).parent().find('.toggle-content');
			var icon = $(this).parent().find('h4>i');
			var h = $(this);

			top.find('h4').removeClass('opened');

			if( content.is(':hidden') ) {
				top.find('.toggle-content').slideUp();
				h.addClass('opened');
				content.slideDown(250);

			} else {
				content.slideUp(50);
			}
	   });
	});
	/* and Accordion */
	
	/* --------- TRUONG JS -----------*/
	$(function(){
		function setCookie(cname,cvalue,exdays){
			var d = new Date();
			d.setTime(d.getTime()+(exdays*24*60*60*1000));
			var expires = "expires="+d.toGMTString();
			document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/";
		}
		function getCookie(cname){
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for(var i=0; i<ca.length; i++) 
			  {
			  var c = ca[i].trim();
			  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
			  }
			return "";
		}
				
		$('.blog-slider').each(function(){
			var el = $(this);
			el.bxSlider({
		      	pagerCustom: el.data('thumb')
		    })	
		    $(el.data('thumb')).bxSlider({
		    	slideWidth: 114,
			    minSlides: 2,
			    maxSlides: 5,
			    slideMargin: 23
		    });
		});
		
		$(".catCounter").lettering();

		$('.listing-view-carousel').each(function(){
			var el = $(this);
			el.owlCarousel({
				direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
			    items : el.data('items'),
			    itemsDesktop : [1199, (el.data('items') < 4)?el.data('items'):4],
				itemsDesktopSmall : [992,3],
				itemsTablet : [768,2],
				itemsTabletSmall: [450,2],
			    navigation : true,
			    navigationText : ["",""],
			    pagination: false
		    });	
		});

		// Virtuemart -----------------

	    var imageProduct = $('.vm-product-media-container');
 		var thumbProduct = $('.vm-product-thumb-slide');
 		imageProduct.each(function(){
 			$(this).owlCarousel({
 				direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
				singleItem : true,
				navigation: true,
				pagination:false,
				afterAction : function(){
					var product = this.$elem.data("product"),
						current = this.currentItem
					;
					$('.vm-product-thumb-slide[data-product="'+product+'"]')
					.find(".owl-item")
					.removeClass("active")
					.eq(current)
					.addClass("active")
					if($('.vm-product-thumb-slide[data-product="'+product+'"]').data("owlCarousel") !== undefined){
						center(current, product)
					}
				}
			});

 		});
 		thumbProduct.each(function(){
 			$(this).owlCarousel({
 				direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
			    items : 4,
			    itemsDesktop      : [1199,4],
			    itemsDesktopSmall     : [979,4],
			    itemsTablet       : [768,4],
			    itemsMobile       : [479,4],
			    navigation: true,
			    pagination:false,
			    navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			    afterInit : function(elem){
			      elem.find(".owl-item").eq(0).addClass("active");
			    }
			 });
 		});

		thumbProduct.on("click", ".owl-item", function(e){console.log('lo');
			e.preventDefault();
			el = $(this);
 			product = el.parents(".vm-product-thumb-slide").data("product");
			var number = el.data("owlItem");
			$('.vm-product-media-container[data-product="'+product+'"]').trigger("owl.goTo",number);
		});

		function center(number, product){
			var thumb = $('.vm-product-thumb-slide[data-product="'+product+'"]')
			var sync2visible = thumb.data("owlCarousel").owl.visibleItems;
			var num = number;
			var found = false;
			for(var i in sync2visible){
			  if(num === sync2visible[i]){
			    var found = true;
			  }
			}
			if(found===false){
			  if(num>sync2visible[sync2visible.length-1]){
			    thumb.trigger("owl.goTo", num - sync2visible.length+2)
			  }else{
			    if(num - 1 === -1){
			      num = 0;
			    }
			    thumb.trigger("owl.goTo", num);
			  }
			} else if(num === sync2visible[sync2visible.length-1]){
			  thumb.trigger("owl.goTo", sync2visible[1])
			} else if(num === sync2visible[0]){
			  thumb.trigger("owl.goTo", num-1)
			}
		};


		var prolist = $('[id^="productListing_"]');
		$('.view-layout a').each(function(){
			var el = $(this);
			el.click(function(){
				$('.view-layout a').removeClass('active');
				el.addClass('active');
				if (el.hasClass('view-list')) {
					el.parents('.listing-view').addClass('list');
					setCookie("listing",'list',1);
				} else{
					el.parents('.listing-view').removeClass('list');
					setCookie("listing",'',1);
				};
				prolist.masonry('layout');
			});
		});

		$('.vm-menu-category .arrow').click( function() {
			var el = $(this);
			var wrap = el.parents('.vm-menu-category');

			var top = el.parent('li'),
			 	topAll = wrap.find('li'),
				content = el.parent().find('.menu'),
				contentAll = wrap.find('.menu');

			topAll.removeClass('opened');
			contentAll.slideUp();

			if( content.is(':hidden') ) {
				top.addClass('opened');
				content.slideDown(200);
			} else {
				content.slideUp(50);
			}
	   	});


		$(".hasChild").each(function(){
			var el = $(this);
			if(el.find("div.fx-subitem").length == 0){
				el.find(".iconsubmenu").remove();
			}
		});
	});
	/* end Truong JS */
	


	/* Breadcrumb */
	$(function(){
		if ($('#block-breadcrumb').length) {
			$('.titlePage').appendTo("#block-breadcrumb .clearfix");
		};
	});
	/* and Blog slider */
	

	/* Cloud move */
	$(function move() {
		$(".cloud").animate( {"top": "+=30px"}, 2500, "linear",
		function() {
			$(".cloud").animate( {"top": "-=30px"}, 2500, "linear",
			function() {
				move();
			});
		});
	});
	/* and Cloud move */

})(jQuery);	


(function($){
	$(function(){
		
	 	$(".selectbox select, .filter-search select").selectify();

/////////////////////////////  Search
		var searchBar = $('#searchtop');
		$('.btnsearchtop').click(function(){
			searchBar.slideToggle(300);
			$('.flexMenuToggle:first').click();
			return false;
		});			 
		$('#search-beack').click(function(){
			searchBar.slideUp(300);
			return false;
		});

/////////////////////////////  Add Class


		/* add class Recent Posts */
		$('.moduleRecentPosts').each(function(){$(this).parent().parent().parent().addClass('divmoduleRecentPosts');});

		/* add class Team Of Prof */
		$('.moduleTeamOfProf').each(function(){$(this).parent().parent().parent().addClass('sectionTeamOfProf');});
		
		/* add class About Numbers */
		$('.moduleAboutNumbers').each(function(){$(this).parent().parent().parent().addClass('sectionAboutNumbers');});

		/* add class Callout */
		$('.moduleCallout').each(function(){$(this).parent().parent().parent().addClass('sectionCallout');});

		/* add class Callout 2 */
		$('.moduleCallout2').each(function(){$(this).parent().parent().parent().addClass('sectionCallout2');});

		$('.parallaxTestimonials').each(function(){$(this).parents('section').addClass('parallaxTestimonialsWapp');});
		

///////////////////////////// owl Carousel

		$(".owl-portfolio").owlCarousel({
			direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
			autoPlay: 3000, //Set AutoPlay to 3 seconds
			items:5,
			itemsDesktop : [1199,4],
			itemsDesktopSmall : [979,3]
		});
		
		$(".owl-carousel-item4").each(function(){
			var el = $(this),
				num = (el.data('item') != "")?el.data('item'):4;
				el.owlCarousel({
					direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
					items : (num < 4)?num:4,
					itemsDesktop : [1199,(num < 4)?num:4],
					navigation : true,
					singleItem : (num == 1)?true:false,
					navigationText : [" "," "]
				});
		});

		$("#owl-demo-4").owlCarousel({
			direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
			autoPlay : 3000,
			stopOnHover : true,
			navigation: true,
			paginationSpeed : 1000,
			goToFirstSpeed : 2000,
			singleItem : true,
			autoHeight : true,
			navigationText : [" "," "],
			transitionStyle:"fade"
		});

		$(".hikashop_products_table_fix").owlCarousel({
			direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
			items : 4,
			navigation : true,
			navigationText : [" "," "]
		});
		$(".owl-demo-6").owlCarousel({
			direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
		    items : 6,
		    navigation : true,
		    navigationText : [" "," "]
	    });

	    $(".owl-demo-8").owlCarousel({
	    	direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
		    items : 3,
		    navigation : true,
		    navigationText : [" "," "]
	    });
	    $(".owl-demo-7").owlCarousel({
	    	direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
		    items : 4,
		    navigation : true,
		    navigationText : ["",""]
	    });
	    $(".owl-demo-9, .owlSingle").owlCarousel({
	    	direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
		    items : 1,
		    singleItem:true,
		    navigation : true,
		    navigationText : [" "," "]
	    });
		$("#owl-demo-10").owlCarousel({
			direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
		    items : 1,
		    singleItem:true,
		    navigation : true,
		    navigationText : [" "," "]
	    });
	    $(".owl-demo-11").owlCarousel({
	    	direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
		    items : 2,
		    itemsDesktop : [1199,2],
			itemsDesktopSmall : [979,2],
		    navigation : true,
		    navigationText : [" "," "]
	    });		
//////////////////////////// Add i choose options  

		var classRtlfa=$( "body" ).hasClass("rtl");
		if(classRtlfa==true) a='left';else a='right';
		var  chooseoptions = $('input[type="submit"][name="choose_options"]');
		var  ichoose = $('<i>',{'class':'fa fa-angle-'+a+' i_choose_options' });
		chooseoptions.after(ichoose); 	
	});	
	$(function(){		
		$('.sectionCallout, .sectionAboutNumbers, .sectionCallout2, .callout-2, .team-of-prof-bg, .parallaxTestimonialsWapp').parallax();
	});
})(jQuery);		
	