<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_sp_testimonial
 * @copyright	Copyright (C) 2010 - 2013 JoomShaper. All rights reserved.
 * @license		GNU General Public License version 2 or later; 
 */

// no direct access
defined('_JEXEC') or die;

//Add css and js file
Helix::addJS('jquery.content_slider.min.js')->addCSS('content_slider_style.css');

$count = count($data);
?>

<div class="sp-testimonial-wrapper <?php echo $params->get('moduleclass_sfx'); ?>" id="sp-testimonial-module">
<?php if( $count>0 ) { ?>

	<div class="mod-sp-testimonial content_slider_wrapper" id="mod-sp-testimonial">
		<?php foreach ($data as $key => $value) { ?>
			<div class="circle_slider_text_wrapper" id="sw<?php echo $key; ?>" style="display: none;">
				<div class="content_slider_text_block_wrap">
					<p><i class="icon-quote-left"></i><?php echo $value['testimonial']; ?></p>
					<small class="muted"><?php echo $value['name']; ?>, <?php echo $value['designation']; ?></small>
				</div>
			</div>
		<?php } ?>
	</div>

	<script type="text/javascript">
	  jQuery(function ($) {
	  	$(document).ready(function() {
	  		var image_array = new Array();

	  		image_array = [<?php foreach($data as $index=>$value) $images[] = "{image: '".JURI::root().$value['avatar']."'}"; echo implode(',',$images);?>];

	        $('#mod-sp-testimonial').content_slider({
				map : image_array,
				plugin_url: <?php echo "'" . JURI::base(true) . '/modules/mod_sp_testimonial/' . "'"; ?>,
				max_shown_items: <?php echo $count; ?>,
				hv_switch: 0,
				active_item: 0,
				wrapper_text_max_height: 550,
				middle_click: 1,
				under_600_max_height: 1170,
				border_radius:	-1,
				automatic_height_resize: 1,
				border_on_off: 0,
				allow_shadow: 0
			});

	  	});
	  });
	</script>

<?php } else {?>
	<p class="alert alert-error">No testimonial found!! Please add some testimonials</p>
<?php } ?>
</div>