<?php
/*---------------------------------------------------------------
# Package - Joomla Template based on Helix Framework   
# ---------------------------------------------------------------
# Author - JoomShaper http://www.joomshaper.com
# Copyright (C) 2010 - 2012 JoomShaper.com. All Rights Reserved.
# license - PHP files are licensed under  GNU/GPL V2
# license - CSS  - JS - IMAGE files  are Copyrighted material 
# Websites: http://www.joomshaper.com
-----------------------------------------------------------------*/

// set document type as text/javascript	
header('Content-type: text/css; charset: UTF-8');
header('Cache-Control: must-revalidate');
header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 3600) . ' GMT');
$url = $_REQUEST['url'].'/css/PIE/pie.php';
?>
#sp-slide-grid .sp_slide_introtext, .sp_countdown_days, .sp_countdown_hours, .sp_countdown_mins, .sp_countdown_secs,
.nssp2.ns2-speaker img.ns2-image, .mod-wrapper h3.header span, .login h1, 
a.readmore, .comments, input.button, a.details, .adminform button, #adminForm button, 
.button, button[type="submit"], input[type="submit"], input[type="button"], 
div.itemCommentsForm form input#submitCommentButton, div.pagination ul li a, 
ul.tabs_container li.tab.active, .blog-speaker .itemInnerDiv, .blog-speaker .itemInnerDiv:hover
{position:relative; behavior:url(<?php echo $url ?>)}