<?php
/*---------------------------------------------------------------
# Package - Joomla Template based on Helix Framework   
# ---------------------------------------------------------------
# Template Name - Shaper Event
# ---------------------------------------------------------------
# Author - JoomShaper http://www.joomshaper.com
# Copyright (C) 2010 - 2013 JoomShaper.com. All Rights Reserved.
# license - PHP files are licensed under  GNU/GPL V2
# license - CSS  - JS - IMAGE files  are Copyrighted material 
# Websites: http://www.joomshaper.com
-----------------------------------------------------------------*/
//no direct accees
defined ('_JEXEC') or die ('resticted aceess');
require_once(dirname(__FILE__).'/lib/helix.php');
$hideThis = '';
if ($helix->hideItem()) $hideThis = 'sp-hidethis';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language;?>" >
<head>
	<?php
		$helix->loadHead();
		$helix->addCSS('template.css,joomla.css,custom.css,modules.css,typography.css,template_' . $helix->direction . '.css,css3.css');
		$helix->addJS('tools.js');
		$helix->getStyle();
	?>
</head>
<?php $helix->addFeatures('ie6warn'); ?>
<body class="bg <?php echo $helix->direction . ' ' . $helix->style ?> clearfix">
	<?php $helix->addFeatures('popup,toppanel'); // top panel ?>
	<div id="header-wrapper" class="clearfix">
		<div class="sp-wrap">
			<div id="header" class="clearfix">
				<?php $helix->addModules("share") // share module ?>
				<?php $helix->addFeatures('logo') // logo ?>
				<div id="hornav-wrapper" class="clearfix">
					<?php $helix->addFeatures('hornav') //Main navigation ?>
				</div>
			</div>	
		</div>
	</div>
	
	<?php $helix->addModules('pagetitle', 'sp_none', 'sp-pagetitle-outer', '', false, true) //Top Title module ?>	
	
	<?php $helix->addModules('slides', 'sp_none', 'sp-slide-grid', '', false, true) //slides module ?>

	<?php $helix->addModules('countdown', 'sp_none', 'sp-countdown-grid', '', false, true) //countdown module ?>
	<?php $helix->addModules('speakers', 'sp_xhtml', 'sp-speakers-grid', '', false, true) //Speakers module ?>
	
	<?php $helix->addModules('bottom1, bottom2, bottom3, bottom4, bottom5, bottom6', 'sp_flat', 'sp-bottom', '30, 40,20', false, true); //positions bottom1-bottom6 ?>
	
	<div class="sp-wrap <?php echo $hideThis; ?> clearfix">
		<?php $helix->loadLayout(); //main layout ?>	
	</div>
	
	<div id="sp-footer-grid" class="clearfix">
		<div class="sp-wrap clearfix">
		<?php $helix->addModules('sponsor') //sponsor module ?>	
		<?php $helix->addModules('ticket') //Ticket Module ?>
			<?php $helix->addModules('breadcrumbs') ?>
			<div id="sp-footer" class="clearfix">
					<?php $helix->addFeatures('copyright,brand,jcredit,validator') /*--- brand, jcredit, validator features ---*/ ?>
					<!-- You need to purchase copyright removal license from http://www.joomshaper.com/pricing?tab=copyright in order to remove brand/www.joomshaper.com link. -->								
			</div>
		</div>
	</div>
	
	<?php $helix->addFeatures('totop') //go to top ?>	
	<?php $helix->addFeatures('analytics,jquery,ieonly'); /*--- analytics, jquery and ieonly features ---*/?>
	<?php $helix->compress(); /* --- Compress CSS and JS files --- */ ?>	
	<?php $helix->getFonts() /*--- Standard and Google Fonts ---*/?>
	<jdoc:include type="modules" name="debug" />
</body>
</html>
