<?php
/*---------------------------------------------------------------
# Package - Joomla Template based on Helix Framework   
# ---------------------------------------------------------------
# Author - JoomShaper http://www.joomshaper.com
# Copyright (C) 2010 - 2013 JoomShaper.com. All Rights Reserved.
# license - PHP files are licensed under  GNU/GPL V2
# license - CSS  - JS - IMAGE files  are Copyrighted material 
# Websites: http://www.joomshaper.com
-----------------------------------------------------------------*/
//no direct accees
defined ('_JEXEC') or die ('resticted aceess');

if ($this->countModules('popup')):
		$this->addJS('sppopup.js');
	?>
	<script type="text/javascript">
		window.addEvent('domready', function() {
			new spPopBoxSlide('.sp-content-box',{
				openTime: 6000
			});
		});
	</script>
	<div class="sp-content-box"> 
		<a href="#" class="sp-pop-close"><?php echo JText::_('SP_CLOSE'); ?></a>
		<a href="#" class="button sp-pop-cookie-close"><?php echo JText::_('SP_DO_NOT_SHOW'); ?></a>
		<?php
			$this->addModules('popup');
		?>
	</div>
	<?php
endif;