/*---------------------------------------------------------------
# Package - Helix Framework  
# ---------------------------------------------------------------
# Author - JoomShaper http://www.joomshaper.com
# Copyright (C) 2010 - 2012 JoomShaper.com. All Rights Reserved.
# license - PHP files are licensed under  GNU/GPL V2
# license - CSS  - JS - IMAGE files  are Copyrighted material 
# Websites: http://www.joomshaper.com
-----------------------------------------------------------------*/
window.addEvent("domready",function(){	
	new Fx.SmoothScroll({ duration: 500, transition: Fx.Transitions.linear}, window);	
	var a = document.id('topofpage');//Goto to Top 
	if (a) {
		a.set('opacity','0').setStyle('display','block');	
		window.addEvent('scroll',function(e) {
			a.fade((window.getScroll().y > 300) ? 'in' : 'out')
		});
		var b = new Fx.Scroll(window);
		a.addEvent('click', function (e) {
			e.stop();
			b.toTop()
		})
	}			
});