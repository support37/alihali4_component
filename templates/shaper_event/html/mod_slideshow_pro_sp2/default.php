<?php
/*
# ------------------------------------------------------------------------
# SlideShow Pro SP2 module for Joomla 2.5
# ------------------------------------------------------------------------
# Copyright (C) 2010 - 2012 JoomShaper.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2. CSS / JS are Copyrighted Commercial,
# Author: JoomShaper.com
# Websites:  http://www.joomshaper.com
# Redistribution, Modification or Re-licensing of this file in part of full, 
# is bound by the License applied. 
# ------------------------------------------------------------------------
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$pre_src='';
?>
<script type="text/javascript">
//<![CDATA[
window.addEvent('domready', function() {
	var sp_slide_content = document.getElements('.sp-slide-content');
	var spSlide<?php echo $uniqid ?> = new slideshow_sp2($('sp-slide<?php echo $uniqid ?>'), {
		size: {width: <?php echo $width ?>, height: <?php echo $height ?>},
		interval: <?php echo $interval ?>,
		fxOptions: {duration:  <?php echo $speed; ?>, transition: Fx.Transitions.<?php echo $transition; ?>},
		transition: <?php echo "'" .$effects. "'"; ?>,
		onWalk: function (i, j) {
			sp_slide_content[j].fade(0);
			sp_slide_content[i].fade(1);
		}	
	});
	
	spSlide<?php echo $uniqid ?>.play();
	
	<?php if ($showarrows) {?>
		spSlide<?php echo $uniqid ?>.addPlayerControls('previous', [$('sp_slide_prev<?php echo $uniqid;?>')]);
		spSlide<?php echo $uniqid ?>.addPlayerControls('next', [$('sp_slide_next<?php echo $uniqid;?>')]);
	<?php } ?>
});

window.addEvent('load', function() {
	document.id('sp-slide<?php echo $uniqid ?>').setStyle('display', ' ');
});
//]]>
</script>
<div class="sp-slide" style="position:relative">
	<div id="sp-slide<?php echo $uniqid ?>" style="overflow:hidden;display:none;position:relative;height:<?php echo $height ?>px;width:<?php echo $width ?>px">
		<?php foreach ($list as $item): ?>
			<div class="sp-slide-content">
				<div class="sp-slide-inner">
					<div class="sp-slide-descs">
						<?php if($showtitle) { ?>
							<?php if($titlelinked) { ?>
								<h2 class="sp-slide-title"><a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></h2>
							<?php } else { ?>
								<h2 class="sp-slide-title"><?php echo $item->title; ?></h2>
							<?php } ?>	
						<?php } ?>
						<?php if($showarticle) { ?>
							<?php echo $item->text; ?>
						<?php } ?>
					</div>	
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
<?php if ($showarrows) { ?>
	<div id="sp_slide_prev<?php echo $uniqid;?>" class="sp_slide_btnprev"></div>
	<div id="sp_slide_next<?php echo $uniqid;?>" class="sp_slide_btnnext"></div>
<?php } ?>	
