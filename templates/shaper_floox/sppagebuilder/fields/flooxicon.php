<?php

/**
 * @package Helix3 Framework
 * @author JoomShaper http://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2017 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or Later
 */
//no direct accees
defined('_JEXEC') or die('resticted aceess');

$flooxiconslist = array(
    '0' => JText::_('COM_SPPAGEBUILDER_ADDON_GLOBAL_NO_FLOOX_ICON'),
    'floox-builder' => 'floox-builder',
    'floox-chat' => 'floox-chat',
    'floox-clock' => 'floox-clock',
    'floox-edit-tools' => 'floox-edit-tools',
    'floox-headphones' => 'floox-headphones',
    'floox-love-fill' => 'floox-love-fill',
    'floox-love-line' => 'floox-love-line',
    'floox-minus' => 'floox-minus',
    'floox-paper-plane' => 'floox-paper-plane',
    'floox-pen-tool' => 'floox-pen-tool',
    'floox-picture' => 'floox-picture',
    'floox-plus' => 'floox-plus',
    'floox-promotion' => 'floox-promotion',
    'floox-search' => 'floox-search',
    'floox-smartphone' => 'floox-smartphone',
    'floox-structure' => 'floox-structure',
);
