<?php
/**
* @package		Joomla.Site
* @subpackage	mod_login
* @copyright	Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
* @license		GNU General Public License version 2 or later; see LICENSE.txt
*/

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');

$user = JFactory::getUser();

?>
<div class="sp-floox-login sp-mod-login">
	<a href="<?php echo JRoute::_('index.php?option=com_users&view=login'); ?>" class="login">
		<i class="fa fa-user-circle-o"></i>
		<?php echo JText::_('FLOOX_LOGIN'); ?>
	</a>/
	<a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>" class="registration">
		<?php echo JText::_('FLOOX_SIGNUP'); ?>
		
	</a>
</div>