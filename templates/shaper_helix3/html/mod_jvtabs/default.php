<?php
$moduleId = 'JVTab'.$module->id;
JVJSLib::add('jquery.plugins.imagesloaded');
JVJSLib::add('jquery.plugins.transform');
$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::root(true).'/modules/mod_jvtabs/assets/css/jvtabs.css');
if((string)$dataConfigs->style != 'none') $doc->addStyleSheet(JUri::root(true)."/modules/mod_jvtabs/assets/css/jvtabs.{$dataConfigs->style}.css");
$doc->addScript(JUri::root(true).'/modules/mod_jvtabs/assets/js/jvtabs.js');
$doc->addScriptDeclaration("
    jQuery(function($){
        new JVTab('#{$moduleId}',{$dataConfigs});
    });
");
?>
<div id="<?php echo $moduleId?>" class="JVTab <?php echo $dataConfigs->suffix?> our-servise-content">
    <div class="JVTab-nav">
        <div class="nav-content">
            <ul class="nav nav-tabs">
                <?php foreach($dataTabs as $tab): ?>
                    <li><a href="#<?php echo $tab->id ?>"><span><?php echo $tab->title ?></span></a></li>
                    <li class="border">.</li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
    <div class="JVTab-content tab-content">
        <?php foreach($dataTabs as $tab): ?>
            <div id="<?php echo $tab->id ?>" class="tab-pane"><?php echo $tab->content ?></div>
        <?php endforeach;?>
    </div>
</div>