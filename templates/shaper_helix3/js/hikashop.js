jQuery(function($){
  // update qty
  	var 
  		aend  = {
  			'WebkitAnimation' : 'webkitAnimationEnd',
			'OAnimation' : 'oAnimationEnd',
			'msAnimation' : 'MSAnimationEnd',
			'animation' : 'animationend'
  		}[Modernizr.prefixed('animation')]
  		,wcart = $('#hikashop_cart_module').on({
	  		'rcart': function(){
	  			$.get(location.href, {}, function(rs){
	  			});	
	  		}
	  	})
	  ;
  	wcart.delegate('[type="text"]', 'keyup', function(){
  		var btnr = $(this)
  		.closest('.shop-card-products-description')
  		.find('[data-remove="item-qcart"]')
  		;
  		if($.isNumeric(this.value) && parseInt(this.value)) {
  			btnr.removeClass('hidden');
  			return;
  		}
  		this.value = 0;
  		btnr.addClass('hidden');
  	}).delegate('[name="hikashop_cart_form"]', 'submit', function(){
  			var 
  				form = $(this);
  			form.closest('.top-bar-nav-shop-card').addClass('process');
  			$.post(this.action, form.serialize()).done(function(data){
  				// update total
  				form.closest('.top-bar-nav-shop-card').removeClass('process');

  				var total=$(data).find('.cart-subtotal .price').html(),
  				citem=$(data).find('.hikashop_small_cart_checkout_link').html(),
				cart_limit=$(data).find('.cart-limit').html();
	  			if(!total){
	  				total=0;
	  				$('.btncarttop .top-bar-nav-shop-card').removeClass('open');
	  			}
	  			$('.cart-subtotal .price').html('<span class="fadeInRight animated">'+total+'</div>');
	  			$('.hikashop_small_cart_checkout_link').html('<span class="fadeInRight animated">'+citem+'</div>');

				wcart.one('update-mini-cart', function(){
					$('.cart-limit').html(cart_limit);
					function resizeInput() {
					  $(this).attr('size', $(this).val().length);
					}
					$('.cart-limit input[type="text"]').keyup(resizeInput).each(resizeInput);
				});
				
                // remove item
  				var isRemove = 0;
				wcart.find('input.hikashop_product_quantity_field').each(function(){
  					if(!this.value || !parseInt(this.value)) {
						isRemove = 1;
  						$(this).closest('.shop-card-products').addClass('fadeOutRight animated').one(aend, function(){
  							$(this).remove();
							wcart.trigger('update-mini-cart');
							
  						});
  					}
  				});
                
				if(!isRemove) { wcart.trigger('update-mini-cart'); }

  			});
  			return false;
  	}).delegate('[data-remove="item-qcart"]', 'click.remove-item', function(){
  		$(this).closest('.shop-card-products').find('[name]').val(0);
  		wcart.find('form').trigger('submit');
  		return false;
  	});

});




jQuery(function($){
  $.extend($.fn, {
    mdrop: function(o){
      var doc = this;
      return doc.delegate(o.mark, ['click', o.mark].join(''), function(){
          var p = $(this).closest('.top-bar-nav-shop-card'), cl = p.find('.cart-limit');
         cl.hasClass('owl-carousel') || doc.trigger('setup-pagi', [cl]);
          p.toggleClass('open');
          
          return false;
        }).on({
          'click.panel-hikashop_small_cart_checkout_link': function(e){
            var target = $(e.target),
              panel = '.'+o.panel,
              sc = target.parents(panel);
            if(target.hasClass(o.panel) || sc.length){
              return;
            }
            $(panel).closest('.top-bar-nav-shop-card').removeClass('open');
          },
          'setup-pagi': function(e, cl){
           cl.owlCarousel({
              singleItem:true,
              pagination:true,
              paginationNumbers:true,
              direction : $("body").hasClass( "rtl" )?'rtl':'ltr'
            });

            function resizeInput() {
              $(this).attr('size', $(this).val().length);
          }
          $('.top-bar-nav-shop-card input[type="text"]').keyup(resizeInput).each(resizeInput);
          }
        });
    }
  });


$(document).mdrop({
      mark: '.hikashop_small_cart_checkout_link',
      panel: 'shop-card'
    });
});


// function hk owlcarousel
(function($){
    $.extend($.fn, {
       syncOwl: function(o) {
        o = $.extend({
                main: {
                    items: 1
               },
                sub: {
                items: 4,
                afterInit : function(el){
                    el.data({
                    items: el.find('.owl-item').each(function(i){ $(this).data({owlItem: i}) })
                   }).data('items').first().addClass("active");
                }
               }
            }, o);

          o.main.afterAction = function(el){                     
                var current = this.currentItem
                    ,sub = el.data('sub');       
                sub.data('items')
                .removeClass("active")
                .eq(current)
                .addClass("active");
                sub.trigger('center', current);
            };

                            
            o.sub.afterInit = function(el){
                el.data({
                items: el.find('.owl-item').each(function(i){ $(this).data({owlItem: i}) })
               }).data('items').first().addClass("active");
            };   

            var mslider = o.main.selector
                ,sslider = o.sub.selector
            ;

            return this.each(function(){
                var  wrap = $(this)
                    ,main = wrap.find(mslider)
                    ,sub = wrap.find(sslider)
               ;  
               sub.owlCarousel(o.sub).delegate('.owl-item', 'click', function(e){
                    e.preventDefault();
                    main.trigger("owl.goTo", $(this).data("owlItem") ); 
               }).on({
                'center': function(e, num) {
                    var sync2 = $(this)
                        ,sync2visible = sync2.data("owlCarousel").owl.visibleItems
                        ,found = $.inArray(num, sync2visible) >  -1 ? 1: 0
                    ;    
                    if(!found){
                        if(num > sync2visible[sync2visible.length-1]){
                            sync2.trigger("owl.goTo", num - sync2visible.length+2);
                        }else{
                            if(num - 1 === -1){
                                num = 0;
                            }
                            sync2.trigger("owl.goTo", num);
                        }
                    } else if(num === sync2visible[sync2visible.length-1]){
                        sync2.trigger("owl.goTo", sync2visible[1]);
                    } else if(num === sync2visible[0]){
                        sync2.trigger("owl.goTo", num-1);
                    }    
                }
               });      console.log(o.main);            
               main.data({sub: sub}).owlCarousel(o.main);
            });     
       } 
    });
})(jQuery);

jQuery(function($){
  $('.jv-slide-hikashop').syncOwl({
     main:{
      selector: '.ntd-owl-mainproduct',
      direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
      items:1,
      singleItem: true
    },
    sub:{
      selector: '.ntd-owl-thumbnailproduct',
      direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
      items :4,
      itemsDesktop      : [1199,4],
      itemsDesktopSmall : [979,4],
      itemsTablet       : [768,4],
      itemsTablet       : [650,8],
      itemsMobile       : [479,4],
      navigation :true,
      navigationText : $("body").hasClass( "rtl" )?['<span class="fa fa-angle-right"></span>','<span class="fa fa-angle-left"></span>']:['<span class="fa fa-angle-left"></span>','<span class="fa fa-angle-right"></span>']
    }
  });

});



/*slider product detail*/
 jQuery(function($){

  function bImage(view){
    view.find('.product-slider').each(function(){
      var el = $(this), thumb = view.find(".shop-slider-pager");
      el.owlCarousel({
        direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
          singleItem:true,
          pagination:false,
          autoHeight:true,
          
          afterAction : syncPosition
        }); 
          thumb.owlCarousel({
            direction : $("body").hasClass( "rtl" )?'rtl':'ltr',
          items : 4,
          itemsDesktop      : [1199,4],
          itemsDesktopSmall : [979,4],
          itemsTablet       : [768,4],
          itemsMobile       : [479,4],
          pagination:false,
          responsiveRefreshRate : 100,
          navigation: true,
        navigationText  : ["<span data-icon='X' class='icon next'></span>","<span data-icon='Q' class='icon prev'></span>"],
          afterInit : function(el){
            el.find(".owl-item").eq(0).addClass("synced");
          }
        });

        function syncPosition(el){
          var current = this.currentItem;
          thumb
            .find(".owl-item")
            .removeClass("synced")
            .eq(current)
            .addClass("synced")
          if(thumb.data("owlCarousel") !== undefined){
            center(current)
          }
        }
   
      thumb.on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).data("owlItem");
        el.trigger("owl.goTo",number);
      });
     
      function center(number){
        var sync2visible = thumb.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for(var i in sync2visible){
          if(num === sync2visible[i]){
            var found = true;
          }
        }
     
        if(found===false){
          if(num>sync2visible[sync2visible.length-1]){
            thumb.trigger("owl.goTo", num - sync2visible.length+2)
          }else{
            if(num - 1 === -1){
              num = 0;
            }
            thumb.trigger("owl.goTo", num);
          }
        } else if(num === sync2visible[sync2visible.length-1]){
          thumb.trigger("owl.goTo", sync2visible[1])
        } else if(num === sync2visible[0]){
          thumb.trigger("owl.goTo", num-1)
        }
        
      }

    });
    view.find('li[rel^="zoom"]').zoom();
    view.find('a[rel^="prettyPhoto"]').prettyPhoto();
  }
  bImage($('.shop-slider-container:not(:hidden)'));


  window.hikashopUpdateVariantData = function(selection) {
    if (selection) {
      var names = ['id', 'name', 'code', 'image', 'price', 'quantity', 'description', 'weight', 'url', 'width', 'length', 'height', 'contact', 'custom_info', 'files'];
      var len = names.length;
      for (var i = 0; i < len; i++) {
        var el = document.getElementById('hikashop_product_' + names[i] + '_main');
        var el2 = document.getElementById('hikashop_product_' + names[i] + selection);
        if (el && el2) {
          
          // quantity
          if(names[i] == 'quantity') {
            var view = $(el), source = $(el2);
            $.each(['.quantity-block', '.hikashop_product_stock'], function(i, mark){
              view.find(mark).html(source.find(mark).html());
            });
          }else {
            el.innerHTML = el2.innerHTML.replace(/_VARIANT_NAME/g, selection);
          }
          
          // image
          names[i] != 'image' || bImage($(el));
        }
      }
      if (typeof this.window['hikashopRefreshOptionPrice'] == 'function') hikashopRefreshOptionPrice();
      if (window.Oby && window.Oby.fireAjax) window.Oby.fireAjax("hkContentChanged");
    }
    return true;
  }

  if ($('.hikashop_product_stock').length) {
    $('#hikashop_product_quantity_field_change_minus_1').appendTo("#quantity-block");
    $('#hikashop_product_quantity_field_1').appendTo("#quantity-block");
    $('#hikashop_product_quantity_field_change_plus_1').appendTo("#quantity-block");
  };
  $("#itemProductToltalVote").html($(".hikashop_total_vote .hasTooltip").html());
});



