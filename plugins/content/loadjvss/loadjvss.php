<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.loadjvss
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Plug-in to enable loading modules into content (e.g. articles)
 * This uses the {loadjvss} syntax
 *
 * @since  1.5
 */
class PlgContentLoadjvss extends JPlugin
{
	protected static $modules = array();

	/**
	 * Plugin that loads module positions within content
	 *
	 * @param   string   $context   The context of the content being passed to the plugin.
	 * @param   object   &$article  The article object.  Note $article->text is also available
	 * @param   mixed    &$params   The article params
	 * @param   integer  $page      The 'page' number
	 *
	 * @return  mixed   true if there is an error. Void otherwise.
	 *
	 * @since   1.6
	 */
	public function onContentPrepare($context, &$article, &$params, $page = 0)
	{
		// Don't run this plugin when the content is being indexed
		if ($context == 'com_finder.indexer')
		{
			return true;
		}

		// Simple performance check to determine whether bot should process further
		if (strpos($article->text, 'loadjvss') === false)
		{
			return true;
		}

		// Expression to search for (positions)
		$regex		= '/{loadjvss\s(.*?)}/i';


		// Find all instances of plugin and put in $matches for loadposition
		// $matches[0] is full pattern match, $matches[1] is the position
		preg_match_all($regex, $article->text, $matches, PREG_SET_ORDER);

		// No matches, skip this
		if ($matches)
		{
			foreach ($matches as $match)
			{
				$matcheslist = explode(',', $match[1]);

				$id = trim($matcheslist[0]);

				$output = $this->_load($id);

				// We should replace only first occurrence in order to allow positions with the same name to regenerate their content:
				$article->text = preg_replace("|$match[0]|", addcslashes($output, '\\$'), $article->text, 1);
			}
		}

	}

	/**
	 * Loads and renders
	 *
	 * @param   string  $id  The id assigned to the item of component jvss 
	 *
	 * @return  string
	 *
	 * @since   1.6
	 */
	protected function _load($id)
	{
		// Include the syndicate functions only once
		require_once JPATH_SITE . '/components/com_jvss/helpers/jvss.php';
		JHtml::_('behavior.framework');
		JHtml::_('jquery.framework');

		$assets = JUri::root()."components/com_jvss/assets/";
		JHtml::stylesheet("{$assets}css/settings.css");
		JHtml::script("{$assets}js/jquery.themepunch.tools.min.js");
		JHtml::script("{$assets}js/jquery.themepunch.revolution.min.js");

		$slider = JvssFrontendHelper::getSS($id);
		ob_start();
		require(JPATH_SITE."/plugins/content/loadjvss/tmpl/default.php");
		return ob_get_clean();
	}

	
}
