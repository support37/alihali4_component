<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_jvss
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<?php if($slider) :?>
<?php $config = $slider->sconfig?>
<?php $sid = JvssFrontendHelper::getPrefixSlider()?>
<?php $awid = "rev_slider_{$sid}_wrapper"?>
<?php $asid = "rev_slider{$sid}"?>
<?php $zstyle = array();?>
<div id="<?php echo $awid?>" 
class="<?php echo JvssFrontendHelper::getWrapperClass($slider->sconfig)?>"
style="<?php echo JvssFrontendHelper::getCssInlineWrapper($slider->sconfig)?>">
	<div id="<?php echo $asid?>" 
    class="<?php echo JvssFrontendHelper::getWrapperClass($slider->sconfig, 'slider')?>"
    style="<?php echo JvssFrontendHelper::getCssInlineWrapper($slider->sconfig, 'slider')?>">
		<ul>
		<?php $index = 0; foreach($slider->params as $k=>$slide):?>
            <?php if( isset( $slide[ 'state' ] ) && intval( $slide[ 'state' ] ) ):?>
			<li 
            <?php if( isset( $slide['id'] ) ):?>
            id="<?php echo $slide['id']?>" 
            <?php endif;?>
             <?php if( isset( $slide['zclass'] ) ):?>
			class="<?php echo $slide['zclass']?>" 
            <?php endif;?>
			<?php if( isset( $slide['attr'] ) ):?>
            <?php echo $slide['attr']?>
            <?php endif;?>
            <?php echo JvssFrontendHelper::getSlideTransition( $slide, $config, $index );?>
            <?php $index ++; ?>
			<?php if( isset( $slide['slotamount'] ) ): ?>
            data-slotamount="<?php echo $slide['slotamount']?>" 
            <?php endif;?>
            <?php if( isset( $slide['masterspeed'] ) ): ?>
			data-masterspeed="<?php echo $slide['masterspeed']?>" 
            <?php endif;?>
            <?php if( isset( $slide['title'] ) ):?>
            data-title="<?php echo $slide['title']?>" 
            <?php endif;?>             
			>
				<!-- MAIN IMAGE -->
				<img src="<?php echo $slide['bgsrc']?>"  
				alt="slider-<?php echo $k?>"  
				<?php echo JvssFrontendHelper::getKenburn( $slide )?> 
				data-bgrepeat="<?php echo $slide['bgrepeat']?>">
                
                <?php if( isset($slide['items']) && is_array($slide['items']) && count($slide['items']) ):?>
                <?php foreach( $slide['items'] as $id => $layer ):?>
                    <div
                    <?php if( isset( $layer['zid'] ) ): ?>id="<?php echo $layer['zid']?>"<?php endif;?> 
                    class="<?php echo JvssFrontendHelper::getLayerClass( $layer )?>"
                    <?php echo JvssFrontendHelper::getPosition( $layer ) ?>
                    <?php if( $layer['easing'] ): ?>
                    <?php echo "data-easing='{$layer['easing']}'\n"?>
                    <?php endif;?>
                    <?php if( $transitin = JvssFrontendHelper::getTransitCustom( $layer, 'in' ) ): ?>
                    <?php echo "data-customin='{$transitin}'\n";?>
                    <?php endif;?>
                    <?php if( isset( $layer['splitin'] ) ): ?>
                    <?php echo "data-splitin='{$layer['splitin']}'\n"?>
                    <?php endif;?>
                    <?php if( isset( $layer['splitout'] ) ): ?>
                    <?php echo "data-splitout='{$layer['splitout']}'\n"?>
                    <?php endif;?>
                    <?php if( isset( $layer['easingout'] ) ): ?>
                    <?php echo "data-endeasing='{$layer['easingout']}'\n"?>
                    <?php endif;?>
                    <?php if( $transitout = JvssFrontendHelper::getTransitCustom( $layer, 'out' ) ): ?>
                    <?php echo "data-customout='{$transitout}'\n"?>
                    <?php endif;?>
                    <?php echo JvssFrontendHelper::getLayerTime( $layer, $config )?>
                    <?php echo JvssFrontendHelper::getSplitDelay( $layer )?>
                    
                    style="<?php echo JvssFrontendHelper::getLayerStyle( $layer )?>"
                    >
                    <?php if( isset( $layer['loop'] )): ?>
                    <?php $loop_type = $layer['loop']['loop_type']?>
                    <?php if( isset( $layer['loop'][ $loop_type ] ) ):?>
                    <div class="tp-layer-inner-rotation <?php echo "{$layer['zstyle']} {$loop_type}"?>"
                    <?php if( $loop_easing = $layer['loop']['loop_easing']): ?>
                    data-easing="<?php echo $loop_easing?>"
                    <?php endif;?>
                    <?php if( $loop_easing = $layer['loop']['speed']): ?>
                    data-speed="<?php echo $loop_easing?>"
                    <?php endif;?>
                    <?php foreach( $layer['loop'][ $loop_type ] as $l_key => $l_item): ?>
                        <?php $l_key = JvssFrontendHelper::getMapKeyLoop( $l_key )?>
                        <?php echo "data-{$l_key}='{$l_item}'"?>
                    <?php endforeach;?>
                    >
                    <?php endif;?>
                    <?php endif;?>
                        <?php echo $layer['content']?>
                    
                    <?php if( isset( $layer['loop'] ) ): ?>
                    <?php if( isset( $layer['loop'][ $loop_type ] ) ):?>
                    </div>
                    <?php endif;?>
                    <?php endif;?>
                    </div>
                    <?php if( $layer['zstyle'] ):?><?php array_push($zstyle, ".tp-caption.{$layer['zstyle']}" )?><?php endif;?>
                <?php endforeach;?>
                <?php endif;?>
			</li>
            <?php endif;?>
		<?php endforeach;?>
		</ul>
        <div class="tp-bannertimer <?php echo JvssFrontendHelper::getTimebarPosition( $config )?>"></div>
	</div>
</div>

<style type="text/css">

<?php if( $styles = JvssFrontendHelper::getStyle( $zstyle ) ):?>
/* css of slide */
<?php echo $styles;?>
<?php endif;?>

<?php if( $slider->customcss):?>
/* css slider custom */
<?php echo $slider->customcss?>
<?php endif;?>

<?php if( $config->slider_type === 'responsitive' ): ?>
<?php printf('#%s,#%s { width:%dpx; height:%dpx; }', $awid, $asid, $config->width, $config->height)?>
<?php foreach( JvssFrontendHelper::getResponsitiveValues( $config ) as $item):?>
<?php 
$strMaxWidth = "";                                            
if($item["maxWidth"] >= 0) {
    $strMaxWidth = "and (max-width: {$item["maxWidth"]}px)";
}
?>
@media only screen and (min-width: <?php echo $item["minWidth"]?>px) <?php echo $strMaxWidth?> {
    <?php printf('#%s,#%s { width:%dpx; height:%dpx; }', $awid, $asid, $item["sliderWidth"], $item["sliderHeight"])?>
}
<?php endforeach;?>
<?php endif;?>
</style>

<script type="text/javascript">
	jQuery(function($) {
        var c = (function(p){ 
            return p || {} ;
        })(<?php echo JvssFrontendHelper::getConfig($config)?>);
		$('#<?php echo "rev_slider{$sid}"?>').revolution(c);
	});	
</script>
<?php endif;?>