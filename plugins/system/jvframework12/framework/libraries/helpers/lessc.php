<?php
/**
 # JV Framework
 # @version		2.5.x
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
 */
defined ( '_JEXEC' ) or die ( 'Restricted access' );


class JVFrameworkHelperLessc extends JVFrameworkHelper {
	protected $_name = 'lessc';
    public    $lessc = null;

    public function __construct(){
        parent::__construct();
        if(!class_exists('lessc')){
            require_once JV::_('path.findPath', 'classes::lessphp/lessc.php');
        }
        $this->lessc = new lessc();
        $this->lessc->setFormatter('compressed');
    }

    public function compile($data, $output, $vars = array(), $inline = false){
        $this->lessc->setImportDir(JV::helper('path')->getPath('less'));
        $this->lessc->setVariables($vars);

        if($inline){
            $content = $this->lessc->compile($data);
            $content = $this->write($output, $content);
        }else{
            $file = is_file($data) ? $data: JV::helper('path')->findPath('less::'.$data);
            $content = $this->autoCompileLess($file, $output);
        }

        return $content;
    }

    public function autoCompileLess($input, $output) {
        // load the cache
        $cacheFile = md5($input).".cache";
        if ($cache = $this['path']->read('caches::jv/'.$cacheFile)) {
            $cache = unserialize($cache);
        }else{
            $cache = $input;
        }
        $newCache = $this->lessc->cachedCompile($cache);

        if (!is_array($cache) || $newCache["updated"] > $cache["updated"]) {
            $this['path']->write('caches::jv/'.$cacheFile, serialize($newCache));

            if(!is_null($output))
                $this->write($output, $newCache['compiled']);
        }

        return $this['path']->url('theme::css-compiled/'.$output);
    }

    public function write($fileName, $content){
        $html = '<html><body></body></html>';
        if(!$this['path']->findPath('theme::css-compiled/index.html')){
            $this['path']->write('theme::css-compiled/index.html', $html);
        }
        if(!$this['path']->findPath('caches::jv/index.html')){
            $this['path']->write('caches::jv/index.html', $html);
        }

        $this['path']->write('theme::css-compiled/'.$fileName, $content);

        return $this['path']->url('theme::css-compiled/'.$fileName);
    }

    public function __call($name, $arguments){
        if(method_exists($this, $name)){
            return call_user_func(array($this, $name), $arguments);
        }else{
            if(is_callable(array($this->lessc, $name))){
                return call_user_func(array($this->lessc, $name), $arguments);
            }
        }

        return false;
    }

    public function createLess($name){
        $less = new JVFrameworkLess($name);
        return $less;
    }
}

class JVFrameworkLess{
    protected $name = '';
    protected $less = array();

    public function __construct($name){
        $this->name = $name;
    }

    public function add($input){
        $this->less[] = "{$input}"; return $this;
    }

    public function import(){
    $args = func_get_args();
    if(count($args)){
        foreach($args as $input){
            $this->less[] = "@import \"{$input}\";";
        }
    }
    return $this;
}

    public function mixin($input){
        $this->less[] = "{$input}"; return $this;
    }

    public function comment($input){
        $this->less[] = "// {$input}"; return $this;
    }

    public function write($output){
        $content = (string) $this;
        $name = md5(serialize($content));
        if(!JV::helper('path')->findPath("caches::jv/{$name}.cache")){
            JV::helper('path')->write("caches::jv/{$name}.cache", $content);
            JV::helper('path')->write("less::{$output}", $content);
        }

        return $output;
    }

    public function __toString(){
        return implode("\n",$this->less);
    }
}