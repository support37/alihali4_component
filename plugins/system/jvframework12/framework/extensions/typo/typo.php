<?php
/**
 # JV Framework
 # @version		1.5.x
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
 */
defined ( '_JEXEC' ) or die ( 'Restricted access' );	

class JVFrameworkExtensionTypo extends JVFrameworkExtension{
	protected static $replacements = array();
	
	public function beforeRender(){
		if ($this ['option']->isRTL ()) {
			$this['asset']->addStyle($this['path']->url('extensions::typo/assets/css/typo.rtl.css'));
		}else{
			$this['asset']->addStyle($this['path']->url('extensions::typo/assets/css/typo.css'));
		}		
	}
	
	/**
	 * Framework Typography
	 */
	public function onContentPrepare($context, &$article, &$params, $page = 0) {
		$bbcode = JV::helper('bbcode');
		$bbcode->setReplacement($this->getReplacement ());

		$article->text = $bbcode->replace ( $article->text );	
	}	

	/**
	 * Framework Typography replacement
	 */
	protected function getReplacement() {
		if(!self::$replacements){
			$db = JFactory::getDbo ();
			$db->setQuery ( 'SELECT * FROM #__jv_typos WHERE published = 1' );
			self::$replacements = $db->loadObjectList ();
		}
		
		return self::$replacements;
		
	}
}