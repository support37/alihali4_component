<?php
/**
 # JV Framework
 # @version		1.5.x
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
 */
defined ( '_JEXEC' ) or die ( 'Restricted access' );	

class JVFrameworkExtensionJSCustom extends JVFrameworkExtension{

	function afterRender() {
		$jcarousel = json_decode($this['option']->get('jcarousel_params'));
        $select2 = $this['option']->get('select2');
        if(!empty($select2)){
            
        }
		$jquery_tab = $this['option']->get('load_jquery_tab');
		
		if(count($jcarousel)  > 0){   
                    //$this['asset']->addStyle($this['path']->url('extensions::jscustom/assets/css/jcarousel.css'));
                    JVJSLib::add('jquery.plugins.imagesloaded');
                    $this['asset']->addScript($this['path']->url('extensions::jscustom/assets/js/jquery.jcarousel.min.js'));
                        $js='';
			foreach($jcarousel->list as $ca){
                $param = $this->getParam($ca);
                if(!$param) $param = '';
                $js .= " $('$ca->element').hide().imagesLoaded(function(){ $(this).show().jcarousel({{$param}});});";
            }
                $this['asset']->addInlineScript("jQuery(function($){   {$js} });" );	
		}
        
        
	}
        
    function getParam($param){
       $data = array();
       foreach($param as $c=>$x){
           if($c !='element' && $c !='title' && $c !='height' ){
               if($x != '' && $x){
                   if($c=='wrap') $data[] = "$c : '$x' ";
                   else $data[] = "$c : $x ";
               }
           }
       }
       if($data) return implode (',', $data);
       return false;
    }
}