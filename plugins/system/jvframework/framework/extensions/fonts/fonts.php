<?php
/**
 # JV Framework
 # @version		1.5.x
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
 */
defined ( '_JEXEC' ) or die ( 'Restricted access' );	

class JVFrameworkExtensionFonts extends JVFrameworkExtension{

	function afterRender() {
            $config = new JVCustomParam($this['option']->get('fonts'));

            $doc = JFactory::getDocument();
            foreach($config as $font){
                if(!count($font->assign)) break;
				$weight = array();
				if($font->weight) foreach ($font->weight as $x){
					$weight[] = $x;
				}
				$jfont = preg_replace ('/\s+/','+',$font->font);
				if($weight) $doc->addStyleSheet('http://fonts.googleapis.com/css?family='.$jfont . ':' . implode (',',$weight) );
                else $doc->addStyleSheet('http://fonts.googleapis.com/css?family='.$jfont);
                $fonts = $font->assign->restore();
                $doc->addStyleDeclaration(implode(',', $fonts)."{font-family: '{$font->font}',  serif;}");
            }
	}
}