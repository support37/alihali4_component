CREATE TABLE IF NOT EXISTS `#__jvportfolio_item` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`name` VARCHAR(255)  NOT NULL ,
`image` VARCHAR(255)  NOT NULL ,
`desc` TEXT NOT NULL ,
`link` VARCHAR(255)  NOT NULL ,
`tag` TEXT NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`date_created` DATETIME NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE `#__jvportfolio_liked` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pfid` int(11) DEFAULT NULL COMMENT 'Portfolio id',
  `u` varchar(255) DEFAULT NULL COMMENT 'User created',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
