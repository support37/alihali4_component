<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

jimport('joomla.application.component.view');

class FView extends JViewLegacy {

    function __construct($config) {
        parent::__construct($config);
    }

    public function display($tpl = null, $cachable = false, $urlparams = false) {
        $view = JRequest::getVar('view');
        $arrView = explode(".", $view);
        $childView = null;
        if (count($arrView) >= 2) {
            $childView = $arrView[1];
        }
        $this->_setLayoutPath($childView);
        if ($arrView[0] != '') {
            HelperCreditbureau::addSubMenu($arrView[0] . '.display');
        } else {
            HelperCreditbureau::addSubMenu('creditbureau.display');
        }
        if (method_exists($this, "view_" . $childView)) {
            $view_method = "view_" . $childView;
            $this->$view_method();
            parent::display();
        } else {
            $this->_setLayoutPath('list');
            $this->view_list();
            parent::display();
            return;
        }
    }

    protected function _setLayoutPath($childView) {
        parent::_setPath('template', $this->_basePath . '/views/' . $this->getName() . '/tmpl/' . $childView);
    }

}

?>
