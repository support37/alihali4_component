DROP TABLE IF EXISTS `#__cb_areas`;

DROP TABLE IF EXISTS `#__cb_areas_projects`;

DROP TABLE IF EXISTS `#__cb_audit`;

DROP TABLE IF EXISTS `#__cb_client_det`;

DROP TABLE IF EXISTS `#__cb_client_det_projects`;

DROP TABLE IF EXISTS `#__cb_client_info`;

DROP TABLE IF EXISTS `#__cb_client_info_projects`;

DROP TABLE IF EXISTS `#__cb_gar_det`;

DROP TABLE IF EXISTS `#__cb_gar_det_projects`;

DROP TABLE IF EXISTS `#__cb_gar_info`;

DROP TABLE IF EXISTS `#__cb_gar_info_projects`;

DROP TABLE IF EXISTS `#__cb_projects`;

