CREATE TABLE IF NOT EXISTS `#__cb_areas` (
    `ar_id` int(10) NOT NULL AUTO_INCREMENT ,
    `ar_aname` varchar(255) NULL,
    `ar_ename` varchar(255) NULL,
    PRIMARY KEY  (`ar_id`)
) DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__cb_areas_projects` (
    `id` int(11) NOT NULL AUTO_INCREMENT ,
    `area_id` int(11) NULL,
    `project_id` int(11) NULL,
    PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__cb_audit` (
    `au_id` int(11) NOT NULL AUTO_INCREMENT ,
    `user_id` int(10) NULL,
    `search_cond` longtext NULL,
    `search_time` datetime NULL,
    PRIMARY KEY  (`au_id`)
) DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__cb_client_det` (
    `client_det_id` int(11) NOT NULL AUTO_INCREMENT ,
    `user_id` int(11) NULL,
    `loan_date` datetime NULL,
    `loan_amount` double NULL,
    `loan_status` varchar(20) NULL COMMENT 'paid or unpaid',
    `late_days` double NULL,
    `id` varchar(255) NULL,
    `c_aname` varchar(255) NULL,
    `c_ename` varchar(255) NULL,
    `a_address` varchar(255) NULL,
    `e_address` varchar(255) NULL,
    PRIMARY KEY  (`client_det_id`)
) DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__cb_client_det_projects` (
    `id` int(11) NOT NULL AUTO_INCREMENT ,
    `client_det_id` int(11) NULL,
    `project_id` int(11) NULL,
    PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__cb_client_info` (
    `client_info_id` int(11) NOT NULL AUTO_INCREMENT ,
    `user_id` int(11) NULL,
    `c_aname` varchar(255) NULL,
    `c_ename` varchar(255) NULL,
    `loans_no` decimal(2,0) NULL,
    `last_loan_st` varchar(15) NULL COMMENT 'paid or unpaid',
    `card_no` varchar(255) NULL,
    `id` decimal(38,0) NULL,
    `black_list` varchar(10) NULL COMMENT 'yes or no',
    `a_address` varchar(250) NULL,
    `e_address` varchar(255) NULL,
    PRIMARY KEY  (`client_info_id`)
) DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__cb_client_info_projects` (
    `id` int(11) NOT NULL AUTO_INCREMENT ,
    `client_info_id` int(11) NOT NULL,
    `project_id` int(11) NOT NULL,
    PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__cb_gar_det` (
    `gar_det_id` int(11) NOT NULL AUTO_INCREMENT ,
    `user_id` int(11) NULL,
    `gar_aname` varchar(255) NULL,
    `gar_amount` double NULL,
    `id` varchar(255) NULL,
    `gar_date` datetime NULL,
    `gar_ename` varchar(255) NULL,
    PRIMARY KEY  (`gar_det_id`)
) DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__cb_gar_det_projects` (
    `id` int(11) NOT NULL AUTO_INCREMENT ,
    `gar_det_id` int(11) NULL,
    `project_id` int(11) NULL,
    PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__cb_gar_info` (
    `gar_info_id` int(11) NOT NULL AUTO_INCREMENT ,
    `user_id` int(11) NULL,
    `g_aname` longtext NULL,
    `g_ename` longtext NULL,
    `act` varchar(255) NULL,
    `id` varchar(255) NOT NULL DEFAULT '0',
    `loans_no` int(5) NOT NULL DEFAULT '0',
    `card_no` varchar(255) NULL,
    `black_list` varchar(255) NULL COMMENT 'yes or no',
    PRIMARY KEY  (`gar_info_id`)
) DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__cb_gar_info_projects` (
    `id` int(11) NOT NULL AUTO_INCREMENT ,
    `gar_info_id` int(11) NULL,
    `project_id` int(11) NULL,
    PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__cb_projects` (
    `project_id` int(10) NOT NULL AUTO_INCREMENT ,
    `p_aname` longtext NULL,
    `p_ename` longtext NULL,
    `a_address` varchar(255) NULL,
    `e_address` varchar(255) NULL,
    PRIMARY KEY  (`project_id`)
) DEFAULT CHARSET=utf8; 

