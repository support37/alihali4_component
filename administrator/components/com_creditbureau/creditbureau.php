<?php


defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

if (!defined('DS')) {

    define('DS', DIRECTORY_SEPARATOR);

}

JLoader::register('FController', JPATH_COMPONENT_ADMINISTRATOR . DS . 'mvc' . DS . 'controller.class.php');
JLoader::register('FModel', JPATH_COMPONENT_ADMINISTRATOR . DS . 'mvc' . DS . 'model.class.php');
JLoader::register('FView', JPATH_COMPONENT_ADMINISTRATOR . DS . 'mvc' . DS . 'view.class.php');
JLoader::register('FTable', JPATH_COMPONENT_ADMINISTRATOR . DS . 'mvc' . DS . 'table.class.php');

if (!defined('JPATH_HELPER')) {
    define('JPATH_HELPER', JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers');
}
include_once (JPATH_HELPER . DS . 'helper.php');
include_once (JPATH_HELPER . DS . 'cb_areas.php');
include_once (JPATH_HELPER . DS . 'cb_areas_projects.php');
include_once (JPATH_HELPER . DS . 'cb_audit.php');
include_once (JPATH_HELPER . DS . 'cb_client_det.php');
include_once (JPATH_HELPER . DS . 'cb_client_det_projects.php');
include_once (JPATH_HELPER . DS . 'cb_client_info.php');
include_once (JPATH_HELPER . DS . 'cb_client_info_projects.php');
include_once (JPATH_HELPER . DS . 'cb_gar_det.php');
include_once (JPATH_HELPER . DS . 'cb_gar_det_projects.php');
include_once (JPATH_HELPER . DS . 'cb_gar_info.php');
include_once (JPATH_HELPER . DS . 'cb_gar_info_projects.php');
include_once (JPATH_HELPER . DS . 'cb_projects.php');
$pathTable = JPATH_COMPONENT_ADMINISTRATOR . DS . 'tables';
HelperCreditbureau::includeFileInFolder($pathTable);

if (JRequest::getVar('task') == null) {
    JRequest::setVar('task', 'creditbureau.display');
}

$controller = FController::getInstance('Creditbureau');
$task = (JRequest::getVar('task') != '') ? JRequest::getVar('task') : 'display';
if ($task && strpos($task, '.') !== false) {
    $valueTask = explode('.', $task);
    $doTask = $valueTask[1];
} else {
    $doTask = $task;
}
JHtml::_('behavior.framework', true);
$controller->execute($doTask);
$controller->redirect();
?>