<?php


defined('_JEXEC') or die('Restricted access');

class CreditbureauViewGarinfo extends FView {

    public function display($tpl = null, $cachable = false, $urlparams = false) {
        if (JRequest::getvar('view') == 'garinfo.detail') {
            JToolBarHelper::title(JText::_("COM_CREDITBUREAU_EDIT"));
            JToolBarHelper::save('garinfo.update');
            JToolBarHelper::cancel('garinfo.cancel');
        } elseif (JRequest::getvar('view') == 'garinfo.form') {
            JToolBarHelper::title(JText::_("COM_CREDITBUREAU_ADD"));
            JToolBarHelper::save('garinfo.add');
            JToolBarHelper::cancel('garinfo.cancel');
        } else {
            JToolBarHelper::title(JText::_('COM_CREDITBUREAU_GARINFO_MENU'));
            JToolBarHelper::addNew('garinfo.addnew');
            JToolBarHelper::editList('garinfo.edit');
            JToolBarHelper::deleteList(JText::_('COM_CREDITBUREAU_DELETE'), 'garinfo.delete');
        }
        parent::display($tpl, $cachable, $urlparams);
    }

    public function view_list() {
        $model = $this->getModel('garinfo', 'CreditbureauModel');
        $paging = "";
        $rows = $model->getGarInfos($paging);
        $this->assignRef('rows', $rows);
        $this->assignRef('pages', $paging);
    }

    public function view_detail() {
        $id = JRequest::getVar('gar_info_id');
        $row = Cbgarinfo::getInfoById($id);
        $this->assignRef('rows', $row);
    }

    public function view_form() {
        
    }

}

?>