<form id="adminForm" name="adminForm" action="index.php?option=com_creditbureau&task=garinfo.display&view=garinfo.list" method="POST">
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_AUDIT_USERNAME'); ?>:</label>
        <?php
        $users = HelperCreditbureau::renderUserSelect();
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_EN'); ?>:</label>
        <?php
        $areas = Cbprojects::getAll();
        HelperCreditbureau::renderSelectWithObject('project_id', $areas, 'project_id', 'p_ename', 0, true);
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_ANAME'); ?>: </label>
        <input type="text" required="true" name="g_aname" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_ENAME'); ?>:</label>
        <input type="text" name="g_ename" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_ACT'); ?>: </label>
        <input type="text" name="act" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_ID'); ?>: </label>
        <input type="text" name="id" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_LOANNO'); ?>: </label>
        <input type="number" name="loans_no" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_CARDNO'); ?>: </label>
        <input type="text" name="card_no" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_BLACKLIST'); ?>: </label>
        <?php echo HelperCreditbureau::renderSelect('black_list', HelperCreditbureau::blackList()); ?>
    </div>
    <input type="hidden" name="option" value="com_creditbureau"/>
    <input type="hidden" name="task" value=""/> 
</form>