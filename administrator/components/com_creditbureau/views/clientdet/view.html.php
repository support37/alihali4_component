<?php

defined('_JEXEC') or die('Restricted access');

class CreditbureauViewClientdet extends FView {

    public function display($tpl = null, $cachable = false, $urlparams = false) {
        if (JRequest::getvar('view') == 'clientdet.detail') {
            JToolBarHelper::title(JText::_("COM_CREDITBUREAU_EDIT"));
            JToolBarHelper::save('clientdet.update');
            JToolBarHelper::cancel('clientdet.cancel');
        } elseif (JRequest::getvar('view') == 'clientdet.form') {
            JToolBarHelper::title(JText::_("COM_CREDITBUREAU_ADD"));
            JToolBarHelper::save('clientdet.add');
            JToolBarHelper::cancel('clientdet.cancel');
        } else {
            JToolBarHelper::title(JText::_('COM_CREDITBUREAU_CLIENTDET_MENU'));
            JToolBarHelper::addNew('clientdet.addnew');
            JToolBarHelper::editList('clientdet.edit');
            JToolBarHelper::deleteList(JText::_('COM_CREDITBUREAU_DELETE'), 'clientdet.delete');
        }
        parent::display($tpl, $cachable, $urlparams);
    }

    public function view_list() {
        $model = $this->getModel('clientdet', 'CreditbureauModel');
        $paging = "";
        $rows = $model->getClientDets($paging);
        $this->assignRef('rows', $rows);
        $this->assignRef('pages', $paging);
    }

    public function view_detail() {
        $id = JRequest::getVar('client_det_id');
        $row = Cbclientdet::getInfoById($id);
        $this->assignRef('rows', $row);
    }

    public function view_form() {
        
    }

}

?>