<form id="adminForm" name="adminForm" action="index.php?option=com_creditbureau&task=clientdet.display&view=clientdet.list" method="POST">
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_AUDIT_USERNAME'); ?>:</label>
        <?php
        $users = HelperCreditbureau::renderUserSelect();
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_EN'); ?>:</label>
        <?php
        $areas = Cbprojects::getAll();
        HelperCreditbureau::renderSelectWithObject('project_id', $areas, 'project_id', 'p_ename', 0, false);
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_ANAME'); ?>: </label>
        <input type="text" name="c_aname" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_ENAME'); ?>:</label>
        <input type="text" name="c_ename" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_LOANDATE'); ?>: </label>
        <?php echo JHtml::_('calendar', 'NOW', 'loan_date', 'loan_date', '%Y-%m-%d', ''); ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_LOANSTATUS'); ?>: </label>
        <?php echo HelperCreditbureau::renderSelect('loan_status', HelperCreditbureau::statusPaidUnpaid()); ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_LOANAMOUNT'); ?>: </label>
        <input type="number" name="loan_amount" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_ID'); ?>: </label>
        <input type="text" name="id" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_LATEDAY'); ?>: </label>
        <input type="number" name="late_days" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_A_ADDRESS'); ?>: </label>
        <input type="text" name="a_address" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_E_ADDRESS'); ?>: </label>
        <input type="text" name="e_address" size="100" value=""/>
    </div>

    <input type="hidden" name="option" value="com_creditbureau"/>
    <input type="hidden" name="task" value=""/> 
</form>