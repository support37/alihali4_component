<?php
defined('_JEXEC') or die('Restricted access');
?>

<form method="post" name="adminForm" id="adminForm">
    <div id="filter-bar" class="btn-toolbar">
        <div class="btn-group pull-right hidden-phone">
            <label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
            <?php echo $this->pages->getLimitBox(); ?>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th width="3%">
                    <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                </th>
                <th width="2%">#</th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_AUDIT_USERNAME'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_ANAME'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_ENAME'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_LOANDATE'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_LOANAMOUNT'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_LOANSTATUS'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_LATEDAY'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_ID'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_A_ADDRESS'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_E_ADDRESS'); ?></th>
            </tr>
        </thead>
        <?php
        $k = 0;
        for ($i = 0; $i < count($this->rows); $i++) {
            $rows = $this->rows[$i];
            ?>
            <tr class="<?php echo "row$k"; ?>">
                <td align="center">
                    <?php echo JHTML::_('grid.id', $i, $rows->client_det_id); ?>
                </td>
                <td align="center">
                    <a href="<?php echo JRoute::_('index.php?option=com_creditbureau&task=clientdet.display&view=clientdet.detail&client_det_id=' . $rows->client_det_id) ?>">
                        <?php echo $rows->client_det_id; ?>
                    </a>

                </td>
                <td><?php echo $rows->username; ?></td>
                <td><?php echo $rows->c_aname; ?></td>
                <td><?php echo $rows->c_ename; ?></td>
                <td><?php echo HelperCreditbureau::formatDate($rows->loan_date,'d/m/Y'); ?></td>
                <td><?php echo $rows->loan_amount; ?></td>
                <td><?php echo $rows->loan_status; ?></td>
                <td><?php echo $rows->late_days; ?></td>
                <td><?php echo $rows->id; ?></td>
                <td><?php echo $rows->a_address; ?></td>
                <td><?php echo $rows->e_address; ?></td>
            </tr>
            <?php
            $k = 1 - $k;
        }
        ?>
        <tfoot>
            <tr>
                <td colspan="14">
                    <?php
                    if ($this->pages):
                        echo $this->pages->getListFooter();
                    endif;
                    ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="task" value="clientdet.displays"/>
</form>