<form id="adminForm" name="adminForm" class="form-validate" action="index.php?option=com_creditbureau&task=clientdet.display&view=clientdet.list" method="POST">
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_AUDIT_USERNAME'); ?>:</label>
        <?php
        $users = HelperCreditbureau::renderUserSelect($this->rows->user_id);
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_EN'); ?>:</label>
        <?php
        $areas = Cbprojects::getAll();
        $exist = Cbclientdetprojects::arrayProjectClientDet($_GET['client_det_id']);
        HelperCreditbureau::renderSelectWithObject('project_id', $areas, 'project_id', 'p_ename', 0, false, $exist);
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_ANAME'); ?>: </label>
        <input type="text" name="c_aname" size="100" value="<?php echo $this->rows->c_aname; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_ENAME'); ?>:</label>
        <input type="text" name="c_ename" size="100" value="<?php echo $this->rows->c_ename; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_LOANDATE'); ?>: </label>
        <?php echo JHtml::_('calendar', $this->rows->loan_date, 'loan_date', 'loan_date', '%Y-%m-%d', ''); ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_LOANSTATUS'); ?>: </label>
        <?php echo HelperCreditbureau::renderSelect('loan_status', HelperCreditbureau::statusPaidUnpaid(), $this->rows->loan_status); ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_LOANAMOUNT'); ?>: </label>
        <input type="number" name="loan_amount" size="100" value="<?php echo $this->rows->loan_amount; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_ID'); ?>: </label>
        <input type="text" name="id" size="100" value="<?php echo $this->rows->id; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_LATEDAY'); ?>: </label>
        <input type="number" name="late_days" size="100" value="<?php echo $this->rows->late_days; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_A_ADDRESS'); ?>: </label>
        <input type="text" name="a_address" size="100" value="<?php echo $this->rows->a_address; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTDET_E_ADDRESS'); ?>: </label>
        <input type="text" name="e_address" size="100" value="<?php echo $this->rows->e_address; ?>"/>
    </div>

    <input type="hidden" value="<?php echo $_GET['client_det_id']; ?>" name="client_det_id"/>
    <input type="hidden" name="option" value="com_creditbureau"/>
    <input type="hidden" name="task" value=""/> 
</form>
