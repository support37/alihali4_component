<?php


defined('_JEXEC') or die('Restricted access');

class CreditbureauViewArea extends FView {

    public function display($tpl = null, $cachable = false, $urlparams = false) {
        if (JRequest::getvar('view') == 'area.detail') {
            JToolBarHelper::title(JText::_("COM_CREDITBUREAU_EDIT"));
            JToolBarHelper::save('area.update');
            JToolBarHelper::cancel('area.cancel');
        } elseif (JRequest::getvar('view') == 'area.form') {
            JToolBarHelper::title(JText::_("COM_CREDITBUREAU_ADD"));
            JToolBarHelper::save('area.add');
            JToolBarHelper::cancel('area.cancel');
        } else {
            JToolBarHelper::title(JText::_('COM_CREDITBUREAU_AREA_MENU'));
            JToolBarHelper::addNew('area.addnew');
            JToolBarHelper::editList('area.edit');
            JToolBarHelper::deleteList(JText::_('COM_CREDITBUREAU_DELETE'), 'area.delete');
        }
        parent::display($tpl, $cachable, $urlparams);
    }

    public function view_list() {
        $model = $this->getModel('area', 'CreditbureauModel');
        $paging = "";
        $rows = $model->getAreas($paging);
        $this->assignRef('rows', $rows);
        $this->assignRef('pages', $paging);
    }

    public function view_detail() {
        $id = JRequest::getVar('ar_id');
        $row = Cbareas::getInfoById($id);
        $this->assignRef('rows', $row);
    }

    public function view_form() {
        
    }

}

?>