<?php


defined('_JEXEC') or die('Restricted access');

class CreditbureauViewProject extends FView {

    public function display($tpl = null, $cachable = false, $urlparams = false) {
        if (JRequest::getvar('view') == 'project.detail') {
            JToolBarHelper::title(JText::_("COM_CREDITBUREAU_EDIT"));
            JToolBarHelper::save('project.update');
            JToolBarHelper::cancel('project.cancel');
        } elseif (JRequest::getvar('view') == 'project.form') {
            JToolBarHelper::title(JText::_("COM_CREDITBUREAU_ADD"));
            JToolBarHelper::save('project.add');
            JToolBarHelper::cancel('project.cancel');
        } else {
            JToolBarHelper::title(JText::_('COM_CREDITBUREAU_PROJECT_MENU'));
            JToolBarHelper::addNew('project.addnew');
            JToolBarHelper::editList('project.edit');
            JToolBarHelper::deleteList(JText::_('COM_CREDITBUREAU_DELETE'), 'project.delete');
        }
        parent::display($tpl, $cachable, $urlparams);
    }

    public function view_list() {
        $model = $this->getModel('project', 'CreditbureauModel');
        $paging = "";
        $rows = $model->getProjects($paging);
        $this->assignRef('rows', $rows);
        $this->assignRef('pages', $paging);
    }

    public function view_detail() {
        $id = JRequest::getVar('project_id');
        $row = Cbprojects::getInfoById($id);
        $this->assignRef('rows', $row);
    }

    public function view_form() {
        
    }

}

?>