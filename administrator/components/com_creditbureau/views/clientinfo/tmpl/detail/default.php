<form id="adminForm" name="adminForm" class="form-validate" action="index.php?option=com_creditbureau&task=clientinfo.display&view=clientinfo.list" method="POST">
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_AUDIT_USERNAME'); ?>:</label>
        <?php
        HelperCreditbureau::renderUserSelect($this->rows->user_id);
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_EN'); ?>:</label>
        <?php
        $areas = Cbprojects::getAll();
        $exist = Cbclientinfoprojects::arrayProjectClientInfo($_GET['client_info_id']);
        HelperCreditbureau::renderSelectWithObject('project_id', $areas, 'project_id', 'p_ename', 0, true,$exist);
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_ANAME'); ?>: </label>
        <input type="text" name="c_aname" size="100" value="<?php echo $this->rows->c_aname; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_ENAME'); ?>:</label>
        <input type="text" name="c_ename" size="100" value="<?php echo $this->rows->c_ename; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_LOANNO'); ?>: </label>
        <input type="number" name="loans_no" size="100" value="<?php echo $this->rows->loans_no; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_LASTLOANSTS'); ?>: </label>
        <?php echo HelperCreditbureau::renderSelect('last_loan_st', HelperCreditbureau::statusPaidUnpaid(), $this->rows->last_loan_st); ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_CARDNO'); ?>: </label>
        <input type="text" name="card_no" size="100" value="<?php echo $this->rows->card_no; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_ID'); ?>: </label>
        <input type="number" name="id" size="100" value="<?php echo $this->rows->id; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_BLACKLIST'); ?>: </label>
        <?php echo HelperCreditbureau::renderSelect('black_list', HelperCreditbureau::blackList(), $this->rows->black_list); ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_A_ADDRESS'); ?>: </label>
        <input type="text" name="a_address" size="100" value="<?php echo $this->rows->a_address; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_E_ADDRESS'); ?>: </label>
        <input type="text" name="e_address" size="100" value="<?php echo $this->rows->e_address; ?>"/>
    </div>

    <input type="hidden" value="<?php echo $_GET['client_info_id']; ?>" name="client_info_id"/>
    <input type="hidden" name="option" value="com_creditbureau"/>
    <input type="hidden" name="task" value=""/> 
</form>
