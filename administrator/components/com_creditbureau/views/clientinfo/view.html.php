<?php


defined('_JEXEC') or die('Restricted access');

class CreditbureauViewClientinfo extends FView {

    public function display($tpl = null, $cachable = false, $urlparams = false) {
        if (JRequest::getvar('view') == 'clientinfo.detail') {
            JToolBarHelper::title(JText::_("COM_CREDITBUREAU_EDIT"));
            JToolBarHelper::save('clientinfo.update');
            JToolBarHelper::cancel('clientinfo.cancel');
        } elseif (JRequest::getvar('view') == 'clientinfo.form') {
            JToolBarHelper::title(JText::_("COM_CREDITBUREAU_ADD"));
            JToolBarHelper::save('clientinfo.add');
            JToolBarHelper::cancel('clientinfo.cancel');
        } else {
            JToolBarHelper::title(JText::_('COM_CREDITBUREAU_CLIENTINFO_MENU'));
            JToolBarHelper::addNew('clientinfo.addnew');
            JToolBarHelper::editList('clientinfo.edit');
            JToolBarHelper::deleteList(JText::_('COM_CREDITBUREAU_DELETE'), 'clientinfo.delete');
        }
        parent::display($tpl, $cachable, $urlparams);
    }

    public function view_list() {
        $model = $this->getModel('clientinfo', 'CreditbureauModel');
        $paging = "";
        $rows = $model->getClientInfos($paging);
        $this->assignRef('rows', $rows);
        $this->assignRef('pages', $paging);
    }

    public function view_detail() {
        $id = JRequest::getVar('client_info_id');
        $row = Cbclientinfo::getInfoById($id);
        $this->assignRef('rows', $row);
    }

    public function view_form() {
        
    }

}

?>