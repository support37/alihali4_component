<table class="table table-bordered">
    <tr>
        <td><?php echo JText::_('COM_CREDITBUREAU_SUM_CLIENT_IN_ALL_PROJECT'); ?></td>
        <td><strong><?php echo Cbclientinfo::getTotalClientAllProject(); ?></strong></td>
        <td><?php echo JText::_('COM_CREDITBUREAU_SUM_PAIDCLIENT_IN_ALL_PROJECT'); ?></td>
        <td><strong><?php echo Cbclientinfo::getTotalClientPaidAllProject('paid'); ?></strong></td>
    </tr>
    <tr>
        <td><?php echo JText::_('COM_CREDITBUREAU_SUM_UNPAIDCLIENT_IN_ALL_PROJECT'); ?></td>
        <td><strong><?php echo Cbclientinfo::getTotalClientPaidAllProject('unpaid'); ?></strong></td>
        <td><?php echo JText::_('COM_CREDITBUREAU_SUM_TOTAL_LOANVALUE_PAID_LOANS'); ?></td>
        <td><strong><?php echo Cbclientdet::getTotalAmountLoans('paid'); ?></strong></td>
    </tr>
    <tr>
        <td><?php echo JText::_('COM_CREDITBUREAU_SUM_TOTAL_LOANVALUE_ALL_PROJECT'); ?></td>
        <td><strong><?php echo Cbclientdet::getTotalAmountLoans(''); ?></strong></td>
        <td><?php echo JText::_('COM_CREDITBUREAU_SUM_TOTAL_LOANVALUE_UNPAID_LOANS'); ?></td>
        <td><strong><?php echo Cbclientdet::getTotalAmountLoans('unpaid'); ?></strong></td>
    </tr>
</table>
<table class="table table-bordered">
    <thead>
        <tr>
            <th colspan="5"><?php echo JText::_('COM_CREDITBUREAU_SUM_CLIENT_IN_PROJECT'); ?></th>
        </tr>
        <tr>
            <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_PROJECT_A_NAME'); ?></th>
            <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_PROJECT_E_NAME'); ?></th>
            <th width="10%"><?php echo JText::_('COM_CREDITBUREAU_SUM_TOTAL_CLIENTS'); ?></th>
            <th width="10%"><?php echo JText::_('COM_CREDITBUREAU_SUM_TOTAL_CLIENTS_PAID'); ?></th>
            <th width="13%"><?php echo JText::_('COM_CREDITBUREAU_SUM_TOTAL_CLIENTS_UNPAID'); ?></th>
            <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_SUM_TOTAL_AMOUNT_OF_LOANS'); ?></th>
        </tr>
    </thead>
    <?php
    $projects = Cbprojects::getAll();
    foreach ($projects as $value) {
        ?>
        <tr>
            <td><?php echo $value->p_aname; ?></td>
            <td><?php echo $value->p_ename; ?></td>
            <td><?php echo Cbclientinfo::getTotalClientProjectAndByStatus($value->project_id, ''); ?></td>
            <td><?php echo Cbclientinfo::getTotalClientProjectAndByStatus($value->project_id, 'paid'); ?></td>
            <td><?php echo Cbclientinfo::getTotalClientProjectAndByStatus($value->project_id, 'unpaid'); ?></td>
            <td><?php echo Cbclientdet::getTotalAmountLoans('', $value->project_id); ?></td>
        </tr>
        <?php
    }
    ?>
</table>
<script type="text/javascript" src="<?php echo JURI::base() . 'components/com_creditbureau/assets/js'; ?>/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages': ['corechart']});
    //Client Info
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Client Info', 'Total'],
            ['Last loan status Paid', <?php echo Cbclientinfo::getTotalClientInfoIds('paid'); ?>],
            ['Last loan status UnPaid', <?php echo Cbclientinfo::getTotalClientInfoIds('unpaid'); ?>],
            ['Black list client info', <?php echo Cbclientinfo::getTotalClientInfoIds('', 'no'); ?>],
            ['Active list client info', <?php echo Cbclientinfo::getTotalClientInfoIds('', 'yes'); ?>]
        ]);

        var options = {
            title: 'Client Info'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
    //Client Det
    google.charts.setOnLoadCallback(drawChart2);

    function drawChart2() {

        var data = google.visualization.arrayToDataTable([
            ['Client Det', 'Total'],
            ['Loan status Paid', <?php echo Cbclientinfo::getTotalClientInfoIds('paid'); ?>],
            ['Loan status UnPaid', <?php echo Cbclientinfo::getTotalClientInfoIds('unpaid'); ?>]
        ]);

        var options = {
            title: 'Client det'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart2'));

        chart.draw(data, options);
    }
    //Guarantee Info
    google.charts.setOnLoadCallback(drawChart3);

    function drawChart3() {

        var data = google.visualization.arrayToDataTable([
            ['Guarantee Info', 'Total'],
            ['Black list Guarantee info', <?php echo Cbgarinfo::getTotalGarInfoIds('no'); ?>],
            ['Active list Guarantee info', <?php echo Cbgarinfo::getTotalGarInfoIds('yes'); ?>]
        ]);

        var options = {
            title: 'Guarantee Info'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart3'));

        chart.draw(data, options);
    }


</script>
<div id="piechart" style="width: 520px; height: 500px;float: left;"></div>
<div id="piechart2" style="width: 520px; height: 500px; float: right"></div>
<div id="piechart3" style="width: 520px; height: 500px;float: left;"></div>