<?php

defined('_JEXEC') or die('Restricted access');
class CreditbureauViewSummary extends FView {

    public function display($tpl = null, $cachable = false, $urlparams = false) {
        JToolBarHelper::title(JText::_('COM_CREDITBUREAU_SUMMARY_MENU'));
        parent::display($tpl, $cachable, $urlparams);
    }

    public function view_list() {
        $model = $this->getModel('summary', 'CreditbureauModel');
        $infomessage = $model->getInfoMessage();
        $this->assignRef('infomassage', $infomessage);
    }

}

?>