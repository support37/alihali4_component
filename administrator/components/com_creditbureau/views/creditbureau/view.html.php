<?php

defined('_JEXEC') or die('Restricted access');
class CreditbureauViewCreditbureau extends FView {

    public function display($tpl = null, $cachable = false, $urlparams = false) {
        JToolBarHelper::deleteList(JText::_('COM_CREDITBUREAU_DELETE'), 'creditbureau.delete');
        JToolBarHelper::title(JText::_('COM_CREDITBUREAU_AUDIT_MENU'));
        parent::display($tpl, $cachable, $urlparams);
    }

    public function view_list() {
        $model = $this->getModel('creditbureau', 'CreditbureauModel');
        $paging = "";
        $rows = $model->getAudits($paging);
        $this->assignRef('rows', $rows);
        $this->assignRef('pages', $paging);
    }

}

?>