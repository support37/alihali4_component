<?php
defined('_JEXEC') or die('Restricted access');
?>

<form method="post" name="adminForm" id="adminForm">
    <div id="filter-bar" class="btn-toolbar">
        <div class="btn-group pull-right hidden-phone">
            <label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
            <?php echo $this->pages->getLimitBox(); ?>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th width="3%">
                    <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                </th>
                <th width="2%">#</th>
                <th width="15%">
                    <?php
                    echo JText::_('COM_CREDITBUREAU_AUDIT_USERNAME');
                    ?>
                </th>

                <th width="35%"><?php echo JText::_('COM_CREDITBUREAU_AUDIT_COND'); ?></th>

                <th width="5%">
                    <?php
                    echo JText::_('COM_CREDITBUREAU_AUDIT_TIME');
                    ?>
                </th>

            </tr>
        </thead>
        <?php
        $k = 0;
        for ($i = 0; $i < count($this->rows); $i++) {
            $rows = $this->rows[$i];
            ?>
            <tr class="<?php echo "row$k"; ?>">
                <td align="center">
                    <?php echo JHTML::_('grid.id', $i, $rows->au_id); ?>
                </td>
                <td align="center"><?php echo $i + 1; ?></td>
                <td>
                    <?php echo $rows->username; ?>
                </td>
                <td><?php echo $rows->search_cond; ?></td>

                <td><?php echo HelperCreditbureau::formatDate($rows->search_time, 'd/m/Y'); ?></td>
            </tr>
            <?php
            $k = 1 - $k;
        }
        ?>
        <tfoot>
            <tr>
                <td colspan="14">
                    <?php
                    if ($this->pages):
                        echo $this->pages->getListFooter();
                    endif;
                    ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="task" value="creditbureau.displays"/>
</form>