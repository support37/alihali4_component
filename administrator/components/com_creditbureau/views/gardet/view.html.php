<?php


defined('_JEXEC') or die('Restricted access');

class CreditbureauViewGardet extends FView {

    public function display($tpl = null, $cachable = false, $urlparams = false) {
        if (JRequest::getvar('view') == 'gardet.detail') {
            JToolBarHelper::title(JText::_("COM_CREDITBUREAU_EDIT"));
            JToolBarHelper::save('gardet.update');
            JToolBarHelper::cancel('gardet.cancel');
        } elseif (JRequest::getvar('view') == 'gardet.form') {
            JToolBarHelper::title(JText::_("COM_CREDITBUREAU_ADD"));
            JToolBarHelper::save('gardet.add');
            JToolBarHelper::cancel('gardet.cancel');
        } else {
            JToolBarHelper::title(JText::_('COM_CREDITBUREAU_GARDET_MENU'));
            JToolBarHelper::addNew('gardet.addnew');
            JToolBarHelper::editList('gardet.edit');
            JToolBarHelper::deleteList(JText::_('COM_CREDITBUREAU_DELETE'), 'gardet.delete');
        }
        parent::display($tpl, $cachable, $urlparams);
    }

    public function view_list() {
        $model = $this->getModel('gardet', 'CreditbureauModel');
        $paging = "";
        $rows = $model->getGarDets($paging);
        $this->assignRef('rows', $rows);
        $this->assignRef('pages', $paging);
    }

    public function view_detail() {
        $id = JRequest::getVar('gar_det_id');
        $row = Cbgardet::getInfoById($id);
        $this->assignRef('rows', $row);
    }

    public function view_form() {
        
    }

}

?>