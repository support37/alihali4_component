<?php


class TableCreditbureauCbclientinfo extends FTable {

    var $client_info_id = 0;
    var $user_id = 0;
    var $c_aname = null;
    var $c_ename = null;
    var $loans_no = 0;
    var $last_loan_st = null;
    var $card_no = null;
    var $id = 0;
    var $black_list = null;
    var $a_address = null;
    var $e_address = null;

    function __construct(&$db) {
        parent::__construct("#__cb_client_info", "client_info_id", $db);
    }

}

?>