<?php


class TableCreditbureauCbclientdet extends FTable {

    var $client_det_id = 0;
    var $user_id = 0;
    var $loan_date = null;
    var $loan_amount = 0;
    var $loan_status = null;
    var $late_days = 0;
    var $id = null;
    var $c_aname = null;
    var $c_ename = null;
    var $a_address = null;
    var $e_address = null;

    function __construct(&$db) {
        parent::__construct("#__cb_client_det", "client_det_id", $db);
    }

}

?>