<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.helper');

class Cbclientinfo {

    public static function add($post = array()) {
        $objTable = JTable::getInstance('CreditbureauCbclientinfo', 'Table');
        if ($objTable->bind($post)) {
            if ($objTable->store()) {
                return $objTable->client_info_id;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function update($post = array()) {
        $objTable = JTable::getInstance('CreditbureauCbclientinfo', 'Table');
        if (!$objTable->bind($post)) {
            return false;
        }
        if (!$objTable->store()) {
            return false;
        }
        return true;
    }

    public static function delete() {
        $arrId = JRequest::getVar('cid', array(), '', 'array');
        if (count($arrId) > 0) {
            $record = JTable::getInstance('CreditbureauCbclientinfo', 'Table');
            foreach ($arrId as $id) {
                if (!$record->delete($id)) {
                    JError::raiseError(500, $record->getError());
                    return false;
                }
            }
        }
        return true;
    }

    public static function getAll() {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__cb_client_info ORDER BY client_info_id DESC";
        $db->setQuery($query);
        $objListResult = $db->loadObjectList();
        if ($objListResult) {
            return $objListResult;
        } else {
            return array();
        }
    }

    public static function getInfoById($id) {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__cb_client_info WHERE client_info_id = {$id}";
        $db->setQuery($query);
        $objResult = $db->loadObject();
        if ($objResult) {
            return $objResult;
        } else {
            return array();
        }
    }

    public static function getTotalClientInfoIds($status = '', $black_list = '', $user_id = 0) {
        $db = JFactory::getDbo();
        $query = "SELECT count(st.client_info_id) as countObject FROM #__cb_client_info AS st JOIN #__users AS u ON st.user_id=u.id WHERE 1 ";
        if ($status == 'paid') {
            $query .= ' AND st.last_loan_st="paid"';
        }
        if ($status == 'unpaid') {
            $query .= ' AND st.last_loan_st="unpaid"';
        }
        if ($black_list == 'yes') {
            $query .= ' AND st.black_list="yes"';
        }
        if ($black_list == 'no') {
            $query .= ' AND st.black_list="no"';
        }
        if ($user_id > 0) {
            $query .= ' AND st.user_id="' . $user_id . '"';
        }
        $db->setQuery($query);
        $object = $db->loadObject();
        if ($object) {
            return $object->countObject;
        } else {
            return false;
        }
    }

    public static function getTotalClientAllProject() {
        $db = JFactory::getDbo();
        $query = "SELECT COUNT(c.client_info_id) AS countObject FROM `#__cb_client_info` AS c JOIN #__cb_client_info_projects AS cp ON c.client_info_id=cp.client_info_id";
        $db->setQuery($query);
        $object = $db->loadObject();
        if ($object) {
            return $object->countObject;
        } else {
            return 0;
        }
    }

    public static function getTotalClientPaidAllProject($loan_status) {
        $db = JFactory::getDbo();
        $query = "SELECT COUNT(c.client_info_id) AS countObject FROM `#__cb_client_info` AS c JOIN #__cb_client_info_projects AS cp ON c.client_info_id=cp.client_info_id WHERE 1";
        if ($loan_status == 'paid') {
            $query .= " AND c.last_loan_st='paid'";
        }
        if ($loan_status == 'unpaid') {
            $query .= " AND c.last_loan_st='unpaid'";
        }
        $db->setQuery($query);
        $object = $db->loadObject();
        if ($object) {
            return $object->countObject;
        } else {
            return 0;
        }
    }

    public static function getTotalClientProjectAndByStatus($project_id, $loan_status) {
        $db = JFactory::getDbo();
        $query = "SELECT COUNT(c.client_info_id) AS countObject FROM `#__cb_client_info` AS c JOIN #__cb_client_info_projects AS cp ON c.client_info_id=cp.client_info_id WHERE cp.project_id='$project_id'";
        if ($loan_status == 'paid') {
            $query .= " AND c.last_loan_st='paid'";
        }
        if ($loan_status == 'unpaid') {
            $query .= " AND c.last_loan_st='unpaid'";
        }
        $db->setQuery($query);
        $object = $db->loadObject();
        if ($object) {
            return $object->countObject;
        } else {
            return 0;
        }
    }

}

?>