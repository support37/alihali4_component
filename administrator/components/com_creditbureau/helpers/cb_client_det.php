<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.helper');

class Cbclientdet {

    public static function add($post = array()) {
        $objTable = JTable::getInstance('CreditbureauCbclientdet', 'Table');
        if ($objTable->bind($post)) {
            if ($objTable->store()) {
                return $objTable->client_det_id;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function update($post = array()) {
        $objTable = JTable::getInstance('CreditbureauCbclientdet', 'Table');
        if (!$objTable->bind($post)) {
            return false;
        }
        if (!$objTable->store()) {
            return false;
        }
        return true;
    }

    public static function delete() {
        $arrId = JRequest::getVar('cid', array(), '', 'array');
        if (count($arrId) > 0) {
            $record = JTable::getInstance('CreditbureauCbclientdet', 'Table');
            foreach ($arrId as $id) {
                if (!$record->delete($id)) {
                    JError::raiseError(500, $record->getError());
                    return false;
                }
            }
        }
        return true;
    }

    public static function getAll() {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__cb_client_det ORDER BY client_det_id DESC";
        $db->setQuery($query);
        $objListResult = $db->loadObjectList();
        if ($objListResult) {
            return $objListResult;
        } else {
            return array();
        }
    }

    public static function getInfoById($id) {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__cb_client_det WHERE client_det_id = {$id}";
        $db->setQuery($query);
        $objResult = $db->loadObject();
        if ($objResult) {
            return $objResult;
        } else {
            return array();
        }
    }

    public static function getTotalClientDetIds($status = '', $user_id = 0) {
        $db = JFactory::getDbo();
        $query = "SELECT count(st.client_info_id) as countObject FROM #__cb_client_det AS st JOIN #__users AS u ON st.user_id=u.id WHERE 1 ";
        if ($status == 'paid') {
            $query .= ' AND st.loan_status="paid"';
        }
        if ($status == 'unpaid') {
            $query .= ' AND st.loan_status="unpaid"';
        }
        if ($user_id > 0) {
            $query .= ' AND st.user_id="' . $user_id . '"';
        }
        $db->setQuery($query);
        $object = $db->loadObject();
        if ($object) {
            return $object->countObject;
        } else {
            return false;
        }
    }

    public static function getTotalAmountLoans($status, $project_id = 0) {
        $db = JFactory::getDbo();
        $query = "SELECT SUM(c.loan_amount) AS countObject FROM `#__cb_client_det` AS c JOIN #__cb_client_det_projects AS cp ON c.client_det_id=cp.client_det_id WHERE 1 ";
        if ($status == 'paid') {
            $query .= ' AND c.loan_status="paid"';
        }
        if ($status == 'unpaid') {
            $query .= ' AND c.loan_status="unpaid"';
        }
        if ($project_id > 0) {
            $query .= " AND cp.project_id='$project_id'";
        }
        $db->setQuery($query);
        $object = $db->loadObject();
        if ($object) {
            if ($object->countObject != null) {
                return $object->countObject;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

}

?>