<?php

class HelperCreditbureau extends JComponentHelper {

    const CB_ADD = "CB_ADD";
    const CB_EDIT = "CB_EDIT";
    const CB_SEARCH = "CB_SEARCH";
    const CB_UPLOAD = "CB_UPLOAD";
    const CB_SUPPER_USER = "Super Users";

    public static function includeFileInFolder($pathFolder) {
        if (file_exists($pathFolder)) {
            $files = scandir($pathFolder);
            for ($i = 0; $i < count($files); $i++) {
                if (is_file($pathFolder . DS . $files[$i]) && file_exists($pathFolder . DS . $files[$i])) {
                    include_once ($pathFolder . DS . $files[$i]);
                }
            }
        }
    }

    public static function formatDate($date, $format = 'm/d/Y') {
        if ($date == '0000-00-00' || $date == '' || $date === '0000-00-00 00:00:00') {
            return false;
        }
        if ($format == '' || $format == null) {
            $format = 'Y-m-d H:i:s';
        }
        $date = date_create($date);
        return date_format($date, $format);
    }

    public static function addSubMenu($task = null) {
        if ($task == 'creditbureau.display') {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_AUDIT_MENU'), 'index.php?option=com_creditbureau&task=creditbureau.display&view=creditbureau.list', true);
        } else {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_AUDIT_MENU'), 'index.php?option=com_creditbureau&task=creditbureau.display&view=creditbureau.list', false);
        }
        if ($task == 'area.display') {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_AREA_MENU'), 'index.php?option=com_creditbureau&task=area.display&view=area.list', true);
        } else {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_AREA_MENU'), 'index.php?option=com_creditbureau&task=area.display&view=area.list', false);
        }
        if ($task == 'project.display') {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_PROJECT_MENU'), 'index.php?option=com_creditbureau&task=project.display&view=project.list', true);
        } else {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_PROJECT_MENU'), 'index.php?option=com_creditbureau&task=project.display&view=project.list', false);
        }
        if ($task == 'clientdet.display') {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_CLIENTDET_MENU'), 'index.php?option=com_creditbureau&task=clientdet.display&view=clientdet.list', true);
        } else {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_CLIENTDET_MENU'), 'index.php?option=com_creditbureau&task=clientdet.display&view=clientdet.list', false);
        }
        if ($task == 'clientinfo.display') {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_CLIENTINFO_MENU'), 'index.php?option=com_creditbureau&task=clientinfo.display&view=clientinfo.list', true);
        } else {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_CLIENTINFO_MENU'), 'index.php?option=com_creditbureau&task=clientinfo.display&view=clientinfo.list', false);
        }
        if ($task == 'gardet.display') {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_GARDET_MENU'), 'index.php?option=com_creditbureau&task=gardet.display&view=gardet.list', true);
        } else {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_GARDET_MENU'), 'index.php?option=com_creditbureau&task=gardet.display&view=gardet.list', false);
        }
        if ($task == 'garinfo.display') {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_GARINFO_MENU'), 'index.php?option=com_creditbureau&task=garinfo.display&view=garinfo.list', true);
        } else {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_GARINFO_MENU'), 'index.php?option=com_creditbureau&task=garinfo.display&view=garinfo.list', false);
        }
        if ($task == 'summary.display') {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_SUMMARY_MENU'), 'index.php?option=com_creditbureau&task=summary.display&view=summary.list', true);
        } else {
            JSubMenuHelper::addEntry(JText::_('COM_CREDITBUREAU_SUMMARY_MENU'), 'index.php?option=com_creditbureau&task=summary.display&view=summary.list', false);
        }
    }

    public static function getAllUsers() {
        $db = JFactory::getDbo();
        $query = "SELECT u.id,u.username FROM #__users AS u ORDER BY u.id DESC";
        $db->setQuery($query);
        $objListResult = $db->loadObjectList();
        if ($objListResult) {
            return $objListResult;
        } else {
            return array();
        }
    }

    public static function renderUserSelect($user_id = 0) {
        $users = self::getAllUsers();
        ob_start();
        ?>
        <select name="user_id">
            <?php
            if (count($users) > 0) {
                foreach ($users as $value) {
                    ?>
                    <option <?php echo $user_id == $value->id ? 'selected="true"' : ''; ?> value="<?php echo $value->id; ?>"><?php echo $value->username; ?></option>
                    <?php
                }
            }
            ?>
        </select>
        <?php
        $contents = ob_get_contents();
        ob_end_clean();
        echo $contents;
    }

    public static function renderSelectWithObject($select_name = '', $obj = array(), $key = null, $value = null, $id = null, $multiple = false, $ids_exist = array()) {
        ob_start();
        ?>
        <select <?php if ($multiple == true) echo 'multiple="true"'; ?> name="<?php echo $select_name; ?><?php if ($multiple == true) echo '[]'; ?>">
            <?php
            if (count($obj) > 0) {
                foreach ($obj as $v) {
                    ?>
                    <option <?php
                    if ($v->$key == $id || in_array($v->$key, $ids_exist)) {
                        echo 'selected="true"';
                    }
                    ?> value="<?php echo $v->$key; ?>"><?php echo $v->$value; ?></option>
                        <?php
                    }
                }
                ?>
        </select>
        <?php
        $contents = ob_get_contents();
        ob_end_clean();
        echo $contents;
    }

    public static function renderSelect($select_name = '', $array = array(), $id = null) {
        ob_start();
        ?>
        <select name="<?php echo $select_name; ?>">
            <?php
            if (count($array) > 0) {
                foreach ($array as $key => $value) {
                    ?>
                    <option <?php echo $key == $id ? 'selected="true"' : ''; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                    <?php
                }
            }
            ?>
        </select>
        <?php
        $contents = ob_get_contents();
        ob_end_clean();
        echo $contents;
    }

    public static function blackList() {
        return array(
            'yes' => JText::_('COM_CREDITBUREAU_YES'),
            'no' => JText::_('COM_CREDITBUREAU_No')
        );
    }

    public static function statusPaidUnpaid() {
        return array(
            'paid' => JText::_('COM_CREDITBUREAU_PAID'),
            'unpaid' => JText::_('COM_CREDITBUREAU_UNPAID')
        );
    }

    public static function isUserLogged() {
        $user = JFactory::getUser();
        if ($user->guest) {
            return false;
        } else {
            return true;
        }
    }

    public static function getUserID() {
        $user = JFactory::getUser();
        if (isset($user->id)) {
            return $user->id;
        } else {
            return 0;
        }
    }

    public static function getGroupByUser() {
        $user_id = self::getUserID();
        $db = JFactory::getDbo();
        $query = "SELECT title FROM #__usergroups WHERE id IN (SELECT group_id FROM #__user_usergroup_map WHERE user_id='$user_id')";
        $db->setQuery($query);
        $objListResult = $db->loadObjectList();
        $res = array();
        if ($objListResult) {
            foreach ($objListResult as $value) {
                $res[] = $value->title;
            }
            return $res;
        } else {
            return array();
        }
    }

    public static function allowPermission($role) {
        $user_group = self::getGroupByUser();
        if (in_array($role, $user_group)) {
            return true;
        } else {
            return false;
        }
    }

}
?>