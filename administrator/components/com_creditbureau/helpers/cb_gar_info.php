<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.helper');

class Cbgarinfo {

    public static function add($post = array()) {
        $objTable = JTable::getInstance('CreditbureauCbgarinfo', 'Table');
        if ($objTable->bind($post)) {
            if ($objTable->store()) {
                return $objTable->gar_info_id;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function update($post = array()) {
        $objTable = JTable::getInstance('CreditbureauCbgarinfo', 'Table');
        if (!$objTable->bind($post)) {
            return false;
        }
        if (!$objTable->store()) {
            return false;
        }
        return true;
    }

    public static function delete() {
        $arrId = JRequest::getVar('cid', array(), '', 'array');
        if (count($arrId) > 0) {
            $record = JTable::getInstance('CreditbureauCbgarinfo', 'Table');
            foreach ($arrId as $id) {
                if (!$record->delete($id)) {
                    JError::raiseError(500, $record->getError());
                    return false;
                }
            }
        }
        return true;
    }

    public static function getAll() {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__cb_gar_info ORDER BY gar_info_id DESC";
        $db->setQuery($query);
        $objListResult = $db->loadObjectList();
        if ($objListResult) {
            return $objListResult;
        } else {
            return array();
        }
    }

    public static function getInfoById($id) {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__cb_gar_info WHERE gar_info_id = {$id}";
        $db->setQuery($query);
        $objResult = $db->loadObject();
        if ($objResult) {
            return $objResult;
        } else {
            return array();
        }
    }

    public static function getTotalGarInfoIds($black_list = '', $user_id = 0) {
        $db = JFactory::getDbo();
        $query = "SELECT count(st.gar_info_id) as countObject FROM #__cb_gar_info AS st JOIN #__users AS u ON st.user_id=u.id WHERE 1 ";
        if ($black_list == 'yes') {
            $query .= ' AND st.black_list="yes"';
        }
        if ($black_list == 'no') {
            $query .= ' AND st.black_list="no"';
        }
        if ($user_id > 0) {
            $query .= ' AND st.user_id="' . $user_id . '"';
        }
        $db->setQuery($query);
        $object = $db->loadObject();
        if ($object) {
            return $object->countObject;
        } else {
            return false;
        }
    }

}

?>