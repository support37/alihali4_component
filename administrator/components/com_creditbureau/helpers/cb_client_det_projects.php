<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.helper');

class Cbclientdetprojects {

    public static function add($post = array()) {
        $objTable = JTable::getInstance('CreditbureauCbclientdetprojects', 'Table');
        if ($objTable->bind($post)) {
            if ($objTable->store()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function update($post = array()) {
        $objTable = JTable::getInstance('CreditbureauCbclientdetprojects', 'Table');
        if (!$objTable->bind($post)) {
            return false;
        }
        if (!$objTable->store()) {
            return false;
        }
        return true;
    }

    public static function delete() {
        $arrId = JRequest::getVar('cid', array(), '', 'array');
        if (count($arrId) > 0) {
            $record = JTable::getInstance('CreditbureauCbclientdetprojects', 'Table');
            foreach ($arrId as $id) {
                if (!$record->delete($id)) {
                    JError::raiseError(500, $record->getError());
                    return false;
                }
            }
        }
        return true;
    }

    public static function getAll() {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__cb_client_det_projects ORDER BY id DESC";
        $db->setQuery($query);
        $objListResult = $db->loadObjectList();
        if ($objListResult) {
            return $objListResult;
        } else {
            return array();
        }
    }

    public static function getInfoById($id) {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__cb_client_det_projects WHERE id = {$id}";
        $db->setQuery($query);
        $objResult = $db->loadObject();
        if ($objResult) {
            return $objResult;
        } else {
            return array();
        }
    }

    public static function deleteByClientDet($id) {
        $db = JFactory::getDbo();
        $query = "DELETE FROM #__cb_client_det_projects WHERE client_det_id = {$id}";
        $db->setQuery($query);
        $db->execute();
    }

    public static function getProjectByClientDetId($id) {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__cb_client_det_projects WHERE client_det_id='$id' ORDER BY id DESC";
        $db->setQuery($query);
        $objListResult = $db->loadObjectList();
        if ($objListResult) {
            return $objListResult;
        } else {
            return array();
        }
    }

    public static function arrayProjectClientDet($id) {
        $res = self::getProjectByClientDetId($id);
        $list = array();
        if (count($res) > 0) {
            foreach ($res as $value) {
                $list[] = $value->project_id;
            }
        }
        return $list;
    }

}

?>