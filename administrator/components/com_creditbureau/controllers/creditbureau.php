<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class CreditbureauControllerCreditbureau extends FController {

    public function __construct($config = array()) {
        parent::__construct($config);
    }

    public function display($model = null, $cachable = false, $urlparams = false) {
        $view_name = JRequest::getVar('view');
        if (!$view_name) {
            $view_name = 'creditbureau.list';
        }
        $view = $this->createView($view_name, 'CreditbureauView');
        $model = $this->createModel('creditbureau', 'CreditbureauModel');
        if ($model) {
            $view->setModel($model);
        }
        $view->display($tpl = null);
    }

    public function delete() {
        $id = JRequest::getVar('id');
        if (Cbaudit::delete($id)) {
            $msg = JText::_('COM_CREDITBUREAU_AREA_DEL_OK');
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_DEL_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('creditbureau.display', 'creditbureau.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

}

?>