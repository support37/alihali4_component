<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class CreditbureauControllerGarinfo extends FController {

    public function __construct($config = array()) {
        parent::__construct($config);
    }

    public function display($model = null, $cachable = false, $urlparams = false) {
        $view_name = JRequest::getVar('view');
        if (!$view_name) {
            $view_name = 'garinfo.list';
        }
        $view = $this->createView($view_name, 'CreditbureauView');
        $model = $this->createModel('garinfo', 'CreditbureauModel');
        if ($model) {
            $view->setModel($model);
        }
        $view->display($tpl = null);
    }

    public function add() {
        $post = JRequest::get('post');
        if ($post['user_id'] != '' || $post['g_aname'] != '' || $post['g_ename'] != '') {
            if ($id = Cbgarinfo::add($post)) {
                if (count($post['project_id']) > 0) {
                    $areas = $post['project_id'];
                    foreach ($areas as $value) {
                        $obj = array(
                            'project_id' => $value,
                            'gar_info_id' => $id
                        );
                        Cbgarinfoprojects::add($obj);
                    }
                }
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_OK');
            } else {
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
            }
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('garinfo.display', 'garinfo.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    function update() {
        $post = JRequest::get('post');
        if ($post['user_id'] != '' || $post['g_aname'] != '' || $post['g_ename'] != '') {
            if (Cbgarinfo::update($post)) {
                $id = $post['gar_info_id'];
                Cbgarinfoprojects::deleteByGarInfo($id);
                if (count($post['project_id']) > 0) {
                    $areas = $post['project_id'];
                    foreach ($areas as $value) {
                        $obj = array(
                            'project_id' => $value,
                            'gar_info_id' => $id
                        );
                        Cbgarinfoprojects::add($obj);
                    }
                }
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_OK');
            } else {
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
            }
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('garinfo.display', 'garinfo.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    public function delete() {
        $id = JRequest::getVar('id');
        if (Cbgarinfo::delete($id)) {
            $msg = JText::_('COM_CREDITBUREAU_AREA_DEL_OK');
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_DEL_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('garinfo.display', 'garinfo.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    function addnew() {
        $link = 'index.php?option=com_creditbureau&task=garinfo.display&view=garinfo.form';
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

    function edit() {
        $arrId = JRequest::getVar('cid', array(), '', 'array');
        foreach ($arrId as $id) {
            $link = 'index.php?option=com_creditbureau&task=garinfo.display&view=garinfo.detail&gar_info_id=' . $id;
        }
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

    function cancel() {
        $link = 'index.php?option=com_creditbureau&task=garinfo.display&view=garinfo.list';
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

}

?>