<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class CreditbureauControllerProject extends FController {

    public function __construct($config = array()) {
        parent::__construct($config);
    }

    public function display($model = null, $cachable = false, $urlparams = false) {
        $view_name = JRequest::getVar('view');
        if (!$view_name) {
            $view_name = 'project.list';
        }
        $view = $this->createView($view_name, 'CreditbureauView');
        $model = $this->createModel('project', 'CreditbureauModel');
        if ($model) {
            $view->setModel($model);
        }
        $view->display($tpl = null);
    }

    public function add() {
        $post = JRequest::get('post');
        if ($post['p_ename'] != '' || $post['p_aname'] != '') {
            if ($project_id = Cbprojects::add($post)) {
                if (count($post['area_id']) > 0) {
                    $areas = $post['area_id'];
                    foreach ($areas as $value) {
                        $project_area = array(
                            'project_id' => $project_id,
                            'area_id' => $value
                        );
                        Cbareasprojects::add($project_area);
                    }
                }
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_OK');
            } else {
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
            }
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('project.display', 'project.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    function update() {
        $post = JRequest::get('post');
        if ($post['p_ename'] != '' || $post['p_aname'] != '') {
            if (Cbprojects::update($post)) {
                $project_id = $post['project_id'];
                Cbareasprojects::deleteByProject($post['project_id']);
                if (count($post['area_id']) > 0) {
                    $areas = $post['area_id'];
                    foreach ($areas as $value) {
                        $project_area = array(
                            'project_id' => $project_id,
                            'area_id' => $value
                        );
                        Cbareasprojects::add($project_area);
                    }
                }
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_OK');
            } else {
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
            }
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('project.display', 'project.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    public function delete() {
        $id = JRequest::getVar('id');
        if (Cbprojects::delete($id)) {
            $msg = JText::_('COM_CREDITBUREAU_AREA_DEL_OK');
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_DEL_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('project.display', 'project.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    function addnew() {
        $link = 'index.php?option=com_creditbureau&task=project.display&view=project.form';
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

    function edit() {
        $arrId = JRequest::getVar('cid', array(), '', 'array');
        foreach ($arrId as $id) {
            $link = 'index.php?option=com_creditbureau&task=project.display&view=project.detail&project_id=' . $id;
        }
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

    function cancel() {
        $link = 'index.php?option=com_creditbureau&task=project.display&view=project.list';
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

}

?>