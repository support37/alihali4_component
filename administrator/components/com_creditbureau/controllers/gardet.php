<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class CreditbureauControllerGardet extends FController {

    public function __construct($config = array()) {
        parent::__construct($config);
    }

    public function display($model = null, $cachable = false, $urlparams = false) {
        $view_name = JRequest::getVar('view');
        if (!$view_name) {
            $view_name = 'gardet.list';
        }
        $view = $this->createView($view_name, 'CreditbureauView');
        $model = $this->createModel('gardet', 'CreditbureauModel');
        if ($model) {
            $view->setModel($model);
        }
        $view->display($tpl = null);
    }

    public function add() {
        $post = JRequest::get('post');
        if ($post['user_id'] != '' || $post['g_aname'] != '' || $post['g_ename'] != '') {
            if ($id = Cbgardet::add($post)) {
                if ($post['project_id'] > 0) {
                    $obj = array(
                        'project_id' => $post['project_id'],
                        'gar_det_id' => $id
                    );
                    Cbgardetprojects::add($obj);
                }
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_OK');
            } else {
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
            }
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('gardet.display', 'gardet.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    function update() {
        $post = JRequest::get('post');
        if ($post['user_id'] != '' || $post['g_aname'] != '' || $post['g_ename'] != '') {
            if (Cbgardet::update($post)) {
                $id = $post['gar_det_id'];
                Cbgardetprojects::deleteByGarDet($id);
                if ($post['project_id'] > 0) {
                    $obj = array(
                        'project_id' => $post['project_id'],
                        'gar_det_id' => $id
                    );
                    Cbgardetprojects::add($obj);
                }
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_OK');
            } else {
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
            }
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('gardet.display', 'gardet.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    public function delete() {
        $id = JRequest::getVar('id');
        if (Cbgardet::delete($id)) {
            $msg = JText::_('COM_CREDITBUREAU_AREA_DEL_OK');
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_DEL_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('gardet.display', 'gardet.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    function addnew() {
        $link = 'index.php?option=com_creditbureau&task=gardet.display&view=gardet.form';
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

    function edit() {
        $arrId = JRequest::getVar('cid', array(), '', 'array');
        foreach ($arrId as $id) {
            $link = 'index.php?option=com_creditbureau&task=gardet.display&view=gardet.detail&gar_det_id=' . $id;
        }
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

    function cancel() {
        $link = 'index.php?option=com_creditbureau&task=gardet.display&view=gardet.list';
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

}

?>