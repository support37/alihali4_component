(function($){
	$.extend($.fn, {
		newSlide: function(o) {
			var doc = $(this);
			return this.delegate('[data-action="new-slide"]', 'click.new-slide', function(){
				doc.trigger('load-data-field', [ $(this) ]);
			}).on({
				'load-data-field': function(e, p, d){
					var index = d ? d.index : $.now()
						,tname = p.closest('li')
						,tcontent = $(p.data('target'))
						,content = $.tmpl(o.tcontent, d || {
							index: index,
							ease: '',
							bgpositionend: '',
							bgpositionstart: '',
							bgposition: '',
							bgrepeat: '',
							bgfit: '',
							kenburns: '',
							saveperformance: '',
							content: '',
							state: '1'
						})
					;
	                
	                tcontent.append(content);                                               
					$.tmpl(o.tname, {index: index}).insertBefore(tname);
					$('[href="#simg-' + index + '"]').tab('show');
					content.schosen();
	                var ominicolor = JV.ominicolors;
	                ominicolor.change = function(v){
	                    $(this).trigger('change');        
	                };
					content.find('.minicolors').each(function(){
						$(this).minicolors(ominicolor);
					});
					content.find('[data-change-editor]').each(function(){
						$(this).trigger('change');
					});
					content.find('[data-field="title"]').each(function(){
						$(this).trigger('keyup.field-title');
					});
					content.sbutton();

					// load children
					doc.trigger('load-child', d);
				},
				'load-child': function(e, d) {
					if(!d || !d.items) { return false; }
					$.each( d.items, function(i, item){
						
						item = $.extend(item, {id: $.now(), index: d.index});

						var s = $( '#layer-zlayer'.replace(/zlayer/, d.index) + '> [data-tag="mark-content"]' );

						doc.trigger('add-layer', [ s, item ] );
					});
				}
			});
		},
		removeSlide: function(){
			return this.delegate('[data-action="remove-slide"]', 'click.remove-slide', function(){
				var p = $(this);
				$(p.data('target')).each(function(){
					$(this).remove();
				});
				p.closest('[data-tag="tab-name"]').remove();
			})
		},
		sdrag: function(){
			return this.each(function(){
				$(this).draggabilly({
					containment: true,
					grid: [5,5],
                    handle: '.handle',
				});
			});
		},
		jvtinymce: function(){
			
			tinymce.PluginManager.add("jvimage", function(a) {
				function b(a, b) {
					function c(a, c) {
						d.parentNode && d.parentNode.removeChild(d), b({
							width: a,
							height: c
						})
					}
					var d = document.createElement("img");
					d.onload = function() {
						c(d.clientWidth, d.clientHeight)
					}, d.onerror = function() {
						c()
					};
					var e = d.style;
					e.visibility = "hidden", e.position = "fixed", e.bottom = e.left = 0, e.width = e.height = "auto", document.body.appendChild(d), d.src = a
				}
				function c(a, b, c) {
					function d(a, c) {
						return c = c || [], tinymce.each(a, function(a) {
							var e = {
								text: a.text || a.title
							};
							a.menu ? e.menu = d(a.menu) : (e.value = a.value, b(e)), c.push(e)
						}), c
					}
					return d(a, c || [])
				}
				function d(b) {
					return function() {
						var c = a.settings.image_list;
						"string" == typeof c ? tinymce.util.XHR.send({
							url: c,
							success: function(a) {
								b(tinymce.util.JSON.parse(a))
							}
						}) : "function" == typeof c ? c(b) : b(c)
					}
				}
				function e(d) {
					function e() {
						var a, b, c, d;
						a = j.find("#width")[0], b = j.find("#height")[0], a && b && (c = a.value(), d = b.value(), j.find("#constrain")[0].checked() && k && l && c && d && (k != c ? (d = Math.round(c / k * d), b.value(d)) : (c = Math.round(d / l * c), a.value(c))), k = c, l = d)
					}
					function f() {
						function b(b) {
							function c() {
								b.onload = b.onerror = null, a.selection && (a.selection.select(b), a.nodeChanged())
							}
							b.onload = function() {
								o.width || o.height || !r || p.setAttribs(b, {
									width: b.clientWidth,
									height: b.clientHeight
								}), c()
							}, b.onerror = c
						}
						i(), e(), o = tinymce.extend(o, j.toJSON()), o.alt || (o.alt = ""), "" === o.width && (o.width = null), "" === o.height && (o.height = null), o.style || (o.style = null), o = {
							src: o.src,
							alt: o.alt,
							width: o.width,
							height: o.height,
							style: o.style,
							"class": o["class"]
						}, a.undoManager.transact(function() {
							return o.src ? (q ? p.setAttribs(q, o) : (o.id = "__mcenew", a.focus(), a.selection.setContent(p.createHTML("img", o)), q = p.get("__mcenew"), p.setAttrib(q, "id", null)), void b(q)) : void(q && (p.remove(q), a.focus(), a.nodeChanged()))
						})
					}
					function g(a) {
						return a && (a = a.replace(/px$/, "")), a
					}
					function h(c) {
						var d = c.meta || {};
						if (m && m.value(a.convertURL(this.value(), "src")), tinymce.each(d, function(a, b) {
							j.find("#" + b).value(a)
						}), !d.width && !d.height) {
							var e = this.value(),
								f = new RegExp("^(?:[a-z]+:)?//", "i"),
								g = a.settings.document_base_url;
							g && !f.test(e) && e.substring(0, g.length) !== g && this.value(g + e), b(this.value(), function(a) {
								a.width && a.height && r && (k = a.width, l = a.height, j.find("#width").value(k), j.find("#height").value(l))
							})
						}
					}
					function i() {
						function b(a) {
							return a.length > 0 && /^[0-9]+$/.test(a) && (a += "px"), a
						}
						if (a.settings.image_advtab) {
							var c = j.toJSON(),
								d = p.parseStyle(c.style);
							delete d.margin, d["margin-top"] = d["margin-bottom"] = b(c.vspace), d["margin-left"] = d["margin-right"] = b(c.hspace), d["border-width"] = b(c.border), j.find("#style").value(p.serializeStyle(p.parseStyle(p.serializeStyle(d))))
						}
					}
					var j, k, l, m, n, o = {},
						p = a.dom,
						q = a.selection.getNode(),
						r = a.settings.image_dimensions !== !1;
					k = p.getAttrib(q, "width"), l = p.getAttrib(q, "height"), "IMG" != q.nodeName || q.getAttribute("data-mce-object") || q.getAttribute("data-mce-placeholder") ? q = null : o = {
						src: p.getAttrib(q, "src"),
						alt: p.getAttrib(q, "alt"),
						"class": p.getAttrib(q, "class"),
						width: k,
						height: l
					}, d && (m = {
						type: "listbox",
						label: "Image list",
						values: c(d, function(b) {
							b.value = a.convertURL(b.value || b.url, "src")
						}, [{
							text: "None",
							value: ""
						}]),
						value: o.src && a.convertURL(o.src, "src"),
						onselect: function(a) {
							var b = j.find("#alt");
							(!b.value() || a.lastControl && b.value() == a.lastControl.text()) && b.value(a.control.text()), j.find("#src").value(a.control.value()).fire("change")
						},
						onPostRender: function() {
							m = this
						}
					}), a.settings.image_class_list && (n = {
						name: "class",
						type: "listbox",
						label: "Class",
						values: c(a.settings.image_class_list, function(b) {
							b.value && (b.textStyle = function() {
								return a.formatter.getCssText({
									inline: "img",
									classes: [b.value]
								})
							})
						})
					});
					var s = [{
                        type: 'container',
                        label: 'Source',
                        layout: 'flex',
                        direction: 'row',
                        align: 'center',
                        spacing: 5,
                        items: [{
                            name: "src",
                            type: "textbox",   
                            filetype: "image",
                            label: "Source",
                            autofocus: !0,
                            onchange: h
                        },{
                            name: "src_choose",
                            type: "button",
                            icon: "image", 
                            onclick: function() {
                                JV.imageok.trigger('fieldid', [ this.parent().items()[0]._id ])
                                .modal('show');
                            }
                        }]
                    },
					m];
					a.settings.image_description !== !1 && s.push({
						name: "alt",
						type: "textbox",
						label: "Image description"
					}), r && s.push({
						type: "container",
						label: "Dimensions",
						layout: "flex",
						direction: "row",
						align: "center",
						spacing: 5,
						items: [{
							name: "width",
							type: "textbox",
							maxLength: 5,
							size: 3,
							onchange: e,
							ariaLabel: "Width"
						},
						{
							type: "label",
							text: "x"
						},
						{
							name: "height",
							type: "textbox",
							maxLength: 5,
							size: 3,
							onchange: e,
							ariaLabel: "Height"
						},
						{
							name: "constrain",
							type: "checkbox",
							checked: !0,
							text: "Constrain proportions"
						}]
					}), s.push(n), a.settings.image_advtab ? (q && (o.hspace = g(q.style.marginLeft || q.style.marginRight), o.vspace = g(q.style.marginTop || q.style.marginBottom), o.border = g(q.style.borderWidth), o.style = a.dom.serializeStyle(a.dom.parseStyle(a.dom.getAttrib(q, "style")))), j = a.windowManager.open({
						title: "Insert/edit image",
						data: o,
						bodyType: "tabpanel",
						body: [{
							title: "General",
							type: "form",
							items: s
						},
						{
							title: "Advanced",
							type: "form",
							pack: "start",
							items: [{
								label: "Style",
								name: "style",
								type: "textbox"
							},
							{
								type: "form",
								layout: "grid",
								packV: "start",
								columns: 2,
								padding: 0,
								alignH: ["left", "right"],
								defaults: {
									type: "textbox",
									maxWidth: 50,
									onchange: i
								},
								items: [{
									label: "Vertical space",
									name: "vspace"
								},
								{
									label: "Horizontal space",
									name: "hspace"
								},
								{
									label: "Border",
									name: "border"
								}]
							}]
						}],
						onSubmit: f
					})) : j = a.windowManager.open({
						title: "Insert/edit image",
						data: o,
						body: s,
						onSubmit: f
					})
				}
				a.addButton("image", {
					icon: "image",
					tooltip: "Insert/edit image",
					onclick: d(e),
					stateSelector: "img:not([data-mce-object],[data-mce-placeholder])"
				}), a.addMenuItem("image", {
					icon: "image",
					text: "Insert image",
					onclick: d(e),
					context: "insert",
					prependToContext: !0
				}), a.addCommand("mceImage", d(e))
			});

			tinymce.PluginManager.add('dlayer', function(editor, url) {
        

                // Ads a menu item to the file menu
                editor.addMenuItem('clonelayer', {
                	text: 'Clone Layer',
                	context: 'file',
                	onclick: function(){
                		var el = $(editor.targetElm)
                            ,m = el.closest('[data-tag="drag"]')
                            ,d = m.find( '[data-field]' ).toObject()
                            ,index = el.closest( '[data-tag="editor"]' )
                            .attr( 'id' ).replace( 'editor-', '')
                        ;   
                        item = $.extend(d, {
                        	id: $.now(), 
                        	index: index, 
                        	content: editor.getContent() 
                       	});

						var s = $( '#layer-zlayer'.replace( /zlayer/, index ) );

						$( document ).trigger('add-layer', [ s, item ] );
                	}
                });

                // Adds a menu item to the file menu
                editor.addMenuItem('dlayer', {
                    text: 'Delete layer',
                    context: 'file',
                    onclick: function() {
                        var el = $(editor.targetElm)
                            ,m = el.closest('[data-tag="drag"]')
                        ;   
                        editor.destroy();
                        $( m.attr('data-timeline') ).remove();
                        m.remove();   
                    }
                });

            });
            
            tinymce.PluginManager.add('jvtransition', function(editor, url) {
                var 
                	layer = $(editor.targetElm).closest('[data-tag="drag"]')
                	,sg = function(){ return '[data-group="zgroup"]'.replace(/zgroup/, this) }
                	,sf = function(){ return '[data-field="zfield"]'.replace(/zfield/, this) }
                	,f = {
                		g: function(){
                			var d = {};
                			layer.find( sg.call(this) ).each(function(){
                				var k = $(this).data('field');
								!k || ( d[ k ] = this.value );
                			});
							return d;
                		},
                		s: function(d){
                			var fields = layer.find( sg.call(this) );
                			$.each(d, function(k, v){
								fields.filter( sf.call(k) ).val( v ).trigger('change');
                			});
                		},
						p: function(d) {
							
							layer.toLayerPosition();
						}
					}
				;
                
        
                // Adds a menu item to the file menu
                editor.addMenuItem('align_pos_style', {
                    text: 'Position & Styling',
                    context: 'tools',
                    onclick: function() {
                        editor.windowManager.open({
                            title: 'Layer General',
                            bodyType: "tabpanel",
                            data: f.g.call( 'pos-style' ),
                            body: 
                            [
                            	{
									title: "Position",
									type: "form",
									items: [
										{
											type: 'listbox', 
											name: 'pos', label: 'Position',
											values: [                          
												{ value: '', text: 'none'},
												{ value: 'lt', text: 'Left top'},
												{ value: 'lc', text: 'Left center'},
												{ value: 'lb', text: 'Left bottom'},
												{ value: 'ct', text: 'Center top'},
												{ value: 'cc', text: 'Center'},
												{ value: 'cb', text: 'Center bottom'},
												{ value: 'rl', text: 'Right left'},
												{ value: 'rc', text: 'Right center'},
												{ value: 'rb', text: 'Right bottom'}
											]
										},
										{
											type: "container",
											label: "Offset",
											layout: "flex",
											direction: "row",
											align: "center",
											spacing: 5,
											items: [{
												name: "x",
												type: "textbox",
												maxLength: 5,
												size: 3,
												ariaLabel: "X"
											},
											{
												type: "label",
												text: "x"
											},
											{
												name: "y",
												type: "textbox",
												maxLength: 5,
												size: 3,
												ariaLabel: "Y"
											}]
										},
										{
											type: 'listbox', 
											name: 'ws', label: 'White Space',
											values: [                          
												{ value: 'normal', text: 'Normal'},
												{ value: 'pre', text: 'Pre'},
												{ value: 'nowrap', text: 'No-wrap'},
												{ value: 'pre-wrap', text: 'Pre-Wrap'},
												{ value: 'pre-line', text: 'Pre-Line'}
											]
										},
										{
											type: "container",
											label: "Dimensions",
											layout: "flex",
											direction: "row",
											align: "center",
											spacing: 5,
											items: [{
												name: "mw",
												type: "textbox",
												maxLength: 5,
												size: 3,
												ariaLabel: "Max width"
											},
											{
												type: "label",
												text: "x"
											},
											{
												name: "mh",
												type: "textbox",
												maxLength: 5,
												size: 3,
												ariaLabel: "Max height"
											}]
										}	
									]
								},
								{
									title: "Styling",
									type: "form",
									items: [
										{
											type: 'listbox', 
											name: 'zstyle', label: 'Style',
											values: JV.zstyle
										},
										{
											name: "zid",
											type: "textbox",
											label: "Id"
										},
										{
											name: "zclass",
											type: "textbox",
											label: "Class",
											size: 50
										}	
									]
								}
                            ],
							onsubmit: function() {
								var d = this.toJSON();
								// save data
								f.s.call( 'pos-style', d );
								// reposition
								f.p.call( this, d );
							}
                        });    
                    }
                });

                function d_looptype() {
					var nd = this.value(),
					dr = function() {
						editor.windowManager.open({
							title: 'Loop Animation',
							data: f.g.call('loop'),
							body: [
								{
									name: "type",
									type: "textbox",
									maxLength: 5,
									size: 3,
									hidden: true,
									value: nd
								},{
									type: "container",
									label: "Speed",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										name: "speed",
										type: "textbox",
										maxLength: 5,
										size: 3,
										ariaLabel: "Loop speed"
									},
									{
										type: "label",
										text: "(0.00 - 10.00)"
									}]
								},
								{
									type: 'listbox',
									minWidth: 350,
									name: 'loop_easing', label: 'Easing',
									values: JV.easing 
								},
								{
									name: nd + "_startdeg",
									type: "textbox",
									maxLength: 5,
									size: 3,
									label: "Start Degree"
								},
								{
									type: "container",
									label: "End Degree",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										name: nd + "_enddeg",
										type: "textbox",
										maxLength: 5,
										size: 3,
										label: "End Degree"
									},
									{
										type: "label",
										text: "(-720 - 720)"
									}]
								},
								{
									type: "container",
									label: "x Origin",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										name: nd + "_xorigin",
										type: "textbox",
										maxLength: 5,
										size: 3,
										ariaLabel: "x Origin"
									},
									{
										type: "label",
										text: "%"
									}]
								},
								{
									type: "container",
									label: "y Origin",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										name: nd + "_yorigin",
										type: "textbox",
										maxLength: 5,
										size: 3,
										ariaLabel: "y Origin"
									},
									{
										type: "label",
										text: "% (-250% - 250%)"
									}]
								}
							], 
							onsubmit: function(){
								f.s.call('loop', this.toJSON());	
							}	
						});
					},
					dialog = {
						'rs-pendulum': dr, 
						'rs-rotate': dr,
						'rs-slideloop': function() {
							editor.windowManager.open({
								title: 'Loop Animation',
								data: f.g.call('loop'),
								body: [
									{
										name: "type",
										type: "textbox",
										maxLength: 5,
										size: 3,
										hidden: true,
										value: nd
									},{
										type: "container",
										label: "Speed",
										layout: "flex",
										direction: "row",
										align: "center",
										spacing: 5,
										items: [{
											name: "speed",
											type: "textbox",
											maxLength: 5,
											size: 3,
											ariaLabel: "Loop speed"
										},
										{
											type: "label",
											text: "(0.00 - 10.00)"
										}]
									},
									{
										type: 'listbox',
										minWidth: 350,
										name: 'loop_easing', label: 'Easing',
										values: JV.easing 
									},
									{
										type: "container",
										label: "x Start Pos.",
										layout: "flex",
										direction: "row",
										align: "center",
										spacing: 5,
										items: [{
											name: "rs_slideloop_xstart",
											type: "textbox",
											maxLength: 5,
											size: 3,
											label: "x Start Pos."
										},
										{
											type: "label",
											text: "px"
										}]
									},
									{
										type: "container",
										label: "x End Pos.",
										layout: "flex",
										direction: "row",
										align: "center",
										spacing: 5,
										items: [{
											name: "rs_slideloop_xend",
											type: "textbox",
											maxLength: 5,
											size: 3,
											label: "x End Pos."
										},
										{
											type: "label",
											text: "px (-2000px - 2000px)"
										}]
									},
									{
										type: "container",
										label: "y Start Pos.",
										layout: "flex",
										direction: "row",
										align: "center",
										spacing: 5,
										items: [{
											name: "rs_slideloop_ystart",
											type: "textbox",
											maxLength: 5,
											size: 3,
											ariaLabel: "y Start Pos."
										},
										{
											type: "label",
											text: "px"
										}]
									},
									{
										type: "container",
										label: "y End Pos.",
										layout: "flex",
										direction: "row",
										align: "center",
										spacing: 5,
										items: [{
											name: "rs_slideloop_yend",
											type: "textbox",
											maxLength: 5,
											size: 3,
											ariaLabel: "y End Pos."
										},
										{
											type: "label",
											text: "px (-2000px - 2000px)"
										}]
									}
								],
								onsubmit: function(){
									f.s.call('loop', this.toJSON());
								}	
							});
						}, 
						'rs-pulse': function() {
							editor.windowManager.open({
								title: 'Loop Animation',
								data: f.g.call('loop'),
								body: [
									{
										name: "type",
										type: "textbox",
										maxLength: 5,
										size: 3,
										hidden: true,
										value: nd
									},{
										type: "container",
										label: "Speed",
										layout: "flex",
										direction: "row",
										align: "center",
										spacing: 5,
										items: [{
											name: "speed",
											type: "textbox",
											maxLength: 5,
											size: 3,
											ariaLabel: "Loop speed"
										},
										{
											type: "label",
											text: "(0.00 - 10.00)"
										}]
									},
									{
										type: 'listbox',
										minWidth: 350,
										name: 'loop_easing', label: 'Easing',
										values: JV.easing 
									},
									{
										name: "rs_pulse_zoomstart",
										type: "textbox",
										maxLength: 5,
										size: 3,
										label: "Start Zoom"
									},
									{
										type: "container",
										label: "End Zoom",
										layout: "flex",
										direction: "row",
										align: "center",
										spacing: 5,
										items: [{
											name: "rs_pulse_zoomend",
											type: "textbox",
											maxLength: 5,
											size: 3,
											label: "End Zoom"
										},
										{
											type: "label",
											text: "(0.00 - 20.00)"
										}]
									}
								],
								onsubmit: function(){
									f.s.call('loop', this.toJSON());
								}	
							});
						}, 
						'rs-wave': function() {
							editor.windowManager.open({
								title: 'Loop Animation',
								data: f.g.call('loop'),
								body: [
									{
										name: "type",
										type: "textbox",
										maxLength: 5,
										size: 3,
										hidden: true,
										value: nd
									},{
										type: "container",
										label: "Speed",
										layout: "flex",
										direction: "row",
										align: "center",
										spacing: 5,
										items: [{
											name: "speed",
											type: "textbox",
											maxLength: 5,
											size: 3,
											ariaLabel: "Loop speed"
										},
										{
											type: "label",
											text: "(0.00 - 10.00)"
										}]
									},
									{
										type: 'listbox',
										minWidth: 350,
										name: 'loop_easing', label: 'Easing',
										values: JV.easing 
									},
									{
										type: "container",
										label: "x Origin",
										layout: "flex",
										direction: "row",
										align: "center",
										spacing: 5,
										items: [{
											name: "rs_wave_xorigin",
											type: "textbox",
											maxLength: 5,
											size: 3,
											label: "x Origin"
										},
										{
											type: "label",
											text: "%"
										}]
									},
									{
										type: "container",
										label: "y Origin",
										layout: "flex",
										direction: "row",
										align: "center",
										spacing: 5,
										items: [{
											name: "rs_wave_yorigin",
											type: "textbox",
											maxLength: 5,
											size: 3,
											label: "y Origin"
										},
										{
											type: "label",
											text: "% (-250% - 250%)"
										}]
									},
									{
										type: "container",
										label: "Angle",
										layout: "flex",
										direction: "row",
										align: "center",
										spacing: 5,
										items: [{
											name: "rs_wave_angle",
											type: "textbox",
											maxLength: 5,
											size: 3,
											ariaLabel: "Angle."
										},
										{
											type: "label",
											text: "(0-360)"
										}]
									},
									{
										type: "container",
										label: "Radius",
										layout: "flex",
										direction: "row",
										align: "center",
										spacing: 5,
										items: [{
											name: "rs_wave_radius",
											type: "textbox",
											maxLength: 5,
											size: 3,
											ariaLabel: "Radius"
										},
										{
											type: "label",
											text: "px (0px - 2000px)"
										}]
									}
								],
								onsubmit: function(){
									f.s.call('loop', this.toJSON());
								}	
							});	
						}
					}[nd]();
                }

                function transitcustom(t){
                	var p = t;
                	t = 'custom-transit-ztype'.replace(/ztype/, t);
                	editor.windowManager.open({
						title: 'Custom animation',
						data: f.g.call( t ),
						body: [
							{
								type: "container",
								label: "Transition",
								layout: "flex",
								direction: "row",
								align: "center",
								spacing: 20,
								minWidth: 300,
								items: [{
									type: "container",
									label: "X",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										type: "label",
										text: "X:"
									},{
										type: 'textbox', 
										name: 'movex' + p,
										size: 5
									},
									{
										type: "label",
										text: "px"
									}]
								},
								{
									type: "container",
									label: "Y",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										type: "label",
										text: "Y:"
									},{
										type: 'textbox', 
										name: 'movey' + p,
										size: 5
									},
									{
										type: "label",
										text: "px"
									}]
								},
								{
									type: "container",
									label: "Z",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										type: "label",
										text: "Z:"
									},{
										type: 'textbox', 
										name: 'movez' + p,
										size: 5
									},
									{
										type: "label",
										text: "px"
									}]
								}]
							},
							{
								type: "container",
								label: "Rotation",
								layout: "flex",
								direction: "row",
								align: "center",
								spacing: 20,
								minWidth: 350,
								items: [{
									type: "container",
									label: "x",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										type: "label",
										text: "X:"
									},{
										type: 'textbox', 
										name: 'rotationx' + p,
										size: 5
									},
									{
										type: "label",
										text: "deg"
									}]
								},
								{
									type: "container",
									label: "y",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										type: "label",
										text: "Y:"
									},{
										type: 'textbox', 
										name: 'rotationy' + p,
										size: 5
									},
									{
										type: "label",
										text: "deg"
									}]
								},
								{
									type: "container",
									label: "z",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										type: "label",
										text: "Z:"
									},{
										type: 'textbox', 
										name: 'rotationz' + p,
										size: 5
									},
									{
										type: "label",
										text: "deg"
									}]
								}]
							},
							{
								type: "container",
								label: "Scale",
								layout: "flex",
								direction: "row",
								align: "center",
								spacing: 20,
								minWidth: 300,
								items: [{
									type: "container",
									label: "x",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										type: "label",
										text: "X:"
									},{
										type: 'textbox', 
										name: 'scalex' + p,
										size: 5
									},
									{
										type: "label",
										text: "%"
									}]
								},
								{
									type: "container",
									label: "y",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										type: "label",
										text: "Y:"
									},{
										type: 'textbox', 
										name: 'scaley' + p,
										size: 5
									},
									{
										type: "label",
										text: "%"
									}]
								}]
							},
							{
								type: "container",
								label: "Skew",
								layout: "flex",
								direction: "row",
								align: "center",
								spacing: 20,
								minWidth: 300,
								items: [{
									type: "container",
									label: "x",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										type: "label",
										text: "X:"
									},{
										type: 'textbox', 
										name: 'skewx' + p,
										size: 5
									},
									{
										type: "label",
										text: "px"
									}]
								},
								{
									type: "container",
									label: "y",
									layout: "flex",
									direction: "row",
									align: "center",
									spacing: 5,
									items: [{
										type: "label",
										text: "Y:"
									},{
										type: 'textbox', 
										name: 'skewy' + p,
										size: 5
									},
									{
										type: "label",
										text: "px"
									}]
								}]
							},
							{
								type: "container",
								label: "Opacity",
								layout: "flex",
								direction: "row",
								align: "center",
								spacing: 5,
								items: [{
									type: 'textbox', 
									name: 'captionopacity' + p,
									minWidth: 360
								},
								{
									type: "label",
									text: "%"
								}]
							},
							{
								type: "container",
								label: "Perspective",
								layout: "flex",
								direction: "row",
								align: "center",
								spacing: 5,
								items: [{
									type: 'textbox', 
									name: 'captionperspective' + p,
									minWidth: 360
								},
								{
									type: "label",
									text: "px"
								}]
							},
							{
								type: "container",
								label: "Origin",
								layout: "flex",
								direction: "row",
								align: "center",
								spacing: 20,
								minWidth: 300,
								items: [{
									type: 'textbox', 
									name: 'origin' + p
								},
								{
									type: "label",
									text: "x% y%"
								}]
							}
						],
						onsubmit: function(){
							f.s.call( t, this.toJSON() );
						}
                	});
                }
                // Adds a menu item to the file menu
                editor.addMenuItem('jvtransition', {
                    text: 'Animation',
                    context: 'tools',
                    onclick: function() {
                        // Open window
                        editor.windowManager.open({
                            title: 'Layer Animation',
                            bodyType: "tabpanel",
                            data: f.g.call( 'transit' ),
                            body: [
                            	{
									title: "Start Transition",
									type: "form",
									items: [
										{
											type: 'listbox',
											minWidth: 300,
											name: 'animationin', label: 'Animation name',
											values: [                          
												{ value: 'tp-fade', text: 'Fade'},
												{ value: 'sft', text: 'Short from Top'},
												{ value: 'sfb', text: 'Short from Bottom'},
												{ value: 'sfr', text: 'Short from Right'},
												{ value: 'sfl', text: 'Short from Left'},
												{ value: 'lft', text: 'Long from Top'},
												{ value: 'lfb', text: 'Long from Bottom'},
												{ value: 'lfr', text: 'Long from Right'},
												{ value: 'lfl', text: 'Long from Left'},
												{ value: 'skewfromright', text: 'Skew From Long Right'},
												{ value: 'skewfromleft', text: 'Skew From Long Left'},
												{ value: 'skewfromrightshort', text: 'Skew From Short Right'},
												{ value: 'skewfromleftshort', text: 'Skew From Short Left'},
												{ value: 'randomrotate', text: 'Random Rotate'},
												{ value: 'customin', text: 'Custom', onselect: function() { transitcustom('in') } }
											]
										},
										{
											type: 'listbox', 
											name: 'easing', label: 'Easing',
											size: 100,
											values: JV.easing 
										},
										{
											type: "container",
											label: "Speed",
											layout: "flex",
											direction: "row",
											align: "center",
											spacing: 5,
											items: [{
												type: 'textbox', 
												name: 'speedin',
												size: 5,
												values: JV.aname 
											},
											{
												type: "label",
												text: "ms"
											}]
										},
										{
											type: 'listbox', 
											name: 'splitin',
											label: 'Split Text per',
											values: JV.splitin 
										},
										{
											type: "container",
											label: "Split Delay",
											layout: "flex",
											direction: "row",
											align: "center",
											spacing: 5,
											items: [{
												type: 'textbox', 
												name: 'splitdelayin',
												size: 5
											},
											{
												type: "label",
												text: "ms (keep it low i.e. 1- 200)"
											}]
										}	
									]
                            	},
                            	{
									title: "End Transition (optional)",
									type: "form",
									items: [
										{
											type: 'listbox', 
											minWidth: 350,
											name: 'animationout', label: 'Animation name',
											values: [
												{ value: 'auto', text: 'Choose Automatic' },
												{ value: 'fadeout', text: 'Fade Out' },
												{ value: 'stt', text: 'Short to Top' },
												{ value: 'stb', text: 'Short to Bottom' },
												{ value: 'stl', text: 'Short to Left' },
												{ value: 'str', text: 'Short to Right' },
												{ value: 'ltt', text: 'Long to Top' },
												{ value: 'ltb', text: 'Long to Bottom' },
												{ value: 'ltl', text: 'Long to Left' },
												{ value: 'ltr', text: 'Long to Right' },
												{ value: 'skewtoright', text: 'Skew To Right' },
												{ value: 'skewtoleft', text: 'Skew To Left' },
												{ value: 'skewtorightshort', text: 'Skew To Right Short' },
												{ value: 'skewtoleftshort', text: 'Skew To Left Short' },
												{ value: 'randomrotateout', text: 'Random Rotate Out' },
												{ value: 'customout', text: 'Custom', onselect: function(){ transitcustom('out')}  }
											] 
										},
										{
											type: 'listbox', 
											name: 'easingout', label: 'Easing',
											size: 100,
											values: JV.easing 
										},
										{
											type: "container",
											label: "Speed",
											layout: "flex",
											direction: "row",
											align: "center",
											spacing: 5,
											items: [{
												type: 'textbox', 
												name: 'speedout',
												size: 5,
												values: JV.aname 
											},
											{
												type: "label",
												text: "ms"
											}]
										},
										{
											type: 'listbox', 
											name: 'splitout',
											label: 'Split Text per',
											values: JV.splitin 
										},
										{
											type: "container",
											label: "Split Delay",
											layout: "flex",
											direction: "row",
											align: "center",
											spacing: 5,
											items: [{
												type: 'textbox', 
												name: 'splitdelayout',
												size: 5
											},
											{
												type: "label",
												text: "ms (keep it low i.e. 1- 200)"
											}]
										}	
									],
									onsubmit: function(){
										f.s.call( 'transit', this.toJSON() );
									}
                            	},
                            	{
									title: "Loop Animation",
									type: "form",
									items: [
										{
											type: 'listbox', 
											name: 'loop_type',
											label: 'Type',
											values: [
												{ value: 'none', text: 'Disabled' },
												{ value: 'rs-pendulum', text: 'Pendulum', onselect: d_looptype },
												{ value: 'rs-rotate', text: 'Rotate', onselect: d_looptype },
												{ value: 'rs-slideloop', text: 'Slideloop', onselect: d_looptype },
												{ value: 'rs-pulse', text: 'Pulse', onselect: d_looptype},
												{ value: 'rs-wave', text: 'Wave', onselect: d_looptype }
											]
										}	
									]
                            	}       
                            ],
                            onsubmit: function(e) {                             
								f.s.call( 'transit', this.toJSON() );                                                                   
                            }
                        });           
                    }
                });

                // Timing & Sort
                editor.addMenuItem('jvtiming', {
                    text: 'Timing & Sort',
                    context: 'tools',
                    onclick: function() {
                        // Open window
                        editor.windowManager.open({
                            title: 'Timing & Sort',
                            data: (function(){
                            	var ftiming = $( layer.attr('data-timeline') ).find('[data-field]')
                            		,r = ftiming.filter( sf.call( 'range' ) ).val().split(";")
                            	;

                            	return {
                            		from: r[0],
                            		to: r[1],
                            		zindex: ftiming.filter( sf.call( 'zindex' ) ).val()
                            	};
                            })(),
                            body: [
                            	{
                            		label: 'Time',
                            		type: 'container',
                            		layout: 'flex',
                            		direction: 'row',
                            		align: 'center',
                            		spacing: 5,
                            		items: [
										{
											name: 'from',
											type: 'textbox'
										},
										{
											type: 'label',
											text: 'ms'	
										},
										{
											name: 'to',
											type: 'textbox'
										},
										{
											type: 'label',
											text: 'ms'	
										}
                            		]
                            	},
                            	{
                            		label: 'z-index',
                            		type: 'textbox',
                            		name: 'zindex'
                            	}
                            ],
                            onsubmit: function(){
                            	var ftiming = $( layer.attr('data-timeline') ).find('[data-field]')
                            		,d = this.toJSON()
                            	;

								ftiming.filter( sf.call( 'range' ) ).data( 'ionRangeSlider' ).update({
									from: d.from,
									to: d.to
								});
								
								ftiming.filter( sf.call( 'zindex' ) ).val( d.zindex );
                            }
                        });
                    }
                });

            });

	        return this;
		},
		stinymce: function() {
			tinymce.init({
                selector: '#' + this.attr('id'),
                inline: true,
                theme: "modern",
                plugins: [
                    "advlist autolink lists link charmap preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern",
                    "dlayer jvimage jvtransition"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "preview media | forecolor backcolor emoticons",
                image_advtab: true,
                relative_urls: false,
                forced_root_block: false
    
           });
			return this;
		},
		addLayer: function(o){
			o = $.extend(o, {
				tmpl: $('[data-tmpl="layer"]').html()
			});
			var doc = $(document);
			return this.delegate('[data-action="add-layer"]', 'click.add-layer', function(){
				var 
					$this = $(this)
					,s = $($this.data('layer'))
					;
				;

				doc.trigger('add-layer', [ s, {id: $.now(), index: $this.data('index')} ]);
			}).delegate('[data-field="zstyle"]', 'change', function(){
				var s = $(this).parent( '[data-tag="drag"]' ).children( '[data-tag="mce"]' );
				$.each( JV.zstyle, function(){
					s.removeClass( this.value );
				});
				s.addClass( this.value );
			}).on({
				'add-layer': function(e, t, d){
					$.each(d, function(k, v) {
						if($.type(v) === 'object') {
							$.extend(d, v);
							d[v] = undefined;	
						}
					});

					if( d.loop && d.loop_type && ( loop_type = d.loop[ d.loop_type ]) ) {
						$.each( loop_type, function(k, v) {
							d[ k ] = v;
						});
					}
					
					var layer = $.tmpl(o.tmpl, d);
					t.append(layer);
					layer.find('[data-tag="mce"]').html(d.content);
					layer.find('[data-field="content"]').val(d.content);
					layer.find('[data-field="zstyle"]').trigger('change');
					layer.sdrag().children('[data-tag="mce"]').stinymce();
					var dl = layer.data('draggabilly');
					dl.position = layer.toLayerPosition();/*{ x: JV.toNum.call(d.x), y: JV.toNum.call(d.y) };*/
					dl.setLeftTop();

					doc.trigger('make-timeline', [ t.closest('[data-tag="editor"]').find('[data-tag="timeline"]'), d ]);
				},
				'make-timeline': function(e, t, d){

					// make sort
					t.children().length || t.sortable({
						containment: 'parent',
						handle: '.handle',
						stop: function(){
							$(this).children().each(function(i){
								$(this).find('[data-tag="zindex"]').val(i);
							});
						}
					});
					// make item
					$.each(JV.orange, function(k, v){
						if( !(k in d) || !d[k] ) {
							d[k] = v;
						}
					});

					var s = $.tmpl( $(t.data('child')).val(), d);
					s.find('[data-tag="range"]').ionRangeSlider();
					t.append( s );
				}
			});
		},
        sbox: function(o) {
            o.imageok.on({
                'fieldid': function(e, f){
                    var btns = $(this).find('[data-tag="image"]').contents()
                    .find('[onclick*="window.parent.jInsertFieldValue"]');  
                    
                    btns.attr({ 'onclick': btns.attr('onclick').replace(/'layer.+'|'jform_images_image_intro'|'mceu_\d+'/, "'" + f + "'" ) });  
                }
            });
            return this.delegate('[data-tag="browse-img"]', 'click.browse-img', function(){
                var target = $(this.hash);
                
                target.trigger('fieldid', [ $(this).data('field') ]);
                target.modal('show');
                
                return false;
            });
        },
        beditor: function() {
            function b(d) {
                if( 'background-image' in d ) {
                    d['background-image'] = 'url(zurl)'.replace(/zurl/, d['background-image']);
                }
                return d;
            }
            return this.delegate('[data-tag="layer"]', 'editor-change', function(e, a){
                $(this).css(a);    
            }).delegate('[data-change-editor]', 'change', function(){
                var f = $(this)
                    ,a = {}
                ;
                a[f.data('kcss')] = this.value;
                b(a);
                $(f.data('change-editor')).trigger('editor-change', [a])
            });
        },
        loadCache: function(o) {

    		var doc = this;

        	if(!JSON.validate(o.c)) { return this; }

        	$.each(JSON.decode(o.c), function(id, item){
        		item.index = id;
        		doc.trigger('load-data-field', [o.p, item]);
        	});

			return this;
        },
        toggleEditor: function(){
        	return this.delegate('[data-mce]', 'click.data-mce', function(){

    			var es = tinymce.editors,
    				eid = $(this).data('mce')
    			;
    			!es.length || $.each(es, function(i, e){
    				if( e.id === eid) {
    					e.focus(); e.fire('activate');		
    				}else {
    					e.fire('deactivate');
    				}
    			});
    			
        	});
        },
        inlineConfig: function(d){
        	var 
        		t = this.first()
        		,m = t.find('[data-tmpl="inline-settings"]')
        		,s = $.tmpl( m.val(), d )
        	;
               
        	m.replaceWith( s );
        	s.sbutton();
        	s.schosen();
        	s.sminicolors();
        	t.delegate('[data-active="save"]', 'click', function(){
        		var delay = t.find('[data-field="delay"]').val();
    			delay = JV.toNum.call(delay) || 9000;
    			JV.orange.to = delay,
				JV.orange.max = delay;

				// re-build range
				t.trigger('re-build');

				t.modal('hide');

				return false;
        	}).on({
        		're-build': function(){
        		    t.trigger('build-range');
                    t.trigger('build-sizer');	
        		},
                'build-range': function(){
                    $('[data-tag="range"]').each(function(){

                        var r = $(this).data('ionRangeSlider');
                        !r || r.update({ max: JV.orange.max });
                        
                    });      
                },
                'build-sizer': function(){
                    $('#layer-sizer').remove();
                    var s = '.com_jvss .tab-content .tab-pane [data-tag="layer"].layer {width: zw;height: zhpx;min-height: initial;} [data-tag="mark-content"] {width: mwpx;height: zhpx;}'
                    	,w = t.find('[data-field="width"]').val()
                    	,h = t.find('[data-field="height"]').val()
                        ,zw = t.find('[name="sconfig[slider_type]"]:checked').val().match(/fullwidth/) ? '100%' : ( w + 'px' )
                    ;
                    s = s.replace(/zw/, zw);
                    s = s.replace(/zh/g, h);
                    s = s.replace(/mw/, w);
                    $('<style>', { id: 'layer-sizer', html: s}).appendTo('body');
                    
                    $(document).trigger( 're-pos-layer' );
                },
                'include-css': function() {
                	var s = $('<style>');
                	$('body').append( s );
                	s.load('index.php?option=com_jvss&task=item.includeCss');
                }
        	});
            t
            .trigger('build-sizer')
            .trigger('include-css');
        	return this;
        },
        toObject: function() {
        	var d = {};
        	this.each(function(){
        		
        		var t = $(this);

        		if( this.type && this.type.match(/radio/) ) {
        			d[ t.attr( 'data-field' ) ] = $( this.name ).val();
        		}
        		d[ t.attr( 'data-field' ) ] = t.val();
        	});
        	return d;
        },
        qtitle: function(){
        	return this.delegate('[data-field="title"]', 'keyup.field-title', function(){
        		$( $(this).data( 'view' ) ).text( this.value || 'Slide' );
        	});
        },
        cloneSlide: function(){
        	var doc = this;
        	return this.delegate('[data-action="clone"]', 'click.clone-slide', function(e){
        		var t = $(this)
        			,r = $( t.attr( 'data-region' ) )
        			,d = r.find( '[data-field]:not([data-gchild])' ).toObject()
        			,childs = {}
        		;
        		r.find( '[data-gchild]' ).each(function(){
        			var f = $( this )
        				id = f.attr( 'data-gchild' )
        			;
        			id in childs || ( childs[ id ] = {} );
        			childs[ id ][ f.attr( 'data-field' ) ] = this.value;
        		});
        		d = $.extend( d, { index: e.timeStamp, items: [] });
        		$.each( childs, function( k, item ){
        			d.items.push( item );
        		});
        		doc.trigger('load-data-field', [ $( t.attr( 'data-builder' ) ), d ] );
        	});
        },
        sortSlide: function(){
        	var ms = '> li:not(.no-drag)';
        	function s( e, ui ){
        		var t = $(this)
        			,itarget = JV.toNum.call( t.attr( 'data-nitem' ) ) * ui.item.index()
        			,source = $( t.attr( 'data-sitem' ) )
        			,target = source.filter( '[data-sindex="' + ui.item.attr( 'data-sindex' ) + '"]' )
        		;

        		target.insertAfter( source.eq( itarget ) );
        	}
        	function i(){
        		$(this).sortable({
        			items: ms,
        			containment: 'parent',
        			stop: s
        		})
        	}
        	return this.each(i);
        },
        bindLayerPosition: function(){
            var htmlLayer = $(this)
                ,objLayer = htmlLayer.find( '[data-field="pos"]' ).val()
                ,layerWidth = htmlLayer.outerWidth()
                ,layerHeight = htmlLayer.outerHeight()
                
                ,container = htmlLayer.closest( '[data-tag="layer"]' ).children('[data-tag="mark-content"]')
                ,totalWidth = container.width()
                ,totalHeight = container.height()
                
                ,position = htmlLayer.position()
                ,posTop = Math.round( position.top )
                ,posLeft = position.left
                
                ,updateY,updateX;
            ;
            objLayer = objLayer || 'lt';
            var objLayer = {
                align_hor: objLayer.substr( 0, 1 ),
                align_vert: objLayer.substr( 1, 1 )    
            };
            switch(objLayer.align_hor){
                case "l":
                    updateX = posLeft;
                break;
                case "r":
                    updateX = totalWidth - posLeft - layerWidth;
                break;
                case "c":
                    updateX = posLeft - ( totalWidth - layerWidth ) / 2;
                break;
            }

            switch(objLayer.align_vert){
                case "t":
                    updateY = posTop;
                break;
                case "b":
                    updateY = totalHeight - posTop - layerHeight;
                break;
                case "c":
                    updateY = posTop - ( totalHeight - layerHeight ) / 2;
                break;
            }
            
            htmlLayer.find( '[data-field="x"]' ).val( updateX );
            htmlLayer.find( '[data-field="y"]' ).val( updateY );
        },
        toLayerPosition: function(){
            var htmlLayer = $(this)
                ,objLayer = htmlLayer.find( '[data-field="pos"]' ).val()
                ,layerWidth = htmlLayer.outerWidth()
                ,layerHeight = htmlLayer.outerHeight()
                
                ,container = htmlLayer.closest( '[data-tag="layer"]' ).children('[data-tag="mark-content"]')
                ,oc			= container.offset()
                ,totalWidth = container.width()
                ,totalHeight = container.height()
                
                ,layerPos = {}                          
                
                ,left = htmlLayer.find( '[data-field="x"]' ).val()
                ,top = htmlLayer.find( '[data-field="y"]' ).val()
                
                ,objCss = {}
            ;
            
            left = JV.toNum.call( left );
            top = JV.toNum.call( top );
            objLayer = objLayer || 'lt';
            var objLayer = {
                align_hor: objLayer.substr( 0, 1 ),
                align_vert: objLayer.substr( 1, 1 )    
            };
            switch(objLayer.align_hor){
                default:
                case "l":
                    objCss["right"] = "auto";
                    objCss["left"]  = left + "px";

                break;
                case "r":
                    objCss["left"]  = "auto";
                    objCss["right"] = left + "px";
                break;
                case "c":
                    var realLeft 	= ( totalWidth - layerWidth ) / 2;
                    realLeft 		= realLeft + left;
                    objCss["left"] 	= realLeft + "px";
                    objCss["right"] = "auto";
                break;
            }

            //handle vertical
            switch(objLayer.align_vert){
                default:
                case "t":
                    objCss["bottom"] = "auto";
                    objCss["top"] = top + "px";
                break;
                case "c":
                    var realTop         = ( totalHeight - layerHeight ) / 2;
                    realTop 			= realTop + top;
                    objCss["top"]       = realTop + "px";
                    objCss["bottom"]    = "auto";
                break;
                case "b":
                    objCss["top"]       = "auto";
                    objCss["bottom"]    = top + "px";
                break;
            }
            
            htmlLayer.css( objCss );
            layerPos = htmlLayer.position();
            
            return { x: layerPos.left, y: layerPos.top };               
        },
        posLayer: function(){
            var t = this;
            return t.on({
                're-pos-layer': function(){
                    $('.editor.active [data-tag="drag"]').each(function(){
                        $(this).toLayerPosition(); 
                    });
                }
            }).delegate('[href^="#editor-"], #inline-config [data-active="save"]', 'click', function(){
                t.trigger( 're-pos-layer' );
            });
        },
        sminicolors: function(){

        	return this.each( function() {

        		$( this ).find('.minicolors').each(function(){
					
					$(this).minicolors( JV.ominicolors );

				});
        	} );	
        },
        schosen: function(){
			
        	return this.each( function(){

        		var e = $( this );
        		
        		e.find('select:not(.chzn-custom-value)').chosen(JV.ochosen.normal);
				
				e.find('.chzn-custom-value').chosen(JV.ochosen.custom);

        	} );	
        },
        sbutton: function(){

        	return this.each( function(){
				
				var e = $( this );

        		e.find('.radio.btn-group label').addClass('btn');
				e.find('.btn-group label:not(.active)').click(function() {
					var label = $(this);
					var input = $('#' + label.attr('for'));

					if (!input.prop('checked')) {
						label.closest('.btn-group').find('label').removeClass('active btn-success btn-danger btn-primary');
						if (input.val() == '') {
							label.addClass('active btn-primary');
						} else if (input.val() == 0) {
							label.addClass('active btn-danger');
						} else {
							label.addClass('active btn-success');
						}
						input.prop('checked', true);
						input.trigger('change');
					}
				});
				e.find('.btn-group input[checked=checked]').each( function() {
					if ($(this).val() == '') {
						$('label[for=' + $(this).attr('id') + ']').addClass('active btn-primary');
					} else if ($(this).val() == 0) {
						$('label[for=' + $(this).attr('id') + ']').addClass('active btn-danger');
					} else {
						$('label[for=' + $(this).attr('id') + ']').addClass('active btn-success');
					}
				});
        	} );	
        },
        renderlayout: function( layouts ) {
            
            if( !layouts || !layouts.length ) { return false; }
            
            var 
            	  lm 		= layouts.length
				, uprogress = 100 / lm
                , i 		= 0
                , m 		= this.first()
            ;
            
            m.modal( 'show' ).on({
                
                'setProgress': function( e, v ) {
                    
                    $( this ).find( '.progress .bar' ).each( function(){
                        
                        var e = $( this ).css( { width: v * uprogress + '%' } );

                        if( v === lm ) {
							
							e.one({

								'transitionend webkitTransitionEnd oTransitionEnd': function(){
									
									m.modal( 'hide' ).trigger( 'applyConfig' ).one( 'hidden.bs.modal', function() {

										$( this ).remove();

									} );
								}
								
							} );
                        }

                    } );
                }
            });
            
            function run() {
                
                var layout = layouts[ i ]
                ;    
                
                if( !layout ) {
                    
                    m.trigger( 'setProgress', lm );
                    
                    return false;
                }
                
                $( layout.selector ).load( location.href, layout.param, run );

                i ++;

                m.trigger( 'setProgress', i );
            }
            
            run( ); 
            
            return this;
        }
	});
})(jQuery);