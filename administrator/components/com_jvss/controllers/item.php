<?php
/**
 * @version     1.0.0
 * @package     com_jvss
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Joomlavi <info@joomlavi.com> - http://www.joomalvi.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');
jimport( 'joomla.filesystem.archive' );
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

/**
 * Item controller class.
 */
class JvssControllerItem extends JControllerForm
{

    function __construct() {
        $this->view_list = 'items';
        parent::__construct();
    }
    
    public function includeCss(){
        
        $styles = JFactory::getDbo()
        ->setQuery("SELECT handle, params FROM `#__jvss_css`;")
        ->loadObjectList();
        
        if( $styles && count($styles) ) {
            
            $str = "";
            foreach( $styles as $sitem ) {
                $str .= sprintf("%s { %s }\n", $sitem->handle, self::obj2Css( json_decode( $sitem->params ) ) );
            }  
            
            die( $str );  
        }
        
        return die();
    }
    
    public static function obj2Css( $arr = array() ){
        $str = ""; 
        foreach( $arr as $k => $v ) {
            $str .= "{$k}:{$v};";    
        }
        return $str;   
    }
    
    public function backUrl() {
        $this->setRedirect( JRoute::_( 'index.php?option=com_jvss' ) );
        $this->redirect();
    }
    
    public function import(){
        $path_temp = JPATH_SITE . "/tmp/";
        $extractdir = $path_temp . time();
        $file = JRequest::getVar('pkg', array(), 'files', 'array');
        
        if( !count( $file ) ) {
            $this->setError('COM_JVSS_ERROR_NO_FILE');
            $this->backUrl();
            return false;
        }; 
        $fpath = $path_temp . $file[ 'name' ];
        JFile::upload( $file[ 'tmp_name' ], $fpath );
        JFolder::exists( $extractdir ) || JFolder::create( $extractdir );              
        if( !JArchive::extract( $fpath, $extractdir ) ) {
            
            $this->setError('COM_JVSS_ERROR_EXTRACT');
            $this->backUrl();
            return false;
        }
        
        // copy image
        $aimage     =  "{$extractdir}/images";
        if( JFolder::exists( $aimage ) ) {
            
            $cf     = end( JFolder::listFolderTree( $aimage ) );
            $cf     = $cf[ "fullname" ];
            $dest   = end( explode( "images", $cf ));
            $dest   =  JPATH_SITE . "/images{$dest}";
            JFolder::exists( $dest ) || JFolder::create( $dest );
            
            foreach( JFolder::files( $cf ) as $i ) {
                JFile::copy( "{$cf}/{$i}", "{$dest}/{$i}" );
            }
        }
        
        // get animate custom
        $acustom = array();
        $acustom_export = "{$extractdir}/custom_animations.txt";
        if( JFile::exists( $acustom_export ) ) {
            
            $acustom    = unserialize( file_get_contents( $acustom_export ) );  
            $acustom    = JvssHelper::buildAnimationCustom( $acustom );   
        }
        
        
        // get data slider
        $slider_export = "{$extractdir}/slider_export.txt";
        $slider = array( 'customcss' => '' );
        if( JFile::exists( $slider_export ) ) {
            
            $slider_export          = unserialize( file_get_contents( $slider_export ) ); 
            
            // params of slider
            $params                 = JvssHelper::getArray( $slider_export, 'params', array() );
            $slider[ 'name' ]       = JvssHelper::getArray( $params, 'title', '' );
            $slider[ 'sconfig' ]    = JvssHelper::filterParams( $params );
            $slider[ 'state' ]      = 1;
            $slider[ 'customcss' ] .= stripslashes( JvssHelper::getArray( $params, 'custom_css', '' ) );
            
            // slides         
            $slides                 = array();
            foreach( JvssHelper::getArray( $slider_export, 'slides', array() ) as $k => $slide ) {
                
                // params of slide
                $oparams_slide              = JvssHelper::getArray( $slide, 'params', array() );
                $params_slide               = $oparams_slide;
                $bgsrc                      = JUri::root() . "images/" . JvssHelper::getArray( $params_slide, 'image', '' );   
                $params_slide[ 'state' ]    = JvssHelper::getState( JvssHelper::getArray( $params_slide, 'state', '' ) );  
                $params_slide               = JvssHelper::mapKey( $params_slide, 'slide' );
                $params_slide               = JvssHelper::getAttrBg( $oparams_slide, $params_slide );
                $params_slide[ 'bgsrc' ]    = $bgsrc;
                
                // item in slide
                $layers = array();
                foreach( JvssHelper::getArray( $slide, 'layers', array() ) as $k => $item ) {
                   
                    // params of item
                    $item_type      = JvssHelper::getArray( $item, 'type', '' ); 
                    $params_item    = JvssHelper::mapKey( $item, 'layer' ); 
                    
                    // params of item - part loop
                    if( ( $loop_animation = JvssHelper::getArray( $item, 'loop_animation', '' ) ) && $loop_animation !== 'none' ) {
                        
                    } 
                    
                    // params of item - part content
                    $params_item[ 'content' ]   = JvssHelper::getLayerType( $item );
                    
                    // params of item - part time
                    $params_item[ 'timeline' ]  = JvssHelper::getTimer( $item );
                    
                    // params of item - part position
                    $params_item[ 'pos' ]       = JvssHelper::getPosition( $item );
                    
                    // params of item - part animationcustomin | animationcustomout
                    $acustom_types               = JvssHelper::getAnimationCustomType( $item );
                    if( count( $acustom ) && count( $acustom_types ) ) {
                        
                        $params_item            = JvssHelper::getAnimationCustom( $acustom, $acustom_types, $params_item );
                        
                    }
                    
                    $layers[ rand( 0, 1000 ) ]  = $params_item;  
                }
                
                $params_slide[ 'items' ]    = $layers;
                $slides[ rand( 0, 1000 ) ]  = $params_slide;
            }
            $slider[ 'params' ] = $slides;   
        }
        
        // get css
        foreach( Jfolder::files( $extractdir, '\.css$' ) as $stylesshet ) {
            $sspath = "{$extractdir}/{$stylesshet}";
            if( !JFile::exists( $sspath ) ) { continue; }
            $slider[ 'customcss' ] .= file_get_contents( $sspath );    
        }
        
        JTable::addIncludePath(implode( DS, array( JPATH_ADMINISTRATOR, "components", "com_jvss", "tables" ) ) );
        $tbl = JTable::getInstance( 'item', 'JvssTable' );
        
        if( ($id = JRequest::getInt( 'id', 0) ) && $tbl->load( $id ) ) {
            
        }
        
        if( !$tbl->bind( $slider ) ) {
            
            return JError::raiseWarning( 500, $row->getError() );
        }
        
        if( !$tbl->store() ) {
            
             JError::raiseWarning( 500, $row->getError() );    
        }
        
        // empty file upload
        !JFolder::exists( $extractdir ) || JFolder::delete( $extractdir );   
        !JFile::exists( $fpath ) || JFile::delete( $fpath );
        
        
        // callback to edit
        $this->setMessage( JText::_( 'COM_JVSS_MSG_IMPORT' ) );
        $this->setRedirect( JRoute::_( "index.php?option=com_jvss&task=item.edit&id={$tbl->id}", false ) );
        $this->redirect();                                
    }

}