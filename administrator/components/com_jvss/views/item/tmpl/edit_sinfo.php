<div class="control-group iname">
	<div class="control-label">Slide Title:</div>
	<div class="controls">
		<input type="text" name="layer[${index}][title]" value="${title}" 
		data-view="#title-${index}" data-field="title">
	</div>
</div><!-- div.iname -->

<div class="control-group istate">
	<div class="control-label">State:</div>
	<div class="controls">
		<select name="layer[${index}][state]" data-field="state">
			<option value="1"
            {{if state}}
                {{if state == 1}} selected="selected"{{/if}}
            {{/if}}
            >True</option>
			<option value="0"
            {{if state}}
                 {{if state == 0}} selected="selected"{{/if}}
            {{/if}}>False</option>
		</select>
	</div>
</div><!-- div.istate -->