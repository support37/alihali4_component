<div role="tabpanel">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist" 
    data-tag="sortslide" data-nitem="5" 
    data-sitem='[data-tag="rslides"] > .tab-pane'>
        <li class="no-drag">
            <a href="javascript:void(0)" data-action="new-slide" data-target='[data-tag="rslides"]'> 
            <span class="icon-new"></span> New Slide
            </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content" data-tag="rslides">
        
    </div><!-- div.tab-content -->

</div><!-- [role="tabpanel"] -->

<textarea class="hidden" data-tmpl="tab-name">
    <li class="dropdown" data-tag="tab-name" data-sindex="${index}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span id="title-${index}">Slide</span>
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#sinfo-${index}" data-toggle="tab">Info</a></li>
          <li><a href="#simg-${index}" data-toggle="tab">Main Image / Background</a></li>
          <li class="divider"></li>
          <li><a href="#editor-${index}" data-toggle="tab">Editor</a></li>     
          <li><a href="#editor-${index}" data-action="add-layer" 
          data-index="${index}" data-layer='#layer-${index} > [data-tag="mark-content"]' data-toggle="tab">Add layer</a></li>
          <li class="divider"></li>
          <li><a href="#config-${index}" data-toggle="tab">Configuration</a></li>
          <li><a href="#sattr-${index}" data-toggle="tab">Attribute</a></li>
          <li class="divider"></li>
          <li><a href="javascript:void(0)"
          data-action="clone"
          data-builder='[data-action="new-slide"]'
          data-region='#simg-${index},#editor-${index},#config-${index},#sattr-${index},#sinfo-${index}'>Clone</a></li>
          <li><a href="javascript:void(0)" data-action="remove-slide" 
          data-target='#simg-${index},#editor-${index},#config-${index},#sattr-${index},#sinfo-${index}'>Remove</a></li>
        </ul>
    </li>
</textarea>

<textarea class="hidden" data-tmpl="tab-content">
    <div class="tab-pane editor" id="editor-${index}" 
    data-tag="editor" data-sindex="${index}">
        <div class="e-scroll" data-tag="scroll">
            <div id="layer-${index}" class="layer" data-tag="layer">
                <div class="" data-tag="mark-content">
                    <div class="inner"></div>
                </div>
            </div>
        </div>
        <h3 class="page-header">Timeline</h3>
        <div class="timeline" data-child="#timeline-item" data-tag="timeline"></div>
    </div><!-- div.editor -->

    <div class="tab-pane sinfo" id="sinfo-${index}" data-sindex="${index}">
        <?php echo $this->loadTemplate('sinfo')?>    
    </div><!-- div.sinfo -->

    <div class="tab-pane config" id="config-${index}" data-sindex="${index}">
        <?php echo $this->loadTemplate('config')?>
    </div><!-- div.config -->

    <div class="tab-pane simg active" id="simg-${index}" data-sindex="${index}">
        <?php echo $this->loadTemplate('simg')?>
    </div><!-- div.simg -->

    <div class="tab-pane sattr" id="sattr-${index}" data-sindex="${index}">
        <?php echo $this->loadTemplate('sattr')?>
    </div><!-- div.sattr -->
</textarea>                                                             

<textarea id="timeline-item" class="hidden" data-tag="timeline-item">
    <div id="timeline-item-${id}" class="item clearfix">
        <div class="span1 handle text-center" data-mce="item-mce-${id}">
            <span type="button" class="btn btn-small"><i class="icon-move"></i></span>
        </div>
        <div class="span11">
            <input type="text" name="layer[${index}][items][${id}][timeline]"
            data-tag="range" data-type="double" data-min="0"
            data-drag-interval="true" data-grid="true" data-field="range"
            data-max="${max}" data-postfix="ms" value="${timeline}"
            data-gchild="${id}">
            <input type="hidden" name="layer[${index}][items][${id}][zIndex]"
            data-tag="zindex" value="${zIndex}" data-field="zindex"
            data-gchild="${id}">
        </div>
    </div>
</textarea>
 
<!-- Modal -->
<div id="imageok" class="modal hide fade" tabindex="-1" 
role="dialog" aria-labelledby="imageokLabel" aria-hidden="true"
data-path="<?php echo JUri::root()?>">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="imageokLabel">Image ok</h3>
    </div>
    <div class="modal-body">
        <iframe data-tag="image" src="<?php echo JUri::base()."index.php?option=com_media&view=images&tmpl=component&fieldid=jform_images_image_intro"?>" height="400"></iframe>
    </div> 
</div>