<?php
/**
 * @version     1.0.0
 * @package     com_jvss
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Joomlavi <info@joomlavi.com> - http://www.joomalvi.com
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.framework');   
JHtml::_('behavior.tooltip');
JHtml::_('behavior.colorpicker');
JHtml::_('behavior.formvalidation');   
JHtml::_('jquery.ui', array('core', 'sortable'));   
JHtml::_('formbehavior.chosen', 'select:not(.chzn-custom-value)');
JHtml::_('formbehavior.chosen', '.chzn-custom-value', null, array('disable_search_threshold'=>1));

JText::script('JGLOBAL_VALIDATION_FORM_FAILED');

// Import CSS
$document = JFactory::getDocument();
$assets = "components/com_jvss/assets/";
$document->addStyleSheet("{$assets}css/jvss.css");                     

/* Import Editor tinymce */       
$document->addScript(JUri::root()."media/editors/tinymce/tinymce.min.js"); 

/* Import Editor codemirror */ 
$document->addStyleSheet(JUri::root()."media/editors/codemirror/lib/codemirror.css");
$document->addStyleSheet(JUri::root()."media/editors/codemirror/addon/hint/show-hint.css");
JHtml::script(JUri::root()."media/editors/codemirror/lib/codemirror.js");
JHtml::script(JUri::root()."media/editors/codemirror/addon/hint/show-hint.js");
JHtml::script(JUri::root()."media/editors/codemirror/addon/hint/css-hint.js");
JHtml::script(JUri::root()."media/editors/codemirror/mode/css/css.js");
JHtml::script( JUri::root()."media/editors/codemirror/addon/display/fullscreen.js"          );

/* Import range slider */

$document->addStyleSheet("{$assets}css/ion.rangeSlider.css");
$document->addStyleSheet("{$assets}css/ion.rangeSlider.skinFlat.css");  
$document->addScript("{$assets}js/ion.rangeSlider.js");

/* Import function tmpl */
$document->addScript("{$assets}js/jquery.tmpl.min.js"); 

/* Import function plugin drag */
$document->addScript("{$assets}js/draggabilly.pkgd.js");

/* Import function plugin */
$document->addScript("{$assets}js/jquery.jvss.func.js");
$document->addScript("{$assets}js/do.js");

?>

<form action="<?php echo JRoute::_('index.php?option=com_jvss&layout=edit&id=' . (int) $this->item->id); ?>" 
method="post" enctype="multipart/form-data" name="adminForm" id="item-form" class="form-validate">
	
    <div class="form-horizontal">
        
        <div class="exclude-custom-param">
            
            <div class="control-group">
                <div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
                <div class="controls"><?php echo $this->form->getInput('name'); ?></div>
            </div>
            
            <input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
            <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
            <input type="hidden" name="task" value="" />
            <?php echo JHtml::_('form.token'); ?>
            
        </div>

		<div class="slides" data-tag="slides"></div>
		
		<div data-tag="wsconfig"></div>
        <div data-tag="wscss"></div>               
    </div>
</form>

<div id="render_layout" class="modal hide fade" data-backdrop="static">
  
    <div class="modal-body">
        <div class="progress progress-success progress-striped">
            <div class="bar" style="width: 0%"></div>
        </div>
    </div>  
    
</div>

<div data-tag="wsimport"></div>              

<textarea data-tmpl="cache" class="hidden"><?php echo json_encode($this->item->params)?></textarea>
<textarea name="jform[sconfig]" id="jform_sconfig" class="hidden"><?php echo $this->item->sconfig; ?></textarea>


<script type="text/html" data-tmpl="layer" class="hidden"></script>
<script>
	window.JV = jQuery.extend(window.JV, {
		ochosen: {
			normal: {
				"disable_search_threshold":10,
				"allow_single_deselect":true,
				"placeholder_text_multiple":"Select some options",
				"placeholder_text_single":"Select an option",
				"no_results_text":"No results match"
			},
			custom: {
				"disable_search_threshold":1,
				"allow_single_deselect":true,
				"placeholder_text_multiple":"Select some options",
				"placeholder_text_single":"Select an option",
				"no_results_text":"No results match"
			}
		},
		ominicolors: {
			control: 'hue',
			position: 'right',
			theme: 'bootstrap'
		}
	});
</script>