<h3 class="page-header">Attribute</h3>
<div class="control-group">
    <div class="control-label">Class: </div>
    <div class="controls">
    	<input type="text" 
        name="layer[${index}][zclass]" id="" class="span12"
        value="${zclass}" data-field="zclass">
    	<span class="help-block">Adds a unique class to the li of the Slide like class="rev_special_class" (add only the classnames, seperated by space)</span>
    </div>
</div>

<div class="control-group">
    <div class="control-label">ID: </div>
    <div class="controls">
    	<input type="text" 
        name="layer[${index}][id]" id="" class="span12"
        value="${id}" data-field="id">
    	<span class="help-block"> 	Adds a unique ID to the li of the Slide like id="rev_special_id" (add only the id)</span>
    </div>
</div>

<div class="control-group">
    <div class="control-label">Attribute: </div>
    <div class="controls">
        <input type="text" 
        name="layer[${index}][attr]" id="" class="span12"
        value="${attr}" data-field="attr">
        <span class="help-block">Add as many attributes as you wish here. (i.e.: data-layer="firstlayer" data-custom="somevalue")</span>
    </div>
</div>
