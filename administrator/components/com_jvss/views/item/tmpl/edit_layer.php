<div class="item" id="item-${id}" data-tag="drag" data-timeline="#timeline-item-${id}">
        <span class="handle icon-move"></span>
        <div id="item-mce-${id}" class="item-mce tp-caption" data-tag="mce">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div> 
        
        <input type="hidden" name="layer[${index}][items][${id}][pos]" 
        data-group="pos-style" data-field="pos" value="${pos}" data-gchild="${id}">      
        <input type="hidden" name="layer[${index}][items][${id}][x]" 
        data-group="pos-style" data-field="x" value="${x}" data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][y]" 
        data-group="pos-style" data-field="y" value="${y}" data-gchild="${id}"> 
        
        <input type="hidden" name="layer[${index}][items][${id}][ws]" value="normal" 
        data-group="pos-style" data-field="ws" data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][mw]" value="auto" 
        data-group="pos-style" data-field="mw" data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][mh]" value="auto" 
        data-group="pos-style" data-field="mh" data-gchild="${id}"> 
        
        <input type="hidden" name="layer[${index}][items][${id}][zstyle]"
        data-group="pos-style" data-field="zstyle" value="${zstyle}" data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][zid]"
        data-group="pos-style" data-field="zid" value="${zid}" data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][zclass]"
        data-group="pos-style" data-field="zclass" value="${zclass}" data-gchild="${id}">
        
        <!-- LAYER ANIMATION -->
        <input type="hidden" name="layer[${index}][items][${id}][animationin]"
        data-group="transit" data-field="animationin" value="${animationin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][easing]"
        data-group="transit" data-field="easing" value="${easing}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][speedin]"
        data-group="transit" data-field="speedin" value="${speedin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][splitin]"
        data-group="transit" data-field="splitin" value="${splitin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][splitdelayin]" 
        data-group="transit" data-field="splitdelayin" value="${splitdelayin}"
        data-gchild="${id}">
        
        <input type="hidden" name="layer[${index}][items][${id}][animationout]" 
        data-group="transit" data-field="animationout" value="${animationout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][easingout]" 
        data-group="transit" data-field="easingout" value="${easingout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][speedout]"
        data-group="transit" data-field="speedout" value="${speedout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][splitout]"
        data-group="transit" data-field="splitout" value="${splitout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][splitdelayout]"
        data-group="transit" data-field="splitdelayout" value="${splitdelayout}"
        data-gchild="${id}">
        
        
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][movexin]" 
        data-group="custom-transit-in" data-field="movexin" value="${movexin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][moveyin]" 
        data-group="custom-transit-in" data-field="moveyin" value="${moveyin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][movezin]" 
        data-group="custom-transit-in" data-field="movezin" value="${movezin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][rotationxin]" 
        data-group="custom-transit-in" data-field="rotationxin" value="${rotationxin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][rotationyin]" 
        data-group="custom-transit-in" data-field="rotationyin" value="${rotationyin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][rotationzin]" 
        data-group="custom-transit-in" data-field="rotationzin" value="${rotationzin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][scalexin]" 
        data-group="custom-transit-in" data-field="scalexin" value="${scalexin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][scaleyin]" 
        data-group="custom-transit-in" data-field="scaleyin" value="${scaleyin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][skewxin]" 
        data-group="custom-transit-in" data-field="skewxin" value="${skewxin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][skewyin]" 
        data-group="custom-transit-in" data-field="skewyin" value="${skewyin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][captionopacityin]" 
        data-group="custom-transit-in" data-field="captionopacityin" value="${captionopacityin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][captionperspectivein]" 
        data-group="custom-transit-in" data-field="captionperspectivein" value="${captionperspectivein}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomin][originin]" 
        data-group="custom-transit-in" data-field="originin" value="${originin}"
        data-gchild="${id}">
        
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][movexout]" 
        data-group="custom-transit-out" data-field="movexout" value="${movexout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][moveyout]" 
        data-group="custom-transit-out" data-field="moveyout" value="${moveyout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][movezout]" 
        data-group="custom-transit-out" data-field="movezout" value="${movezout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][rotationxout]" 
        data-group="custom-transit-out" data-field="rotationxout" value="${rotationxout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][rotationyout]" 
        data-group="custom-transit-out" data-field="rotationyout" value="${rotationyout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][rotationzout]" 
        data-group="custom-transit-out" data-field="rotationzout" value="${rotationzout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][scalexout]" 
        data-group="custom-transit" data-field="scalexout" value="${scalexout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][scaleyout]" 
        data-group="custom-transit-out" data-field="scaleyout" value="${scaleyout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][skewxout]" 
        data-group="custom-transit-out" data-field="skewxout" value="${skewxout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][skewyout]" 
        data-group="custom-transit-out" data-field="skewyout" value="${skewyout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][captionopacityout]" 
        data-group="custom-transit-out" data-field="captionopacityout" value="${captionopacityout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][captionperspectiveout]" 
        data-group="custom-transit-out" data-field="captionperspectiveout" value="${captionperspectiveout}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][animationcustomout][originout]" 
        data-group="custom-transit-out" data-field="originout" value="${originout}"
        data-gchild="${id}">
        
        <input type="hidden" name="layer[${index}][items][${id}][loop][loop_type]"
        data-group="transit" data-field="loop_type" value="${loop_type}"
        data-gchild="${id}">   
        <input type="hidden" name="layer[${index}][items][${id}][loop][speed]" 
        data-group="loop" data-field="speed" value="${speed}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][loop_easing]" 
        data-group="loop" data-field="loop_easing" value="${loop_easing}"
        data-gchild="${id}">
        
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-pendulum][rs_pendulum_startdeg]" 
        data-group="loop" data-field="rs_pendulum_startdeg" value="${rs_pendulum_startdeg}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-pendulum][rs_pendulum_enddeg]" 
        data-group="loop" data-field="rs_pendulum_enddeg" value="${rs_pendulum_enddeg}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-pendulum][rs_pendulum_xorigin]" 
        data-group="loop" data-field="rs_pendulum_xorigin" value="${rs_pendulum_xorigin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-pendulum][rs_pendulum_yorigin]" 
        data-group="loop" data-field="rs_pendulum_yorigin" value="${rs_pendulum_yorigin}"
        data-gchild="${id}">
        
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-rotate][rs_rotate_startdeg]" 
        data-group="loop" data-field="rs_rotate_startdeg" value="${rs_rotate_startdeg}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-rotate][rs_rotate_enddeg]" 
        data-group="loop" data-field="rs_rotate_enddeg" value="${rs_rotate_enddeg}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-rotate][rs_rotate_xorigin]" 
        data-group="loop" data-field="rs_rotate_xorigin" value="${rs_rotate_xorigin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-rotate][rs_rotate_yorigin]" 
        data-group="loop" data-field="rs_rotate_yorigin" value="${rs_rotate_yorigin}"
        data-gchild="${id}">
        
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-slideloop][rs_slideloop_xstart]" 
        data-group="loop" data-field="rs_slideloop_xstart" value="${rs_slideloop_xstart}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-slideloop][rs_slideloop_xend]" 
        data-group="loop" data-field="rs_slideloop_xend" value="${rs_slideloop_xend}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-slideloop][rs_slideloop_ystart]" 
        data-group="loop" data-field="rs_slideloop_ystart" value="${rs_slideloop_ystart}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-slideloop][rs_slideloop_yend]" 
        data-group="loop" data-field="rs_slideloop_yend" value="${rs_slideloop_yend}"
        data-gchild="${id}">
        
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-pulse][rs_pulse_zoomstart]" 
        data-group="loop" data-field="rs_pulse_zoomstart" value="${rs_pulse_zoomstart}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-pulse][rs_pulse_zoomend]" 
        data-group="loop" data-field="rs_pulse_zoomend" value="${rs_pulse_zoomend}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-wave][rs_wave_xorigin]" 
        data-group="loop" data-field="rs_wave_xorigin" value="${rs_wave_xorigin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-wave][rs_wave_yorigin]" 
        data-group="loop" data-field="rs_wave_yorigin" value="${rs_wave_yorigin}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-wave][rs_wave_angle]" 
        data-group="loop" data-field="rs_wave_angle" value="${rs_wave_angle}"
        data-gchild="${id}">
        <input type="hidden" name="layer[${index}][items][${id}][loop][rs-wave][rs_wave_radius]" 
        data-group="loop" data-field="rs_wave_radius" value="${rs_wave_radius}"
        data-gchild="${id}">
        
        <!-- LAYER ANIMATION -->
        
        
        
        <textarea class="hidden" name="layer[${index}][items][${id}][content]" 
        data-field="content" data-gchild="${id}"></textarea>
    </div>