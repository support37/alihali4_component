<div id="inline-simport" class="modal hide fade">
    <form action="<?php echo JRoute::_("index.php?option=com_jvss&task=item.import")?>" enctype="multipart/form-data" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Import Template</h3>
        </div>
        <div class="modal-body">
            <div class="control-group">
                <div class="control-label">Template package file</div>
                <div class="controls"><input type="file" name="pkg"></div>
            </div>
        </div><!-- modal-body -->

        <div class="modal-footer">
            <button class="btn btn-primary">Save changes</button>
        </div><!-- modal-footer -->
        <input type="hidden" name="id" value="<?php echo JRequest::getInt( 'id', 0) ?>"/>
    </form><!-- modal -->
</div>