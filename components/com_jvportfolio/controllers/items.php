<?php
/**
 * @version     1.0.0
 * @package     com_jvportfolio
 * @copyright   Copyright (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      joomlavi <info@joomlavi.com> - http://www.joomlavi.com
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';
require_once JPATH_COMPONENT.'/helpers/jvportfolio.php';

/**
 * Items list controller class.
 */
class JvportfolioControllerItems extends JvportfolioController
{
	
    public function toggleVote() {
        $db = JFactory::getDbo();
        $u = JvportfolioFrontendHelper::getUid();
        $pfid = JRequest::getInt('pfid', 0);
        if(!$pfid) {
            return null;
        }
        $rs = $db
        ->setQuery("SELECT id 
                    FROM #__jvportfolio_liked 
                    WHERE u = '{$u}' AND pfid={$pfid}")
        ->loadColumn(); 
        $rs = trim(implode(',',$rs), ',');
        if(empty($rs)) {
            $db
            ->setQuery("INSERT INTO #__jvportfolio_liked(pfid, u) 
                        VALUES ({$pfid}, '{$u}')")
            ->execute();
        }
        else {
            $db
            ->setQuery("DELETE FROM #__jvportfolio_liked 
                        WHERE id in ({$rs})")
            ->execute();
        }
        header('Content-Type: application/json');
    die(json_encode(array(
        'data'=>intval($db
        ->setQuery("SELECT count(*) as c 
                    FROM #__jvportfolio_liked 
                    WHERE pfid={$pfid}")
        ->loadObject()->c)
        )));
    }
}