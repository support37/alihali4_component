<?php

/**
 * @version     1.0.0
 * @package     com_jvportfolio
 * @copyright   Copyright (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      joomlavi <info@joomlavi.com> - http://www.joomlavi.com
 */
defined('_JEXEC') or die;

class JvportfolioFrontendHelper {
    public static function getTags($ids = ''){
        if(is_array($ids)) $ids = implode(',',$ids);
		if(empty($ids)) return false;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select("title, alias");
        $query->from('`#__tags`');
        $query->where( "id in ({$ids})");

        return $db->setQuery($query)->loadObjectList();
    } 
    public static function build(&$item){
        
            $namedTags = array(); 
            $aliasTags = array();

            // Get the tag names of each tag id
            $row = self::getTags($item->tag);

            // Read the row and get the tag name (title)
            if($row){
                foreach ($row as $value) {
                    if ($value && isset($value->title) ) {
                        $namedTags[] = trim($value->title);
                    }
                    if ($value && isset($value->alias) ) {
                        $aliasTags[] = '"'.trim($value->alias).'"';
                    }
                }
                    
    
            }
            
            
            // Finally replace the data object with proper information
            $item->tag = !empty($namedTags) ? implode(', ',$namedTags) : $item->tag;
            $item->aliasTags = !empty($aliasTags) ? implode(',',$aliasTags) : '';    
    }
    
    public static function getPrefixCol($col){
        $cols = array(
            1=>'portfolio one',
            2=>'portfolio two',
            3=>'portfolio three',
            4=>'portfolio4 four',
            5=>'portfolio six',
            '_imasonry'=>'portfolio3 portfolio-masonry'
        );
        return isset($cols[$col]) ? $cols[$col] : $col;
    }
    public static function getActionQView($normal='', $active=''){
         $qview = array(
            'view'=>'#pfo-item-id',
            'c'=>array(
                array(
                    'removeClass'=>$normal,
                    'addClass'=>$active
                ),
                array(
                    'removeClass'=>$active,
                    'addClass'=>$normal
                )
            )
       );
       return json_encode($qview);
    }
    
    public static function getCtrlSort($data = array()){
        foreach($data as $i=>$item){
            $data[$i] = array(
                'value'=>$item, 
                'text'=>JText::_("COM_JVPORTFOLIO_CONFIGURATION_MENU_ITEM_SORT_".strtoupper($item))
            );    
        }
        
        return JHtml::_('select.genericlist', $data, 'csort', null, 'value', 'text', 'date');
    }
    
    public static function getUid(){
        $u = JFactory::getUser();
        if($u->guest) {
            return $_SERVER["REMOTE_ADDR"];
        }
        return $u->id;
    } 
    public static function checkJs($name){
        $scripts = JFactory::getDocument()->_scripts;
        foreach($scripts as $src=>$attr) {
            if(!strcmp($name, $src)) return true;
        }
        return false;
    }
}
