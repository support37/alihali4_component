<?php

/**
 * @version     1.0.0
 * @package     com_jvportfolio
 * @copyright   Copyright (C) 2014. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      joomlavi <info@joomlavi.com> - http://www.joomlavi.com
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Jvportfolio.
 */
class JvportfolioViewItems extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;
    protected $params;
	/**
     * Overwrite contructor
     */
    public function __construct($config = array()){
        $app                = JFactory::getApplication();
        $layout = $app->getMenu()->getActive()->params->get('layout', 'default');
        parent::__construct(array('layout'=>$layout));    
    }
	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{   
        $app                = JFactory::getApplication();
        JRequest::setVar('limit', $app->getMenu()->getActive()->params->get('limit', 20));
        JRequest::setVar('limitstart', 0);
                
        $this->state		= $this->get('State');
		$data 				= $this->get('Items');
        $this->items		= $data['items'];
		$this->tags			= $data['tags'];
        $this->pagination	= $this->get('Pagination');
        $this->params       = $app->getParams('com_jvportfolio');
        
        // Check for errors.
        if (count($errors = $this->get('Errors'))) {;
            throw new Exception(implode("\n", $errors));
        }
        
        JHtml::_('behavior.framework');
        JHtml::_('jquery.framework');
        JHtml::_('bootstrap.framework');
        $this->assets = JUri::root().'components/com_jvportfolio/assets';
        if(!JvportfolioFrontendHelper::checkJs('jquery.imagesloaded.js')){
            JHtml::script("{$this->assets}/js/jquery.imagesloaded.js");    
        }
        JHtml::script("{$this->assets}/js/modernizr.custom.min.js");
        JHtml::script("{$this->assets}/js/jquery.shuffle.js");
        JHtml::script("{$this->assets}/js/jquery.infinitescroll.js");  
        JHtml::script("{$this->assets}/js/frontend.jvportfolio.js");
        JHtml::stylesheet("{$this->assets}/css/pfo.css");  
        
        $this->_prepareDocument();
        parent::display($tpl);
	}


	/**
	 * Prepares the document
	 */
	protected function _prepareDocument()
	{
		$app	= JFactory::getApplication();
		$menus	= $app->getMenu();
		$title	= null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();
		if($menu)
		{
            $this->mparams = $menu->params;
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
            
            if($this->items) {
                $mfetch = $this->mparams->get('mfetch', 'scroll');
                $this->document->addScriptDeclaration("
                    window.JV = jQuery.extend(window.JV, {mfetch: '{$mfetch}'});
                ");
                
                if(intval($this->mparams->get('filter'))) {
                    $this->document->addScript("{$this->assets}/js/extend.filter.portfolio.js");
                    $this->document->addScriptDeclaration("window.JV = jQuery.extend(window.JV, {cfilter: 1});");
                }
                if($this->mparams->get('sort', 0)) {
                    $this->document->addScriptDeclaration("window.JV = jQuery.extend(window.JV, {csort: 1});");
                }
                switch($mfetch){
                    case 'button':
                    $this->document->addScript("{$this->assets}/js/extend.scrf.js");
                    break;
                    case 'nav':
                    $this->document->addScript("{$this->assets}/js/jquery.simplePagination.js");
                    $this->document->addScript("{$this->assets}/js/extend.scrf.js");
                    break;    
                }
                $citems = '#frm-portfolio .box-portfolio .pfo-item';
                $pageTotal = $this->pagination->pagesTotal;
                $link = JUri::root().$menu->link;
                $this->document->addScriptDeclaration("
                    jQuery(function($){
                        var pf = $('#frm-portfolio .box-portfolio'),
                            ppf = pf.closest('#frm-portfolio'),
                            msg = ppf.find('.pf-load')
                        ;
                        pf.infinitescroll({
                            itemSelector : '{$citems}',
                            loading: {msg: msg},
                            maxPage: (function(mp){return mp})({$pageTotal}),
                            path: function(p){
                                var 
                                limit = {$this->pagination->limit},
                                url = '{$link}&Itemid={$menu->id}&limit={$this->pagination->limit}&limitstart=0&format=hfetch'
                                ; 
                                return url.replace(/limitstart=\d+/, ['limitstart',(p-1)*limit].join('='));   
                            }
                        }, function(items){                   
                            $(items).imagesLoaded(function(){
                                pf.shuffle('appended', $(items)); 
                            });
                        });
                        if(window.JV.cfilter) {
                            ppf.extendFilterScrf({pf : pf});
                        }
                        if(window.JV.mfetch == 'button'){
                            pf.extendScrfBtn({
                                doc: $(document), 
                                mark: ppf.find('.load-more'),
                                maxPage: (function(mp){return mp})({$pageTotal})
                            });
                        }
                        if(window.JV.mfetch == 'nav'){
                            pf.extendScrfNav({    
                                mark: ppf.find('[data-nav]'),
                                pagination: {
                                    pages: (function(mp){return mp})({$pageTotal}),
                                    itemsOnPage: (function(mp){return mp})({$this->pagination->limit})
                                }
                            });
                        }
                        !window.JV.csort || ppf.find('#csort').asort({pf: pf});
                        pf.imagesLoaded(function(){
                            pf.shuffle({itemSelector: '{$citems}'});
                            msg.hide(0);
                        });
                        $(document)
                        .pfQuickView({pf: pf})
                        .pfVote()
                        ;
                    });
                ");  
            } 
		} else {
			$this->params->def('page_heading', JText::_('COM_JVPORTFOLIO_DEFAULT_PAGE_TITLE'));
		}
		$title = $this->params->get('page_title', '');
		if (empty($title)) {
			$title = $app->getCfg('sitename');
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		}
		elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}    
	}    
    	
}
