<?php
   /**
    * @version     1.0.0
    * @package     com_portfolio
    * @copyright   Copyright (C) 2014. All rights reserved.
    * @license     GNU General Public License version 2 or later; see LICENSE.txt
    * @author      joomlavi <info@joomlavi.com> - http://www.joomlavi.com
    */
   // no direct access
   defined('_JEXEC') or die;
   $tag = $this->state->get('a.tag', '');
   if(!$tag) $tag = $this->tags;
?>
   <?php 
   if($tags = JvportfolioFrontendHelper::getTags($tag)):?>
  <div class="portfolioFilter hidden-xs">
      <div class="filter-link">
         <a class="current" data-filter="all" href="javascript:"><?php echo JText::_('COM_JVPORTFOLIO_CONFIGURATION_MENU_ITEM_TAG_ALL')?></a> 
         <div class="bottom-border"></div>
      </div>
      <?php foreach($tags as $item):?>
      <div class="filter-link">
         <a class="" data-filter="<?php echo $item->alias?>" href="javascript:"><?php echo $item->title?></a> 
         <div class="bottom-border"></div>
      </div>
      <?php endforeach;?>
   </div>
<?php endif;?>