<?php
   /**
    * @version     1.0.0
    * @package     com_portfolio
    * @copyright   Copyright (C) 2014. All rights reserved.
    * @license     GNU General Public License version 2 or later; see LICENSE.txt
    * @author      joomlavi <info@joomlavi.com> - http://www.joomlavi.com
    */
   // no direct access
   defined('_JEXEC') or die;   
   $isFilter = (intval($this->mparams->get('filter')));                                                                
   $isSort = intval($this->mparams->get('sort', 0));
   $column = $this->mparams->get('column', 3);
   $ncol = intval($column)*2;
   $qview = JvportfolioFrontendHelper::getActionQView("col-md-{$column}", "col-md-{$ncol}");
   $prefixCol = JvportfolioFrontendHelper::getPrefixCol($column);     
   ?>
	<?php if ($this->params->get('show_page_heading', 1)) : ?>
        <h1 class="titlePage"> <span> <?php echo $this->escape($this->params->get('page_heading')); ?></span> </h1>
	<?php endif; ?>
   
<?php if($this->items):?>
<div id="frm-portfolio" class="portfolio<?php echo $column?>">
   <?php if($isFilter+$isSort):?>
   <div class="container topPortfolio">
      <?php if($isSort):?>
      <?php echo $this->loadTemplate('csort')?>
      <?php endif;?>
      <?php if($isFilter):?>
      <?php echo $this->loadTemplate('cfilter')?>
      <?php endif;?> 
   </div>
   <?php endif;?>
   <div class="box-portfolio row <?php echo($isFilter ? 'portfolioContainer': '')?>">
      <?php foreach($this->items as $item):?>
      <div id="pfo-item-<?php echo $item->id?>" class="pfo-item col-md-<?php echo $column?>" data-groups='[<?php echo $item->aliasTags?>]' data-name="<?php echo strtolower($item->name)?>" data-date="<?php echo strtotime($item->date_created)?>" data-like="<?php echo $item->cliked?>">
         <div class="overaly">
            <div class="corner">&nbsp;</div>
            <a class="pluss c-pointer" href="<?php echo $item->link?>">pluss</a>
            
            <div class="pfo-inner"><div class="pfo-inner2">
               <?php if($this->mparams->get('showLiked', 0)):?>
               <a class="likeheart" href="<?php echo JUri::root()."?option=com_jvportfolio&task=items.toggleVote&pfid={$item->id}"?>" data-pfvote="<?php echo $item->id?>"><i class="<?php echo ($item->lactive ? 'active' : '')?> fa fa-heart">&nbsp;<?php echo $item->cliked?></i></a>
               <?php endif;?>

               <?php if($this->mparams->get('hasTitle', 0)):?>
               <a class="pfo-title" href="<?php echo $item->link?>"><?php echo $item->name?></a>
               <?php endif;?>

            <?php if($this->mparams->get('hasDate', 0)):?>
            <div class="gray-italic"><?php echo date('d-m-Y', strtotime($item->date_created))?></div>
            <?php endif;?>
            <?php if($this->mparams->get('hasTag', 0)):?>
            <div class="gray-italic"><?php echo $item->tag?></div>
            <?php endif;?>
            </div></div>
            
            
         </div>
         <img alt="<?php echo $item->name?>" src="<?php echo JUri::root().$item->image ?>">        
      </div>
      <?php endforeach;?>
      <div class="pf-load">
         <div class="box">
            <img src="<?php echo "{$this->assets}/images/load-yellow.gif"?>" alt="loading"/>
            <div class=""><?php echo JText::_('Loading the next set of posts...')?></div>
         </div>
      </div>
   </div>
   <?php if($this->pagination->pagesTotal):?>
   <?php echo $this->loadTemplate('nav')?>
   <?php endif;?>
</div>
<?php endif;?>
