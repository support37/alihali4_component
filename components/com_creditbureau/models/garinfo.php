<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class CreditbureauModelGarinfo extends JModelLegacy {

    function getGarInfos(&$paging) {
        $user = JFactory::getUser();
        $user_id = 0;
        if (isset($user->id))
            $user_id = $user->id;
        $db = JFactory::getDbo();
        $query = "SELECT st.*,u.username FROM #__cb_gar_info AS st JOIN #__users AS u ON st.user_id=u.id WHERE st.user_id='$user_id' ORDER BY st.gar_info_id DESC";

        $mainframe = JFactory::getApplication();
        $itemPage = $mainframe->getCfg('list_limit');
        // Get pagination request variables
        $limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $itemPage, 'int');
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
        // In case limit has been changed, adjust it
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
        $db->setQuery($query, $limitstart, $limit);

        /* @var $db JDatabase */
        $listStreets = $db->loadObjectList();
        $paging = $this->getPaging();

        if ($listStreets) {
            return $listStreets;
        } else {
            return array();
        }
    }

    function getTotalRecord() {
        $user = JFactory::getUser();
        $user_id = 0;
        if (isset($user->id))
            $user_id = $user->id;
        $db = JFactory::getDbo();
        $query = "SELECT count(st.gar_info_id) as countObject FROM #__cb_gar_info AS st JOIN #__users AS u ON st.user_id=u.id WHERE st.user_id='$user_id'";
        $db->setQuery($query);
        $object = $db->loadObject();
        if ($object) {
            return $object->countObject;
        } else {
            return false;
        }
    }

    function getPaging() {
        jimport('joomla.html.pagination');
        $limitstart = $this->getState('limitstart');
        $limit = $this->getState('limit');
        $_pagination = new JPagination($this->getTotalRecord(), $limitstart, $limit);
        if ($_pagination) {
            return $_pagination;
        } else {
            return false;
        }
    }

}

?>