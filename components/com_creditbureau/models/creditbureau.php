<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class CreditbureauModelCreditbureau extends JModelLegacy {

//for client
    function getClients(&$paging, $search_type, $textsearch) {
        $db = JFactory::getDbo();
        $query = "SELECT st.*,u.username FROM #__cb_client_info AS st JOIN #__users AS u ON st.user_id=u.id WHERE 1";
        if ($search_type == 'client_name') {
            $query .= " AND (st.c_aname LIKE '%" . $textsearch . "%' OR st.c_ename LIKE '%" . $textsearch . "%')";
        }
        if ($search_type == 'client_id') {
            $textsearch = rtrim($textsearch, '0');
            $query .= " AND st.id LIKE '%" . $textsearch . "%'";
        }
        $query .= " ORDER BY st.client_info_id DESC";
        $mainframe = JFactory::getApplication();
        $itemPage = $mainframe->getCfg('list_limit');
        // Get pagination request variables
        $limit = 100;
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
        // In case limit has been changed, adjust it
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
        $db->setQuery($query, $limitstart, $limit);

        /* @var $db JDatabase */
        $listStreets = $db->loadObjectList();
        $paging = $this->getPaging($search_type, $textsearch);

        if ($listStreets) {
            return $listStreets;
        } else {
            return array();
        }
    }

    function getTotalRecord($search_type, $textsearch) {
        $db = JFactory::getDbo();
        $query = "SELECT count(st.client_info_id) as countObject FROM #__cb_client_info AS st JOIN #__users AS u ON st.user_id=u.id WHERE 1 ";
        if ($search_type == 'client_name') {
            $query .= " AND (st.c_aname LIKE '%" . $textsearch . "%' OR st.c_ename LIKE '%" . $textsearch . "%')";
        }
        if ($search_type == 'client_id') {
            $textsearch = rtrim($textsearch, '0');
            $query .= " AND st.id LIKE '%" . $textsearch . "%'";
        }
        $db->setQuery($query);
        $object = $db->loadObject();
        if ($object) {
            return $object->countObject;
        } else {
            return false;
        }
    }

    function getPaging($search_type, $textsearch) {
        jimport('joomla.html.pagination');
        $limitstart = $this->getState('limitstart');
        $limit = $this->getState('limit');
        $_pagination = new JPagination($this->getTotalRecord($search_type, $textsearch), $limitstart, $limit);
        if ($_pagination) {
            return $_pagination;
        } else {
            return false;
        }
    }

//for guarantee 
    function getGars(&$paging, $search_type, $textsearch) {
        $db = JFactory::getDbo();
        $query = "SELECT st.*,u.username FROM #__cb_gar_info AS st JOIN #__users AS u ON st.user_id=u.id WHERE 1";
        if ($search_type == 'gar_name') {
            $query .= " AND (st.g_aname LIKE '%" . $textsearch . "%' OR st.g_aname LIKE '%" . $textsearch . "%')";
        }
        if ($search_type == 'gar_id') {
            $textsearch = rtrim($textsearch, '0');
            $query .= " AND st.id LIKE '%" . $textsearch . "%'";
        }
        $query .= " ORDER BY st.gar_info_id DESC";
        $mainframe = JFactory::getApplication();
        $itemPage = $mainframe->getCfg('list_limit');
        // Get pagination request variables
        $limit = 100;
        $limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
        // In case limit has been changed, adjust it
        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
        $db->setQuery($query, $limitstart, $limit);

        /* @var $db JDatabase */
        $listStreets = $db->loadObjectList();
        $paging = $this->getPagingGar($search_type, $textsearch);

        if ($listStreets) {
            return $listStreets;
        } else {
            return array();
        }
    }

    function getTotalRecordGar($search_type, $textsearch) {
        $db = JFactory::getDbo();
        $query = "SELECT count(st.gar_info_id) as countObject FROM #__cb_gar_info AS st JOIN #__users AS u ON st.user_id=u.id WHERE 1 ";
        if ($search_type == 'gar_name') {
            $query .= " AND (st.g_aname LIKE '%" . $textsearch . "%' OR st.g_aname LIKE '%" . $textsearch . "%')";
        }
        if ($search_type == 'gar_id') {
            $textsearch = rtrim($textsearch, '0');
            $query .= " AND st.id LIKE '%" . $textsearch . "%'";
        }
        $db->setQuery($query);
        $object = $db->loadObject();
        if ($object) {
            return $object->countObject;
        } else {
            return false;
        }
    }

    function getPagingGar($search_type, $textsearch) {
        jimport('joomla.html.pagination');
        $limitstart = $this->getState('limitstart');
        $limit = $this->getState('limit');
        $_pagination = new JPagination($this->getTotalRecordGar($search_type, $textsearch), $limitstart, $limit);
        if ($_pagination) {
            return $_pagination;
        } else {
            return false;
        }
    }

}

?>