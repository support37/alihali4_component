<form id="adminForm" name="adminForm" action="index.php?option=com_creditbureau&task=gardet.display&view=gardet.list" method="POST">
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_EN'); ?>:</label>
        <?php
        $areas = Cbprojects::getAll();
        HelperCreditbureau::renderSelectWithObject('project_id', $areas, 'project_id', 'p_ename', 0, true);
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_DET_ANAME'); ?>: </label>
        <input type="text" required="true" name="gar_aname" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_DET_ENAME'); ?>:</label>
        <input type="text" name="gar_ename" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_DET_AMOUNT'); ?>: </label>
        <input type="number" name="gar_amount" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_DET_ID'); ?>: </label>
        <input type="text" name="id" size="100" value=""/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_DET_DATE'); ?>: </label>
        <?php echo JHtml::_('calendar', 'NOW', 'gar_date', 'gar_date', '%Y-%m-%d', ''); ?>
    </div><br/>
    <input class="btn btn-primary" type="submit" name="save" value="<?php echo JText::_('COM_CREDITBUREAU_SAVE'); ?>"/>
    <a class="btn btn-primary" href="<?php echo JRoute::_('index.php?option=com_creditbureau&task=gardet.display&view=gardet.list'); ?>"><?php echo JText::_('COM_CREDITBUREAU_CANCEL'); ?></a>
    <input type="hidden" name="option" value="com_creditbureau"/>
    <input type="hidden" name="task" value="gardet.add"/> 
</form>