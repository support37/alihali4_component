<form id="adminForm" name="adminForm" class="form-validate" action="index.php?option=com_creditbureau&task=gardet.display&view=gardet.list" method="POST">
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_EN'); ?>:</label>
        <?php
        $areas = Cbprojects::getAll();
        $exist = Cbgardetprojects::arrayProjectGarDet($_GET['gar_det_id']);
        HelperCreditbureau::renderSelectWithObject('project_id', $areas, 'project_id', 'p_ename', 0, true, $exist);
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_DET_ANAME'); ?>: </label>
        <input type="text" required="true" name="gar_aname" size="100" value="<?php echo $this->rows->gar_aname; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_DET_ENAME'); ?>:</label>
        <input type="text" name="gar_ename" size="100" value="<?php echo $this->rows->gar_ename; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_DET_AMOUNT'); ?>: </label>
        <input type="number" name="gar_amount" size="100" value="<?php echo $this->rows->gar_amount; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_DET_ID'); ?>: </label>
        <input type="text" name="id" size="100" value="<?php echo $this->rows->id; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_DET_DATE'); ?>: </label>
        <?php echo JHtml::_('calendar', 'NOW', 'gar_date', 'gar_date', '%Y-%m-%d', ''); ?>
    </div>
    <br/>
    <input class="btn btn-primary" type="submit" name="save" value="<?php echo JText::_('COM_CREDITBUREAU_SAVE'); ?>"/>
    <a class="btn btn-primary" href="<?php echo JRoute::_('index.php?option=com_creditbureau&task=gardet.display&view=gardet.list'); ?>"><?php echo JText::_('COM_CREDITBUREAU_CANCEL'); ?></a>
    <input type="hidden" value="<?php echo $_GET['gar_det_id']; ?>" name="gar_det_id"/>
    <input type="hidden" name="option" value="com_creditbureau"/>
    <input type="hidden" name="task" value="gardet.update"/> 
</form>
