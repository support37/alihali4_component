<?php
defined('_JEXEC') or die('Restricted access');
?>

<form method="post" name="adminForm" id="adminForm">
    <div id="filter-bar" class="btn-toolbar">
        <div class="btn-group pull-right hidden-phone">
            <?php if (HelperCreditbureau::allowPermission(HelperCreditbureau::CB_ADD)) { ?>
                <a class="btn btn-primary" href="<?php echo JRoute::_('index.php?option=com_creditbureau&task=garinfo.display&view=garinfo.form'); ?>"><?php echo JText::_('COM_CREDITBUREAU_ADD'); ?></a>
            <?php } ?>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th width="3%">
                    <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                </th>
                <th width="2%">#</th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_AUDIT_USERNAME'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_ANAME'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_ENAME'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_ACT'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_ID'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_LOANNO'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_CARDNO'); ?></th>
                <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_BLACKLIST'); ?></th>
            </tr>
        </thead>
        <?php
        if (count($this->rows) > 0) {
            $k = 0;
            for ($i = 0; $i < count($this->rows); $i++) {
                $rows = $this->rows[$i];
                ?>
                <tr class="<?php echo "row$k"; ?>">
                    <td align="center">
                        <?php echo JHTML::_('grid.id', $i, $rows->gar_info_id); ?>
                    </td>
                    <td align="center">
                        <?php if (HelperCreditbureau::allowPermission(HelperCreditbureau::CB_EDIT)) { ?>
                            <a href="<?php echo JRoute::_('index.php?option=com_creditbureau&task=garinfo.display&view=garinfo.detail&gar_info_id=' . $rows->gar_info_id) ?>">
                                <?php echo JText::_('COM_CREDITBUREAU_EDIT'); ?>
                            </a>
                        <?php } ?>
                    </td>
                    <td><?php echo $rows->username; ?></td>
                    <td><?php echo $rows->g_aname; ?></td>
                    <td><?php echo $rows->g_ename; ?></td>
                    <td><?php echo $rows->act; ?></td>
                    <td><?php echo $rows->id; ?></td>
                    <td><?php echo $rows->loans_no; ?></td>
                    <td><?php echo $rows->card_no; ?></td>
                    <td><?php echo $rows->black_list; ?></td>
                </tr>
                <?php
                $k = 1 - $k;
            }
        } else {
            ?>
            <tr>
                <td colspan="14"><?php echo JText::_('COM_CREDITBUREAU_SEARCH_NODATA'); ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="task" value="garinfo.displays"/>
</form>