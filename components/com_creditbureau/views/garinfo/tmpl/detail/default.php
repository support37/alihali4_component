<form id="adminForm" name="adminForm" class="form-validate" action="index.php?option=com_creditbureau&task=garinfo.display&view=garinfo.list" method="POST">
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_PROJECT_EN'); ?>:</label>
        <?php
        $areas = Cbprojects::getAll();
        $exist = Cbgarinfoprojects::arrayProjectGarInfo($_GET['gar_info_id']);
        HelperCreditbureau::renderSelectWithObject('project_id', $areas, 'project_id', 'p_ename', 0, true, $exist);
        ?>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_ANAME'); ?>: </label>
        <input type="text" required="true" name="g_aname" size="100" value="<?php echo $this->rows->g_aname; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_ENAME'); ?>:</label>
        <input type="text" name="g_ename" size="100" value="<?php echo $this->rows->g_ename; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_ACT'); ?>: </label>
        <input type="text" name="act" size="100" value="<?php echo $this->rows->act; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_ID'); ?>: </label>
        <input type="text" name="id" size="100" value="<?php echo $this->rows->id; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_LOANNO'); ?>: </label>
        <input type="number" name="loans_no" size="100" value="<?php echo $this->rows->loans_no; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_CARDNO'); ?>: </label>
        <input type="text" name="card_no" size="100" value="<?php echo $this->rows->card_no; ?>"/>
    </div>
    <div class="adminline">
        <label><?php echo JText::_('COM_CREDITBUREAU_GAR_INFO_BLACKLIST'); ?>: </label>
        <?php echo HelperCreditbureau::renderSelect('black_list', HelperCreditbureau::blackList(), $this->rows->black_list); ?>
    </div>
    <br/>
    <input class="btn btn-primary" type="submit" name="save" value="<?php echo JText::_('COM_CREDITBUREAU_SAVE'); ?>"/>
    <a class="btn btn-primary" href="<?php echo JRoute::_('index.php?option=com_creditbureau&task=garinfo.display&view=garinfo.list'); ?>"><?php echo JText::_('COM_CREDITBUREAU_CANCEL'); ?></a>
    <input type="hidden" value="<?php echo $_GET['gar_info_id']; ?>" name="gar_info_id"/>
    <input type="hidden" name="option" value="com_creditbureau"/>
    <input type="hidden" name="task" value="garinfo.update"/> 
</form>
