<?php

defined('_JEXEC') or die('Restricted access');

class CreditbureauViewClientdet extends FView {

    public function display($tpl = null, $cachable = false, $urlparams = false) {
        parent::display($tpl, $cachable, $urlparams);
    }

    public function view_list() {
        if (HelperCreditbureau::isUserLogged()) {
            $model = $this->getModel('clientdet', 'CreditbureauModel');
            $paging = "";
            $rows = $model->getClientDets($paging);
            $this->assignRef('rows', $rows);
            $this->assignRef('pages', $paging);
        } else {
            $app = JFactory::getApplication();
            $message = JText::_('COM_CREDITBUREAU_MUST_LOGGED');
            $url = JRoute::_('index.php?option=com_users&view=login');
            $app->redirect($url, $message);
        }
    }

    public function view_detail() {
        if (!HelperCreditbureau::allowPermission(HelperCreditbureau::CB_EDIT)) {
            $app = JFactory::getApplication();
            $message = JText::_('COM_CREDITBUREAU_PERMISSION');
            $url = JRoute::_('index.php?option=com_users&view=login');
            $app->redirect($url, $message);
        }
        $id = JRequest::getVar('client_det_id');
        $row = Cbclientdet::getInfoById($id);
        $this->assignRef('rows', $row);
    }

    public function view_form() {
        if (!HelperCreditbureau::allowPermission(HelperCreditbureau::CB_ADD)) {
            $app = JFactory::getApplication();
            $message = JText::_('COM_CREDITBUREAU_PERMISSION');
            $url = JRoute::_('index.php?option=com_users&view=login');
            $app->redirect($url, $message);
        }
    }

}

?>