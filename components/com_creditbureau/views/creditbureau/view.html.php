<?php

defined('_JEXEC') or die('Restricted access');

class CreditbureauViewCreditbureau extends FView {

    public function display($tpl = null, $cachable = false, $urlparams = false) {
        parent::display($tpl, $cachable, $urlparams);
    }

    public function view_list() {
        
    }

    public function view_mysummary() {
        if (HelperCreditbureau::isUserLogged()) {
            $user = JFactory::getUser();
            $this->assignRef('user_id', $user->id);
        } else {
            $app = JFactory::getApplication();
            $message = JText::_('COM_CREDITBUREAU_MUST_LOGGED');
            $url = JRoute::_('index.php?option=com_users&view=login');
            $app->redirect($url, $message);
        }
    }

    public function view_search() {
        if (HelperCreditbureau::isUserLogged()) {
            if (!HelperCreditbureau::allowPermission(HelperCreditbureau::CB_SEARCH)) {
                $app = JFactory::getApplication();
                $message = JText::_('COM_CREDITBUREAU_PERMISSION');
                $url = JRoute::_('index.php?option=com_users&view=login');
                $app->redirect($url, $message);
            }
            $model = $this->getModel('creditbureau', 'CreditbureauModel');
            $paging = "";
            $search_type = '';
            $textsearch = '';
            if (isset($_REQUEST['search_type']))
                $search_type = $_REQUEST['search_type'];
            if (isset($_REQUEST['textsearch']))
                $textsearch = $_REQUEST['textsearch'];
            if ($search_type == 'client_name' || $search_type == 'client_id') {
                $rows = $model->getClients($paging, $search_type, $textsearch);
            }
            if ($search_type == 'gar_name' || $search_type == 'gar_id') {
                $rows = $model->getGars($paging, $search_type, $textsearch);
            }
            if ($textsearch != '') {
                $user = JFactory::getUser();
                $obj = array(
                    'user_id' => $user->id,
                    'search_cond' => $textsearch,
                    'search_time' => date('Y-m-d H:i:s')
                );
                Cbaudit::add($obj);
            }
            $this->assignRef('rows', $rows);
            $this->assignRef('pages', $paging);
        } else {
            $app = JFactory::getApplication();
            $message = JText::_('COM_CREDITBUREAU_MUST_LOGGED');
            $url = JRoute::_('index.php?option=com_users&view=login');
            $app->redirect($url, $message);
        }
    }

}

?>