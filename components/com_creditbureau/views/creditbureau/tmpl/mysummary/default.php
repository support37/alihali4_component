
<script type="text/javascript" src="<?php echo JURI::base() . 'components/com_creditbureau/assets/js'; ?>/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages': ['corechart']});
    //Client Info
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Client Info', 'Total'],
            ['Last loan status Paid', <?php echo Cbclientinfo::getTotalClientInfoIds('paid', $this->user_id); ?>],
            ['Last loan status UnPaid', <?php echo Cbclientinfo::getTotalClientInfoIds('unpaid', $this->user_id); ?>],
            ['Black list client info', <?php echo Cbclientinfo::getTotalClientInfoIds('', 'no', $this->user_id); ?>],
            ['Active list client info', <?php echo Cbclientinfo::getTotalClientInfoIds('', 'yes', $this->user_id); ?>]
        ]);

        var options = {
            title: 'Client Info'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
    //Client Det
<?php
if (Cbclientinfo::getTotalClientInfoIds('paid', $this->user_id) > 0 || Cbclientinfo::getTotalClientInfoIds('unpaid', $this->user_id) > 0) {
    ?>
        google.charts.setOnLoadCallback(drawChart2);
<?php } ?>
    function drawChart2() {

        var data = google.visualization.arrayToDataTable([
            ['Client Det', 'Total'],
            ['Loan status Paid', <?php echo Cbclientinfo::getTotalClientInfoIds('paid', $this->user_id); ?>],
            ['Loan status UnPaid', <?php echo Cbclientinfo::getTotalClientInfoIds('unpaid', $this->user_id); ?>]
        ]);

        var options = {
            title: 'Client det'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart2'));

        chart.draw(data, options);
    }
    //Guarantee Info
<?php
if (Cbgarinfo::getTotalGarInfoIds('no', $this->user_id) > 0 || Cbgarinfo::getTotalGarInfoIds('yes', $this->user_id) > 0) {
    ?>
        google.charts.setOnLoadCallback(drawChart3);
<?php } ?>
    function drawChart3() {

        var data = google.visualization.arrayToDataTable([
            ['Guarantee Info', 'Total'],
            ['Black list Guarantee info', <?php echo Cbgarinfo::getTotalGarInfoIds('no', $this->user_id); ?>],
            ['Active list Guarantee info', <?php echo Cbgarinfo::getTotalGarInfoIds('yes', $this->user_id); ?>]
        ]);

        var options = {
            title: 'Guarantee Info'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart3'));

        chart.draw(data, options);
    }


</script>
<div id="piechart" style="width: 520px; height: 500px;float: left;"></div>
<div id="piechart2" style="width: 520px; height: 500px; float: right"></div>
<div id="piechart3" style="width: 520px; height: 500px;float: left;"></div>