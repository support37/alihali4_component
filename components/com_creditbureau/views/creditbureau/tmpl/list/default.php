
<script type="text/javascript" src="<?php echo JURI::base() . 'components/com_creditbureau/assets/js'; ?>/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages': ['corechart']});
    //Client Info
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Client Info', 'Total'],
            ['Last loan status Paid', <?php echo Cbclientinfo::getTotalClientInfoIds('paid'); ?>],
            ['Last loan status UnPaid', <?php echo Cbclientinfo::getTotalClientInfoIds('unpaid'); ?>],
            ['Black list client info', <?php echo Cbclientinfo::getTotalClientInfoIds('', 'no'); ?>],
            ['Active list client info', <?php echo Cbclientinfo::getTotalClientInfoIds('', 'yes'); ?>]
        ]);

        var options = {
            title: 'Client Info'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
    //Client Det
    google.charts.setOnLoadCallback(drawChart2);

    function drawChart2() {

        var data = google.visualization.arrayToDataTable([
            ['Client Det', 'Total'],
            ['Loan status Paid', <?php echo Cbclientinfo::getTotalClientInfoIds('paid'); ?>],
            ['Loan status UnPaid', <?php echo Cbclientinfo::getTotalClientInfoIds('unpaid'); ?>]
        ]);

        var options = {
            title: 'Client det'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart2'));

        chart.draw(data, options);
    }
    //Guarantee Info
    google.charts.setOnLoadCallback(drawChart3);

    function drawChart3() {

        var data = google.visualization.arrayToDataTable([
            ['Guarantee Info', 'Total'],
            ['Black list Guarantee info', <?php echo Cbgarinfo::getTotalGarInfoIds('no'); ?>],
            ['Active list Guarantee info', <?php echo Cbgarinfo::getTotalGarInfoIds('yes'); ?>]
        ]);

        var options = {
            title: 'Guarantee Info'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart3'));

        chart.draw(data, options);
    }


</script>
<div id="piechart" style="width: 520px; height: 500px;float: left;"></div>
<div id="piechart2" style="width: 520px; height: 500px; float: right"></div>
<div id="piechart3" style="width: 520px; height: 500px;float: left;"></div>