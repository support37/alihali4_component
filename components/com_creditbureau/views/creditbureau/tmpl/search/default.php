<?php
$search_type = '';
if (isset($_REQUEST['search_type']))
    $search_type = $_REQUEST['search_type'];
?>
<form action="" method="post" class="form-inline">
    <div class="form-group mx-sm-3 mb-2">
        <label><?php echo JText::_('COM_CREDITBUREAU_SEARCH_TYPE'); ?></label>
        <select name="search_type" class="form-control">
            <option <?php echo $search_type == 'client_name' ? 'selected="true"' : ''; ?> value="client_name"><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_NAME'); ?></option>
            <option <?php echo $search_type == 'client_id' ? 'selected="true"' : ''; ?> value="client_id"><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_ID'); ?></option>
            <option <?php echo $search_type == 'gar_name' ? 'selected="true"' : ''; ?> value="gar_name"><?php echo JText::_('COM_CREDITBUREAU_GARINFO_NAME'); ?></option>
            <option <?php echo $search_type == 'gar_id' ? 'selected="true"' : ''; ?> value="gar_id"><?php echo JText::_('COM_CREDITBUREAU_GARINFO_ID'); ?></option>
        </select>&nbsp;&nbsp;
        <label><?php echo JText::_('COM_CREDITBUREAU_SEARCH_KEYWORD'); ?></label>
        <input type="text" class="form-control" name="textsearch" value="<?php echo isset($_REQUEST['textsearch']) ? $_REQUEST['textsearch'] : ''; ?>" id="textsearch" placeholder="<?php echo JText::_('COM_CREDITBUREAU_SEARCH_KEYWORD'); ?>">
    </div>
    <button type="submit" name="btn_search" class="btn btn-primary"><?php echo JText::_('COM_CREDITBUREAU_SEARCH_BTN'); ?></button>
    <input type="hidden" name="option" value="com_creditbureau"/>
    <input type="hidden" name="task" value=""/> 
</form>
<?php
if (isset($_POST['btn_search']) && in_array($search_type, array('client_name', 'client_id'))) {
    if (count($this->rows) > 0) {
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_ENAME'); ?></th>
                    <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_ANAME'); ?></th>
                    <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_LASTLOANSTS'); ?></th>
                    <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENT_ID'); ?></th>
                    <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_CARDNO'); ?></th>
                    <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_CLIENTINFO_BLACKLIST'); ?></th>
                </tr>
            </thead>
            <?php
            $k = 0;
            for ($i = 0; $i < count($this->rows); $i++) {
                $rows = $this->rows[$i];
                ?>
                <tr class="<?php echo "row$k"; ?>">
                    <td><?php echo $rows->c_ename; ?></td>
                    <td><?php echo $rows->c_aname; ?></td>
                    <td><?php echo $rows->last_loan_st; ?></td>
                    <td><?php echo $rows->id; ?></td>
                    <td><?php echo $rows->card_no; ?></td>
                    <td><?php echo $rows->black_list; ?></td>
                </tr>
                <?php
                $k = 1 - $k;
            }
            ?>
        </table>
        <?php
    } else {
        ?>
        <table class="table table-striped">
            <tr>
                <td colspan="6"><?php echo JText::_('COM_CREDITBUREAU_SEARCH_NODATA'); ?></td>
            </tr>
        </table>
        <?php
    }
}
if (isset($_POST['btn_search']) && in_array($search_type, array('gar_name', 'gar_id'))) {
    if (count($this->rows) > 0) {
        ?>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th width="20%">
                        <?php
                        echo JText::_('COM_CREDITBUREAU_GAR_NAME');
                        ?>
                    </th>
                    <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_GAR_LOAN_NO'); ?></th>
                    <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_GAR_ID'); ?></th>
                    <th width="20%"><?php echo JText::_('COM_CREDITBUREAU_GAR_BLACKLIST_STATUS'); ?></th>
                </tr>
            </thead>
            <?php
            $k = 0;
            for ($i = 0; $i < count($this->rows); $i++) {
                $rows = $this->rows[$i];
                ?>
                <tr class="<?php echo "row$k"; ?>">
                    <td>
                        <?php echo $rows->g_aname; ?>
                    </td>
                    <td><?php echo $rows->loans_no; ?></td>
                    <td><?php echo $rows->id; ?></td>
                    <td><?php echo $rows->black_list; ?></td>
                </tr>
                <?php
                $k = 1 - $k;
            }
            ?>
        </table>
        <?php
    } else {
        ?>
        <table class="table table-striped">
            <tr>
                <td colspan="6"><?php echo JText::_('COM_CREDITBUREAU_SEARCH_NODATA'); ?></td>
            </tr>
        </table>
        <?php
    }
}