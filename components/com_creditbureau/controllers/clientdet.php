<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class CreditbureauControllerClientdet extends FController {

    public function __construct($config = array()) {
        parent::__construct($config);
    }

    public function display($model = null, $cachable = false, $urlparams = false) {
        $view_name = JRequest::getVar('view');
        if (!$view_name) {
            $view_name = 'clientdet.list';
        }
        $view = $this->createView($view_name, 'CreditbureauView');
        $model = $this->createModel('clientdet', 'CreditbureauModel');
        if ($model) {
            $view->setModel($model);
        }
        $view->display($tpl = null);
    }

    public function add() {
        $post = JRequest::get('post');
        $post['user_id'] = HelperCreditbureau::getUserID();
        if ($post['user_id'] != '' || $post['c_aname'] != '' || $post['c_ename'] != '') {
            if ($id = Cbclientdet::add($post)) {
                if (count($post['project_id']) > 0) {
                    $areas = $post['project_id'];
                    foreach ($areas as $value) {
                        $obj = array(
                            'project_id' => $value,
                            'client_det_id' => $id
                        );
                        Cbclientdetprojects::add($obj);
                    }
                }
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_OK');
            } else {
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
            }
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('clientdet.display', 'clientdet.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    function update() {
        $post = JRequest::get('post');
        $post['user_id'] = HelperCreditbureau::getUserID();
        if ($post['user_id'] != '' || $post['c_aname'] != '' || $post['c_ename'] != '') {
            if (Cbclientdet::update($post)) {
                $id = $post['client_det_id'];
                Cbclientdetprojects::deleteByClientDet($id);
                if (count($post['project_id']) > 0) {
                    $areas = $post['project_id'];
                    foreach ($areas as $value) {
                        $obj = array(
                            'project_id' => $value,
                            'client_det_id' => $id
                        );
                        Cbclientdetprojects::add($obj);
                    }
                }
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_OK');
            } else {
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
            }
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('clientdet.display', 'clientdet.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    public function delete() {
        $id = JRequest::getVar('id');
        if (Cbclientdet::delete($id)) {
            $msg = JText::_('COM_CREDITBUREAU_AREA_DEL_OK');
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_DEL_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('clientdet.display', 'clientdet.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    function addnew() {
        $link = 'index.php?option=com_creditbureau&task=clientdet.display&view=clientdet.form';
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

    function edit() {
        $arrId = JRequest::getVar('cid', array(), '', 'array');
        foreach ($arrId as $id) {
            $link = 'index.php?option=com_creditbureau&task=clientdet.display&view=clientdet.detail&client_det_id=' . $id;
        }
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

    function cancel() {
        $link = 'index.php?option=com_creditbureau&task=clientdet.display&view=clientdet.list';
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

}

?>