<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class CreditbureauControllerClientinfo extends FController {

    public function __construct($config = array()) {
        parent::__construct($config);
    }

    public function display($model = null, $cachable = false, $urlparams = false) {
        $view_name = JRequest::getVar('view');
        if (!$view_name) {
            $view_name = 'clientinfo.list';
        }
        $view = $this->createView($view_name, 'CreditbureauView');
        $model = $this->createModel('clientinfo', 'CreditbureauModel');
        if ($model) {
            $view->setModel($model);
        }
        $view->display($tpl = null);
    }

    public function add() {
        $post = JRequest::get('post');
        $post['user_id'] = HelperCreditbureau::getUserID();
        if ($post['user_id'] != '' || $post['c_aname'] != '' || $post['c_ename'] != '') {
            if ($id = Cbclientinfo::add($post)) {
                if (count($post['project_id']) > 0) {
                    $areas = $post['project_id'];
                    foreach ($areas as $value) {
                        $obj = array(
                            'project_id' => $value,
                            'client_info_id' => $id
                        );
                        Cbclientinfoprojects::add($obj);
                    }
                }
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_OK');
            } else {
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
            }
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('clientinfo.display', 'clientinfo.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    function update() {
        $post = JRequest::get('post');
        $post['user_id'] = HelperCreditbureau::getUserID();
        if ($post['user_id'] != '' || $post['c_aname'] != '' || $post['c_ename'] != '') {
            if (Cbclientinfo::update($post)) {
                $id = $post['client_info_id'];
                Cbclientinfoprojects::deleteByClientInfo($id);
                if (count($post['project_id']) > 0) {
                    $areas = $post['project_id'];
                    foreach ($areas as $value) {
                        $obj = array(
                            'project_id' => $value,
                            'client_info_id' => $id
                        );
                        Cbclientinfoprojects::add($obj);
                    }
                }
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_OK');
            } else {
                $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
            }
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_SAVE_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('clientinfo.display', 'clientinfo.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    public function delete() {
        $id = JRequest::getVar('id');
        if (Cbclientinfo::delete($id)) {
            $msg = JText::_('COM_CREDITBUREAU_AREA_DEL_OK');
        } else {
            $msg = JText::_('COM_CREDITBUREAU_AREA_DEL_UNOK');
        }
        $app = JFactory::getApplication();
        $link = $this->_createUrl('clientinfo.display', 'clientinfo.list', 'html', 'default');
        $app->redirect($link, $msg);
    }

    function addnew() {
        $link = 'index.php?option=com_creditbureau&task=clientinfo.display&view=clientinfo.form';
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

    function edit() {
        $arrId = JRequest::getVar('cid', array(), '', 'array');
        foreach ($arrId as $id) {
            $link = 'index.php?option=com_creditbureau&task=clientinfo.display&view=clientinfo.detail&client_info_id=' . $id;
        }
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

    function cancel() {
        $link = 'index.php?option=com_creditbureau&task=clientinfo.display&view=clientinfo.list';
        $app = JFactory::getApplication();
        $app->redirect($link);
    }

}

?>