<?php

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

if (!defined('DS')) {

    define('DS', DIRECTORY_SEPARATOR);

}

JLoader::register('FController', JPATH_COMPONENT . DS . 'mvc' . DS . 'controller.class.php');
JLoader::register('FModel', JPATH_COMPONENT . DS . 'mvc' . DS . 'model.class.php');
JLoader::register('FView', JPATH_COMPONENT . DS . 'mvc' . DS . 'view.class.php');
JLoader::register('FTable', JPATH_COMPONENT . DS . 'mvc' . DS . 'table.class.php');

if (!defined('JPATH_HELPER')) {
    define('JPATH_HELPER', JPATH_COMPONENT_ADMINISTRATOR . DS . 'helpers');
}
include_once (JPATH_HELPER . DS . 'helper.php');
$pathTable = JPATH_COMPONENT_ADMINISTRATOR . DS . 'tables';
HelperCreditbureau::includeFileInFolder($pathTable);
HelperCreditbureau::includeFileInFolder(JPATH_HELPER);

if (JRequest::getVar('task') == null) {
    JRequest::setVar('task', 'creditbureau.display');
}

$controller = FController::getInstance('Creditbureau');
$task = (JRequest::getVar('task') != '') ? JRequest::getVar('task') : 'display';
if ($task && strpos($task, '.') !== false) {
    $valueTask = explode('.', $task);
    $doTask = $valueTask[1];
} else {
    $doTask = $task;
}

$controller->execute($doTask);
$controller->redirect();

$doc = JFactory::getDocument();
//$doc->addScript(JURI::base() . 'templates/t2design/js/jquery-1.10.2.min.js');
//$doc->addStyleSheet(JUri::base().'/components/com_pgallery/assets/source/jquery.fancybox.css');
//$doc->addScript(JUri::base().'/components/com_pgallery/assets/source/jquery.fancybox.js');
//$doc->addStyleSheet(JUri::base().'/components/com_pgallery/assets/css/npstyle.css');
//$doc->addScript(JUri::base().'/components/com_pgallery/assets/js/npscript.js');
?>