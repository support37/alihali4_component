<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_jvportfolio
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_jvportfolio
 *
 * @package     Joomla.Site
 * @subpackage  mod_jvportfolio
 * @since       1.5
 */

require_once JPATH_SITE.'/components/com_jvportfolio/helpers/jvportfolio.php';
class ModJvPortfolioHelper
{
    public static function toggleVoteAjax() {
        $db = JFactory::getDbo();
        $u = JvportfolioFrontendHelper::getUid();
        $pfid = JRequest::getInt('pfid', 0);
        if(!$pfid) {
            return null;
        }
        $rs = $db
        ->setQuery("SELECT id FROM #__jvportfolio_liked WHERE u = '{$u}'")
        ->loadObject();
        if(is_null($rs)) {
            $db
            ->setQuery("INSERT INTO #__jvportfolio_liked(pfid, u) VALUES ({$pfid}, '{$u}')")
            ->execute();
        }
        else {
            $db
            ->setQuery("DELETE FROM #__jvportfolio_liked WHERE id = {$rs->id}")
            ->execute();
        }
        
        return $db
        ->setQuery("SELECT count(*) as c FROM #__jvportfolio_liked WHERE u = '{$u}'")
        ->loadObject()->c;
    }
    
    public static function getItems($tag, $offset, $limit){
        $db = JFactory::getDbo();
        $uid = JvportfolioFrontendHelper::getUid();
        $where = "";
        if($tag) {
            $tag = implode(',', $tag);
            $where = "WHERE a.tag in ({$tag})";
        }
        $q = 'SELECT DISTINCT a.*,'.
            "(select count(*) from #__jvportfolio_liked as l where l.pfid = a.id) as cliked,".
            "(select count(*) from #__jvportfolio_liked as l where l.u = '{$uid}') as lactive
            FROM `#__jvportfolio_item` AS a
            {$where}
            ORDER BY a.date_created DESC";
        $rs = $db->setQuery($q, $offset, $limit)->loadObjectList();
        if(is_null($rs)) return 0;
        $tags = array();
        foreach($rs as &$item) {
            $tags = array_merge($tags, explode(',', $item->tag));
            JvportfolioFrontendHelper::build($item);    
        }
        $tags = array_unique($tags);
        $q = "SELECT DISTINCT count(*) as rows FROM `#__jvportfolio_item` AS a {$where}";
        return (object)array('items'=>$rs, 'total'=>$db->setQuery($q)->loadObject()->rows, 'tags'=>$tags);
    }
    
    public static function loadTemplate($template){
        return JModuleHelper::getLayoutPath('mod_jvportfolio', $template);
    }	
}
