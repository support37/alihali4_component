<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_jvportfolio
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';
$tag = $params->get('tags', 0);  
$limitstart = JRequest::getInt('start', 0);
$limit= $params->get('limit', 20);
$rs = ModJvPortfolioHelper::getItems($tag, $limitstart, $limit);
if(is_null($rs)) return false;
$items = $rs->items;  
if(!$tag) $tag = $rs->tags;
$total = $rs->total;
$pagination = new JPagination($rs->total, $limitstart, $limit);
$template = $params->get('layout', 'default');
$column = $params->get('column', 3);
$com_assets = JUri::root().'components/com_jvportfolio/assets'; 
$document = JFactory::getDocument();
JFactory::getLanguage()->load('com_jvportfolio');

// setup config
JHtml::_('behavior.framework');
JHtml::_('jquery.framework');
JHtml::_('bootstrap.framework');
$com_assets = JUri::root().'components/com_jvportfolio/assets'; 
if(!JvportfolioFrontendHelper::checkJs('jquery.imagesloaded.js')){
    JHtml::script("{$com_assets}/js/jquery.imagesloaded.js");    
}
JHtml::script("{$com_assets}/js/modernizr.custom.min.js");
JHtml::script("{$com_assets}/js/jquery.shuffle.js");
JHtml::script("{$com_assets}/js/jquery.infinitescroll.js");  
JHtml::script("{$com_assets}/js/frontend.jvportfolio.js");
JHtml::stylesheet("{$com_assets}/css/pfo.css");  
$mfetch = $params->get('mfetch', 'scroll');
$document->addScriptDeclaration("
    window.JV = jQuery.extend(window.JV, {mfetch: '{$mfetch}'});
");

if(intval($params->get('filter'))) {
    $document->addScript("{$com_assets}/js/extend.filter.portfolio.js");
    $document->addScriptDeclaration("window.JV = jQuery.extend(window.JV, {cfilter: 1});");
}    
if($params->get('sort', 0)) {
    $document->addScriptDeclaration("window.JV = jQuery.extend(window.JV, {csort: 1});");
}
switch($mfetch){
    case 'button':
    $document->addScript("{$com_assets}/js/extend.scrf.js");
    break;
    case 'nav':
    $document->addScript("{$com_assets}/js/jquery.simplePagination.js");
    $document->addScript("{$com_assets}/js/extend.scrf.js");
    break;    
}
$citems = "#mod-frm-portfolio-{$module->id} .box-portfolio .pfo-item";   
$menu = JFactory::getApplication()->getMenu()->getActive();
$document->addScriptDeclaration("
    jQuery(function($){
        var pf = $('#mod-frm-portfolio-{$module->id} .box-portfolio'),
            ppf = pf.closest('#mod-frm-portfolio-{$module->id}'),
            msg = ppf.find('.pf-load')
        ;
        pf.infinitescroll({
            itemSelector : '{$citems}',
            loading: {msg: msg},
            maxPage: (function(mp){return mp})({$pagination->pagesTotal}),
            path: function(p){
                var 
                limit = {$limit},
                url = 'start=0&xformat=hfetch&ajax=1',
				j = '&'
                ; 
				if(!location.search) j = '?';
				url = [location.href, url].join(j);
                return url.replace(/start=\d+/, ['start',(p-1)*limit].join('='));   
            }
        }, function(items){                   
            $(items).imagesLoaded(function(){
                pf.shuffle('appended', $(items)); 
            });
        });
        if(window.JV.cfilter) {
            ppf.extendFilterScrf({pf : pf});
        }
        if(window.JV.mfetch == 'button'){
            pf.extendScrfBtn({
                doc: $(document), 
                mark: ppf.find('.load-more'),
                maxPage: (function(mp){return mp})({$pagination->pagesTotal})
            });
        }
        if(window.JV.mfetch == 'nav'){
            pf.extendScrfNav({    
                mark: ppf.find('[data-nav]'),
                pagination: {
                    pages: (function(mp){return mp})({$pagination->pagesTotal}),
                    itemsOnPage: (function(mp){return mp})({$limit})
                }
            });
        }
        !window.JV.csort || ppf.find('#csort').asort({pf: pf});
        pf.imagesLoaded(function(){
            pf.shuffle({itemSelector: '{$citems}'});
            msg.hide(0);
        });
        $(document)
        .pfQuickView({event: 'click.mod-jvportfolio-{$module->id}', pf: pf})
        .pfVote({event: 'click.mod-jvportfolio-{$module->id}'})
        ;
    });
");
            
require JModuleHelper::getLayoutPath('mod_jvportfolio', $template);

if(JRequest::getInt('ajax', 0)) die();
