<?php
$moduleId = 'JVTab'.$module->id;
JVJSLib::add('jquery.plugins.imagesloaded');
JVJSLib::add('jquery.plugins.transform');
$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::root(true).'/modules/mod_jvtabs/assets/css/jvtabs.css');
if((string)$dataConfigs->style != 'none') $doc->addStyleSheet(JUri::root(true)."/modules/mod_jvtabs/assets/css/jvtabs.{$dataConfigs->style}.css");
$doc->addScript(JUri::root(true).'/modules/mod_jvtabs/assets/js/jvtabs.js');
$doc->addScriptDeclaration("
    jQuery(function($){
        new JVTab('#{$moduleId}',{$dataConfigs});
    });
");
?>
<div id="<?php echo $moduleId?>" class="JVTab <?php echo $dataConfigs->suffix?>">
    <div class="JVTab-nav">
        <div class="nav-content">
            <ul>
                <?php foreach($dataTabs as $tab): ?>
                    <li><a href="#<?php echo $tab->id ?>"><span><?php echo $tab->title ?></span></a></li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
    <div class="JVTab-content">
        <?php foreach($dataTabs as $tab): ?>
            <div id="<?php echo $tab->id ?>"><?php echo $tab->content ?></div>
        <?php endforeach;?>
    </div>
</div>