
var JVTabsAdmin = window.JVTabsAdmin || (function($){
    return $.extend(function(ops){
        var
            btnPanel = $(ops.button),
            button = $('<a>',{ 
                href: 'javascript:void(0)',
                'class': 'btn', 
                id: 'btnJVTabs'
            }).append('JV Tabs...').prependTo(btnPanel),
            editor = ops.editor,
            selection = editor.selection,
            params = new CustomField(),
            editNode = function(node){
                var node = $(node);
                params.data().clear();
                if(node.is('a.jvtabs')){
                    params.data().data(JSON.parse(node.html()));
                }
                dialog.modal('show');
            },
            shorParams = function(data){
                
                return data;
            }
        ;
        var
            btnApply = $('<button>',{type: 'button', 'class': 'btn btn-primary', 'data-dismiss':'modal'}).append('Save'),
            btnCancel = $('<button>',{type: 'button', 'class': 'btn btn-default', 'data-dismiss':'modal'}).append('Close'),
            btnClose = $('<button>',{type:'button','class':"close", 'data-dismiss':"modal", 'aria-hidden':"true"}).html('&times;'),
            head = $('<div>',{'class': 'modal-header'}).append(btnClose,$('<h4>').append('JV Tabs config')),
            body = $('<div>',{'class': 'modal-body'}).append(params),
            footer = $('<div>',{'class': 'modal-footer'}).append(btnApply,btnCancel),
            dialog = $('<div>',{'class': 'modal fade',id:'jvtabsconfig'}).append(head,body,footer)
        ;
        btnApply.click(function(){
            if(params.data().validate() > 0) return false;
            
            var 
                node = $(selection.getNode()),
                data = JSON.stringify(shorParams(params.data().data()))
            ;
            
            if(node.is('a.jvtabs')){
                node.html(data);
                return;
            }
            var newTab = $('<a>',{'class':'jvtabs',contentEditable: false}).html(data)[0];
            if(node.is('img')){
                node.after(newTab);
            }else{
                selection.setNode(newTab);
            }
        });
        var doc = $(editor.getDoc())
            .on('dblclick','a.jvtabs',function(){
                editNode(this);
            }).on('click','a.jvtabs',function(){ 
                $(this).addClass('focus');
            }).bind('mouseup',function(e){            
                doc.find('a.jvtabs.focus').not(e.target).removeClass('focus');
            });
        
        
        button.click(function(){
            editNode(selection.getNode());
        });
        
        
    },{
        
    });
})(jQuery);