<?php
class JVTabsSource{
    private $tabs = array(),$params;
    private static $count = 0;
    private static $docID = 0;
    public $id;
    
    static function fillAssets($fn,$args = array()){
        $cacheDoc = JFactory::getDocument();
        $jslibLoaded = JVJSLib::$loaded;
        JVJSLib::$loaded = array();
        $doc = JDocument::getInstance('html', array(
            'type' => 'jvtabs',
            'id' =>  self::$docID ++
        ));
        JFactory::$document = $doc;
        $doc = JFactory::getDocument();
        $content = call_user_func_array($fn,$args);
        $assets = array(
            'scripts' => array(),
            'script' => array(),
            'styles' => array(),
            'content' => $content
        );
        
        foreach($doc->_scripts as $k => $v) $assets['scripts'][] = array('src' => $k, "cache" => true);
        foreach($doc->_script as $v) $assets['script'][] = $v;
        foreach($doc->_styleSheets as $k => $v) $assets['styles'][] = array('src' => $k, "cache" => true);
        foreach($doc->_style as $v) $assets['styles'][] = $v;
        JFactory::$document = $cacheDoc;
        JVJSLib::$loaded = $jslibLoaded;
        return $assets;
    }
    function __construct($params,$module){
        $this->params = $params;
        $this->id = 'JVTab'.$module->id;
    }
    private function from_position($param){
        $modules = JModuleHelper::getModules((string)$param->position);
        $tabs = array();
        foreach($modules as $module){
            switch($this->params->config->get('load','all')){
                case 'all': $content = JModuleHelper::renderModule($module); break;
                case 'cache': $content = self::fillAssets(array('JModuleHelper','renderModule'),array($module)); break;
            }
            
            $tabs[] = array(
                'title' =>  $module->title,
                'id'    => 'module-'.$module->id,
                'content' => $content
            );
        }   
        return $tabs;
    }
    
    private function from_article($param,$load){
        require_once('sources/article.php');
        return JVTabArticle::getArticle($param,$load);
    }
    private function from_queryarticle($param,$load = 'all'){
        require_once('sources/article.php');
        return JVTabArticle::getArticles($param,$load);
    }
    private function from_k2item($param,$load = 'all'){
        require_once('sources/k2.php');
        return JVTabK2::getK2Item($param,$load);
    }
    private function from_queryk2($param,$load = 'all'){
        require_once('sources/k2.php');
        return JVTabK2::getK2Items($param,$load);
    }
    
    public function data(){
        foreach($this->params->tabs as $source){
            $method = 'from_'.$source->state('selected');
            if(!method_exists($this,$method)) continue;
            $tabs = $this->{$method}($source,(string)$this->params->config->get('load','all'));
            if(!$tabs) continue;
            foreach($tabs as $tab){
                $tab['id'] = $this->id . '-' . $tab['id'];
                if(is_array($tab['content'])){
                    $this->params->config['cache.'.$tab['id']] = $tab['content'];
                    $tab['content'] = '';
                }
                $this->tabs[] = (object)$tab;
            }
        }
        return $this->tabs;
    }
}
?>
