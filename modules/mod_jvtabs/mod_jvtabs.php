<?php
   /**
# plugin system jvslidepro_article - JV Slide Article
# @versions: 1.6.x,1.7.x,2.5.x,3.x
# ------------------------------------------------------------------------
# author    Open Source Code Solutions Co
# copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
# @license - http://www.gnu.org/licenseses/gpl-3.0.html GNU/GPL or later.
# Websites: http://www.joomlavi.com
# Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

if(class_exists('JVCustomParam')){
    require_once(dirname(__FILE__).'/libs/sources.php');
    JHtml::_('jquery.framework');
    $doc = JFactory::getDocument();
    $dataConfigs = new JVCustomParam($params->get('tabconfig',array()));
    $source = new JVTabsSource((object)array(
        'tabs' => new JVCustomParam($params->get('tabsource', array())),
        'config'=> $dataConfigs
    ),$module);
    $dataTabs = $source->data();
    $file = JModuleHelper::getLayoutPath('mod_jvtabs',(string)$dataConfigs->get('layout','default'));
    if(!is_file($file)) $file = JModuleHelper::getLayoutPath('mod_jvtabs','default');
    include($file);
}else JError::raiseWarning(null,"Make sure you have installed and enabled, set Order first the newest version of jvlibs plug-in to use module JV Slide Pro");
?>
