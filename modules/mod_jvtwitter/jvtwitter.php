<?php
/**
# mod_jvtwitter - JV Twitter
# @version		3.0
# ------------------------------------------------------------------------
# author    Open Source Code Solutions Co
# copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
# Websites: http://www.joomlavi.com
# Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/

// No direct access to this file
define( '_JEXEC', 1 );

$filePath = dirname(__FILE__);
$path 	  = dirname(dirname($filePath ));

define('JPATH_BASE', $path );
define( 'DS', DIRECTORY_SEPARATOR );

$filename = pathinfo(__FILE__);
$filename =  $filename['basename'];
$uri = str_replace(DS, '/', str_replace($_SERVER['DOCUMENT_ROOT'], '', JPATH_BASE));

$_SERVER['SCRIPT_FILENAME'] = $uri. '/' . $filename;
$_SERVER['SCRIPT_NAME']= $uri. '/' . $filename;
$_SERVER['REQUEST_URI'] = $uri.'?'. $_SERVER['QUERY_STRING'];
$_SERVER['PHP_SELF']    = $uri. '/' . $filename;

if (!defined('_JDEFINES')) {
    require_once JPATH_BASE.'/includes/defines.php';
}

require_once JPATH_BASE.'/includes/framework.php';

$id = $_REQUEST['id'];
if($id){
    $lang = JFactory::getLanguage();
    $lang->load('mod_jvtwitter');

    $db = JFactory::getDbo();
    $db->setQuery('SELECT params FROM #__modules WHERE id='.$id);
    $params = new JRegistry($db->loadResult());
    require_once dirname(__FILE__).'/helpers.php';
    exit(modJVTwitterHelper::getItems($params));
}else{
    exit(json_encode(array('rs'=>'0', 'msg'=>'Bad Request!')));
}