<?php
/**
# mod_jvtwitter - JV Twitter
# @version		3.0
# ------------------------------------------------------------------------
# author    Open Source Code Solutions Co
# copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
# Websites: http://www.joomlavi.com
# Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');

class JFormFieldConnect extends JFormField{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
    protected $type = 'Connect';

    /*protected function getLabel(){
        return false;
    }*/

    /**
        * Method to get the field input.
        *
        * @return	string	The field input.
        * @since	1.6
        */
    protected function getInput(){
        $document = JFactory::getDocument();
        if(version_compare(JVERSION,'3.0')<0){
            $document->addScript(JURI::root().'modules/mod_jvtwitter/assets/js/jquery.min.js');
        }else{
            JHtml::_('Jquery.framework');
        }
        $document->addScript(JURI::root().'modules/mod_jvtwitter/assets/js/jquery.noconflict.js');
        $document->addScript(JURI::root().'modules/mod_jvtwitter/assets/js/jvtwitter.connect.js');
        $document->addStyleSheet(JURI::root().'modules/mod_jvtwitter/assets/css/jvtwitter.connect.css');
        if($this->value){
            $user = json_decode($this->value);
            $avatar = $user->avatar;
            $name = $user->name;
        }else{
            require_once JPATH_ROOT.'/modules/mod_jvtwitter/libs/twitter/configs.php';
            if(!class_exists('TwitterOAuth')){
                require_once JPATH_ROOT.'/modules/mod_jvtwitter/libs/twitter/twitteroauth.php';
            }
            $connection = new TwitterOAuth(JVTWITTER_API,JVTWITTER_SECRET);

            $session = JFactory::getSession();
            $token = $session->get('tw_token');
            $tokensec = $session->get('tw_tokensec');

            $verifier = isset($_GET['oauth_verifier']) ? $verifier = $_GET['oauth_verifier'] : '';

            if($token && $tokensec && $verifier){
                $connection = new TwitterOAuth(JVTWITTER_API,JVTWITTER_SECRET,$token,$tokensec);
                $access_token = $connection->getAccessToken($_GET['oauth_verifier']); //require
                $content = $connection->get('account/verify_credentials');
                $user = (object)array(
                    'id'                => $content->id,
                    'name'		=> $content->name,
                    'username'          => $content->screen_name,
                    'email'		=> $content->id.'@testtwitter.com',
                    'avatar'            => $content->profile_image_url,
                    'token'             => $access_token['oauth_token'],
                    'token_secret'      => $access_token['oauth_token_secret'],
                );
                ?>
            <script>window.opener.jQuery('#jform_params_connect').val('<?php echo json_encode($user);?>');window.close();</script>
            <?php
                //exit;
            }
            $user = '';
            $avatar = 'http://www.gravatar.com/avatar/a06082e4f876182b547f635d945e744e?s=32&d=mm';
            $name = 'No Accounts';
            $request_token = $connection->getRequestToken(JURI::getInstance()->toString());
            // Save temporary credentials to session.
            $token = $request_token['oauth_token'];
            $session->set('tw_token',$token);
            $session->set('tw_tokensec',$request_token['oauth_token_secret']);
            $loginUrl = $connection->getAuthorizeURL($token);
        }
        ob_start();
        ?>
            <div class="social-accounts">
                <div class="value-cc">
                    <?php if(!$this->value){?>
                    <div class="social-connect-button">
                        <a id="twitter_signin" class="social-login" href="javascript:void(0);" data="<?php echo $loginUrl?>">
                            <span>Sign in with Twitter.</span>
                        </a>
                    </div>
                    <?php }?>
                    <ul style="margin: 0; padding: 0">
                        <li class="social-accounts-item none">
                            <img width="32" height="32" src="<?php echo $avatar;?>">
                            <span class="name"><?php echo $name;?></span>
                            <?php if($user){ ?>
                            <span class="disconnect" ><a href="javascript:void(0)">Disconnect</a></span>
                            <?php } ?>
                        </li>
                    </ul>
                    <textarea style="display: none" name="<?php echo $this->name;?>" id="<?php echo $this->id;?>"><?php echo $this->value;?></textarea>
                </div>
            </div>
        <?php
        return ob_get_clean();
    }
}
