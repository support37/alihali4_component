<?php
/**
# mod_jvtwitter - JV Twitter
# @version		3.0
# ------------------------------------------------------------------------
# author    Open Source Code Solutions Co
# copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
# Websites: http://www.joomlavi.com
# Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.form.formfield');

class JFormFieldDateFormat extends JFormField{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
    protected $type = 'DateFormat';

    /*protected function getLabel(){
        return false;
    }*/

    /**
        * Method to get the field input.
        *
        * @return	string	The field input.
        * @since	1.6
        */
    protected function getInput(){
        ob_start();
        ?>
        <input type="text" name="<?php echo $this->name;?>" id="<?php echo $this->id;?>" value="<?php echo $this->value;?>">
        <a href="http://momentjs.com/docs/#/parsing/string-format/" target="_blank"><?php echo JText::_('MOD_JVTWITTER_TWEET_DATE_FORMAT_INSTRUCTION');?></a>
        <?php
        return ob_get_clean();
    }
}
