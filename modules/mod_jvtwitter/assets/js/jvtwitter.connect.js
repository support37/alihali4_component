/**
 # mod_jvtwitter - JV Twitter
 # @version		3.0
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
 -------------------------------------------------------------------------*/
(function($) {
    var c = document.getElementsByTagName('html')[0];
    c.className += ' social-js';
    $(function() {
        $('.social-login').click(function(e) {
            var $window = $(this),
            $auth_window = window.open($(this).attr('data'), "ServiceAssociate", 'width=700,height=400'),
            auth_poll = null;
            auth_poll = setInterval(function() {
                if ($auth_window.closed) {
                    clearInterval(auth_poll);
                    Joomla.submitbutton('module.apply');
                }
            }, 100);
        });
        $('.disconnect a').click(function(){
            $('#jform_params_connect').val('');
            Joomla.submitbutton('module.apply');
        })
    });
})($JVTwitter);
