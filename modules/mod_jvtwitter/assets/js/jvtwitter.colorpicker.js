/**
 # mod_jvtwitter - JV Twitter
 # @version		3.0
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
 -------------------------------------------------------------------------*/
(function($) {
    $(function() {
        $('input[type="text"].jvclorpicker').ColorPicker({
            onShow: function(){
                $(this).ColorPickerSetColor(this.value.substring(1));
            },
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val('#'+hex);
                $(el).ColorPickerHide();
                $(el).css({ 'background-color' : '#'+hex} );

                color = $(el).val();
                if(color == '') color = '#ffffff';
                else $(el).css('background-color', '#'+color);
                $(el).attr('title', '#'+color);
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            }
        })
        .bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value.substring(1));
        });

        //Set initial Color
        $('input[type="text"].jvclorpicker').each(function(){
            var color = $(this).val();
            if(color != '')
                $(this).css( { "background-color" : color });
            $(this).attr('title', color);

            $(this).change(function(){
                var color = $(this).val();
                if(color == ''){
                    $(this).css('background-color', '#ffffff');
                    $(this).attr('title', '#ffffff');
                }else{
                    if(color.indexOf('#') != 0) $(this).val(color.substring(1));
                    $(this).attr('title', $(this).val());
                    $(this).css({ "background-color" : $(this).val() });
                }
            });
        });
    });
})($JVTwitter);
