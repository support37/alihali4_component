/**
 # mod_jvtwitter - JV Twitter
 # @version		3.0
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
 -------------------------------------------------------------------------*/
(function($){
    $.fn.JVTwitter = function(options){
        JVTwitterModelProfile = function(item){
            this.avatar = item.profile_image_url.replace('_normal','_reasonably_small');
            this.name = item.name;
            this.username = item.screen_name;
            this.screen_name = '@' + this.username;
            this.location = item.location;
            this.bio = TT.parseUrl(item.description, item.entities.description.urls);
            this.tweets = item.statuses_count;
            this.following = item.friends_count;
            this.followers = item.followers_count;
            this.listed = item.listed_count;
            this.url = 'https://twitter.com/' + this.username;
            if(item.url!=null && item.entities.url!=null){
                this.web = TT.parseUrl(item.url, item.entities.url.urls);
            }else{
                this.web = '';
            }
        };

        JVTwitterModelItem = function(item, params){
            var self = this;
            if(item.retweeted_status != null){
                this.avatar = item.retweeted_status.user.profile_image_url;
                this.name = item.retweeted_status.user.name;
                this.username = item.retweeted_status.user.screen_name;
                this.text = item.retweeted_status.text;
                if(item.retweeted_status.entities.urls) this.text = TT.parseUrl(this.text, item.retweeted_status.entities.urls);
                if(item.retweeted_status.entities.media) this.text = TT.parseMedia(this.text, item.retweeted_status.entities.media);
                if(item.retweeted_status.entities.hashtags) this.text = TT.parseHashtag(this.text, item.retweeted_status.entities.hashtags);
                if(item.retweeted_status.entities.user_mentions) this.text = TT.parseUserMention(this.text, item.retweeted_status.entities.user_mentions);
                this.created = TT.parseTwitterDate(item.retweeted_status.created_at, parseInt(params.tweet_date_format_type)) + ' ';
                this.via = ' ' + item.retweeted_status.source;
                this.retweeted = true;
            }else{
                this.avatar = item.user.profile_image_url;
                this.name = item.user.name;
                this.username = item.user.screen_name;
                this.text = item.text;
                if(item.entities.urls) this.text = TT.parseUrl(this.text, item.entities.urls);
                if(item.entities.media) this.text = TT.parseMedia(this.text, item.entities.media);
                if(item.entities.hashtags) this.text = TT.parseHashtag(this.text, item.entities.hashtags);
                if(item.entities.user_mentions) this.text = TT.parseUserMention(this.text, item.entities.user_mentions);
                this.created = TT.parseTwitterDate(item.created_at, parseInt(params.tweet_date_format_type)) + ' ';
                this.via = ' ' + item.source;
                this.retweeted = false;
            }
            if(!parseInt(params.tweet_retweeted)) this.retweeted = false;
            if($(this.via).find('a').length){
                this.via = $(this.via).attr('target','_blank').wrap('<div>').parent().html();
            }
            this.screen_name = '@' + this.username;
            this.url = 'https://twitter.com/' + this.username;
        };

        var JVTwitterModel = function(options){
            var self = this;
            // Params
            self.params = ko.observable({});
            // Profile
            self.profile = ko.observable({});
            self.showProfile = ko.observable(false);
            self.showProfileInfo = ko.observable(false);
            self.showProfileLocation = ko.observable(false);
            self.showProfileBio = ko.observable(false);
            self.showProfileWeb = ko.observable(false);
            self.showProfileMore = ko.observable(false);
            self.showProfileShareButton = ko.observable(false);
            self.showProfileFollowButton = ko.observable(false);
            self.showProfileHashtagButton = ko.observable(false);
            self.showProfileMentionButton = ko.observable(false);
            // Tweets
            self.tweets = ko.observableArray([]);
            self.showTweet = ko.observable(false);
            // Loading
            self.showLoading = ko.observable(false);
            // Message
            self.message = ko.observable('');
            self.showMessage = ko.observable(false);

            self.reload = ko.observable(true);

            if(options.autoRefresh){
                setInterval(function(){
                    self.reload(true);
                }, options.refreshTime);
            }
            // Get data
            ko.computed(function(){
                if(self.reload()){
                    $.ajax({
                        'url': options.requestURL,
                        'dataType': 'json',
                        beforeSend: function(){
                            self.showLoading(true);
                            self.showProfile(false);
                            self.showTweet(false);
                            self.showMessage(false);
                        },
                        success: function(data){
                            self.showLoading(false);
                            if(data.rs == '1'){
								if(data.list.error != null){
									self.showMessage(true);
									self.showTweet(false);
									self.message(data.list.error);
								}else{
                                    TT.params = data.params;
									self.params(data.params);
									self.profile(new JVTwitterModelProfile(data.list[0].user));
                                    self.tweets([]);
									$.each(data.list, function(){
										self.tweets.push(new JVTwitterModelItem(this, TT.params));
									});
									//show profile
									self.showProfileLocation(self.profile().location != '' && data.params.profile_location == 1);
									self.showProfileBio(self.profile().bio != '' && data.params.profile_bio == 1);
									self.showProfileWeb(self.profile().web != '' && data.params.profile_web == 1);
									self.showProfileMore(data.params.profile_tweets == 1 || data.params.profile_following == 1 ||
										data.params.profile_followers == 1 || data.params.profile_listed == 1);
									self.showProfileShareButton(data.params.profile_share_button == 1);
                                    self.showProfileFollowButton(data.params.profile_follow_button == 1);
                                    self.showProfileHashtagButton(data.params.profile_hashtag_button == 1);
                                    self.showProfileMentionButton(data.params.profile_mention_button == 1);

									self.showProfileInfo(self.showProfileLocation() || self.showProfileBio() || self.showProfileWeb() || self.showProfileMore() || self.showProfileShareButton() || self.showProfileFollowButton() || self.showProfileHashtagButton() || self.showProfileMentionButton());
									self.showProfile(data.params.profile == 1 && (self.showProfileInfo() || data.params.profile_avatar == 1 || data.params.profile_name == 1 && data.params.profile_username == 1));
									//show tweet
                                    self.showTweet(data.params.tweet == 1 && (data.params.tweet_avatar == 1 || data.params.tweet_name == 1 || data.params.tweet_username == 1 || data.params.tweet_text == 1 || data.params.tweet_created == 1 || data.params.tweet_via == 1));

									self.showMessage(false);
									self.reload(false);
								}
                            }else{
                                self.showMessage(true);
                                self.showTweet(false);
                                self.message(data.msg);
                            }
                        }
                    });
                }

            });
        };

        var TT = {
            options: {
                autoRefresh: 1,
                refreshTime: 300
            },

            init: function(els, options){
                var self = this;
                options = $.extend(self.options, options);
                $(els).each(function(){
                    ko.applyBindings(new JVTwitterModel(options), this);
                });
            },

            parseTwitterDate: function(dateString, type){
                var date = new Date(dateString.replace(/^\w+ (\w+) (\d+) ([\d:]+) \+0000 (\d+)$/, "$1 $2 $4 $3 UTC"));
                if(type) return moment(date).format(TT.params.tweet_date_format);
                return moment(date).fromNow();
            },

            parseUrl: function(item, array){
                $.each(array, function(i, url){
                    item = item.replace(url.url, '<a href="'+url.expanded_url+'" class="jvTwitterUrl" target="_blank">'+url.display_url+'</a>');
                });
                return item;
            },

            parseHashtag: function(item, array){
                $.each(array, function(i, tag){
                    var text = '#'+tag.text;
					var regex = new RegExp(text, 'ig');
                    var url = 'https://twitter.com/search?q=' + encodeURIComponent(text) + '&src=hash';
                    item = item.replace(regex, '<a href="' + url + '" class="jvTwitterHashtag" target="_blank"><span>#</span>' + tag.text + '</a>');
                });
                return item;
            },

            parseUserMention: function(item, array){
                $.each(array, function(i, user){
                    var text = '@'+user.screen_name;
                    var regex = new RegExp(text, 'ig');
                    item = item.replace(regex, '<a href="https://twitter.com/' + user.screen_name + '" class="jvTwitterUserMention" target="_blank"><span>@</span>' + user.screen_name + '</a>');
                });
                return item;
            },

            parseMedia: function(item, array){
                $.each(array, function(i, url){
                    item = item.replace(url.url, '<a href="'+url.expanded_url+'" class="jvTwitterMedia" target="_blank">'+url.display_url+'</a>');
                });
                return item;
            }
        };

        return TT.init(this, options);
    }

})($JVTwitter);