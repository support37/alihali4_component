<?php
/**
# mod_jvtwitter - JV Twitter
# @version		3.0
# ------------------------------------------------------------------------
# author    Open Source Code Solutions Co
# copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
# Websites: http://www.joomlavi.com
# Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
class modJVTwitterHelper
{
    static public function getItems($params) {
        $cache = $params->get('cache','1');
        if($cache){
            jimport('joomla.filesystem.folder');
            jimport('joomla.filesystem.file');

            $cachePath = JPATH_ROOT.'/cache/jvtwitter/';
            if(!is_dir($cachePath)){
                JFolder::create(dirname($cachePath));
            }

            $cacheFile = $cachePath.'jvtwitter.cache.json';
            if(is_file($cacheFile) && time() - filemtime($cacheFile) < (int)$params->get('cache_time','30') ){
                $cache = json_decode(file_get_contents($cacheFile));
                if($cache->list) exit(readfile($cacheFile));
            }
        }

        // Check CURL
        if (!function_exists ( 'curl_version' )) {
            return json_encode(array('rs'=>'0', 'msg'=>JText::_('MOD_JVTWITTER_NOT_FOUND_CURL')));
        }

        require_once JPATH_ROOT.'/modules/mod_jvtwitter/libs/twitter/configs.php';
        if(!class_exists('TwitterOAuth')){
            require_once JPATH_ROOT.'/modules/mod_jvtwitter/libs/twitter/twitteroauth.php';
        }

        if($params->get('connect')){
            $user = json_decode($params->get('connect'));

            $connection = new TwitterOAuth(JVTWITTER_API,JVTWITTER_SECRET,$user->token,$user->token_secret);

            if($params->get('username')){
                $user->username = $params->get('username');
            }

            $statuses = $connection->get('statuses/user_timeline', array('screen_name' => $user->username, 'count' => $params->get('tweet_num','5')));

            if(isset($statuses->errors)){
                return json_encode(array('rs'=>'0', 'msg'=>$statuses->errors[0]->message));
            }else{
                $params = $params->toArray();
                //unset important data
                unset($params['connect']);
                unset($params['layout']);
                unset($params['moduleclass_sfx']);

                $out = json_encode(array('rs'=>'1', 'list'=>$statuses, 'params'=> $params));

                if($cache){
                    JFile::write($cacheFile, $out);
                }

                exit($out);
            }
        }else{
            return json_encode(array('rs'=>'0', 'msg'=>JText::_('MOD_JVTWITTER_PLEASE_CONNECT_TWITTER_ACCOUNT')));
        }
    }

    static public function buildCss($selector, $attributes = array(), $customCss = '') {
        $css = array ();
        foreach ( $attributes as $key => $value ) {
            if ($value) array_push ( $css, "{$key}:{$value};" );
        }

        if ($css || $customCss) {
            array_unshift ( $css, '{' ); array_unshift ( $css, $selector ); array_push ( $css, $customCss ); array_push ( $css, '}' );
        }
        if(!count($css))
            return;
        return implode ( '', $css );
    }
}
