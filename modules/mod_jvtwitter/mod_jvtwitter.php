<?php
/**
 # mod_jvtwitter - JV Twitter
 # @version		3.0
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
-------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helpers.php';

$screen_name = $params->get('username','');
$user = json_decode($params->get('connect'));
if(!$screen_name && isset($user->username)){
    $screen_name = $user->username;
}
require JModuleHelper::getLayoutPath('mod_jvtwitter', $params->get('layout', 'default'));