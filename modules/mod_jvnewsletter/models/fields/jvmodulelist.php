<?php
/**
 # mod_jvnewsletter - JV NEWSLETTER
 # @version        1.0
 # ------------------------------------------------------------------------
 # author    Open Source Code Solutions Co
 # copyright Copyright (C) 2011 joomlavi.com. All Rights Reserved.
 # @license - http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL or later.
 # Websites: http://www.joomlavi.com
 # Technical Support:  http://www.joomlavi.com/my-tickets.html
 -------------------------------------------------------------------------*/
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
                         
defined('_JVNSEXEC') or require (dirname(__FILE__). "/../../services/_services.php"); 


jimport('Joomla.form.fields.checkboxes');

class JFormFieldJvModuleList extends JFormFieldCheckboxes
{
    protected $type = 'JvModuleList';
    
    protected $filter = 'mod_jvnewsletter';
    
    protected function getOptions()
    {
        $doc = JFactory::getDocument();
        $doc->addStyleDeclaration("
        .jv-checkbox li{
            line-height: 26px;
            list-style: none;
            height: 26px;
        }
        .jv-checkbox li input.lb-cb{
            float: left;
            margin: 3px 10px 0 0;
        }
        .jv-checkbox li label.lb-cb{
        
        }
        ");
        $this->filter = ($ftl = $this->element['mod'])? $ftl : 'mod_jvnewsletter';
        $modules = getParamsList($this->filter);        
        $options = array();

        foreach ($modules as $option)
        {
            // Create a new option object based on the <option /> element.
            $tmp = JHtml::_(
                'select.option', 
                (string) $option->id, 
                trim((string) $option->title."[".$option->params->get('chose')."]"), 
                'value', 
                'text',
                false
            );
            $tmp->class="lb-cb";
                        
            // Add the option object to the result set.
            $options[] = $tmp;
        }

        reset($options);

        return $options;
    }
}
?>
