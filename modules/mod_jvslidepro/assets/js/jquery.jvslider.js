JVSlider = (function($){
    return $.extend(function(el,ops){
        var
            self = arguments.callee,
            el = $(el),
            This = this,
            $this = $(this)
        ;
        if(!el.length) return;
        if(el.data('JVSlider')) return el.data('JVSlider');
        ops = $.extend({},self.options,ops);
        var
            items, itemPanel = el.find('.items:first'), curent = {
                active: ops.active,
                pos: 0
            }, totalSize, slideSize,
            reset = function(){
                items = itemPanel.children();
                slideSize = itemPanel.innerWidth();
                totalSize = 0;
                items.each(function(i){
                    this.JVSlider = {before: totalSize, index: i}
                    totalSize += $(this).outerWidth() + parseInt($(this).css('margin-left')) + parseInt($(this).css('margin-right'));
                    this.JVSlider.after = totalSize;
                });
                setPos(0);
                glideTo(curent.active);
            },
            setPosition = function(index,pos){
                var left = this.JVSlider.before - pos;
                if(-left >  totalSize - slideSize) left = totalSize + left;
                $(this).css('translateX', left);
            },
            setPos = function(pos){
                curent.pos = pos; 
                items.each(function(i){
                    setPosition.call(this,i,pos);
                });
            },
            findVisibles = function(){
                for(var i = 0; i < items.length; i++){
                    if(items[i].JVSlider.before - curent.pos > 0 ){
                        
                    }
                }
            },
            glideTo = function(i){
                i = i % items.length;
                curent.active = i;
                var itemPos = items[i].JVSlider, toPos;
                if(itemPos.before - curent.pos < 0){
                    toPos = itemPos.before;
                    curent.waitPrev = i;
                }else if(itemPos.after - curent.pos > slideSize){
                    toPos = itemPos.after - slideSize;
                    curent.waitNext = i;
                }else return;
                
                //if(){}
                $this.stop().animate({curentPos: toPos},{
                    step: function(i){
                        setPos(i);
                    },
                    complete: function(){
                        findVisibles();
                    }
                });
                
            }
        ;
        itemPanel.on('click','li',function(){
            glideTo(this.JVSlider.index);
        });
        el.imagesLoaded(function(){
            reset();
        });
        
        $.extend(this,{
            curentPos: 0,
            next: function(){ glideTo(curent.waitNext + 1);},
            prev: function(){ glideTo(curent.waitPrev + 1);},
            to: function(i){},
            first: function(){},
            last: function(){}
        });
        el.find('.next').click(this.next);
        el.find('.prev').click(this.prev);
    },{
        options: {
            active: 0,
            loop: true,
            ctrl: true
        }
    });
})(jQuery);